<script type="text/javascript" src="/view/javascript/jquery/jquery-1.7.1.min.js"></script>
<?php
$server_name = getenv('SERVER_NAME');
$request_uri = getenv('REQUEST_URI');
$url = 'http://'.$server_name.$request_uri;

function add_date($givendate,$day=0,$mth=0,$yr=0){
    $givendate = $givendate. ' 00:00:00';
    $cd = strtotime($givendate);
    $newdate = date('Y-m-d', mktime(date('h',$cd),
                  date('i',$cd), date('s',$cd), date('m',$cd)+$mth,
                  date('d',$cd)+$day, date('Y',$cd)+$yr));
  return $newdate;
}
?>
<style>
body{
    width: 800px;
}

#company-info {
  float:left;
  font-weight:bold;
  
    }
#company-info .title{
  
  font-size:30px;
  padding-bottom:10px;
  
}
 #company-info ul{
  padding-left:20px;
}
#company-info li{
  list-style:none;
  text-align:left;
  font-size:20px;
}
#statement {
  float:right;
  font-weight:bold;
  padding-right: 30px;
}
#statement .title{
  font-size:30px;
  padding-bottom:10px;
}
 #statement ul{
  padding-right:20px;
}
#statement li{
  list-style:none;
  text-align:right;
  font-size:18px;
}
#billinfo{
  clear:both;
  padding: 20px;
}
#shipto.lable {
  background-color:lightgray;
  font-weight:bold;
}

#shipto{
  width: 200px;
  border:1px solid rgb(200,200,200);
  border-collapse: collapse;
 
}
#shipto thead{
  background-color:rgb(234,234,234);
}
#shipto tbody td{
  border-top:1px solid rgb(200,200,200);
}
#shipto ul{
    padding-top: 10px;
    list-style:none;
    margin-left:-20px;
}
#detail{
  margin-top:20px;
  margin-left:20px;
  width:95%;
  border: 0px solid rgb(200,200,200);
  border-collapse: collapse;
  border-spacing: 0;   

}

#detail td{
  text-align:center;
  height:32px;
  border: 1px solid rgb(200,200,200);
}
#detail.label{
  font-weight:bold;
  background-color: lightgray;
  height:32px;
}
/*
#detail thead{
    background-color:rgb(234,234,234);
}*/
#intable{
    width:100%;
    border:0px;
    border-spacing: 0;
    border-collapse: collapse;
}
#intable td{
    border: 0px;
}
#intable thead{
    border-collapse: collapse;
    border-bottom: 1px solid rgb(200,200,200);
}

td#t-order, td#t-balance {
  font-weight:bold;
}
</style>
<script type="text/javascript" src="view/javascript/jquery/jquery.min.js"></script>

<?php
/*
echo '<pre>'; print_r($company); echo '</pre>';
echo '<pre>'; print_r($store); echo '</pre>';
echo '<pre>'; print_r($list); echo '</pre>';
*/
?>

<div id='sheet'>
    <div id='top'>
    <div id='company-info'>
        <ul>
        <li class='title'><?php echo ucwords($company['name']) ?></li>
        <?php
        if ( isset($company['address']) ) {
        ?>
        <li><?php echo $company['address'] ?></li>
        <?php
        }
        ?>
        <li><?php echo $company['email'] ?></li>
        <li><?php echo $company['phone'] ?></li>
        </ul>
    </div>
    <div id="statement">
        <ul>
            <li class="title">Statement &amp; Receipt</li>
            <li>Paid Date : <?php echo substr($list[0]['paid_date'],0,10) ?></li>
            <li>Rep : <?php echo $this->user->getUserName(); ?></li>
        </ul>
    </div>
    </div>

    <div id='billinfo'>

        <table id='shipto'>
            <thead>
                <tr>
                <td style='padding-left:20px; height: 32px;'>Account Information</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                 <td>
                        <ul>
                            <li style="font-weight: bold;"><?php echo $store['name'] ?></li>
                            <li><?php echo $store['address1'] ?></li>
                            <li><?php echo $store['city'] ?> , <?php echo $store['state'] ?>, <?php echo $store['zipcode'] ?></li>
                            <li>TEL : <?php echo $store['phone1'] ?></li>
                        </ul>
                    </td>
                </tr>
            </tbody>
        </table>

    </div>

    <table id='detail' width="100%">
        <thead>
        <tr>
            <td class='label'>Transaction ID</td>
            <td class='label'>Order</td>
            <td class='label'>Order-At</td>
            <td class='label'>Paid</td>
            <td class='label'>Balance</td>
        </tr>
        </thead>
        <?php
            $tOrder = $tBalance = 0;
            foreach( $list as $tx ) {
                $order_date = substr( $tx['order_date'], 0 , 10 );

                $txid = $tx['txid'];

                $order_price = $tx['order_price'];
                $balance = $tx['balance'];

                $tOrder += $order_price;
                $tBalance += $balance;
        ?>
            <tr style='padding:0px'>
                <td><?php echo $txid ?></td>
                <td><?php echo $tx['order_price'] ?></td>
                <td><?php echo $order_date; ?></td>
                <td><?php echo $tx['payed_sum']; ?></td>
                <td><?php echo $balance ?></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="4">
                    <table id="intable">
                        <thead><tr>
                            <td>Item</td>
                            <td>Price</td>
                            <td>Qty</td>
                            <td>Total</td>
                        </tr></thead>
                        <tr>
                        <?php
                        foreach( $tx['items'] as $item ) {
                        ?>
                        <tr>
                            <td><?php echo $item['model'] ?></td>
                            <td><?php echo $item['price1'] ?></td>
                            <td><?php echo $item['order_quantity'] ?></td>
                            <td><?php echo $item['total_price'] ?></td>
                        <tr>
                        <?php
                        }
                        ?>
                    </table>
                </td>
            </tr>
        <?php
            }
        ?>
    
    <tr style="background-color:rgb(234,234,234);">
        <td colspan=5>
            <?php
            if ( !empty($list[0]['memo']) ) {
                echo 'Memo : ' . $list[0]['memo'];
            } else {
                echo '&nbsp;';
            }
            ?>
        </td>
    </tr>
    <tr>
        <td colspan="2">&nbsp;</td>
        <td style="text-align:right; font-weight:bold; padding-right:10px;" >Total</td>
        <td id='t-order'><?php echo $list[0]['total']; ?></td>
        <td id='t-balance'><?php echo round($tBalance,2); ?></td>
    </tr>
    </table>
    <div style='  float: right; margin: -390px 47px;'>
        <button id='print'>Print</button>
    </div>
</div>

<script>
    $('#print').bind('click',function(e) {
        window.print();
    });
</script>
