<?php echo $header; ?>
<?php if ($error_warning){ ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<?php if ($success) { ?>
<div class="success"><?php echo $success; ?></div>
<?php } ?>
<style>
.box {
  z-index:10;
}
.content .name_in_list{
  color:purple;
  cursor:pointer;
}
#detail{
  position : absolute;
  top: 100px;
  left: 100px;
  visibility:hidden;
  border: 1px dotted green;
  z-index:2;
}
</style>

<div class="box">
            <div class="heading">
    <h1 style="background-image: url('view/image/product.png');">
      Account List</h1>
    <div class="buttons">
        <?php
        if (count($total) > 0){
      ?> 
        <a onclick="location = '<?php echo $export; ?>'" class="button">
        <span><?php echo $total; ?> Export</span></a>
        <?php
    }
      ?>
        <a class="button btn_insert">
        <span>Insert</span></a>
        <!--a onclick="$('#form').attr('action', '<?php echo $update; ?>'); $('#form').submit();" class="button">
        <span>Update</span></a-->
        <!-- todo. need to think about check box usability and delete, delete not require for account , besso-201103 -->
        <!--a onclick="$('form').submit();" class="button">
        <span>Delete</span></a-->
        <!--a onclick="$('#detail').html(); $('#detail').css('visibility','hidden');" class="button">
        <span>Close</span></a-->
    </div>
    </div>

    <?php
    // reset filter for quick re-query , besso-201103 
    // $filter_accountno = '';
        $filter_name = '';
        $filter_storetype = '';
        $filter_city = '';
        $filter_state = '';
    // $filter_phone1 = '';
    // todo. block later for user leveling
        //$filter_salesrep = '';
        //$filter_status = '';
        if (!$filter_status) $filter_status = '';
  ?>
    <div class="content">
    <?php 
        // no delete or so
        $delete = ''; 
    ?>
    <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="table">
        <tbody>
            <tr class="filter">
            <td class="center">
                    <input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" />
            </td>
            <td>
                <input type="text" class="input-small" name="filter_accountno" value="<?php echo $filter_accountno; ?>" placeholder="Account No"/>
            </td>
            <td class='center' style='width:150px;'>
                <input type="text" class="input-medium" name="filter_name" value="<?php echo $filter_name; ?>" placeholder="Store Name" />
            </td>
            <td>
                <select class='select input-medium' name='filter_storetype'>
                    <?php if (strtolower($filter_storetype) == 'w') { ?>
                        <option value="">Wholesale or Retail</option>
                        <option value="w" selected="selected">W</option>
                        <option value="r">Retail</option>
                    <?php } elseif (strtolower($filter_storetype) == 'r') { ?>
                        <option value="">Wholesale or Retail</option>
                        <option value="w">Wholesale</option>
                        <option value="r" selected="selected">Retail</option>
                    <?php } else { ?>
                        <option value="" selected="selected">Wholesale or Retail</option>
                        <option value="w">Wholesale</option>
                        <option value="r">Retail</option>
                    <?php }?>
                </select>
            </td>
            <td>
                <input type="text" class="input-small" name="filter_city" value="<?php echo $filter_city; ?>" placeholder="City" />
            </td>
            <td>
                <input type="text" class="input-small" name="filter_state" value="<?php echo $filter_state; ?>" placeholder="State" />
            </td>
            <td align="left">
                <input type="text" class="input-small" name="filter_phone1" value="<?php echo $filter_phone1; ?>" style="text-align: left;" placeholder="Phone" />
            </td>
  
            <td align="left">
                <?php
                // todo. how to make beautiful user leveling , besso-201103 
                //if ('11' != $this->user->getGroupID()){
                if ($this->user->getGroupID()){
                ?>
                <input type="text" class="input-small" name="filter_salesrep" value="<?php echo $filter_salesrep; ?>" placeholder="Rep" />
                <?php
            } else {
                echo $this->user->getUserName();
            }
                ?>
            </td>
            <td>Balance</td>
            <td>
                <?php 
                $aStoreCode = $this->config->getStoreStatus(); 
                ?>
                <select class='select input-small' name="filter_status">
                    <option value="" <?php if (''==$filter_status) echo 'selected'; ?>>All</option>
                    <option value="0" <?php if ('0'==$filter_status) echo 'selected'; ?>><?php echo $aStoreCode['0']; ?></option>
                    <option value="1" <?php if ('1'==$filter_status) echo 'selected'; ?>><?php echo $aStoreCode['1']; ?></option>
                    <!--option value="2" <?php if ('2'==$filter_status) echo 'selected'; ?>><?php echo $aStoreCode['2']; ?></option>
                    <option value="3" <?php if ('3'==$filter_status) echo 'selected'; ?>><?php echo $aStoreCode['3']; ?></option-->
                    <option value="9" <?php if ('9'==$filter_status) echo 'selected'; ?>><?php echo $aStoreCode['9']; ?></option>
                </select>    
            </td>
            <td>
                <a onclick='filter();' class="button btn_filter">
                    <span><?php echo $button_filter; ?></span>
                </a>
            </td>
            </tr>

            <!--tr class="filter">
            <td colspan=2>
            <td class='center' colspan='3'>
                <button onclick="$('input[name=filter_order_date_from]').val('2000-01-01'); return false;">All</button>
                <input type="text" class="input-small" class='date_pick' name="filter_order_date_from" value="<?php echo $filter_order_date_from; ?>" style='width:70px;' />
              -
                <input type="text" class="input-small" class='date_pick' name="filter_order_date_to" value="<?php echo $filter_order_date_to; ?>" style='width:70px;' />
            </td>
            <td colspan=2>
              Over <input type="text" class="input-small" name="filter_balance" value="<?php echo $filter_balance; ?>" style='width:70px;' />
            </td>
            <td colspan=3'></td>
            <td align="right">
                <a onclick='filter();' class="button btn_filter">
                    <span><?php echo $button_filter; ?></span>
                </a>
            </td>
            </tr-->
 
            <?php if ($store) { ?>
            <?php foreach ($store as $row) { ?>
            <tr>
            <td style="text-align: center;"><?php if ($row['selected']) { ?>
                <input type="checkbox" class='id_in_list' name="selected[]" value="<?php echo $row['id']; ?>" checked="checked" />
                <?php } else { ?>
                <input type="checkbox" class='id_in_list' name="selected[]" value="<?php echo $row['id']; ?>" />
                <?php } ?>
                <input type='hidden' name='view' value='proxy' />
                <input type='hidden' class='address1_in_list' name='address1' value='<?php echo $row['address1']; ?>' />
                <input type='hidden' class='zipcode_in_list' name='zipcode' value='<?php echo $row['zipcode']; ?>' />
                <input type='hidden' class='fax_in_list' name='fax' value='<?php echo $row['fax']; ?>' />
            </td>
            <td class='center accountno_in_list'>
                <?php echo $row['accountno']; ?>
            </td>
            <!--td class='center name_in_list'-->
            <td>
                <a href="/statement/sheet&id=<?php echo $row['id']; ?>" target='new'>
                <?php echo $row['name']; ?></a>
            </td>
            <td class='storetype_in_list'>
                <?php echo ( $row['storetype'] == 'W' ) ? 'Wholesale' : 'Retail' ; ?>
            </td>
            <td class='city_in_list'>
                <?php echo $row['city']; ?>
            </td>
            <td class='state_in_list'>
                <?php echo $row['state']; ?>
            </td>
            <td class='phone1_in_list'>
                <?php echo $row['phone1']; ?>
            </td>
            <td class='salesrep_in_list'>
                <?php echo $row['salesrep']; ?>
            </td>
            <td class='salesrep_in_list'>
                <?php echo $row['balance']; ?>
            </td>
            <td class='status_in_list'><?php echo $aStoreCode[$row['status']]; ?></td>
            <td>
                <a class='button edit'><span>More</span></a>
            </td>
            </tr>
            <?php } // end foreach ?>

            <?php } else { ?>
            <tr>
            <td class="center" colspan="11">No Result</td>
            </tr>
            <?php } ?>
        </tbody>
        </table>
    </form>
    <div class="pagination"><?php echo $pagination; ?></div>
    </div>
</div>
<!-- common detail div -->
<div id='detail' class='ui-widget-content'></div>

<script type="text/javascript">
$(document).ready(function(){
  
    $('.btn_insert').bind('click',function(event){
        $.ajax({
        type:'get',
        url:'/store/list/callInsertPannel',
        dataType:'html',
        data:'token=<?php echo $token; ?>',
      beforesend:function(){
        //console.log('beforesend');
    },
      complete:function(){
        //console.log('complete');
    },
        success:function(html){
        $('#detail').css('visibility','visible');
        $('#detail').html(html);
        //$('#detail').draggable(); 
    },
        fail:function(){
        //console.log('fail : no response from proxy');
    }
    });
    });
  
    // query store condition , besso-201103
    /***/
    $('.btn_filter').click(function(){
    //alert('clk');
    filter();
    });
    /***/
  function filter(){
      var url = '/statement/statement';
    var filter_name = $('input[name=\'filter_name\']').attr('value');
      if (filter_name) {
          url += '&filter_name=' + encodeURIComponent(filter_name);
    }
      var filter_accountno = $('input[name=\'filter_accountno\']').attr('value');
      if (filter_accountno) {
          url += '&filter_accountno=' + encodeURIComponent(filter_accountno);
    }
      var filter_storetype = $('input[name=\'filter_storetype\']').attr('value');
      if (filter_storetype) {
          url += '&filter_storetype=' + encodeURIComponent(filter_storetype);
    }
    var filter_storetype = $('select[name=\'filter_storetype\']').attr('value');
        if (filter_storetype != '') {
        url += '&filter_storetype=' + encodeURIComponent(filter_storetype);
    }    
      var filter_city = $('input[name=\'filter_city\']').attr('value');
      if (filter_city) {
          url += '&filter_city=' + encodeURIComponent(filter_city);
    }
      var filter_state = $('input[name=\'filter_state\']').attr('value');
      if (filter_state) {
          url += '&filter_state=' + encodeURIComponent(filter_state);
    }
      var filter_phone1 = $('input[name=\'filter_phone1\']').attr('value');
      if (filter_phone1) {
          url += '&filter_phone1=' + encodeURIComponent(filter_phone1);
    }
      var filter_salesrep = $('input[name=\'filter_salesrep\']').attr('value');
      if (filter_salesrep) {
          url += '&filter_salesrep=' + encodeURIComponent(filter_salesrep);
    }
      var filter_balance = $('input[name=\'filter_balance\']').attr('value');
      if (filter_balance ){
          url += '&filter_balance=' + encodeURIComponent(filter_balance);
    }
      var filter_status = $('select[name=\'filter_status\']').attr('value');
      if (filter_status != '*') {
          url += '&filter_status=' + encodeURIComponent(filter_status);
    }
      location = url;
    }
    $('#form input').keydown(function(e) {
      if (e.keyCode == 13) {
          filter();
    }
    });

    $('.list').click(function(event){
    var $tgt = $(event.target);
        if ($tgt.is('a.edit>span')){
      var $pnt = $tgt.parents('tr'),
            $ele_chkbox = $pnt.find('.id_in_list'),
            $store_id = $ele_chkbox.val();
        $.ajax({
        type:'get',
        url:'/store/list/callUpdatePannel',
        dataType:'html',
        data:'token=<?php echo $token; ?>&store_id=' + $store_id,
        beforesend:function(){
            //console.log('beforesend');
        },
        complete:function(){
            //console.log('complete');
        },
        success:function(html){
            $('#detail').css('visibility','visible');
            $('#detail').html(html);
            $('#detail').draggable(); 
        },
        fail:function(){
            //console.log('fail : no response from proxy');
        }
    });
    }
    }); // end of click event
        $('#form').bind('focusin',function(event){
    var $tgt = $(event.target);
        if ($tgt.is('input.date_pick')){
        //$(".date-pick").datePicker({startDate:'01/01/1996'});
        $(".date_pick").datePicker({
        clickInput:true,
        createButton:false,
        startDate:'2000-01-01'
    });
    }
    });
    });

</script>
<?php echo $footer; ?>
