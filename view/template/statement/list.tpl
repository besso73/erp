<?php echo $header; ?>
<?php if ($error_warning){ ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>

<!--div class='help' style='width:100%;font-size:12px'>
<pre>
Default : list all unpaid AR list for all sales
Batch Print need to edit brower setting ( firefox possible, IE ??? )
</pre>
</div-->


<div class="box">
  
    <div class="center"></div>
  
    <div class="heading">
        <h1 style="background-image: url('view/image/product.png');">Statement list</h1>
        <div class="buttons">
            <?php if (count($total) > 0){ ?> 
                <a onclick="location = '<?php echo $export; ?>'" class="btn btn-warning">
                <span>Export</span></a>
            <?php } ?>
            <a onclick="location = '<?php echo $lnk_insert; ?>'" class="btn btn-warning"><span>Insert</span></a>
            <!-- <a id='batch_print' class="btn btn-warning"><span>Print</span></a> 
             href='<?php echo $lnk_import; ?>' class="btn btn-warning"><span>QB</span></a-->
        </div>
    </div>
    <div class="content">
        <div id='lmenu'></div>
        <form action="<?php echo $lnk_delete; ?>" method="post" enctype="multipart/form-data" id="form">
            <table class="table">
            <thead>
                <tr>
                    <td colspan='2' class="center">ACCOUNT</td>
                    <td colspan='1' class="center">BALANCE</td> 
                    <td colspan='1' class="center">PAID</td> 
                    <td colspan='1' class="center">PAID-AT</td>
                    <td colspan='1' class="center">TYPE</td>
                    <td colspan='1' class="center">MEMO</td>
                    <td></td>
                </tr>
            </thead>
            <tbody> 
                <tr class="filter">
                    <td class='center' colspan='2'>
                        <input type="text" class="input-small" name="filter_store_name" value="<?php echo $filter_store_name; ?>" style='width:100px;' />
                    </td>
                    <td class='center' style='width:150px;'></td>
                    <td class='center' colspan='2'>
                        <input type="text" class="input-small" class='date_pick' name="filter_order_date_from" value="<?php echo $filter_order_date_from; ?>" style='width:75px;' />
                        -
                        <input type="text" class="input-small" class='date_pick' name="filter_order_date_to" value="<?php echo $filter_order_date_to; ?>" style='width:75px;' />
                    </td>
                    <!--td class='right'><input type="text" class="input-small" name="filter_price" value="<?php echo $filter_total; ?>" size='5' /></td-->
                    <td class='center'>
                        <select name="filter_paid_type">
                            <option value="" <?php if ($filter_paid_type == '') echo "selected"; ?>>-------</option>
                            <option value="" <?php if ($filter_paid_type == '3') echo "selected"; ?>>Cash</option>
                            <option value="done" <?php if ($filter_paid_type == '1') echo "selected"; ?>>Credit Card</option>
                            <option value="yet" <?php if ($filter_paid_type == '2') echo "selected"; ?>>Check</option>
                        </select>
                    </td>
                    <td class='center'>
                        <input type="text" class="input-small" name="filter_memo" value="<?php echo $filter_memo; ?>" />
                    </td>
                    <td class="center"><a onclick="$.fn.filter();" class="btn btn-warning"><span>Filter</span></a></td>
                </tr>
                <?php
                if ( !empty($list) ) {
                    foreach ($list as $row) {

                        // it has sub array for txid list. so we show it as nested way
                        if ( !empty($row) ) {
                            $statement_id = $row[0]['statement_id'];
                            $store_id     = $row[0]['store_id'];
                            $store_name   = $row[0]['name'];
                            $accountno    = $row[0]['accountno'];
                            $paid_amount  = $row[0]['total'];
                            $paid_type    = $row[0]['paid_type'];
                            $paid_date    = substr($row[0]['paid_date'], 0, 10);
                            $memo         = $row[0]['memo'];

                            $sum = $row[0]['sum'];
                            //$paid_sum = $sum['paid_sum'];   // from TX
                            $balance_total = $sum['balance_sum'];
                            $paid_total = $sum['paid_amount']; // from Statement, actual paid
                        }
                        ?>
                            <tr>
                            <td style="text-align: center;">
                                <?php if ($row[0]['selected']) { ?>
                                    <input type="checkbox" name="selected[]" value="<?php echo $statement_id; ?>" checked="checked" />
                                <?php } else { ?>
                                    <input type="checkbox" name="selected[]" value="<?php echo $statement_id; ?>" />
                                <?php } ?>
                            </td>
                            <td class="center">
                                <a href="#">
                                    <?php echo "$store_name ( $accountno )"; ?>
                                </a>
                            </td>
                            <td class="center" style="color:red;"><?php echo $balance_total; ?></td>
                            <td class="center"><b><?php echo $paid_total; ?></b></td>
                            <td class="center"><b><?php echo $paid_date; ?></b></td>
                            <td class="center">
                                    <?php
                                if ( $paid_type == '1' ) echo "Credit Card";
                                if ( $paid_type == '2' ) echo "Check";
                                if ( $paid_type == '3' ) echo "Cash";
                                  ?>
                            </td>
                            <td class="center"><?php echo $memo; ?></td>
                            <td class="center">
                                    <a href="/statement/list/sheet&id=<?php echo $statement_id ?>" class='btn btn-warning' target="_blank">Statement</a>
                            </td>
                        </tr>
                        <tr>
                            <td colspan='2'></td>
                            <td colspan='6'>
                                    <table id="table-table">
                                    <thead>
                                    <tr>
                                      <td>TXID</td>
                                      <td>ORDERED</td>
                                      <td>ORDER-AT</td>
                                      <td>PAID-AT</td>
                                      <td>PAID</td>
                                      <td>BALANCE</td>
                                    </tr>
                                    </thead>
                                    <?php
                                      foreach ($row as $tx) {
                                        $diff = $this->util->date_diff($tx['order_date'], $tx['paid_date']);
                                      ?>
                                    <tr>
                                      <td>
                                          <a href="/sales/order&mode=show&txid=<?php echo $tx['txid']; ?>">
                                          <?php echo $tx['txid']; ?>
                                          </a>
                                      </td>
                                      <td><?php echo $tx['order_price']; ?></td>
                                      <td><?php echo substr($tx['order_date'], 0, 10); ?></td>
                                      <td><?php echo substr($tx['paid_date'], 0, 10); ?><?php echo $diff ?></td>
                                      <td><?php echo $tx['paid_amount']; ?></td>
                                      <td><?php echo $tx['balance']; ?></td>
                                    </tr>
                                    <?php
                                     }
                                      ?>
                                    </table>
                            </td>
                        <tr>
                    <?php 
                    } // end for
                } else { 
                ?>
                    <tr>
                        <td class="center" colspan="9">No Result</td>
                    </tr>
                    <?php } ?>
            </tbody>
            </table>
        </form>
        <div class="pagination"><?php echo $pagination; ?></div>
    </div>
</div>

<style>
#ship_detail{
  position:absolute;
  top:200px;
  left:200px;
  width:400px;
  visibility:hidden;
  background-color:white;
  border:1px dotted blue;
  padding:10px;
}

#table-table{
    margin-top:10px;
    width:100%;
}


</style>
<div id='ship_detail'>
    <form action="/sales/" method="post" enctype="multipart/form-data">
    <table>
    <tr>
        <td class=label>SHIP</td>
        <td class=context>
        <input type='hidden' name=txidList value=''>
        <select name=method[]>
            <option value='truck' selected>truck</option>
            <option value='ups'>ups</option>
            <option value='cod'>cod</option>
            <option value='visit'>visit</option>
            <option value='etc'>etc</option>
        </select>
        </td>
        <td class=label>DATE</td>
        <td class=context>
        <input type=text name=ship_date[] value='<?php echo date("Y-m-d"); ?>' style='width:40%' /></td></tr>
    <tr>
        <td class=label>LIFT</td><td class=context>
        <input type=text name=lift[] value='' size=5 /></td>
        <td class=label>COD</td><td class=context>
        <input type=text name=cod[] value='' size=5 />
        <p class=plus style='float:right;margin:0px;margin-right:2px;' />
        <input type='hidden' name='ship_user[]' value='<?php echo $this->user->getUserName(); ?>' /></td>
    </tr>
    <tr>
        <td colspan=4><textarea name='ship_comment[]' style='width:300px;height:100px;'/>
        </textarea></td></tr>
    <tr>
        <td colspan=4>
        <input type=submit value='Confirm' />
        <input type=button value='Close' onclick="$('#ship_detail').css('visibility','hidden');"/>
        </td>
    </tr>
    </table>
    </form>
</div>

<!--iframe id='order_detail' name='order_detail' src='' width=500px height=400px></iframe-->

<script type="text/javascript">
$(document).ready(function(){
    // batch_ship
    $('#batch_ship').click(function(){
    // set txidList for batch process
    var $ele_checkbox = $('form tbody input:checkbox:checked'),
        $len = $ele_checkbox.length,
        $txidList = [];
        $ele_checkbox.each(function(idx){
        $txidList.push($(this).val());
    });
    // show ship method and others
        if ($txidList.length > 0){
        $('#ship_detail').find('input[name=txidList]').val($txidList);
        $('#ship_detail').css('visibility','visible');
    }
    });

    $('#batch_print').click(function(){
    // set txidList for batch process
    var $ele_checkbox = $('form tbody input:checkbox:checked'),
        $len = $ele_checkbox.length,
        $txidList = [];
        $ele_checkbox.each(function(idx){
      /***** todo. populate new iframe and print that , besso 201105 
        frame = 'frame' + idx;
        $html = "<iframe id='" + frame + "' name='" + frame + "' src='' width=1px height=1px></iframe>";
        $('#content').append($html);
      
        // we must use relative path for iframe control , besso 201105 
        // http://huuah.com/jquery-and-iframe-manipulation/ sigh~
      var $ele_iframe = $('iframe#'+frame),
            $txid = $(this).val(),
            $param = '?txid='+$txid,
            $url='/sales/order/<?php echo $token; ?>' + $param,
            $name = 'name' + idx;
        $ele_iframe.attr('src',$url);
        *****/
      var winHdr = null;
      var $txid = $(this).val(),
            $param = '&txid='+$txid,
            $name = 'name' + idx,
            $url='/statement/order' + $param;
      winHdr = window.open($url,$name);
      winHdr.onload = function(){
        winHdr.print();
        winHdr.close();
    }
    });
    });

    $.fn.filter = function(){
        url = '/statement/list';
      var filter_store_name = $('input[name=\'filter_store_name\']').attr('value');
      if (filter_store_name) url += '&filter_store_name=' + encodeURIComponent(filter_store_name);
      var filter_txid = $('input[name=\'filter_txid\']').attr('value');
      if (filter_txid) url += '&filter_txid=' + encodeURIComponent(filter_txid);
        var filter_bankaccount = $('select[name=\'filter_bankaccount\']').attr('value');
      if (filter_bankaccount)  url += '&filter_bankaccount=' + encodeURIComponent(filter_bankaccount);
      var filter_order_date_from = $('input[name=\'filter_order_date_from\']').attr('value');
      if (filter_order_date_from)  url += '&filter_order_date_from=' + encodeURIComponent(filter_order_date_from);
      var filter_order_date_to = $('input[name=\'filter_order_date_to\']').attr('value');
      if (filter_order_date_to)  url += '&filter_order_date_to=' + encodeURIComponent(filter_order_date_to);
      var filter_ship = $('select[name=\'filter_ship\']').attr('value');
      if (filter_ship) url += '&filter_ship=' + encodeURIComponent(filter_ship);
      var filter_order_user = $('select[name=\'filter_order_user\']').attr('value');
      if (filter_order_user) url += '&filter_order_user=' + encodeURIComponent(filter_order_user);
      var filter_payed = $('select[name=\'filter_payed\']').attr('value');
      if (filter_payed != '*') url += '&filter_payed=' + encodeURIComponent(filter_payed);
      location = url;
    }
    $('#form input').keydown(function(e){
      if (e.keyCode == 13) $.fn.filter();
    });

    // date picker binding
    $('#form').bind('focusin',function(event){
    var $tgt = $(event.target);
        if ($tgt.is('input.date_pick')){
        //$(".date-pick").datePicker({startDate:'01/01/1996'});
        $(".date_pick").datePicker({
        clickInput:true,
        createButton:false,
        startDate:'2000-01-01'
    });
    }
    });
});
</script>
<?php echo $footer; ?>
