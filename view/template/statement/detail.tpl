<?php echo $header; ?>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>

<link rel='stylesheet' type='text/css' href='view/template/statement/detail.css' />
<script type='text/javascript' src='view/template/statement/atc/jquery/jquery.metadata.js'></script>
<script type='text/javascript' src='view/template/statement/atc/src/jquery.auto-complete.js'></script>
<link rel='stylesheet' type='text/css' href='view/template/statement/atc/src/jquery.auto-complete.css' />


<div class="box">
    <div class="center"></div>
    <div class="heading">
        <h1 style="background-image: url('view/image/product.png');">
            Payment Panel
            <!--a href='<?php echo $lnk_list; ?>' style='text-decoration:none;'>Back to List</a-->
        </h1>
        <div class="buttons"></div>
    </div>

    <div class="content">

        <form action='/statement/statement/process' id='statement-form' method='post'>

        <div id='account-atc-wrap'>
            ACCOUNT  <input type=text name='filter_accountno' class='atc input-large' id="account-atc" />
            <button class="btn btn-warning" id="search-order">Search</button>
        </div>

        <div id='account-info'>
          <div id="account_info_header" style="display:none;"><p id="account_info_title">Account Infotmation</p></div>
            <table style="margin-left:20px;"></table>
            <!--button style="margin-top:10px;" class="btn btn-warning" id="update-store" data-id=". + item.id + .">Update Account Info</button-->
        </div>

        <div id='account_history'></div>

        <div id='storeHistory'>
            <?php include "view/template/statement/finance.tpl"; ?>
        </div>

        </form>
    </div>
</div>

<?php echo $footer; ?>

<!-- common detail div -->
<div id='detail'></div>
<?php require_once('view/template/statement/help.tpl'); ?>

<script type="text/javascript">
$(document).ready(function(){

    $.fn.showAccountInfo = function(item) {
        // [{"id":"1","accountno":"test","name":"test","storetype":"R","address1":"2307 N Arlington Heights Rd","address2":null,"city":"Arlington Heights","state":"il","zipcode":"60004","phone1":"1234567890","phone2":"","salesrep":"admin","status":"1","fax":"","lat":null,"lng":null,"chrt":null,"parent":null,"comment":null,"email":"cutemom@live.com","billto":null,"shipto":null,"discount":"0","owner":"Julie"}]
        var html = '';
        html = '<tr>';
        html+= '    <td>Account : <input type="text" style="width:100px;" value="'+ item.accountno +'" readonly=""><input type="hidden" name="id" value="' + item.id + '"></td>';
        html+= '    <td>Name : <input type="text" style="width:100px;" value="'+ item.name +'" readonly=""></td>';
        html+= '    <td>Type : <input type="text" style="width:50px;" value="'+ item.storetype +'" readonly=""></td>';
        html+= '    <td>Phone : <input type="text" style="width:100px;" value="'+ item.phone1 +'" readonly=""></td>';
        html+= '    <td>Email : <input type="text" style="width:100px;" value="'+ item.email +'" readonly=""></td>';
        html+= '    <td>Contact : <input type="text" style="width:50px;" value="'+ item.owner +'" readonly=""></td>';
        html+= '</tr>';

        $('#account-info table').append(html).show();
    }

    $.fn.setAccountDetailPanel = function(store_id) {

        $.ajax({
            url:"/sales/list/ajax_getList",
            dataType:"json",
            data: { sid: store_id },
            success: function(list) {
                if ( list.length > 0 ) {
                    $('#finance-detail-panel').show();
                    }
                $(list).each(function(idx, item) {
                    var html = '';
                    html = '<tr>';
                    html+= '<td> <input type="text" name="txid[]" style="width:105px;" value="'+ item.txid +'" readonly=""> </td>';
                    html+= '<td> <input type="text" name="rep[]" style="width:40px;" value="'+ item.order_user +'" readonly=""> </td>';
                    html+= '<td> <input type="text" name="order_date[]" class="right" style="width:75px;" value="'+ item.order_date +'" readonly=""> </td>';
                    html+= '<td> <input type="text" name="ship_date[]" class="right" style="width:75px;" value="'+ item.shipped_date +'" readonly=""> </td>';
                    html+= '<td> <input type="text" name="invoice_no[]"  class="input-mini right" value="'+ item.invoice_no +'" readonly=""> </td>';
                    html+= '<td> <input type="text" name="total[]" class="input-mini right" value="'+ item.total +'" readonly=""> </td>';
                    html+= '<td> <input type="text" name="paid[]" class="input-mini right" value="'+ item.payed_sum +'" readonly=""> </td>';
                    html+= '<td> <input type="text" name="balance[]" class="input-mini right" value="'+ item.balance +'" readonly=""> </td>';
                    html+= '<td> <input type="text" name="paying[]" value="0" class="input-mini right"> </td>';
                    html+= '<td class="td-btn"> <button id="apply-product" class="btn" style="display:none;">Add</button> </td>';
                    html+= '</tr>';

                    $('#finance-detail-panel tbody').append(html);

                    });
                

            }
        });
                    }

    $("html, body").animate({ scrollTop: 0 }, "slow");

    // auto complete for account
    $( "#account-atc" )
    .focus()
    .autocomplete({
        max : 50,
        source: function (request, response) {
            /*
            var p = localStorage.getItem('pp');
            if ( p == null ) {
              */
            $.ajax({
                url:"/store/atc/accounts",
                dataType:"json",
                data: { query: request.term },
                success: function(data) {
                    response(data);
                    }
            });
            /*
            } else {
                var text, result = [];
                op = jQuery.parseJSON(p);
                $.each(op, function(idx,o) {
                    console.log(o['name']); 
                    text = o['name'].toLowerCase();
                    if ( text.match(request.term) || text.match(request.term) ) {
                        result.push(o)
                    }
                    });
                var data = JSON.stringify(result);
                response(data);
            }
              */
        },
        select: function(event,ui) {
            // get transactions
            $( "#account-atc" ).val( ui.item.name + " (" + ui.item.accountno + ")");
            $.fn.showAccountInfo(ui.item);
            $.fn.setAccountDetailPanel(ui.item.id);
            $('#paid-panel').show();
            $('#paid-amount').select().focus(); // TODO. not working. i guess lazy response of ajax
            return false;
        },
        minLength: 2
    })
    .data( "autocomplete" )._renderItem = function( ul, item ) {
        return $( "<li></li>" )
        .data( "item.autocomplete", item )
        .append( "<a>" + item.name + " (" + item.accountno + ")" )
        .appendTo( ul );
    };

    $('#apply-pay').bind('click', function(e) {

    });

    // =====> legacy
    <?php //include "view/template/statement/jqgrid/storeHistory.php"; ?>

    $('.memo_update').bind('click',function(e){
    var $el_comment = $('#comment'),
        $comment = $el_comment[0].value;
        $.ajax({
            type:'post',
            url:'<?php echo HTTP_SERVER; ?>/statement/detail/updateComment',
            dataType:'text',
            data:'store_id=<?php echo $store_id; ?>&comment=' + $comment,
            success:function(text){
                if ('success' == text){
                    $p = $('.memo_update').position();
                    $imgCss = {
                    'visibility':'visible',
                    'width':'80px',
                    'height':'20px',
                    'top':$p.top-30,
                    'left':$p.left-30,
                    'background-color':'black',
                    'color':'white',
                    'text-align':'center'
                    }
                    $('#detail').css($imgCss);
                    $('#detail').html('success');
                  //$('#detail').draggable(); 
                    }
            },
            fail:function(){}
        });
    });

    $.fn.arHistory = function(){
        $.ajax({
            type:'get',
            url:'<?php echo HTTP_SERVER; ?>/statement/detail/arHistory',
            dataType:'html',
            data:'store_id=<?php echo $store_id; ?>',
            success:function(html){
                $('#account_history').html(html);
            },
            fail:function(){
                //debugger;
                //console.log('fail : no response from proxy');
            }
        });
    }

    <?php
    if ( !empty($store_id) ) {
    ?>
    $.fn.arHistory();
    <?php
    }
    ?>
    

    // need to check what prevent atc under focus status, james
        //$('input.atc').focus();

    // quick search
    /***
    $('input[name=filter_accountno]').bind('keydown',function(e){
        $tgt = $(e.target);
        if ( e.keyCode == '13' ){  
            location.href = '/statement/detail&token=<?php echo $this->session->data['token']; ?>&accountno='+$(this).val();    
        }
    });
      ***/

    //$('input.atc').focus().autoComplete();
    /*
        .bind('mousedown',function(e){
            $this = $(this);
            $options = {
            'width':'150px'
        }
        $this.autoComplete();
    })
      */
});
</script>

