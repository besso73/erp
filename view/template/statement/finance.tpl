<style>
#finance-detail-panel, #finance-list-panel {
    margin: auto;
    text-align: center;
    display: table;
    margin-top: 20px;
    margin-bottom: 20px;
    border: 1px solid #E2E2E2;
    /* padding-right: 10px; */
}
</style>

<?php
/* response from ajax accounts 
approve_status: null
approved_user: null
balance: "10.00"
executor: null
invoice_no: null
order_date: "2015-04-02 10:36:38"
order_user: "admin"
payed_yn: "done"
shipped_yn: "N"
sign_yn: "N"
status: "1"
store_id: "1"
store_name: "test"
subtotal: "10.00"
term: null
total: "10.00"
txid: "test-20150402-1"
*/
?>



    <div id="finance-detail-panel" style="display:none;">
        <table>
            <thead>
                    <tr>
                            <td>TXID</td>
                            <td>REP</td>
                            <td>ORDER-AT</td>
                            <td>SHIP-AT</td>
                            <td>INVOICE#</td>
                            <td>ORDER</td>
                            <td>PAID</td>
                            <td>BALANCE</td>
                            <td>PAY</td>
                    </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

    <div id="paid-panel" style="display:none;">
        <h3>Process Payment</h3>
        <table style="position:relative;margin:auto;">
            <thead>
                <tr>
                    <td>PAID</td>
                    <td>TYPE</td>
                    <td>Date</td>
                    <!--td>CARD#</td-->
                    <td>MEMO</td>
                    <td>PAY</td>
                </tr>
            </thead>
            <tbody>
                    <tr>
                            <td>
                                <input type="number" id="paid_amount" name="paid_amount" style="width:100px;background-color:orange;" value="0"> 
                            </td>
                            <td>
                                <select name="paid_type">
                                    <option value='1'>Credit Card</option>
                                    <option value='2'>Check</option>
                                    <option value='3'>Cash</option>
                            </select>
                            </td>
                            <td> <input type="text" name="paid_date" class="date_pick" style="width:75px;" value="<?php echo date('Y-m-d'); ?>"> </td>
                            <!--td> <input type="text" name="paid_code" class="right" style="width:75px;" value=""> </td-->
                            <td> <input type="text" name="memo" class="right" style="width:300px;" value=""> </td>
                            <td class="td-btn"> 
                                <input type="submit" id="apply-pay" class="btn btn-warning" value="Submit Pay">
                            </td>
                    </tr>
            </tbody>
        </table>
    </div>


<style>
#detail{
    position : absolute;
    top: 100px;
    left: 100px;
    visibility:hidden;
    border: 1px dotted green;
    z-index:2;
}
</style>

<div id='detail'></div>

<script>



// Main method to manipulate payment
$('#paid_amount')
.bind('focusin', function(e){

    var that, paid;

    that = $(this);
    paid = that.val();
    paid = parseFloat(paid);

    /* 
       * start over if there is already paid amount
       */
    if ( paid > 0 ) {
        alert("You can't modify the typed amount. \n Let us start over the process.");
        window.location.reload();
    }

})
.bind('click', function(e) {
    $(this).select();
})
.bind('focusout', function(e) {
    $.fn.manipulateBalances($(this).val());
})
;

// main method to manipulate balance of each orders
$.fn.manipulateBalances = function(total) {

    var eOrders = $('#finance-detail-panel tbody tr');
    eOrders.each(function(idx, row){

        ePaid = $(row).find('input[name="paid[]"]');
        eBalance = $(row).find('input[name="balance[]"]');
        ePaying = $(row).find('input[name="paying[]"]');
        paid = ePaid.val();
        balance = eBalance.val();
        paying = ePaying.val();
        if ( paid == '' ) paid = 0;
        if ( balance == '' ) balance = 0;
        if ( paying == '' ) paying = 0;

        total = parseFloat(total);
        paid = parseFloat(paid);
        balance = parseFloat(balance);
        paying = parseFloat(paying);

        // if balance is negative num
        if ( balance < 0 ) {
            total += -1 * balance;
            eBalance.val( 0 );
        } else {
            // if balance of the row is larger than paid
            if ( balance > total ) {
                eBalance.val( balance - total );
                ePaid.val ( paid + total );
                ePaying.val ( total );
                total = 0;
            } else {
                eBalance.val( 0 );
                ePaid.val ( paid + balance );
                ePaying.val ( balance );
                total -= balance; 
            }   
        }
        if ( total == 0 )   return false;

    });

    // more paying than ordered
    if ( total > 0 ) {
        eBalance.val( -1 * total );
    }
    
}


// =======> Legacy

$('#finance').bind('click',function(e){
    
    // todo. IE dont support trim(), add it
    if (typeof String.prototype.trim !== 'function'){   
        String.prototype.trim = function(){     
            return this.replace(/^\s+|\s+$/g, '');    
        }
    }
    
    $tgt = $(e.target);
        if ( $tgt.is('input[name=paid]') ){
        $tgt.select();
    }
        if ($tgt.is('.detail_pay')){
        $prt = $tgt.parents('tr');
        $el_txid = $prt.find('.txid');
        $txid    = $el_txid.html().trim();
        $.ajax({
            type:'get',
            url:'/statement/detail/callHistoryPannel',
            dataType:'html',
            data:'token=<?php echo $token; ?>&txid=' + $txid,
            success:function(html){
                $imgCss = {
                  'visibility':'visible',
                  'top':100,
                  'left':100,
                  'background-color':'white',
                  'text-align':'center',
                  'z-index':99
                    }
                $('#detail').css($imgCss);
                $('#detail').html(html);
                // $('#detail').draggable();
            }
        });
    }
});

$('#finance')
.bind('focusin',function(e){
    // set paid before transaction
    $tgt = $(e.target);
    if ( $tgt.is('input.mod')){
        $_paid = $tgt.val();
        $_memo = $tgt.parents('tr').find('input[name=pay_num]').val();
        $_method = $tgt.parents('tr').find('select[name=method]').val();
    }
    if ( $tgt.is('input[name=pay_num]')){
        $tgt.select();
    }
})
.bind('keydown',function(e){
    $tgt = $(e.target);
    $bankaccount = $('select[name=bankaccount]').val();

    //if ( $tgt.is('input[name=paid]') && e.keyCode == '13' ){
    if ( $tgt.is('input.mod') && e.keyCode == '13' ){
        //console.log('tgt is input.mod');
        var $row = $('#finance tbody').children('tr'),
            $paid_diff = parseFloat($tgt.val()) - parseFloat($_paid);
            $paid_diff = $paid_diff.toFixed(2);
        //console.log($paid_diff);;
        //console.log($paid_diff +' before process');
        // initialize target's value
        if ($_method == 'bounce' && $paid_diff >= 0){
            alert('input minus value(-) under bounce');
            return false;
        }

        /*** PROCESS
        1. check input paid is larger than balance
        2. check if idx row has balance or not, if not next
        3. minus paid - balance if balance exist
          3-1. if paid - balance > 0 , next loop
          3-2. and set that row balance = 0
        4. if remain balance > 0 . stop
          ***/
        if ($paid_diff > 0){
            $tgt.val(0);
            for($i=0;$i<$row.length;$i++){
                var $currentRow = $($row[$i]),
                    $el_txid    = $currentRow.find('a.txid'),
                    $el_order   = $currentRow.find('.detail_pay'),
                    $el_balance = $currentRow.find('td.balance'),
                    $el_method  = $currentRow.find('select[name=method]'),
                    $el_pay_num = $currentRow.find('input[name=pay_num]'),
                    $el_paid    = $currentRow.find('input[name=paid]');
                $txid    = $el_txid.html().trim();
                $order   = $el_order.html().trim();
                $balance = $el_balance.html().trim();
                //$method  = $el_method.val();
                $method = $_method;
                //$pay_num = $el_pay_num.val();
                $pay_num = $_memo;
                //debugger;
                $paid = $el_paid.val();

                // check the changed value well
                if ($balance == 0) continue;
                
                // if paid is larger than the row balance, all balance is paid
                if ( parseFloat($paid_diff) > parseFloat($balance)){
                    ////console.log($paid_diff + 'diff is larger than balance' + $balance);
                    $el_balance.html(0);
                    $paid = parseFloat($paid) + parseFloat($balance);
                    $paid = $paid.toFixed(2);
                    $el_paid.val($paid);
                    $paid_diff = parseFloat($paid_diff) - parseFloat($balance);
                    $paid_diff = $paid_diff.toFixed(2);
                    $tgt.select();
                    // if last row 
                    if ($i == $row.length-1 ){
                        $el_balance.html( -1 * $paid_diff );
                        $hdr = $.fn.updateFinance($txid,$paid,-1 * $paid_diff,$method,$pay_num,$bankaccount);
                        $hdr.success(function(){
                          location.href = "/statement/detail&store_id=<?php echo $store_id; ?>";
                        });
                         Break;
                    } else {
                        $.fn.updateFinance($txid,$balance,'0',$method,$pay_num,$bankaccount);
                        //debugger;
                        continue;
                    }
                    } else {
                    ////console.log('diff is smaller than balance : ' + $balance);
                    //alert($balance);
                    $paid = parseFloat($paid_diff) + parseFloat($_paid);
                    $paid = $paid.toFixed(2);
                    $el_paid.val($paid);
                    $balance = parseFloat($balance) - parseFloat($paid_diff);
                    $balance = $balance.toFixed(2);
                    $el_balance.html($balance);
                    $tgt.parents('tr').find('input[name=pay_num]').focus();
                    $hdr = $.fn.updateFinance($txid,$paid_diff,$balance,$method,$pay_num,$bankaccount);
                    $hdr.success(function(){
                        location.href = "/statement/detail&store_id=<?php echo $store_id; ?>";
                    });

                    $paid_diff = 0;
                  break;
                    }
            } // for each
        } else {  // bounce !!
            //console.log('paid_diff < 0 start : ' + $paid_diff);
            // should reverse loop
            $paid_diff = $tgt.val();
            for($i=$row.length-1;$i>=0;$i--){
                var $currentRow = $($row[$i]),
                    $el_txid    = $currentRow.find('a.txid'),
                    $el_order   = $currentRow.find('.detail_pay'),
                    $el_balance = $currentRow.find('td.balance'),
                    $el_method  = $currentRow.find('select[name=method]'),
                    $el_pay_num = $currentRow.find('input[name=pay_num]'),
                    $el_paid    = $currentRow.find('input[name=paid]');
                $txid    = $el_txid.html().trim();
                $order   = $el_order.html().trim();
                $balance = $el_balance.html().trim();
                //$method  = $el_method.val();
                $method = $_method;
                //$pay_num = $el_pay_num.val();
                $pay_num = $_memo;
                $paid = $el_paid.val();
            
                // if diff is 0, we dont need to calculate that row
                if ($paid_diff == 0 ){
                  continue;
                    }
            
                //todo. we have one more business bug
                //todo. if some rollback over the sum(paid), the remain will be disappeared !! -0-
                
                // if remain balance < 0, we need to add the balance value with diff
                if ( parseFloat($balance) < 0 ){
                    $paid_diff = parseFloat($paid_diff) - parseFloat($balance);
                    $paid_diff = $paid_diff.toFixed(2);
                    }
                // todo. check it!
                if ($i == $row.length-1) $paid = $_paid;
            
                if ( parseFloat($paid_diff) * -1 > parseFloat($paid) ){
                    
                    if ( $paid == 0 ) continue;
              
                    if ($el_paid.is('input.mod'))  $paid = $_paid;
                    $diff = -1 * parseFloat($paid) + parseFloat($balance);
                    $diff = $diff.toFixed(2);
                    $paid_diff = parseFloat($paid_diff) + parseFloat($paid);
                    $paid_diff = $paid_diff.toFixed(2);
                    $el_paid.val(0);
                    $balance = parseFloat($order);
                    $el_balance.html($balance);

                    $hdr = $.fn.updateFinance($txid, parseFloat($paid) * -1 ,$order,$method,$pay_num,$bankaccount,$diff);
                  
                    if ($i == 0){
                        if ( $paid_diff > 0 ){
                            // insert new dummy new order
                            // todo. if rollback amount is larger than DB sum, now we lose that remain data
                        }
                        $hdr.success(function(){
                            // call new ajax for insert dummy txid
                            if ($_method == 'bounce'){
                             $bouncehdr = $.fn.insertFinance($txid,$pay_num);
                             $bouncehdr.success(function(e){
                              location.href = "/statement/detail&store_id=<?php echo $store_id; ?>";
                            });
                        }
                        location.href = "/statement/detail&store_id=<?php echo $store_id; ?>";
                        //debugger;
                    });
                     Break;
                    } else {
                        continue;
                    }
                    } else {
                    //debugger;
                    //console.log('bounce task break area start');
                    if ($balance > 0){
                        $paid = parseFloat($paid) + parseFloat($paid_diff);
                    } else {
                        $paid = parseFloat($paid) + parseFloat($paid_diff) + (parseFloat($balance) * 1) ;
                    }
                    $paid = $paid.toFixed(2);
                    $el_paid.val($paid);
                    $balance = parseFloat($balance) + ( parseFloat($paid_diff) * -1 );
                    $balance = $balance.toFixed(2);
                    $el_balance.html($balance);
                    $diff = parseFloat($paid_diff);
                    $diff = $diff.toFixed(2);

                    $hdr = $.fn.updateFinance($txid,$diff,$balance,$method,$pay_num,$bankaccount,$diff);
                    $hdr.success(function(){
                        // call new ajax for insert dummy txid
                        if ($_method == 'bounce'){
                            $bouncehdr = $.fn.insertFinance($txid,$pay_num);
                            $bouncehdr.success(function(e){
                             location.href = "/statement/detail&store_id=<?php echo $store_id; ?>";
                           });
                        }   
                        location.href = "/statement/detail&store_id=<?php echo $store_id; ?>";
                    });
                    $paid_diff = 0;

                break;
                    }
            } // forloop
        }   // $paid_diff > 0
    //setTimeout(function(){      location.href = "/statement/detail&token=<?php echo $txid; ?>&store_id=<?php echo $store_id; ?>";    },1000);
    }   // keycode 13
});


$.fn.insertFinance = function($txid,$memo){
    $hdr = $.ajax({
    type:'get',
    url:'/statement/detail/insertFinance&txid=' + $txid + '&store_id=<?php echo $store_id ?>&memo=' + $memo,
    dataType:'html'
    });
  return $hdr;
}

$.fn.updateFinance = function($txid,$paid,$balance,$method,$pay_num,$bankaccount,$diff){
    $hdr = $.ajax({
    type:'get',
    url:'/statement/detail/updateFinance',
    data: 'txid=' + $txid + '&paid=' + $paid + '&method=' + $method + '&pay_num=' + encodeURIComponent($pay_num) + '&balance=' + $balance + '&bankaccount=' + $bankaccount + '&diff=' + $diff,
    dataType:'html'
    });
  return $hdr;
}
</script>
