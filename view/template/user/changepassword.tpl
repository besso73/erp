<div class='box'>
    <form action="/user/user/updatepassword" method="post" enctype="multipart/form-data" id="form-change-password" class="form-signin" role="form" style="margin:0;">
        <h4 class="form-signin-heading">New Password</h4>
        <input type="password" name="password" id='change-password' value="" class="form-control" autofocus="true" />
        <button id='ajaxSubmit' class="btn btn-lg btn-primary btn-block" style="margin-left:0;">Change</button>
    </form>
</div>

<script type="text/javascript">
$.fn.ajaxSubmit = function(form){
    $.post(form.attr('action'), form.serialize(), function(data){
        $.fancybox.close();
    });
}

$('#form-change-password input').keydown(function(e){
    if (e.keyCode == 13){
        form = $('#form-change-password');
        $.fn.ajaxSubmit(form);
    }
});

$('#ajaxSubmit').bind('click',function(e){
    form = $('#form-change-password');
    $.fn.ajaxSubmit(form);
});
</script>
