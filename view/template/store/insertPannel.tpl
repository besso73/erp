<style>
#tab_transfer,#tab_history { display:none; }
#form .form {  width:400px;  }

table.form tr td:first-child {
    width: 85px;
}

td.label {
    background-color: #E2E2E2;
    line-height: 32px;
    width: 200px;
}

td.content {
    text-align: left;
}

.box > .heading {
    width: 600px;
}

.box > .content {
    width: 600px;
}
</style>

<?php
//echo '<pre>'; print_r($store); echo '</pre>'; exit;

$mode = 'insert';
if ( isset($store['id'])) {
        $mode = 'update';
}

$id        = isset($store['id']) ? $store['id'] : '' ;
$accountno = isset($store['accountno']) ? $store['accountno'] : '' ;
$name      = isset($store['name']) ? $store['name'] : '' ;
$storetype = isset($store['storetype']) ? $store['storetype'] : 'R' ;
$address1  = isset($store['address1']) ? $store['address1'] : '' ;
$address2  = isset($store['address2']) ? $store['address2'] : '' ;
$city      = isset($store['city']) ? $store['city'] : '' ;
$state     = isset($store['state']) ? $store['state'] : '' ;
$zipcode   = isset($store['zipcode']) ? $store['zipcode'] : '' ;
$phone1    = isset($store['phone1']) ? $store['phone1'] : '' ;
$phone2    = isset($store['phone2']) ? $store['phone2'] : '' ;
$salesrep  = isset($store['salesrep']) ? $store['salesrep'] : $this->user->getUsername();
$status    = isset($store['status']) ? $store['status'] : '1' ;
$fax       = isset($store['fax']) ? $store['fax'] : '' ;
$lat       = isset($store['lat']) ? $store['lat'] : '' ;
$lng       = isset($store['lng']) ? $store['lng'] : '' ;
$chrt      = isset($store['chrt']) ? $store['chrt'] : '' ;
$parent    = isset($store['parent']) ? $store['parent'] : '' ;
$email     = isset($store['email']) ? $store['email'] : '' ;
$billto    = isset($store['billto']) ? $store['billto'] : '' ;
$shipto    = isset($store['shipto']) ? $store['shipto'] : '' ;
$discount  = isset($store['discount']) ? $store['discount'] : 0 ;
$owner     = isset($store['owner']) ? $store['owner'] : '' ;
$comment     = isset($store['comment']) ? $store['comment'] : '' ;

//echo '<pre>'; print_r($owner); echo '</pre>'; exit;    

?>
<div class="box" style='background-color:white;'>
    <div class="heading">
        <h1>Update Account</h1>
        <div class="buttons">

            <button class="btn btn-success submit-update-store">Save</button>
            <!-- <a onclick="$('#detail').html(); $('#detail').css('visibility','hidden');" class="button"><span>Cancel</span></a> -->
        </div>
    </div>
    <div class="content">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="updateForm" style="margin-bottom:0;">
        <div id="tab_general">
            <table class="form">
                    <tr>
                            <td class='label'>Account</td>
                            <td class='content'>
                                <input type="hidden" name="id" value="<?php echo $id; ?>" />
                                <input type="hidden" name="mode" value="<?php echo $mode; ?>" />
                                <input type="text" name="accountno" <?php if ($mode == 'insert') echo "id='account-check'"; ?> class="input-small" value="<?php echo $accountno; ?>" style='background-color:#eeeeee' placeholder='Account#' /> / 
                                <select name='storetype' class='input-mini'>
                                <option value='R' <?php if ($storetype == 'R') echo "selected"; ?>>R</option>
                                <option value='W' <?php if ($storetype == 'W') echo "selected"; ?>>W</option>
                            </select>
                        / <input type="text" name="salesrep" class='input-small' value="<?php echo $salesrep; ?>" placeholder="SalesRep" readonly />
                            </td>
                    </tr>
                    <tr>
                            <td class='label'>Status</td>
                            <td class='content'>
                                <?php $aStoreCode = $this->config->getStoreStatus(); ?>
                                <select name="status">
                                <option value="0" <?php if ($status == '0') echo "selected"; ?>><?php echo $aStoreCode['0']; ?></option>
                                <option value="1" <?php if ($status == '1') echo "selected"; ?>><?php echo $aStoreCode['1']; ?></option>
                                <option value="2" <?php if ($status == '2') echo "selected"; ?>><?php echo $aStoreCode['2']; ?></option>
                                <option value="3" <?php if ($status == '3') echo "selected"; ?>><?php echo $aStoreCode['3']; ?></option>
                                <option value="9" <?php if ($status == '9') echo "selected"; ?>><?php echo $aStoreCode['9']; ?></option>
                            </select>
                            </td>
                    </tr>
                    <tr><td class='label'>Name</td>
                            <td class='content'>
                                <input type="text" name="name" class="input-large" value="<?php echo $name; ?>" />
                            </td>
                    </tr>
                    <tr><td class='label'>Address</td>
                            <td class="content">
                                <input type="text" name="address1" class="input-large" value="<?php echo $address1; ?>" /></td></tr>
                    <tr>
                            <td class='label'>City/State/Zip</td>
                            <td class='content'>
                                <input type="text" name="city" class="input-medium" value="<?php echo $city; ?>" placeholder="City" />/
                                <input type="text" name="state" class="input-small" value="<?php echo $state; ?>" placeholder="State" />/
                                <input type="text" name="zipcode" class="input-small" value="<?php echo $zipcode; ?>" placeholder="Zipcode" />
                            </td>
                    </tr>
                    <tr>
                        <td class='label'>Phone/SubPhone</td>
                        <td class='content'><input type="text" name="phone1" class="input-medium" value="<?php echo $phone1; ?>" /> / <input type="text" name="phone2" size="12" value="<?php echo $phone2; ?>" /></td>
                    </tr>
                    <tr><td class='label'>fax</td>
                            <td class='content'><input type="text" name="fax" class="input-medium" value="<?php echo $fax; ?>" /></td></tr>
                    <tr>
                        <td class='label'>Contact / Email</td>
                        <td class='content'>
                            <input type="text" name="owner" size="20" value="<?php echo $owner; ?>" /> / 
                            <input type="text" name="email" size="20" value="<?php echo $email; ?>" /></td>
                    </tr>
                    <!--tr>
                            <td class='label'>Chicago Area</td>
                            <td class='content' style="width:150px;">
                                <select name="chrt">
                                    <option value="0" >Not Chicago retail</option>
                                    <option value="1" >Chicago Area</option>
                            </select>
                            </td>
                    </tr-->
                    <!--tr>
                            <td class='label'>Host account</td>
                            <td class='content'><input type="text" name="parent" size="20" value="" /></td>
                    </tr-->
                    <?php
                if ($storetype != 'R') {
                    ?>
                    <tr>
                            <td class='label'>Discount</td>
                            <td class='content'>
                                <input type="number" name="discount" class="input-mini" value="<?php echo $discount; ?>" />%
                            <!--input type="text" name="dc1_desc" class="input-medium" value="<?php echo $discount; ?>" /-->
                            </td>
                    </tr>
                    <?php
                    }
                    ?>
                    <tr>
                            <td class='label'>Discount</td>
                            <td class='content'>
                                <input type="text" name="discount" class="input-mini" value="<?php echo $discount; ?>" /> %
                            </td>
                    </tr>
                    <tr>
                            <td class='label'>Memo</td>
                            <td class='content'>
                            <textarea name="comment" style='width:400px;'><?php echo $comment; ?></textarea>
                            </td>
                    </tr>

            </table>
        </div>
        </form>
    </div>
</div>
<script>
/*
$(document).ready(function(){
        $.tabs('#tabs a'); 
        $.fn.arHistory = function(){
        $.ajax({
            type:'get', dataType:'html',
            url:'/sales/order/arHistory',
            data:'store_id=<?php echo $id; ?>',
            success:function(html){ $('#tab_ar_history').html(html);  }
        });
    }
        $.fn.arHistory();
});
*/
</script>
