<?php
$descLen = explode(chr(13),$description);
if ( count($descLen) > 1 ){
        $descHeight = count( $descLen ) * 25;
        $descHeight = 'height:'.$descHeight.'px';
}

if ( empty($salesrep) || $salesrep == '' ) $salesrep = 'admin';
?>

<script type='text/javascript' src='view/template/statement/atc/jquery/jquery.metadata.js'></script>
<script type='text/javascript' src='view/template/statement/atc/src/jquery.auto-complete.js'></script>
<link rel='stylesheet' type='text/css' href='view/template/statement/atc/src/jquery.auto-complete.css' />

<table id='storeinfo-table' cellpadding="0" cellspacing="0">
    <tr>
        <td align='left'>
            <input type='hidden' name='salesrep' value='<?php echo $salesrep; ?>' />
            <input type='hidden' name='store_id' value='<?php echo $store_id; ?>' />
            <input type='hidden' name='txid' value='<?php echo $txid; ?>' />
            <input type='hidden' name='mode' value='<?php echo $mode; ?>' />
            <input type='hidden' name='async' value='false' />
            <input type='hidden' name='ajax' value='0' />
            <input type="text" class='input-large' id="account-name" name='store_name' value="<?php echo $store_name; ?>" placeholder='Search Account Name' autocomplete="off" style="background-color:rgb(217, 255, 161);"/>
            <input type="text" name='accountno' value='<?php echo $accountno; ?>' class='input-small' placeholder="Account no" autocomplete="off" />
            <input type="text" name='storetype' value='<?php echo $storetype; ?>' class='input-mini hide' placeholder="Type" readonly />
            <?php
            if ('insert' == $mode) {
                $accountControlText = 'New';
                $account_class = 'account-new';
            } else {
                $accountControlText = 'Edit';
                $account_class = 'account-edit';
            }
            ?>
            <!--button id="account-insert" class="btn btn-warning <?php echo $account_class; ?>"
             data-id="<?php echo $store_id; ?>"><?php echo $accountControlText; ?></button-->
        </td>
    </tr>
    <tr>
        <td>
            <input type="text" class="input-medium" name='phone1' value='<?php echo $phone1; ?>' placeholder='Phone' />
            <input type="text" class="input-medium" name='fax' value='<?php echo $fax; ?>'  placeholder='Fax'  />
            <?php
            if ('update' == $mode) {
            ?>
            <button id="account-history" class="btn">History</button>
            <?php
            }
            ?>
        </td>
    </tr>
    <tr>
        <td class='context'>
            <input type="text" class="input-small" name='city' value='<?php echo $city; ?>' style='width:48%' placeholder='City'/>
            <input type="text" class="input-small" name='state' value='<?php echo $state; ?>' style='width:10%' placeholder='ST'/>
            <input type="text" class="input-small" name='zipcode' value='<?php echo $zipcode; ?>' style='width:24%' placeholder='Zipcode' />
        </td>
    </tr>
    <tr>
        <td>
            <input type="text" name='address1' value='<?php echo $address1; ?>' class='input-xlarge' placeholder="Address" />
            <input type="text" class='date_pick input-small' name='order_date' value='<?php echo $order_date; ?>' />
        </td>
    </tr>
    <tr>
        <td>
            <textarea name='description' style='min-height:70px;width:98%; scroll-y:auto;' placeholder='Add Meno Here'>
                    <?php echo $description; ?>
            </textarea>
        </td>
    </tr>
</table>

<script>
$('document').ready(function(){
    // auto complete for account
    $( "#account-name" )
    .focus()
    .autocomplete({
        max : 50,
        source: function (request, response) {
            /*
            var p = localStorage.getItem('pp');
            if ( p == null ) {
              */
            $.ajax({
                url:"/store/atc/accounts",
                dataType:"json",
                data: { query: request.term },
                success: function(data) {
                    response(data);
                    }
            });
            /*
            } else {
                var text, result = [];
                op = jQuery.parseJSON(p);
                $.each(op, function(idx,o) {
                    console.log(o['name']); 
                    text = o['name'].toLowerCase();
                    if ( text.match(request.term) || text.match(request.term) ) {
                        result.push(o)
                    }
                    });
                var data = JSON.stringify(result);
                response(data);
            }
              */
        },
        select: function(event,ui) {

            // get transactions
            $( "#storeinfo input[name=store_id]" ).val( ui.item.id );
            $("#storeinfo input[name=accountno]").val( ui.item.accountno );
            $("#storeinfo input[name=store_name]").val( ui.item.name );
            $("#storeinfo input[name=storetype]").val( ui.item.storetype );
            $("#storeinfo input[name=address1]").val( ui.item.address1 );
            $("#storeinfo input[name=city]").val( ui.item.city );
            $("#storeinfo input[name=state]").val( ui.item.state );
            $("#storeinfo input[name=zipcode]").val( ui.item.zipcode );
            $("#storeinfo input[name=phone1]").val( ui.item.phone1 );
            $("#storeinfo input[name=fax]").val( ui.item.fax );
            /* $("#storeinfo input[name=salesrep]").val( ui.item.salesrep ); */
            return false;

        },
        minLength: 2
    })
    .data( "autocomplete" )._renderItem = function( ul, item ) {
        return $( "<li></li>" )
        .data( "item.autocomplete", item )
        .append( "<a>" + item.name + " (" + item.accountno + ")" )
        .appendTo( ul );
    }; 
});
</script>
