<?php
// todo. manually subtract by ship_code and ship_lift
$freegood_percent = 0;
if ($total > 0){
  if ( ($total - $ship_cod - $ship_lift) > 0 ){
        $freegood_percent = ( $freegood_amount / ( $total - $ship_cod - $ship_lift ) ) * 100 ;
        $freegood_percent = round( $freegood_percent , 2);
    }
}
?>
<div id='payment'>
    <table>
    <tr>
        <td class='label'>
        Amount
        </td>
        <td class='context'>
        <input type='text' name='total' value='<?php echo $total; ?>' size=7/>
        <!--p class='added_amount' style='color:red;display:inline;'>
        <?php
            // plus add'ed value ( lift / cod )
            //$add = $ship_cod + $ship_lift;
            //if ($add > 0) echo '(' . $add . ')';
        ?>
        </p-->
        </td>
        <td class='label no_print_invoice'>
        Promo
        </td>
        <td class='context no_print_invoice'>
        <?php
        if ( isset($sales) && $total > 0 ){
        $aLast = end($sales); 
        ?>
        <input type='text' name='promotion_sum' value='<?php echo $aLast[count($aLast)-1]['promotion_sum']; ?>' size=6 />
        (<?php echo round($aLast[count($aLast)-1]['promotion_sum']/$total*100,1); ?> %)
        <!-- use later -->
        <input type='hidden' name='payed_sum' value='<?php echo $payed_sum; ?>' size=8 />
        <?php
        }
        ?>
        </td>
        <td class='label no_print_invoice'>Damage</td>
        <td class='context no_print_invoice'>
        <?php if ( isset($sales) && $total > 0 ){  ?>
        <input type='text' name='damage_sum' value='<?php echo $aLast[count($aLast)-1]['damage_sum']; ?>' size=6 />
        (<?php echo round($aLast[count($aLast)-1]['damage_sum']/$total*100,1); ?> %)
        <!-- use later -->
        <input type='hidden' name='balance' value='<?php echo $balance; ?>' size=8 style='background-color:yellow' />
        <?php } ?>
        </td>
        <td class='label'>
        Free
        </td>
        <td class='context'>
        <input type=text id='freegood_amount' value='<?php echo $freegood_amount; ?>' size=5 readonly />
        </td>
        <td class='label'>
        Percent
        </td>
        <td class='context'>
        <input type=text id='freegood_percent' value='<?php echo $freegood_percent; ?>' readonly style='<?php if ($freegood_percent > 10) echo "color:red;"?>' size=5 />
        </td>
    </tr>
    </table>
</div>
