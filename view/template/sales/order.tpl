<?php echo $header; ?>
<?php if ($error_warning){ ?><div class="warning"><?php echo $error_warning; ?></div><?php } ?>
<link rel='stylesheet' type='text/css' href='view/template/sales/order.css' />
<script>
function checkInput(e) {
    //if ( e.keyCode==13 && e.srcElement.type != 'input' ) { 
    if ( e.which == 13 ) {
        return false;
    }
}
</script>
<div class="box">
    <div class="heading np hide">
        <h1 style='font-size:14px;padding:7px 10px 5px 2px;'></h1>
        <div class="buttons" style='float:right'>
            <a class="button save_order"><span>Save</span></a>
            <a id='show_invoice' class="button"><span>Preview</span></a>
            <?php if ($mode == 'show'){ ?>
                    <a href="/sales/order&txid=<?php echo $txid; ?>" class="button"><span>Edit</span></a>
                    <a onclick="location = '<?php echo $lnk_list; ?>';" class="button"><span>List</span></a>
                    <a id='print' onclick='printOrder()' class='button'><span>Print</span></a>
            <?php } ?>
        </div>
    </div>
    <div id="order-container">
        <form action='<?php echo $order_action; ?>' method='post' id='order-form' onKeyPress='return checkInput(event);'>
        <div id='base'>
            <div style='width:500px;float:left;'>
                    <div id='storeinfo'>
                            <?php require_once('view/template/sales/storeinfo.tpl'); ?>
                    </div>
            </div>
            <div style='width:250px;float:left'>
                    <div id='ship'>
                            <?php require_once('view/template/sales/ship.tpl'); ?>
                    </div>
                    <div>
                            <!-- todo : block payment area -->
                            <!--div style='float:left;width:100px;'>
                            <table style='width:200px' id='cpay'>
                            <tr>
                                    <td>PC Date <input type=text name='pc_date' class='date_pick' value='<?php echo $pc_date ?>' style='width:70px' /></td>
                                    <td style='border-left:1px dotted #e9e9e9;'>
                                        <input type=text name='post_check' style='width:60px' value='<?php echo $post_check ?>' />
                                    </td>
                            </tr>
                            <tr>
                                    <td>Current Check</td>
                                    <td><input type=text name='cur_check'  style='width:60px' value='<?php echo $cur_check ?>' /></td>
                            </tr>
                            <tr>
                                    <td>Current Cash</td>
                                    <td><input type=text name='cur_cash'   style='width:60px' value='<?php echo $cur_cash ?>' /></td>
                            </tr>
                            </table>
                            </div-->
                            <div id='account_history' style='float:right;width:180px;'></div>
                    </div>
            </div>
        </div>
        
        <?php //require_once('view/template/sales/payment.tpl'); ?>
        <!-- order info -->
        <div id='order'>
            <?php require_once('view/template/sales/sales.tpl'); ?>
        </div>
    </div>
    </form>
    <div class='footer hide'>
        <!-- fileupload -->
        <?php //require_once('view/template/sales/fileupload.tpl'); ?>
    </div>
</div>
<?php echo $footer; ?>

<style>
#floatmenu{
    top:300px;  left:900px; width:70px; position:absolute;
    border:1px solid red; background-color:orange;  visibility:hidden;
}
#floatmenu div{ text-align:left;  margin:1px; }
#floatmenu div input{ width:60px; color:red;  }
#floatmenu button{  width:60px; }
</style>

<div id='floatmenu' class='np'>
    <div>
        <input type='text' name='float_total' value='' readonly/>
    </div>
    <div>
        <input type='text' name='float_freegood_percent' value='' readonly/>
    </div>
    <div>
        <button type="button" id='show'>Confirm</button>
        <!--button type="button" id='edit'>Edit</button-->
    </div>
</div>

<script type='text/javascript' src='/js/order.js'></script>
<script type='text/javascript' src='/js/jquery.tablesorter.js'></script>

<script>
    $("#help-container").show('<?php echo $this->route ?>');
</script>

<?php require_once('view/template/sales/help.tpl'); ?>




