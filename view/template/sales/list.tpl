<?php echo $header; ?>
<?php if ($error_warning) { ?><div class="warning"><?php echo $error_warning; ?></div><?php } ?>
<?php if ($success){ ?><div class="success"><?php echo $success; ?></div><?php } ?>

<?php
//$invoice_txid = isset($_GET['invoice_txid']) ? $_GET['invoice_txid'] : false;
//$mode = isset($_GET['mode']) ? $_GET['mode'] : 'update';
//if (false != $invoice_txid && 'insert' == $mode) {
//    $invoice_lnk = HTTP_SERVER . 'invoice/sheet&txid=' . $invoice_txid;
//    $popup_link = "<script>window.open('". $invoice_lnk ."')</script>";
//    echo $popup_link;
//}
?>

<div class="box">
    <div class="heading">
        <h1 style='padding-left:10px;'>
            <?php if ($manager == true){ ?>
                <input type=text name='notice' style='width:400px;' id='update_notice' value='<?php echo $notice[0]['notice'] ?>' />
            <?php } else {  ?>
                <font color='red'><?php echo $notice[0]['notice'] ?></font>
            <?php } ?>
        </h1>
        <div class="buttons">
            <!--button class="btn export">Export</button-->
            <button onclick="location = '<?php echo $lnk_insert; ?>'" class="btn btn-warning">Insert</button>
            <!--a id='batch_ship' class="button"><span>Ship</span></a-->
            <!--a id='batch_print' class="button"><span>Print</span></a-->
            <button onclick="$('#form').submit();" class="btn">Delete</button>
        </div>
    </div>
    <div class="content">
    <form action="<?php echo $lnk_delete; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="table">
            <thead>
            <tr>
                    <th width="1" style="text-align: center;">
                            <input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" style='margin-bottom:6px;margin-left:3px;' />
                    </th>
                    <th>
                    P.O.Number
                    </th>
                    <th>
                    Account Name
                    </th>
                    <th>
                      Order DATE
                    </th>
                    <th>
                    Price
                    </th>
                    <th style='text-align:center;'>
                    Invoice
                    </th>
                    <th>
                    REP
                    </th>
                    <!--th>Ship</th-->
                    <th></th>
            </tr>
            </thead>
            <tbody>
            <tr class="filter">
                    <td colspan='2'>
                            <input type="text" name="filter_txid" value="<?php echo $filter_txid; ?>" style='width:125px;' />
                    </td>
                    <td>
                            <input type="text" class="input-medium" name="filter_store_name" value="<?php echo $filter_store_name; ?>" style='width:125px;' />
                    </td>
                    <td class='left' colspan='3'>
                            <input type="text" class='date_pick input-small' name="filter_order_date_from" value="<?php echo $filter_order_date_from; ?>"  />
                  -
                            <input type="text" class='date_pick input-small' name="filter_order_date_to" value="<?php echo $filter_order_date_to; ?>"  />
                    </td>
                    <td>
                            <!--select name='filter_order_user' class='input-small'>
                            <option value=''>---</option>
                            <?php
                      if ( !$filter_order_user ) $filter_order_user = $this->user->getUsername();
                        $aSales = $this->user->getSales();
                        foreach($aSales as $row){
                        $rep = $row;
                        $selected = ( $filter_order_user == $rep ) ? 'selected' : '' ;
                          echo "<option value='$rep' $selected>$rep</option>";
                     }
                            ?>
                            </select-->
                    </td>
                    <!--td></td-->
                    <td align="right">
                            <button id="search-order" class="btn">Search</button>
                    </td>
            </tr>
            <?php
            if ($txs){
            $total = 0;
            foreach ($txs as $tx){ 
                
                $onhold = false;
                if ('1' != $tx['status']) {
                    $onhold = true;
                    }
                
                $bg_css = '';
                if ($onhold) {
                    $bg_css = 'style=background-color:#EEE;';
                    }
                
                /*
                if ('yet' == $tx['payed_yn']){
                    switch($tx['pay_due']){
                        case '30':
                             $bg_css = 'style=background-color:#fdf8a0;';  // yello
                             Break;
                        case '60':
                             $bg_css = 'style=background-color:#a0fdb8;';  // green, 30-60
                             Break;
                        case '90':
                             $bg_css = 'style=background-color:#84eafc;';  // blue
                             Break;
                        case '120':
                             $bg_css = 'style=background-color:#fc8d84;';  // red
                         Break;
                    }
                    }
                $approve_status = $tx['approve_status'];
                if ($approve_status == 'pending')  $approve_status = "<font color='red'>$approve_status</font>";
                  */
                //$order_user = ( $tx['executor'] != $tx['order_user'] ) ? $tx['order_user'] . ' / <font size=1>' . $tx['executor'] . '</font>' : $tx['order_user'];
                $order_user = $tx['order_user'];
                $total += $tx['total'];
                
            ?>
            <tr <?php echo $bg_css?>>
                    <td style="text-align: center;">
                            <?php if ($tx['selected']){ ?>
                            <input type="checkbox" name="selected[]" value="<?php echo $tx['txid']; ?>" checked="checked" style="margin-bottom:6px;" />
                            <?php } else { ?>
                            <input type="checkbox" name="selected[]" value="<?php echo $tx['txid']; ?>" style="margin-bottom:6px;" />
                            <?php } ?>
                            <input type="hidden" name="store_id" value="<?php echo $tx['store_id']; ?>" />
                    </td>
                    <td>
                            <!--a href="<?php echo $tx['action'][0]['href']; ?>"-->
                            <a href="/sales/order&mode=show&txid=<?php echo $tx['txid']; ?>">
                            <?php echo $tx['txid']; ?>
                            </a>
                    </td>
                    <td class="left list_store"><?php echo $tx['store_name']; ?></td>
                    <td><?php echo $tx['order_date']; ?></td>
                    <td><?php echo $tx['total']; ?></td>
                    <td style='text-align:center;'>
                            <?php
                    if ( 0 != $tx['invoice_no']) {
                          ?>
                            <?php echo $tx['invoice_no']; ?>
                            <?php
                    } else {
                          ?>
                            <a href="/sales/order&invoice=true&txid=<?php echo $tx['txid']; ?>" class='btn btn-warning'>Issue</a>
                            <?php
                    }
                          ?>
                    </td>
                    <!--td><?php echo $tx['balance']; ?></td-->
                    <td><?php echo $order_user ?></td>
                    <!--td><input type="checkbox" name='shipped'></td-->
                    <td>
                            <?php if (0 != $tx['invoice_no']) { ?>
                                <a href="sales/order&invoice=true&txid=<?php echo $tx['txid']; ?>" class='btn'>Edit</a>
                            <?php } else { ?>
                                <a href='<?php echo $tx['action'][0]['href']; ?>' class='btn'>Edit</a>
                            <?php } ?>
                    </td>
            </tr>
            <?php } ?>
            <?php } else { ?><tr><td class="center" colspan="11">No Result</td></tr><?php } ?>
            </tbody>
        </table>
    </form>
    <div id='show_total' style='float:right;padding-right:20px;'>
        <?php if ( isset($total) ){  ?>
        Total : <?php echo $total ?> ( <?php echo count($txs) ?> )
        <?php } ?>
    </div>

    <div class="pagination"><?php echo $pagination; ?></div>
</div>
<style>
#ship_detail{
    position:absolute;
    top:200px;
    left:200px;
    width:400px;
    visibility:hidden;
    background-color:white;
    border:1px dotted blue;
    padding:10px;
}
</style>
<div id='ship_detail'>
    <form action="/sales/" method="post" enctype="multipart/form-data">
    <table>
        <tr>
            <td class=label>SHIP</td>
            <td class=context>
                    <input type='hidden' name=txidList value=''>
                    <select name=method[]>
                        <option value='truck' selected>truck</option>
                        <option value='ups'>ups</option>
                        <option value='cod'>cod</option>
                        <option value='visit'>visit</option>
                        <option value='etc'>etc</option>
                    </select>
            </td>
            <td class=label>DATE</td>
            <td class=context>
            <   input type=text name=ship_date[] value='<?php echo date("Y-m-d"); ?>' style='width:40%' /></td></tr>
        <tr>
            <td class=label>LIFT</td><td class=context>
                    <input type=text name=lift[] value='' size=5 /></td>
            <td class=label>COD</td><td class=context>
                    <input type=text name=cod[] value='' size=5 />
                    <p class=plus style='float:right;margin:0px;margin-right:2px;' />
                    <input type='hidden' name='ship_user[]' value='<?php echo $this->user->getUserName(); ?>' /></td>
        </tr>
        <tr>
            <td colspan=4><textarea name='ship_comment[]' style='width:300px;height:100px;'/>
            </textarea></td></tr>
        <tr>
            <td colspan=4>
                    <input type=submit value='Confirm' />
                    <input type=button value='Close' onclick="$('#ship_detail').css('visibility','hidden');"/>
            </td>
        </tr>
    </table>
    </form>
</div>
<style>
#detail{
    position : absolute;
    top: 100px;
    left: 100px;
    visibility:hidden;
    border: 1px dotted green;
    z-index:2;
}
</style>
<div id='detail'></div>
<!--iframe id='order_detail' name='order_detail' src='' width=500px height=400px></iframe-->
<script type="text/javascript">
$(document).ready(function(){
    // batch_ship
        $('#batch_ship').click(function(){
    // set txidList for batch process
    var $ele_checkbox = $('form tbody input:checkbox:checked'),
        $len = $ele_checkbox.length,
        $txidList = [];
        $ele_checkbox.each(function(idx){
        $txidList.push($(this).val());
    });
    // show ship method and others
        if ($txidList.length > 0){
        $('#ship_detail').find('input[name=txidList]').val($txidList);
        $('#ship_detail').css('visibility','visible');
    }
    });
            $('#batch_print').click(function(){
    // set txidList for batch process
    var $ele_checkbox = $('form tbody input:checkbox:checked'),
        $len = $ele_checkbox.length,
        $txidList = [];
        $ele_checkbox.each(function(idx){
      /***** todo. populate new iframe and print that , besso 201105 
        frame = 'frame' + idx;
        $html = "<iframe id='" + frame + "' name='" + frame + "' src='' width=1px height=1px></iframe>";
        $('#content').append($html);

        // we must use relative path for iframe control , besso 201105 
        // http://huuah.com/jquery-and-iframe-manipulation/ sigh~
      var $ele_iframe = $('iframe#'+frame),
            $txid = $(this).val(),
            $param = '?txid='+$txid,
            $url='/sales/order/<?php echo $token; ?>' + $param,
            $name = 'name' + idx;
        $ele_iframe.attr('src',$url);
        *****/
      var winHdr = null;
      var $txid = $(this).val(),
            $param = '&txid='+$txid,
            $name = 'name' + idx,
            $url='/sales/order' + $param;
      winHdr = window.open($url,$name);
      winHdr.onload = function(){
        winHdr.print();
        winHdr.close();
    }
    });

    setTimeout(function(){
        for (var i=0; i<window.frames.length; i++){
        if (navigator.appName == "Microsoft Internet Explorer"){ 
            // todo. it cannot support WIN XP high over
            var PrintCommand = '<object ID="PrintCommandObject" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></object>';
          document.body.insertAdjacentHTML('beforeEnd', PrintCommand); 
          PrintCommandObject.ExecWB(6, -1); PrintCommandObject.outerHTML = ""; 
        } else {
          window.frames[i].focus();
          window.frames[i].print();
        }
    }
    },3000);
    // reset
        $txidList = [];
    });
            $.fn.filter = function(){

        var url = '/sales/list';
        var filter_txid = $('input[name=\'filter_txid\']').attr('value');
        if (filter_txid) url += '&filter_txid=' + encodeURIComponent(filter_txid);
        var filter_store_name = $('input[name=filter_store_name]').attr('value');
        if (filter_store_name) url += '&filter_store_name=' + encodeURIComponent(filter_store_name);
        var filter_order_date_from = $('input[name=\'filter_order_date_from\']').attr('value');
        if (filter_order_date_from)  url += '&filter_order_date_from=' + encodeURIComponent(filter_order_date_from);
        var filter_order_date_to = $('input[name=\'filter_order_date_to\']').attr('value');
        if (filter_order_date_to)  url += '&filter_order_date_to=' + encodeURIComponent(filter_order_date_to);
        /*
        var filter_total = $('input[name=\'filter_total\']').attr('value');
        if (filter_total)  url += '&filter_total=' + encodeURIComponent(filter_total);
        var filter_ship = $('select[name=\'filter_ship\']').attr('value');
        if (filter_ship) url += '&filter_ship=' + encodeURIComponent(filter_ship);
        var filter_payed = $('select[name=\'filter_payed\']').attr('value');
        if (filter_payed != '*') url += '&filter_payed=' + encodeURIComponent(filter_payed);
        var filter_order_user = $('select[name=filter_order_user]').attr('value');
        if (filter_order_user != '') url += '&filter_order_user=' + encodeURIComponent(filter_order_user);
        var filter_approve_status = $('#filter_approve_status').attr('value');
        if (filter_approve_status != '') url += '&filter_approve_status=' + encodeURIComponent(filter_approve_status);
        var filter_status = $('#filter_status').attr('value');
        if (filter_status != '') url += '&filter_status=' + encodeURIComponent(filter_status);
          */
        location = url;
    }
        $('button#search-order').bind('click', function(e){
        e.preventDefault();
        $.fn.filter();
    })
            $('.filter input').keydown(function(e){
        //e.preventDefault();
        if (e.keyCode == 13){
            $.fn.filter();
        }
    });
            $('input[name="shipped"]').bind('click', function(e) {
        var tgt = $(e.target),
            pnt = tgt.parents('tr'),
            txid = pnt.find('input[name="selected[]"]').val(),
            shipped = tgt.val();
        debugger;
        return false;
        $.ajax({
            type: 'get',
            url: 'sales/list/updateShippedYN',
            dataType:'html',
            data:'txid=' + txid + '&shipped_yn=' + shipped,
            success:function(html){
                p = tgt.position();
                imgCss = {
                    'visibility':'visible',
                    'width':'150px',
                    'height':'20px',
                    'top':$p.top-30,
                    'left':$p.left-30,
                    'background-color':'black',
                    'color':'white',
                    'text-align':'center'
                    }
                $('#detail').css(imgCss);
                $('#detail').html('success');
                setTimeout(function() {
                    $('#detail').hide();
                    }, 2000);
            }
        });
    });

    /* TODO Store update later - 201406
        $('#form').click(function(event){
        var $tgt = $(event.target);
        if ($tgt.is('td.list_store')){
            var $pnt = $tgt.parents('tr'),
                $ele_chkbox = $pnt.find('input[name=store_id]'),
                $store_id = $ele_chkbox.val();
            $.ajax({
              type:'get',
              url:'/store/list/callUpdatePannel',
              dataType:'html',
              data:'token=<?php echo $token; ?>&store_id=' + $store_id,
              success:function(html){
                $('#detail').css('visibility','visible');
                $('#detail').html(html);
                //$('#detail').draggable(); 
            }
            });
        }
    });
      */

    // date picker binding
        $('#form').bind('focusin',function(event){
    var $tgt = $(event.target);
        if ($tgt.is('input.date_pick')){
        //$(".date-pick").datePicker({startDate:'01/01/1996'});
        $(".date_pick").datepicker({
        clickInput:true,
        createButton:false,
        startDate:'2000-01-01'
    });
    }
    });
            $('#update_notice').bind('keydown',function(e){
        $tgt = $(e.target);
        if ( e.keyCode == 13 ){
        $notice = $(this).val();
        $.ajax({
        type:'get',
        url:'/sales/list/updateNotice',
        dataType:'html',
        data:'notice=' + $notice,
        success:function(html){
            $p = $tgt.position();
            $imgCss = {
            'visibility':'visible',
            'width':'150px',
            'height':'20px',
            'top':$p.top-10,
            'left':$p.left-30,
            'background-color':'black',
            'color':'white',
            'text-align':'center'
            }
            $('#detail').css($imgCss);
            $('#detail').html('success');
        }
    });
    }
    });
            $('a.export').bind('click',function(e){
        e.preventDefault();
        var $ele_checkbox = $('form tbody input:checkbox:checked'),
            $len = $ele_checkbox.length,
            txidList = [];
        $ele_checkbox.each(function(idx){
            txidList.push("'" + $(this).val() + "'");
        });
        if (txidList.length == 0){
            alert('Select Transactions, Huh?');
            return false;
        }
        
        var txids = txidList.join(',');
        
        location.href = '/sales/list/export&txids=' + txids;
        /*
        $.ajax({
            type:'get',
            url:'/sales/list/export',
            dataType:'html',
            data:'txids=' + txids,
            success:function(res){
            }
        });
          */
    });
});
</script>
<?php echo $footer; ?>

