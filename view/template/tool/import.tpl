<?php header("Content-Type: text/html; charset=UTF-8") ?>
<?php echo $header; ?>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<?php if ($success) { ?>
<div class="success"><?php echo $success; ?></div>
<?php } ?>

<div class="box">
    <div class="heading">
    <h1 style="background-image: url('view/image/backup.png');">CSV Import</h1>
    </div>
    <div class="content">
        
        <pre>
            You need to keep the defined format, <span style="color:red">Download sample csv first at below!!</span>
            Don't change file name and every column names in CSV
        </pre>

        <br/></br>

        <form action="<?php echo $csv_import; ?>" method="post" enctype="multipart/form-data" id="csv_product_import">

            <table class="form">
                <tr>
                    <td>
                        <a href="/report/csv/export?q=products" style="color:red;" class="csv_export">
                        Download existing products</a></td>
                    <td>
                        <a href="data/csv_samples/products.csv">
                        Download products sample</a></td>
                    <td colspan="1"></td>
                </tr>
                <tr>
                    <td>Import Products</td>
                    <td>
                        <input type="hidden" name="table" value="products">
                        <input type="file" name="csv_import" />
                    </td>
                    <td>
                        <a onclick="$('#csv_product_import').submit();" class="btn">Import</a>
                    </td>
                </tr>
            </table>
        
        </form><br/></br>

        <form action="<?php echo $csv_import; ?>" method="post" enctype="multipart/form-data" id="csv_account_import">

            <table class="form">
                <tr>
                    <td>
                        <a href="/report/csv/export?q=stores" style="color:red;" class="csv_export">
                        Download existing clients</a></td>
                    <td>
                        <a href="data/csv_samples/stores.csv">
                        Download products sample</a></td>
                    <td colspan="1"></td>
                </tr>
                <tr>
                    <td>Import Stores</td>
                    <td>
                        <input type="hidden" name="table" value="stores">
                        <input type="file" name="csv_import" />
                    </td>
                    <td>
                        <a onclick="$('#csv_account_import').submit();" class="btn">Import</a>
                    </td>
                </tr>
            </table>
        
        </form><br/></br>

    </div>
</div>
<script type="text/javascript"><!--

//--></script>
<?php echo $footer; ?>
