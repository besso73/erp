<?php echo $header; ?>
<div class="box">

    <div class="heading">
        <h1 style="background-image: url('view/image/report.png');">Export CSV</h1>
    </div>
    <?php
        $today = date("Ymd"); 
        $tomonth = date("Ym");
    ?>
    <div class="content">
    <style>
        h3 { margin-top:0;padding-top: 5px; }
        li { list-style : none; }
        input.q {
            width:800px;height:30px;text-align:left;padding-left:20px;margin-bottom:10px;
        }
    </style>

    <h3> Category & Product </h3>
    <ul>

        <li>
            <input type=button class='q' name='categories' value="Get All categories">
        </li>

        <li>
            <input type=button class='q' name='price_base' value="Price information of Products">
        </li>

        <li>
            <input type=button class='q' name='product_base' value="Basic All Product Information">
        </li>

    </ul>

    <h3> Sales </h3>
    <ul>
        
        <li>
            <input type=button class='q' name='today_orders' value="Today orders">
        </li>

        <li>
            <input type=button class='q' name='yesterday_orders' value="Yesterday orders">
        </li>

        <li>
            <input type=button class='q' name='this_month' value="This month orders">
        </li>

        <li>
            <input type=button class='q' name='last_month' value="Last month orders">
        </li>

    </ul>


    <h3> Account </h3>
    <ul>
        <li>
            <input type=button class='q' name='accounts' value="Get All Accounts">
        </li>
    </ul>
    </div>
</div>
<script type="text/javascript"><!--
$(document).ready(function() {
    $('input.q').bind('click',function(e) {
        var q = $(this).attr('name');
        var url = '';
        url = '/report/csv/export&q=' + q;
        window.open(url);
    });
});
//--></script>
<?php echo $footer; ?>
