<?php echo $header; ?>
<div class="box">
  
    <div class="heading">
    <h1>Product Statistics</h1>
    <h1 style='float:right;'>
        <a href="/common/home">Basic</a>
      &nbsp;
        <a href="/report/account">Account</a>
    </h1>
    </div>
    <div class="content">
    <style>
        //calendar{
        width:930px;
    }
      .calendar{
        float:left;
    }
        //calendar_past .ui-widget-header{
        background: url("images/ui-bg_gloss-wave_35_f6a828_500x100.png") repeat-x scroll 50% 50% #999999;
    }
        //fromto{
        margin-top:200px;
    }
        //fromto input{
        height:24px; width:150px;
        font-size:20px;
        background-color:#e5e5e5;
    }
    </style>
    <div id='calendar'>
        <!--div id='calendar_past' class='calendar' style='margin-right:10px;'></div-->
        <div id='calendar_this' class='calendar'></div>
    </div>
    <div id='fromto'>
        <button id='reset'> Reset </button>
        <input type=text id='from' /> - <input type=text id='to' />
        <button id='search'> Search </button>
        <button id='search_today'> Today </button>
        <button id='search_mdiff'> Diff from Last Month </button>
        <button id='search_ydiff'> Diff from Last Year </button>
        <!--span style='font-size:14px;color:red;'> * Drag the area that you like </span-->
    </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
  
    /*
    // week number
  Date.prototype.getWeek = function(){
    var onejan = new Date(this.getFullYear(),0,1);
    var week = Math.ceil((((this - onejan) / 86400000) + onejan.getDay()+1)/7);
        return week
    }
  */

    /*
    $( "#calendar_past" ).datepicker({
    dateFormat:'mm-dd-yy',
    numberOfMonths:2,
    defaultDate:'02-01-2011',
    onSelect:function(dateText){
        $('#fromto #from').val(dateText);
    }
    });
  */

    $( "#calendar_this" ).datepicker({
    dateFormat:'mm-dd-yy',
    numberOfMonths:4,
    defaultDate:'<?php echo $baseDate ?>',
    onSelect:function(dateText,inst){
        //console.log('onSelect');
        //http://stackoverflow.com/questions/6056287/jquery-ui-datepicker-prevent-refresh-onselect
      inst.inline = false;  // to prevent reload
        $from = $('#from').val();
        $to = $('#to').val();
        if ($from == ''){
        $('#from').val(dateText);
        } else {
        if ( $from > dateText ){
          alert('Please Choose Correct Date!!');
        } else {
            $('#to').val(dateText);
        }
    }
    }
    });

    $('#reset').click(function(e){
        $('#from').val('');
        $('#to').val('');
    });

    $('#search').click(function(e){
        $.fn.search('search');
    });

    $('#search_mdiff').click(function(e){
        $.fn.search('mdiff');
    });

    $('#search_ydiff').click(function(e){
        $.fn.search('ydiff');
    });

    $('#search_today').click(function(e){
        $.fn.search('today');
    });

  var $idx = 0;
  var now = new Date(),
      curr_hour = now.getHours(),
      curr_week = now.getDay();
  if ( curr_hour < 17 ){
        if ( curr_week == 1 ){
      now.addDays(-3);
        } else {
      now.addDays(-1);
    }
    }
  var curr_date = now.getUTCDate();
  var curr_month = now.getUTCMonth();
  var curr_year = now.getFullYear();
  curr_date = (curr_date < 10) ? '0' + curr_date : curr_date ;
  curr_month = curr_month + 1;
  curr_month = (curr_month < 10) ? '0' + curr_month : curr_month ;
  var base_date = curr_month + '-' + curr_date + '-' + curr_year;

    $.fn.search = function($id){
    //console.log($id);
        $from = $('#from').val();
        $to = ( $('#to').val() == '' ) ? $from : $('#to').val();
        if ( $from == $to )  $id = 'today';
        if ( $id == 'today' ){
        // pop with system date configuration
        $title = 'Report' + $idx;
        $from = $('#from').val();
        if ( !$from ){
        $from = base_date;
    }
        $to = $from;

        //console.log($from); console.log($to);
        $.fn.popWindow($id,$title,$from,$to);
        $idx++;
    }else if ( $id == 'mdiff'){
        // set last month
        $yfrom = $from.substring(6,10);
        $mfrom = $from.substring(0,2) - 1;
        $dfrom = $from.substring(3,5);
        $pfrom = new Date($yfrom,$mfrom,$dfrom);
        $pfrom = $pfrom.addMonths(-1);
        $yfrom = $pfrom.getFullYear();
        $mfrom = ( ($pfrom.getUTCMonth() + 1) < 10) ? '0' + ($pfrom.getUTCMonth() + 1) : ($pfrom.getUTCMonth() + 1) ;
        $dfrom = ($pfrom.getUTCDate() < 10) ? '0' + $pfrom.getUTCDate() : $pfrom.getUTCDate() ;
        $pfrom = $mfrom + '-' + $dfrom + '-' + $yfrom;

        //console.log($yfrom);
        //console.log($mfrom);
        //console.log($dfrom);
        //console.log($pfrom);
      
        $yto = $to.substring(6,10);
        $mto = $to.substring(0,2) - 1;
        $dto = $to.substring(3,5);
        $pto = new Date($yto,$mto,$dto);
        //console.log($pto);
        $pto = $pto.addMonths(-1);
        //console.log($pto);
        $yto = $pto.getFullYear();
        $mto = ( ($pto.getUTCMonth() + 1) < 10) ? '0' + ($pto.getUTCMonth() + 1) : ($pto.getUTCMonth() + 1) ;
        $dto = ($pto.getUTCDate() < 10) ? '0' + $pto.getUTCDate() : $pto.getUTCDate() ;
        $pto = $mto + '-' + $dto + '-' + $yto;
      
        //return false;
                if ( $pfrom ){
        $title = 'Report' + $idx;
        $.fn.popWindow($id,$title,$pfrom,$pto);
        $idx++;
        } else {
        alert('Please Choose Date');
    }
    
        if ( $from ){
        $title = 'Report' + $idx;
        $.fn.popWindow($id,$title,$from,$to);
        $idx++;
        } else {
        alert('Please Choose Date');
    }

      
    }else if ( $id == 'ydiff'){
        // set last month
        $yfrom = $from.substring(6,10);
        $mfrom = $from.substring(0,2) -1 ;
        $dfrom = $from.substring(3,5);
        $pfrom = new Date($yfrom,$mfrom,$dfrom);
        $pfrom = $pfrom.addYears(-1);
        $yfrom = $pfrom.getFullYear();
        $mfrom = ( ($pfrom.getUTCMonth() + 1) < 10) ? '0' + ($pfrom.getUTCMonth() + 1) : ($pfrom.getUTCMonth() + 1) ;
        $dfrom = ($pfrom.getUTCDate() < 10) ? '0' + $pfrom.getUTCDate() : $pfrom.getUTCDate() ;
        $pfrom = $mfrom + '-' + $dfrom + '-' + $yfrom;

        //console.log($yfrom);
        //console.log($mfrom);
        //console.log($dfrom);
        //console.log($pfrom);
      
        $yto = $to.substring(6,10);
        $mto = $to.substring(0,2) -1 ;
        $dto = $to.substring(3,5);
        $pto = new Date($yto,$mto,$dto);
        $pto = $pto.addYears(-1);
        $yto = $pto.getFullYear();
        $mto = ( ($pto.getUTCMonth() + 1) < 10) ? '0' + ($pto.getUTCMonth() + 1) : ($pto.getUTCMonth() + 1) ;
        $dto = ($pto.getUTCDate() < 10) ? '0' + $pto.getUTCDate() : $pto.getUTCDate() ;
        $pto = $mto + '-' + $dto + '-' + $yto;
        if ( $pfrom ){
        $title = 'Report' + $idx;
        $.fn.popWindow($id,$title,$pfrom,$pto);
        $idx++;
        } else {
        alert('Please Choose Date');
    }
    
        if ( $from ){
        $title = 'Report' + $idx;
        $.fn.popWindow($id,$title,$from,$to);
        $idx++;
        } else {
        alert('Please Choose Date');
    }
    
        } else {
        if ( $from ){
        $title = 'Report' + $idx;
        $.fn.popWindow($id,$title,$from,$to);
        $idx++;
        } else {
        alert('Please Choose Date');
    }
    }
    
    // after search reset from, to
        $('#from,#to').val('');
    };

    // purely window open
    $.fn.popWindow =function($id,$title,$from,$to){
    //alert($title);
        $url='/report/product/search&id=' + $id + '&from=' + $from + '&to=' + $to;
    window.open($url,$title,'directories=0,titlebar=0,toolbar=0,location=0,status=0,menubar=0,scrollbars=yes,resizable=no,width=1200,height=1000');
    //window.open($url,$title,'width=1200,height=1000');
    //window.open($url,'Report','width=1100,height=900');
    }

    // today is default.
        //$.fn.search('today');

});
</script>
