<style>
@media print {
  body, td, th, input, select, textarea, option, optgroup{  font-size: 14px;  }
  #header,#menu,#footer{  display:none; }
  #content .left,.right{  display:none; }
  #content .heading div.buttons{  display:none; }
  .np{  display:none; }
}
</style>
<?php
    $heading_title = 'Montly';
    $export = '';
    $lnk_action = '';
?>
<?php echo $header; ?>
<?php if ($error_warning){ ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<?php if ($success){ ?>
<div class="success"><?php echo $success; ?></div>
<?php } ?>
<style>
.box{ z-index:10; }
.content .name_in_list{ color:purple; cursor:pointer; }
#detail{
  position:absolute;  top:100px;  left:100px;
  visibility:hidden;  border:1px dotted green;  z-index:2;
}
#content{ width:900px;  }
.box .content{  border:none;  }
</style>

<div class="box">
            <div class="heading">
    <h1><?php echo $heading_title; ?></h1>
    <h1 style='float:right;'>
        <a href="/common/home">Basic</a>
      &nbsp;
        <a href="/report/account">Account</a>
    </h1>
    </div>

    <div class="content">
    <form action="<?php echo $lnk_action; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="table">
        <tbody>
            <!-- it's only for page module , besso-201103 -->
            <tr class="filter np">
            <td class="center" colspan='2' style='width:120px'>
                <div>
                    <input type="text" class='date_pick' name="filter_from" value="<?php echo $filter_from; ?>" style='width:70px;' /> -
                    <input type="text" class='date_pick' name="filter_to" value="<?php echo $filter_to; ?>" style='width:70px;' />
                </div>
                <div>
                    <a href='<?php echo $lnk_pmonth ?>'>
                    <?php echo $pmonth_label ?>
                    </a>&nbsp;
                    <a href='<?php echo $lnk_tmonth ?>'>
                    <?php echo $tmonth_label ?>
                    </a>&nbsp;
                    <a href='<?php echo $lnk_nmonth ?>'>
                    <?php echo $nmonth_label ?>
                    </a>
                </div>
            </td>
            <td colspan='8'>
                <a onclick='filter();' class="button btn_filter">
                    <span>Search</span>
                </a>
            </td>
            </tr>
            <tr style='height:20px'><td colspan='10'></td></tr>

            <!-- this month -->
            <!-- this month -->
            <!-- this month -->
            <?php if ( count($stat) > 0 ){ ?>
            <?php
            if ( isset($filter_from) ){
            $this_month = substr($filter_from,0,7);
            } else {
            $this_month = date("Y-m");
            }
          ?>
            <tr style='height:20px;'><td colspan='10'>
            <h2>Month Lookup : <?php echo $this_month ?></h2></td>
            </tr>
            <tr style='background-color:#e2e2e2;'>
            <td class="center" colspan=7>REP</td>
            <td class="center" colspan=7>SUM</td>
            </tr>
            <?php
            //$this->log->aPrint( $stat );
          foreach ($stat as $order_user => $row){
            $order_sum = 0;
          ?>
            <tr style='background-color:<?php echo $bg_td ?>'>
            <td class='center' colspan=7 style='text-align:left;'>
                <a class='prod_detail_month'><?php echo $order_user ?></a>
                <table id='<?php echo $group ?>' style='margin-left:40px;display:none;'>
                    <?php
                $i=0;
                  foreach($row as $k => $v){
                $bg = '';
                if ( $i < 3 )  $bg = 'style=background-color:skyblue';
                if ( $i > count($row) - 4 ) $bg = 'style=background-color:violet';
                $i++;
                    ?>
                    <tr <?php echo $bg ?>>
                        <td><?php echo $k ?></td>
                        <td><?php echo $v['qty'] ?></td>
                        <td><?php echo $v['total'] ?></td>
                        <td><?php echo $v['percent'] ?> %</td>
                    </tr>
                    <?php
                  $order_sum += $v['total'];
                    }
                    ?>
                    <div id='salesorder' style='float:right'></div>
                </table>
            </td>
            <td class='center'><?php echo $order_sum; ?></td>
            </tr>
            <?php
            } // end foreach 
          ?>
            <?php } else { ?>
            <tr>
            <td class="center" colspan="8">No Result</td>
            </tr>
            <?php } ?>
            <tr style='height:40px'><td colspan='10'></td></tr>
        </tbody>
        </table>
    </form>
    </div>
</div>
<!-- common detail div -->
<div id='detail' class='ui-widget-content'></div>

<script type="text/javascript">
$(document).ready(function(){
    $.fn.filter = function(){
        url = '/report/sales';
      var filter_from = $('input[name=\'filter_from\']').attr('value');
      if (filter_from)  url += '&filter_from=' + encodeURIComponent(filter_from);
      var filter_to = $('input[name=\'filter_to\']').attr('value');
      if (filter_to)  url += '&filter_to=' + encodeURIComponent(filter_to);
      location = url;
    }

    // date picker binding
    $('#form').bind('focusin',function(event){
    var $tgt = $(event.target);
        if ($tgt.is('input.date_pick')){
        //$(".date-pick").datePicker({startDate:'01/01/1996'});
        $(".date_pick").datepicker({
        clickInput:true,
        createButton:false,
        startDate:'2000-01-01'
    });
    }
    });
    $('.btn_filter').bind('click',function(e){
        $.fn.filter();
    });

    $('.prod_detail').bind('click',function(e){
        $tgt = $(e.target);
        $group = $tgt.text();
        $pnt = $tgt.parents('td');
        $display = $pnt.find('table').css('display');
        if ($display == 'none'){
        $pnt.find('table').css('display','block');
        $pnt.find('div').css('display','block');
        // add ajax
        $.ajax({
        type:'get',
        url:'/report/product/ordersales',
        dataType:'json',
        data:'filter_from=<?php echo $this_day ?>&filter_to=<?php echo $this_day ?>&group=' + encodeURIComponent($group),
        success:function(data){
            //obj = $.parseJSON(data);
            $.each(data,function(i,row){
            html = '<p>' + row['rep'] + ' : ' + row['qty'] + '</p>';
            $pnt.find('#salesorder').append(html);
            });
        }
    });
    }
        if ($display == 'block'){
        $pnt.find('table').css('display','none');
        $pnt.find('div').css('display','none')
      .html('');
    }
    });

    $('.prod_detail_month').bind('click',function(e){
        $tgt = $(e.target);
        $group = $tgt.text();
    //debugger;    console.log($group);
        $pnt = $tgt.parents('td');
        $display = $pnt.find('table').css('display');
        if ($display == 'none'){
        $pnt.find('table').css('display','block');
        $pnt.find('div').css('display','block');
        // add ajax
        $.ajax({
        type:'get',
        url:'/report/product/ordersales',
        dataType:'json',
        data:'filter_from=<?php echo $filter_from ?>&filter_to=<?php echo $filter_to ?>&group=' + encodeURIComponent($group),
        success:function(data){
            $.each(data,function(i,row){
            html = '<p>' + row['rep'] + ' : ' + row['qty'] + '</p>';
            $pnt.find('#salesorder').append(html);
            });
        }
    });
    }
        if ($display == 'block'){
        $pnt.find('table').css('display','none');
        $pnt.find('div').css('display','none')
      .html('');
    }
    });
});
</script>
<?php echo $footer; ?>
