<?php echo $header; ?>
<?php if ($error_warning){ ?><div class="warning"><?php echo $error_warning; ?></div><?php } ?>
<?php if ($success){ ?><div class="success"><?php echo $success; ?></div><?php } ?>
<div class="box">
            <div class="heading">
    <h1 style="background-image: url('view/image/product.png');">
        <?php echo $heading_title; ?>
    </h1>
    <div class="buttons">
        <a class="button btn_search"><span>Search</span></a>
        <!--a onclick="$('#form').attr('action', '<?php echo $copy; ?>'); $('#form').submit();" class="button"><span><?php echo $button_copy; ?></span></a-->
        <!--a onclick="$('form').submit();" class="button"><span><?php echo $button_delete; ?></span></a-->
    </div>
    </div>
    <div class="content">
    <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="table">
        <thead>
            <tr>
            <td class="left"><?php if ($sort == 'name'){ ?>
                <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>">Name</a>
                <?php } else { ?>
                <a href="<?php echo $sort_name; ?>">Name</a>
                <?php } ?></td>
            <td class='center'>Model</td>
            <td class='center'>Update Date</td>
            <td class='center'>Rep</td>
            <td class='center'>Change</td>
            <td class='center'>Inventory</td>
            </tr>
        </thead>
        <tbody>
            <tr class="filter">
            <td class="center"></td>
            <td><input type="text" name="filter_model" value="<?php echo $filter_model; ?>" style='width:70px;' /></td>
            <td class='center' colspan=2>
                <input type="text" class='date_pick' name="filter_from" value="<?php echo $filter_from; ?>" style='width:70px;' />
              -
                <input type="text" class='date_pick' name="filter_to" value="<?php echo $filter_to; ?>" style='width:70px;' />
            </td>
            <td colspan='4'></td>
            </tr>
            <?php 
            if (isset($phistory)){
          ?>
            <?php
            $tempDiff = '';
            $idx = 0;
          foreach ($phistory as $history){
            if ( $tempDiff != substr($history['up_date'],0,10) ){
                if ( $idx%2 == 1 ){
                $bgcolor = '#e2e2e2';
            } else {
                $bgcolor = 'white';
            }
                $tempDiff = substr($history['up_date'],0,10);
                $idx++;
            }
          ?>
            <tr style='background-color:<?php echo $bgcolor; ?>;'>
            <td class="left"><?php echo $history['name']; ?></td>
            <td class="left">
                <a href="/product/price&filter_pid=<?php echo $history['final']; ?>" target="new">
                <?php echo $history['model']; ?>
                </a>
            </td>
            <td class="left"><?php echo $history['up_date']; ?></td>
            <td class='center'><?php echo $history['rep']; ?></td>
            <td class='center'><?php echo $history['diff']; ?></td>
            <td class='center'><?php echo $history['final']; ?></td>
            </tr>
            <?php
            }
          ?>
            <?php } else { ?>
            <tr>
            <td class="center" colspan="9"><?php echo $text_no_results; ?></td>
            </tr>
            <?php } ?>
        </tbody>
        </table>
    </form>
    <div class="pagination"><?php echo $pagination; ?></div>
    </div>
</div>

<!-- common detail div -->
<style>
#detail{
  position : absolute;
  top: 100px;
  left: 200px;
  visibility:hidden;
  border: 1px dotted green;
  background-color:white;
  z-index:99;
}
</style>
<div id='detail'></div>

<script>
$(document).ready(function(){
    $('.btn_search').bind('click',function(){
        $.fn.filter();
    });

    $.fn.filter = function(){
        url = '/material/history';
      var filter_code = $('input[name=\'filter_code\']').attr('value');
      if (filter_code)  url += '&filter_code=' + encodeURIComponent(filter_code);
      var filter_name = $('input[name=\'filter_name\']').attr('value');
      if (filter_name) url += '&filter_name=' + encodeURIComponent(filter_name);
      var filter_product = $('input[name=\'filter_product\']').attr('value');
      if (filter_product) url += '&filter_product=' + encodeURIComponent(filter_product);
      var filter_cat = $('select[name=\'filter_cat\']').attr('value');
      if (filter_cat){
          url += '&filter_cat=' + encodeURIComponent(filter_cat);
    }
      var filter_history_from = $('input[name=\'filter_history_from\']').attr('value');
      if (filter_history_from != '*'){
          url += '&filter_history_from=' + encodeURIComponent(filter_history_from);
    }
      var filter_history_to = $('input[name=\'filter_history_to\']').attr('value');
      if (filter_history_to != '*'){
          url += '&filter_history_to=' + encodeURIComponent(filter_history_to);
    }
      location = url;
    }

    $('.filter input').keydown(function(e){
      if (e.keyCode == 13){
        $.fn.filter();
    }
    });

    // date picker binding
    $('#form').bind('focusin',function(event){
    var $tgt = $(event.target);
        if ($tgt.is('input.date_pick')){
        //$(".date-pick").datePicker({startDate:'01/01/1996'});
        $(".date_pick").datepicker({
        clickInput:true,
        createButton:false,
        startDate:'2000-01-01'
    });
    }
    });

});
</script>
<?php echo $footer; ?>
