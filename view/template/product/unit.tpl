<?php echo $header; ?>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<style>

.box > .content{
  height: 600px;
}

table {
	font-size: 1em;
}

.ui-draggable, .ui-droppable {
	background-position: top;
}

#list { 
  float:left;
  list-style-type: none; margin: 0; padding: 0; 
  width: 400px; 
}
#list li { 
  margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; 
  font-size: 1.4em; height: 18px; 
  width: 400px;
}
#list li span { position: absolute; margin-left: -1.3em; }

#unit { 
  float: right;
  list-style-type: none; margin: 0; padding: 0; 
  width: 450px; 
  min-height: 300px;
  background-color: #E9E9E9;
}
#unit li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em; height: 18px; 
  width: 400px;
}
</style>
<!--script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script-->


<div class="box">
  <div class="heading">
    
    <div style="float:left">
      <select name="category" id="category" style="margin-top:5px; margin-left:20px;width:200px;">
        <option value="0">Choose Category</option>
        <?php
        foreach ( $categories as $cat ) {
          echo '<option value="' . $cat['category_id'] . '">' . $cat['name'] . '</option>';
        }
        ?>
      </select>
    </div>
    <div style="float:left">
      <button id="save-unit" class="btn btn-primary" style="margin-top:5px; margin-left:200px;width:100px;">Save</button>
    </div>
    <div style="float:right;">
      <select name="subcategory" id="subcategory" style="margin-top:5px; margin-right:20px;width:200px;">
        <option value="0">Choose subCategory</option>
        <?php
        foreach ( $categories as $cat ) {
          echo '<option value="' . $cat['category_id'] . '">' . $cat['name'] . '</option>';
        }
        ?>
      </select>
    
    </div>
  </div>
  <div class="content">
  </div>
</div>



<script>
$( function() {

  $( "select#category" ).on("change", function(e){
    var cid = $(this).val();

    if ( cid == '0' ) {
      $('select#subcategory').find('option').remove().end().hide();
        return false;
      }

      $.ajax({
        url : 'product/unit/getSubCategories&cid=' + cid,
        method : 'GET',
        dateType : 'JSON',
        success: function(data) {
          $('select#subcategory').find('option').remove().end();
          if ( data != '' ) {
                var ele = jQuery.parseJSON(data);
                
                //$('select#subcategory').append($("<option value='0'>Choose Sub Category</option>");
                
                $(ele).each(function( idx, o ) {
                    $('select#subcategory')
                    .append($("<option></option>")
                    .attr("value",o.category_id)
                    .text(o.name)).
                    show();
                }); 
          }
        }
      });
      
      $.ajax({
        url : 'product/unit/displayUnits?category_id=' + cid,
        method : 'GET',
        dateType : 'HTML',
        success: function(html) {
            if ( html != '' ) {
                $('div.content').append(html);
            }
        }
      });
  });

  $('div.content').on('click',function(e) {
    $( "#list" ).sortable();
    $( "#list" ).disableSelection();
    $( "#list li" ).draggable({
      //revert: "valid",
      helper: "clone",
      appendTo: "#unit",
      connectToSortable: "#unit li"
    });
  
    $( "#unit" )
    .droppable({
      activeClass: "ui-state-default",
        hoverClass: "ui-state-hover",
        accept: ":not(.ui-sortable-helper)",
        drop: function (event, ui) {
          if ( $('select#subcategory').val() == 0 ) {
            alert ( 'choose sub category first');
            return false;
          }
          $(ui.draggable).clone().appendTo(this);
        },
         accept: function(draggable) {
          return $(this).find("#" + draggable.attr("id")).length == 0;
         }
    })
    .sortable({
      items: "li",
      connectWith: "li",
      sort: function () {
        $(this).removeClass("ui-state-default");
      }
    });
  });
  
});
</script>


<?php echo $footer; ?>
