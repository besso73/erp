<?php echo $header; ?>
<style>
#tab_transfer,#tab_history{
  display:none;
}
#form .form{
  width:400px;
}
table.form tr td:first-child {
  width: 100px;
  background-color:white;
}

td.label {
    background-color: #E2E2E2;
    line-height: 32px;
    width: 200px;
}

td.content {
    text-align: left;
}

.box > .heading {
    width: 600px;
}

.box > .content {
    width: 600px;
}
</style>
<div class="box" style='background-color:white;'>
    <div class="heading">
        <h1>Insert Barcode</h1>
        <!--div class="buttons">
            <a class="btn btn-primary save">Save</a>
        </div-->
    </div>
    <div class="content">
        <h3 id='result' style='display:none;color:red;text-align:center;'></h3>

        <div id='barcode-atc-container'>
            <div id='barcode-control-container'>
                    <button class="btn" id="product-insert">New Product</button>
            </div>
            <input id='barcode-atc' type='text' class='input-xlarge' \>
        </div>

        <div id='barcode-detail-panel'>
            <form method='post' action='product/price/updateBarcode' id='barcode-form'>
            <table>
                    <tr>
                            <td>Item.No</td>
                            <td>Description</td>
                            <td>Unit.Price</td>
                            <td>Inventory</td>
                    </tr>
                    <tr>
                            <td>
                                <input type='text' name='model' class='input-mini'/>
                                <input type='hidden' name='product_id'/>
                                <input type='hidden' name='mode' value='update' />
                            </td>
                            <td>
                                <input type='text' name='name' class='input-large'/>
                            </td>
                            <td>
                                <input type='number' name='rt_price' value='0' class='input-mini'/>
                            </td>
                            <td>
                                <input type='number' name='quantity' value='0' class='input-mini'/>
                            </td>
                    </tr>
                    <tr>
                            <td colspan='3'>
                                <input type='number' name='barcode' class='input-xlarge' placeholder='barcode'/>
                            </td>
                            <td>
                                <input type='button' id='barcode-submit' class='input-mini btn' value='submit'/>
                            </td>
                    </tr>
            </table>
            </form>
        </div>

    </div>
</div>
<script type='text/javascript' src='js/main.js'></script>
<script type='text/javascript' src='js/barcode.js'></script>