<?php echo $header; ?>
<?php if ($error_warning){ ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<?php if ($success){ ?>
<div class="success"><?php echo $success; ?></div>
<?php } ?>
<div class="box">
            <div class="heading">
    <h1>Product</h1>
    <div class="buttons">

        <button class="btn btn-warning" id="flush-product-cache" style="display:none;" >Upload Products in Memory</button>
        <button class="btn btn-warning" id="product-insert">New Product</button>
        <a onclick="$('form').submit();" class="btn">Delete</a>
        <a href="#" class="btn">Search</a>

    </div>
    <!--div class="buttons">
        <?php
        if (isset($count)){
        //$export = 'export';
        $button_export = 'export';
      ?>
        total <?php echo $count; ?> =>
        <a onclick="location = '<?php echo $export; ?>'" class="button">
        <span><?php echo $button_export; ?></span></a>
        <?php
    }
      ?>
    </div-->
    </div>
    <div class="content">
    <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="table">
        <thead>
            <tr>
                <td width="1" style="text-align: center;">
                    <input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" />
                </td>
                <!--td class="center"></td-->
                <td class="center">
                    <?php if ($sort == 'p.name'){ ?>
                    <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                    <?php } ?>
                </td>
                <td class="center"><?php if ($sort == 'p.model'){ ?>
                    <a href="<?php echo $sort_model; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_model; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_model; ?>">Category Name</a>
                    <?php } ?></td>
                <td class="center">
                    <?php if ($sort == 'p.rt_price'){ ?>
                    <a href="<?php echo $sort_rt_price; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_rt_price; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_rt_price; ?>"><?php echo $column_rt_price; ?></a>
                    <?php } ?>
                </td>
                <td class="center">
                    <?php if ($sort == 'p.ws_price'){ ?>
                    <a href="<?php echo $sort_ws_price; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_ws_price; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_ws_price; ?>"><?php echo $column_ws_price; ?></a>
                    <?php } ?>
                </td>
                <td class="center">
                    <?php if ($sort == 'p.thres'){ ?>
                        <a href="<?php echo $sort_thres; ?>" class="<?php echo strtolower($order); ?>">Threshold</a>
                    <?php } else { ?>
                        <a href="<?php echo $sort_thres; ?>">Threshold</a>
                    <?php } ?>
                </td>
                <td class="center"><?php if ($sort == 'p.quantity'){ ?>
                    <a href="<?php echo $sort_quantity; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_quantity; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_quantity; ?>"><?php echo $column_quantity; ?></a>
                    <?php } ?>
                    +/-
                </td>
            </tr>
        </thead>
        <tbody>
            <tr class="filter">
            <td class='center'>
            </td>
            <td class='left'>
                    <input type="text" class="input-large" name="filter_name" value="<?php echo $filter_name; ?>" /></td>
            <td class='center'>
                <input type="text" name="filter_model" value="<?php echo $filter_model; ?>" class="input-small" />
                <!--input type="text" name="filter_model_to" value="<?php echo $filter_model_to; ?>" class="input-mini"  /-->
            </td>
            <td colspan='4'></td>
            </tr>
            <?php if (isset($products)){ ?>
            <?php foreach ($products as $product){ ?>
            <tr class='main_list'>
            <td style="text-align: center;"><?php if ($product['selected']){ ?>
                <input type="hidden" name="product_id" value="<?php echo $product['product_id']; ?>" />
                <input type="checkbox" name="selected[]" value="<?php echo $product['product_id']; ?>" checked="checked" />
                <?php } else { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $product['product_id']; ?>" />
                <?php } ?>
                <input type="hidden" name="product_id" value="<?php echo $product['product_id']; ?>" >
            </td>
            <!--td><img class='more' src="<?php echo $product['image']; ?>" alt="<?php echo $product['name']; ?>" style="margin:0px; padding:0px; border:1px solid #DDDDDD;cursor:pointer;" /></td-->
            <td class="left" style='width:200px;'><?php echo $product['name']; ?></td>
            
            <!--td class="center more" style='color:blue;cursor:pointer;'><?php echo $product['model']; ?></td-->
            
            <td class="center more" style='color:blue;cursor:pointer;'><?php echo $product['cname']; ?></td>
            
            
            <td class="center">
                <input type="number" name="rt_price" value="<?php echo $product['rt_price']; ?>" class="input-mini" />
            </td>
            <td class="center">
                <input type="number" name="ws_price" value="<?php echo $product['ws_price']; ?>" class="input-mini" />
            </td>
            <td class="center">
                <input type="number" name="thres" value="<?php echo $product['thres']; ?>" class="input-mini" />
            </td>
            <td class='center'>
                <input type="number" name="quantity" value="<?php echo $product['quantity']; ?>" class="input-small" readonly style='background-color:#e2e2e2;' />
                <input type="number" name="plus" class='plus input-xmini' value="0" />
            </td>
            </tr>
            <?php } ?>
            <?php } else { ?>
            <tr>
            <td class="center" colspan="9"><?php echo $text_no_results; ?></td>
            </tr>
            <?php } ?>
        </tbody>
        </table>
    </form>
    <div class="pagination"><?php echo $pagination; ?></div>
    </div>
</div>

<style>
#detail{
  position:absolute;  top:100px; left:200px;
  visibility:hidden;  border:1px dotted green;
  background-color:white; z-index:99;
}
</style>
<div id='detail'></div>

<script type="text/javascript">
<?php
/* TODO
 * If one is just used only that script, Define JS and CSS in the page
 * But it's shared, Do define the method in common javascript and css with proper naming
 * It's better to maintenance, duh
 * besso, 2014-03
 */
?>
$(document).ready(function(){
        $('tbody').bind('click',function(e){
        $tgt = $(e.target);
        if ($tgt.is('input')){
        $tgt.select();
    }
    });
    
        $('.filter').bind('keydown',function(e){
        if ('13' == e.keyCode){
        $tgt = $(e.target);
        if ($tgt.is('input')){
          filter();
        }
    }
    });
    
        $('.more').bind('click',function(e){
        e.preventDefault();
        var that = $(e.target),
            product_id = that.parents('tr').find('input[name="product_id"]').val();
        if (product_id == '') {
            alert('No product_id');
            return false;
    }
        var data = {
            mode: 'update',
            product_id: product_id
        }
        $.ajax({
            type:'get',
            url:'/product/price/callUpdatePannel',
            dataType:'html',
            data: data,
            success:function(html){
                $.fancybox(html);
            }
        });
    });
    
    $('.btn_insert').bind('click',function(event){
        $.ajax({
            type:'get',
            url:'/product/price/callUpdatePannel',
            dataType:'html',
            data:'token=<?php echo $token; ?>&ddl=insert',
            success:function(html){
                $('#detail').css('visibility','visible');
                $('#detail').html(html);
                //$('#detail').draggable(); 
            },
            fail:function(){
                //console.log('fail : no response from proxy');
            }
        });
    });
    
    // unbind handling to prevent duplicated event driven
    var handler = function(e){
        if ('13' == e.keyCode){
        $tgt = $(e.target);
        if ($tgt.is('input.plus')){
            $.fn.updatePackage($tgt);
        }
        
        if ($tgt.is("input['name=rt_price']")){
            var $pnt = $tgt.parents('tr'),
                $product_id = $pnt.find('input[name=product_id]').val(),
                $ws_price = $pnt.find('input[name=ws_price]').val(),
                $rt_price = $pnt.find('input[name=rt_price]').val();
            $.ajax({
            type:'get',
            url:'/product/price/updatePrice',
            dataType:'html',
            data:'token=<?php echo $token; ?>&product_id=' + $product_id + '&rt_price=' + $rt_price + '&ws_price=' + $ws_price,
            success:function(html){
                $p = $tgt.position();
                $imgCss = {
                'visibility':'visible',
                'width':'150px',
                'height':'20px',
                'top':$p.top-30,
                'left':$p.left-30,
                'background-color':'black',
                'color':'white',
                'text-align':'center'
            }
                $('#detail').css($imgCss);
                $('#detail').html('success');
            }
            });
        }
    
        // update threshold
        if ($tgt.is("input['name=thres']")){
            var $pnt = $tgt.parents('tr'),
                $product_id = $pnt.find('input[name=product_id]').val(),
                $thres = $pnt.find('input[name=thres]').val();
            $.ajax({
            type:'get',
            url:'/product/price/updateThres',
            dataType:'html',
            data:'token=<?php echo $token; ?>&product_id=' + $product_id + '&thres=' + $thres,
            success:function(html){
                $p = $tgt.position();
                $imgCss = {
                'visibility':'visible',
                'width':'150px','height':'20px',
                'top':$p.top-30,'left':$p.left-30,
                'background-color':'black',
                'color':'white','text-align':'center'
            }
                $('#detail').css($imgCss);
                $('#detail').html('success');
            }
            });
        }
    }
    }
        $('.main_list').bind('keydown',handler);

    //package is update automatically 
        $.fn.updatePackage = function($tgt){
        $product_id = $tgt.parents('tr').find('input[name=product_id]').val();
        $quantity_el = $tgt.parents('tr').find('input[name=quantity]');
      var $key = $tgt[0].name,
            $val = $tgt[0].value;
        if ('plus' == $key){
        $param = '&' + $key + '=' + $val + '&product_id=' + $product_id;
        $.ajax({
          type:'get',
          url:'/product/price/updatePackage',
          dataType:'html',
          data:$param,
          success:function(html){
            $p = $tgt.position();
            $imgCss = {
              'visibility':'visible', 'width':'150px',
              'height':'20px',  'top':$p.top-30,
              'left':$p.left-30,  'background-color':'black',
              'color':'white',  'text-align':'center'
            }
            $('#detail').css($imgCss);
            $('#detail').html('success : ' + $val );
            //$('#detail').draggable();
            $quantity_el.val( parseInt($val) + parseInt($quantity_el.val()) );
            $tgt.val('0');  $tgt.select();
            }
        });
    }
    }
})

    function filter(){
        url = '/product/price';
        
        var filter_name = $('input[name=\'filter_name\']').attr('value');
        //if (filter_name) url += '&filter_name=' + encodeURIComponent(filter_name);
        if (filter_name) url += '&filter_name=' + window.btoa(filter_name);
        
        var filter_model = $('input[name=\'filter_model\']').attr('value');
        if ( filter_model != '' ) {
            url += '&filter_model=' + encodeURIComponent(filter_model);
        }
        /***
        var filter_price = $('input[name=\'filter_price\']').attr('value');
        if (filter_price)  url += '&filter_price=' + encodeURIComponent(filter_price);
        var filter_oem = $('select[name=\'filter_oem\']').attr('value');
        if (filter_oem)  url += '&filter_oem=' + encodeURIComponent(filter_oem);
        var filter_quantity = $('input[name=\'filter_quantity\']').attr('value');
        if (filter_quantity) url += '&filter_quantity=' + encodeURIComponent(filter_quantity);
        var filter_cat = $('select[name=\'filter_cat\']').attr('value');
        if (filter_cat != '*') url += '&filter_cat=' + encodeURIComponent(filter_cat);
          ***/
        location = url;
    }
</script>

<?php echo $footer; ?>
