<?php echo $header; ?>
<?php if ($error_warning){ ?><div class="warning"><?php echo $error_warning; ?></div><?php } ?>
<?php if ($success){ ?><div class="success"><?php echo $success; ?></div><?php } ?>

<style>
.box{ z-index:10; }
.content .name_in_list{ color:purple; cursor:pointer; }
#detail{  position:absolute;  top:100px;  left:100px; visibility:hidden;  border:1px dotted green;  z-index:2;  }
td.accountno_in_list{
    padding-left: 20px;
}
td.name_in_list{
    width: 200px;
}
</style>

<div class="box">
    <div class="heading">
        <h1>Manage Company</h1>
        <div class="buttons">
            <?php if (count($total) > 0) { ?>
            <!--a onclick="#" class="btn btn-warning">Export</a-->
            <?php } ?>
            <a class="btn btn-warning insert-company-panel" href='<?php echo $lnk_insert; ?>' >Insert</a>
            <a id="delete-company" class="btn">Delete</a>
        </div>
    </div>
    <?php
        $filter_name = $filter_companytype = $filter_city = $filter_state = '';
        if (!$filter_status) $filter_status = '';
    ?>
    <div class="content">
        <form action="/company/list/delete" method="post" enctype="multipart/form-data" id="form-company">
        <table class="table">
            <thead>
            <tr>
                    <td width="1" style="text-align:center;">
                            <input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" style='margin-top:0;' />
                    </td>
                    <td class="left">Login Id</td>
                    <td class="left">
                            <?php if ($sort == 'name'){ ?>
                            <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>">Name</a>
                            <?php } else { ?>
                            <a href="<?php echo $sort_name; ?>">Name</a>
                            <?php } ?>
                    </td>
                    <td class="left">
                            <?php if ($sort == 'email'){ ?>
                            <a href="<?php echo $sort_email; ?>" class="<?php echo strtolower($order); ?>">Type</a>
                            <?php } else { ?>
                            <a href="<?php echo $sort_email; ?>">Email</a>
                            <?php } ?>
                    </td>
                    <td class="left">Phone</td>
                    <td class="left">
                            <?php if ($sort == 'status'){ ?>
                            <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>">Status</a>
                            <?php } else { ?>
                            <a href="<?php echo $sort_status; ?>">Status</a>
                            <?php } ?>
                    </td>
                    <td class="left">Added at</td>
                    <td class="right">Action</td>
            </tr>
            </thead>
            <tbody>
            <tr class="filter">
                    <td></td>
                    <td><input type="text" class="input-small" name="filter_id" value="" /></td>
                    <td><input type="text" class="input-small" name="filter_name" value="" /></td>
                    <td><input type="text" class="input-small" name="filter_email" value="" /></td>
                    <td><input type="text" class="input-small" name="filter_phone" value="" /></td>
                    <td>
                            <select class="input-small" name="filter_status">
                            <option value="1" <?php if ('1'==$filter_status) echo 'selected'; ?>>Active</option>
                            <option value="0" <?php if ('0'==$filter_status) echo 'selected'; ?>>InActive</option>
                            </select>
                    </td>
                    <td><input type="text" class="input-small" name="filter_date_added" value="" /></td>
                    <td align="right"><a class="btn btn_filter">Filter</a></td>
            </tr>
            <?php if ($list){ ?>
            <?php
            foreach ($list as $row){
                $bg_td = '';
                //if ( $row['status'] == '0' ) $bg_td = 'gray';
                //if ( $row['status'] == '9' ) $bg_td = '#e2e2e2';
            ?>
            <tr style='background-color:<?php echo $bg_td ?>'>
                    <td style="text-align: center;"><?php if ($row['selected']){ ?>
                            <input type="checkbox" class='id_in_list' name="selected[]" value="<?php echo $row['id']; ?>" checked="checked" style='margin-top:0;' />
                            <?php } else { ?>
                            <input type="checkbox" class='id_in_list' name="selected[]" value="<?php echo $row['id']; ?>" style='margin-top:0;' />
                            <?php } ?>
                            <input type='hidden' name='view' value='proxy' />
                            <input type='hidden' class='address1_in_list' name='address1' value='<?php echo $row['address1']; ?>' />
                            <input type='hidden' class='zipcode_in_list' name='zipcode' value='<?php echo $row['zipcode']; ?>' />
                            <input type='hidden' class='fax_in_list' name='fax' value='<?php echo $row['fax']; ?>' />
                    </td>
                    <td class='left accountno_in_list'><?php echo $row['companyid']; ?></td>
                    <td class='name_in_list'><?php echo $row['name']; ?></td>
                    <td class='center companytype_in_list'><?php echo $row['email']; ?></td>
                    <td class='center city_in_list'><?php echo $row['phone']; ?></td>
                    <td class='center state_in_list'><?php echo $row['status']; ?></td>
                    <td class='center salesrep_in_list'><?php echo $row['date_added']; ?></td>
                    <td class="center">
                            <a class='btn update-company-panel' data-id="<?php echo $row['id']; ?>" href="<?php echo $lnk_insert; ?>">Edit</a>
                    </td>
            </tr>
            <?php } // end foreach ?>
            <?php } else { ?><tr><td class="center" colspan="11">No Result</td></tr><?php } ?>
            </tbody>
        </table>
        </form>
        <div class="pagination"><?php echo $pagination; ?></div>
    </div>
</div>
<!-- common detail div -->
<div id='detail' class='ui-widget-content'></div>

<script type="text/javascript">

$(document).ready(function(){

    $('#delete-company').bind('click',function(){
        
        $('#form-company').submit();
    });


    $.fn.filter = function(){

        var url = '/company/list';

        var filter_name = $('input[name=\'filter_name\']').attr('value');
        if (filter_name) {
            url += '&filter_name=' + window.btoa(filter_name);
        }
        var filter_accountno = $('input[name=\'filter_accountno\']').attr('value');
        if (filter_accountno)  url += '&filter_accountno=' + encodeURIComponent(filter_accountno);
        var filter_companytype = $('select[name=\'filter_companytype\']').attr('value');
        if (filter_companytype != '')  url += '&filter_companytype=' + encodeURIComponent(filter_companytype);
        var filter_city = $('input[name=\'filter_city\']').attr('value');
        if (filter_city) url += '&filter_city=' + encodeURIComponent(filter_city);
        var filter_state = $('input[name=\'filter_state\']').attr('value');
        if (filter_state)  url += '&filter_state=' + encodeURIComponent(filter_state);
        var filter_phone1 = $('input[name=\'filter_phone1\']').attr('value');
        if (filter_phone1) url += '&filter_phone1=' + encodeURIComponent(filter_phone1);
        var filter_salesrep = $('input[name=\'filter_salesrep\']').attr('value');
        if (filter_salesrep) url += '&filter_salesrep=' + encodeURIComponent(filter_salesrep);
        var filter_balance = $('input[name=\'filter_balance\']').attr('value');
        if (filter_balance) url += '&filter_balance=' + encodeURIComponent(filter_balance);
        var filter_status = $('select[name=\'filter_status\']').attr('value');
        if (filter_status != '*')  url += '&filter_status=' + encodeURIComponent(filter_status);
        location = url;
    }
            $('.btn_filter').bind('click',function(e){  $.fn.filter(); });
            $('#form input').keydown(function(e){ if (e.keyCode == 13) $.fn.filter(); });
    
        $('.list').click(function(event){
        var $tgt = $(event.target);
        if ($tgt.is('a.edit>span')){
            var $pnt = $tgt.parents('tr'),
                $ele_chkbox = $pnt.find('.id_in_list'),
                $company_id = $ele_chkbox.val();
            $.ajax({
                type:'get', dataType:'html',
                url:'/company/list/callUpdatePannel',
                data:'token=<?php echo $token; ?>&company_id=' + $company_id,
                success:function(html){
                    $('#detail').css('visibility','visible');
                    $('#detail').html(html);
                    //$('#detail').draggable();
                    }
            });
        }
    }); // end of click event
            $.fn.printLabel = function($idlist){
        $param = '&idlist=' + $idlist,
        $url='/company/list/printLabel' + $param;
        window.open($url);
    }
});


</script>

<?php echo $footer; ?>
