<style>
#tab_transfer,#tab_history{
    display:none;
}
#form .form{
    width:400px;
}

table.form {
    margin-bottom:0;
}

table.form tr td:first-child {
    width: 100px;
    background-color:white;
}

table.form tr td:first-child {
    background-color: #E2E2E2;
    line-height: 32px;
    width: 100px;
}

td.content {
    text-align: left;
}

/*
#fancybox-content .box {
    margin-bottom: 0px;
}

#fancybox-content .box > .heading {
    width: 600px;
}

#fancybox-content .box > .content {
    width: 600px;
    min-height: 0;
}
*/

</style>
<div class="box" style='background-color:white;'>

    <div class="heading">
        <h1>Manage Comapny</h1>
        <div class="buttons">
            <a class="btn btn-success save-company">Save</a>
        </div>
    </div>
    <div class="content">
        <!--div id="tabs" class="htabs">
            <a tab="#tab_general">Base Info</a>
        </div-->
        <form action="company/list/update" id="updateForm">
        <div id="tab_general">
            <?php
            if ('update' == $mode){
                $id         = $item['id'];
                $companyid  = $item['companyid'];
                $name       = $item['name'];
                $email      = $item['email'];
                $phone      = $item['phone'];
                $status     = $item['status'];
                $date_added = $item['date_added'];
            } else {
                $id         = '';
                $companyid  = 'test';
                $name       = 'babo';
                $email      = 'besso@live.com';
                $phone      = '123456789';
                $status     = '1';
                $date_added = ''; 
            }
            ?>
            <table class="form" border=0>
                    <tr>
                            <td class='label'>ID</td>
                            <td class='content'>
                                <input type="hidden" name="id" value="<?php echo $id; ?>" />
                                <input type="text" name="companyid" id='companyid' value="<?php echo $companyid; ?>" placeholder="ID" <?php if ($mode == 'update') { ?> readonly <?php } ?> />
                                <select name="status">
                                    <option value="1" <?php if ('1'==$status) echo 'selected'; ?>>Active</option>
                                    <option value="0" <?php if ('0'==$status) echo 'selected'; ?>>Inactive</option>
                            </select>
                            </td>
                    </tr>
                    <tr>
                            <td class='label'>Name</td>
                            <td class='content'>
                                <input type="text" name="name" value="<?php echo $name; ?>" class="input-xlarge" />
                            </td>
                    </tr>
                    <tr>
                            <td class='label'>Email</td>
                            <td class='content'>
                                <input type="text" name="email" value="<?php echo $email; ?>" class="input-xlarge"/></td>
                    </tr>
                    <tr>
                            <td class='label'>Phone</td>
                            <td class='content'>
                                <input type="text" name="phone" class="input-small" value="<?php echo $phone; ?>" placeholder='Phone' />
                            </td>
                    </tr>
            </table>
        </div>
        
        
        </form>
    </div>
</div>
