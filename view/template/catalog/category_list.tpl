<?php echo $header; ?>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<?php if ($success) { ?>
<div class="success"><?php echo $success; ?></div>
<?php } ?>

<!--style>
.box > .content {
    width:100%;
    border:none;    
}
</style-->

<div class="box">
    <div class="heading">
        <h1 style="background-image: url('view/image/category.png');">
            Categories
            (Total : <?php echo isset($total) ? $total : 0 ; ?>)
        </h1>
        <div class="buttons">
            <!--a href="product/category/mapview" class="btn"><span>Map View</span></a-->
            <a onclick="location = '<?php echo $insert; ?>'" class="btn btn-warning"><span><?php echo $button_insert; ?></span></a>
            <a onclick="$('#form').submit();" class="btn"><span><?php echo $button_delete; ?></span></a>
        </div>
    </div>
    <div class="content">
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
            <table class="table">
            <thead>
                <tr>
                    <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
                    <td class="left"><?php echo $column_name; ?></td>
                    <td class="right"><?php echo $column_sort_order; ?></td>
                    <td class="right"><?php echo $column_action; ?></td>
                </tr>
            </thead>
            <tbody>
                    <?php if ($categories) { ?>
                    <?php foreach ($categories as $category) { ?>
                    <tr>
                        <td style="text-align: center;"><?php if ($category['selected']) { ?>
                            <input type="checkbox" name="selected[]" value="<?php echo $category['category_id']; ?>" checked="checked" />
                            <?php } else { ?>
                            <input type="checkbox" name="selected[]" value="<?php echo $category['category_id']; ?>" />
                            <?php } ?></td>
                        <td class="left"><?php echo $category['name']; ?></td>
                        <td class="right"><?php echo $category['sort_order']; ?></td>
                        <td class="right">
                            <?php foreach ($category['action'] as $action) { ?>
                            <a href="<?php echo $action['href']; ?>" class='btn'><?php echo $action['text']; ?></a>
                            <?php } ?>
                        </td>
                    </tr>
                    <?php } ?>
                    <?php } else { ?>
                    <tr>
                    <td class="center" colspan="4"><?php echo $text_no_results; ?></td>
                    </tr>
                    <?php } ?>
            </tbody>
            </table>
        </form>

<!---------------------- for tree navigation test
        <style>

        .node {
          cursor: pointer;
        }

        .node circle {
          fill: #fff;
          stroke: steelblue;
          stroke-width: 1.5px;
        }

        .node text {
          font: 10px sans-serif;
        }

        .link {
          fill: none;
          stroke: #ccc;
          stroke-width: 1.5px;
        }
            </style>

        <script src="http://d3js.org/d3.v3.min.js"></script>
        <script>

        var margin = {top: 20, right: 120, bottom: 20, left: 120},
            width = 960 - margin.right - margin.left,
            height = 800 - margin.top - margin.bottom;
            

        var i = 0,
            duration = 750,
            root;
            var tree = d3.layout.tree()
            .size([height, width]);

        var diagonal = d3.svg.diagonal()
            .projection(function(d) { return [d.y, d.x]; });

        var svg = d3.select("div#content .box .content").append("svg")
            //.attr("width", width + margin.right + margin.left)
            .attr("width", "100%")
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


        d3.json("/data/categories.json", function(error, flare) {

            root = flare;
            root.x0 = height / 2;
            root.y0 = 0;

            function collapse(d) {
                if (d.children) {
                    d._children = d.children;
                    d._children.forEach(collapse);
                    d.children = null;
                    }
            }

            root.children.forEach(collapse);
            update(root);
        });

        d3.select(self.frameElement).style("height", "800px");

        function update(source) {

            // Compute the new tree layout.
            var nodes = tree.nodes(root).reverse(),
              links = tree.links(nodes);

            // Normalize for fixed-depth.
          nodes.forEach(function(d) { d.y = d.depth * 180; });

            // Update the nodes…
            var node = svg.selectAll("g.node")
              .data(nodes, function(d) { return d.id || (d.id = ++i); });

            // Enter any new nodes at the parent's previous position.
            var nodeEnter = node.enter().append("g")
              .attr("class", "node")
              .attr("transform", function(d) { return "translate(" + source.y0 + "," + source.x0 + ")"; })
              .on("click", click)
              .on("dbclick", dbclick);

          nodeEnter.append("circle")
              .attr("r", 1e-6)
              .style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; });

          nodeEnter.append("text")
            .attr("x", function(d) { return d.children || d._children ? -10 : 10; })
            .attr("dy", ".35em")
            .attr("text-anchor", function(d) { return d.children || d._children ? "end" : "start"; })
            .attr("code", function(d) { return d.code } )
            .text(function(d) { return d.name; })
            .style("fill-opacity", 1e-6);
            

            // Transition nodes to their new position.
            var nodeUpdate = node.transition()
              .duration(duration)
              .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; });

          nodeUpdate.select("circle")
              .attr("r", 4.5)
              .style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; });

          nodeUpdate.select("text")
              .style("fill-opacity", 1);

            // Transition exiting nodes to the parent's new position.
            var nodeExit = node.exit().transition()
              .duration(duration)
              .attr("transform", function(d) { return "translate(" + source.y + "," + source.x + ")"; })
              .remove();

          nodeExit.select("circle")
              .attr("r", 1e-6);

          nodeExit.select("text")
              .style("fill-opacity", 1e-6);

            // Update the links…
            var link = svg.selectAll("path.link")
              .data(links, function(d) { return d.target.id; });

            // Enter any new links at the parent's previous position.
          link.enter().insert("path", "g")
              .attr("class", "link")
              .attr("d", function(d) {
                var o = {x: source.x0, y: source.y0};
                return diagonal({source: o, target: o});
            });

            // Transition links to their new position.
          link.transition()
              .duration(duration)
              .attr("d", diagonal);

            // Transition exiting nodes to the parent's new position.
          link.exit().transition()
              .duration(duration)
              .attr("d", function(d) {
                var o = {x: source.x, y: source.y};
                return diagonal({source: o, target: o});
            })
              .remove();

            // Stash the old positions for transition.
          nodes.forEach(function(d) {
            d.x0 = d.x;
            d.y0 = d.y;
            });
        }

        // Toggle children on click.
        function click(d) {
            if (d.children) {
            d._children = d.children;
            d.children = null;
            } else {
            d.children = d._children;
            d._children = null;
            }
          update(d);
        }

        // Toggle children on click.
        function dbclick(d) {
            console.log('db click');
        }
        </script>
------------------------->

    </div>

</div>
<?php echo $footer; ?>
