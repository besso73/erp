<?php echo $header; ?>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>

<div class="box">

    <div class="heading">
        <h1 style="background-image: url('view/image/category.png');"><?php echo $heading_title; ?></h1>
        <div class="buttons">
            <a onclick="$('#form').submit();" class="btn btn-success">
                    <span><?php echo $button_save; ?></span></a>
            <a onclick="location = '<?php echo $cancel; ?>';" class="btn"><span><?php echo $button_cancel; ?></span></a>
        </div>
    </div>

    <div class="content">
    
    <!-- cutemom hide two tabs
    <div id="tabs" class="htabs">
        <a tab="#tab_general"><?php echo $tab_general; ?></a>
        <a tab="#tab_data"><?php echo $tab_data; ?></a>
    </div> -->

    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <div id="tab_general">
        <!--div id="languages" class="htabs">
            <?php foreach ($languages as $language) { ?>
            <a tab="#language<?php echo $language['language_id']; ?>"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a>
            <?php } ?>
        </div-->

        <?php foreach ($languages as $language) { ?>
        <div id="language<?php echo $language['language_id']; ?>">
            <table class="form">
                <tr>
                    <td>Parent Category</td>
                    <td>
                    <?php
                    //echo '<pre>'; print_r($parent_id); echo '</pre>';  
                    //echo '<pre>'; print_r($categories); echo '</pre>';  
                    ?>
                        <input type="hidden" name="mode" value="<?php echo $mode ?>" /> 
                        <input type="hidden" name="category_id" value="<?php echo $category_id; ?>" /> 
                        <select id='cat-pcode' name="parent_id">
                            <option value="0"><?php echo $text_none; ?></option>
                            <?php
                            if ( !empty($categories) ) {
                                foreach ($categories as $category) {

                                    $selected = false;
                                    if ( $parent_id != '0' && $parent_id == $category['category_id'] ) {
                                        $selected = true;
                                    }
                            ?>
                                    <option value="<?php echo $category['category_id']; ?>" <?php if ( $selected ) { echo 'selected="selected"'; } ?>> <?php echo $category['name']; ?></option>
                            <?php
                                }   // end foreach
                            }   // !empty
                            ?>
                        </select>
            
                    </td>
                </tr>
                <tr>
                    <td><span class="required">*</span> <?php echo $entry_name; ?></td>
                    <td>
                        <?php
                        if ( isset($name) ) $category_description[1]['name'] = $name;
                        ?>
                        <input type="text" id="cat-name" name="category_description[<?php echo $language['language_id']; ?>][name]" size="100" value="<?php echo isset($category_description[$language['language_id']]) ? $category_description[$language['language_id']]['name'] : ''; ?>" />
                    </td>
                </tr>
                <tr style="display:none;">
                    <td>Code</td>
                    <td>
                        <input type="text" name="code" id="cat-code" size="3" value="<?php echo isset($code) ? $code : ''; ?>" readonly/> * Code is generated automatically based on Category Name
                    </td>
                </td>
                <tr class="hidden">
                    <td><?php echo $entry_meta_keywords; ?></td>
                    <td><textarea name="category_description[<?php echo $language['language_id']; ?>][meta_keywords]" cols="40" rows="5"><?php echo isset($category_description[$language['language_id']]) ? $category_description[$language['language_id']]['meta_keywords'] : ''; ?></textarea></td>
                </tr>
                <tr class="hidden">
                    <td><?php echo $entry_meta_description; ?></td>
                    <td><textarea name="category_description[<?php echo $language['language_id']; ?>][meta_description]" cols="40" rows="5"><?php echo isset($category_description[$language['language_id']]) ? $category_description[$language['language_id']]['meta_description'] : ''; ?></textarea></td>
                </tr>

                <tr class="hidden">
                    <td><?php echo $entry_description; ?></td>
                    <td><textarea name="category_description[<?php echo $language['language_id']; ?>][description]" id="description<?php echo $language['language_id']; ?>"><?php echo isset($category_description[$language['language_id']]) ? $category_description[$language['language_id']]['description'] : ''; ?></textarea></td>
                </tr>


                <tr class="hidden">
                    <td><?php echo $entry_store; ?></td>
                    <td><div class="scrollbox">
                        <?php $class = 'even'; ?>
                    <div class="<?php echo $class; ?>">
                            <?php if (in_array(0, $category_store)) { ?>
                            <input type="checkbox" name="category_store[]" value="0" checked="checked" />
                            <?php echo $text_default; ?>
                            <?php } else { ?>
                            <input type="checkbox" name="category_store[]" value="0" />
                            <?php echo $text_default; ?>
                            <?php } ?>
                    </div>
                        <?php foreach ($stores as $store) { ?>
                        <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                    <div class="<?php echo $class; ?>">
                            <?php if (in_array($store['store_id'], $category_store)) { ?>
                            <input type="checkbox" name="category_store[]" value="<?php echo $store['store_id']; ?>" checked="checked" />
                            <?php echo $store['name']; ?>
                            <?php } else { ?>
                            <input type="checkbox" name="category_store[]" value="<?php echo $store['store_id']; ?>" />
                            <?php echo $store['name']; ?>
                            <?php } ?>
                    </div>
                        <?php } ?>
                    </div></td>
                </tr>
                <tr class='hidden'>
                    <td><?php echo $entry_keyword; ?></td>
                    <td><input type="text" name="keyword" value="<?php echo $keyword; ?>" /></td>
                </tr>

                <tr class='hidden'>
                    <td><?php echo $entry_image; ?></td>
            
                    <td valign="top">

                        <input type="hidden" name="image" value="<?php echo $image; ?>" id="image" />

                    <img src="<?php echo $image; ?>" alt="" id="preview" class="image" onclick="image_upload('image', 'preview');" style="cursor:pointer" />
                    </td>
                </tr>
                
                <tr>
                    <td><?php echo $entry_status; ?></td>
                    <td><select name="status">
                        <?php if ($status) { ?>
                            <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                            <option value="0"><?php echo $text_disabled; ?></option>
                        <?php } else { ?>
                            <option value="1"><?php echo $text_enabled; ?></option>
                            <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                        <?php } ?>
                    </select></td>
                </tr>
                <tr>
                    <td><?php echo $entry_sort_order; ?></td>
                    <td><input type="text" name="sort_order" value="<?php echo $sort_order; ?>" size="1" /></td>
                </tr>
            </table>
        </div>
        <?php } ?>
        </div>

    </form>
    </div>
</div>
    <script type="text/javascript" src="view/javascript/ckeditor/ckeditor.js"></script>
<script type="text/javascript"><!--
<?php foreach ($languages as $language) { ?>
CKEDITOR.replace('description<?php echo $language['language_id']; ?>', {
    filebrowserBrowseUrl: '/common/filemanager',
    filebrowserImageBrowseUrl: '/common/filemanager',
    filebrowserFlashBrowseUrl: '/common/filemanager',
    filebrowserUploadUrl: '/common/filemanager',
    filebrowserImageUploadUrl: '/common/filemanager',
    filebrowserFlashUploadUrl: '/common/filemanager'
});
<?php } ?>
//--></script>
<!--script type="text/javascript" src="view/javascript/jquery/ui/ui.draggable.js"></script>
<script type="text/javascript" src="view/javascript/jquery/ui/ui.resizable.js"></script>
<script type="text/javascript" src="view/javascript/jquery/ui/ui.dialog.js"></script>
<script type="text/javascript" src="view/javascript/jquery/ui/external/bgiframe/jquery.bgiframe.js"></script-->
<script type="text/javascript">
<!--

$('#cat-name').bind('focusout', function(e) {

    var mode = $('input[name="mode"]').val();
    if ( mode == 'update' ) return false;

    var pcode = $('select#cat-pcode').val();
    var name = $('input#cat-name').val();
    var eCode = $('input#cat-code');

    $.ajax({
        url: '/product/category/decideCategoryCode',
        type: 'POST',
        data: 'pcode=' + encodeURIComponent(pcode) + '&name=' + encodeURIComponent(name),
        dataType: 'text',
        success: function(code) {
            if ( code ) {
                eCode.val(code);
            } else {
                alert('fail to generate Category Code');
            }
        },
        fail: function() {
            alert('fail')
        }
    });
})

function image_upload(field, preview) {

    $('#dialog').remove();
        $('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="/common/filemanager&field=' + encodeURIComponent(field) + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');

    $('#dialog').dialog({
        title: '<?php echo $text_image_manager; ?>',
        close: function (event, ui) {
            if ($('#' + field).attr('value')) {
                $.ajax({
                    url: '/common/filemanager/image',
                    type: 'POST',
                    data: 'image=' + encodeURIComponent($('#' + field).val()),
                    dataType: 'text',
                    success: function(data) {
                        if ( data ) {
                           $('#' + preview).replaceWith('<img src="' + data + '" alt="" id="' + preview + '" class="image" onclick="image_upload(\'' + field + '\', \'' + preview + '\');" />');
                        }
                    },
                      fail: function() {
                        alert('fail')
                    }
                });
            }
        },
        bgiframe: false,
        width: 700,
        height: 400,
        resizable: false,
        modal: false
    });

};

//--></script>
<script type="text/javascript"><!--
$.tabs('#tabs a'); 
$.tabs('#languages a');
//--></script>
<?php echo $footer; ?>
<?php
exit;
?>
