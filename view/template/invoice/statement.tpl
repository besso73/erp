<script type="text/javascript" src="view/javascript/jquery/jquery-1.7.1.min.js"></script>
<?php
$server_name = getenv('SERVER_NAME');
$request_uri = getenv('REQUEST_URI');
$url = 'http://'.$server_name.$request_uri;

function add_date($givendate,$day=0,$mth=0,$yr=0){
    $givendate = $givendate. ' 00:00:00';
    $cd = strtotime($givendate);
    $newdate = date('Y-m-d', mktime(date('h',$cd),
                  date('i',$cd), date('s',$cd), date('m',$cd)+$mth,
                  date('d',$cd)+$day, date('Y',$cd)+$yr));
  return $newdate;
}
?>
<style>
table {
  width:400px;
  height:20px;
}
#company-info {
  float:left;
  font-weight:bold;
  
    }
#company-info .title{
  
  font-size:30px;
  padding-bottom:10px;
  
}
 #company-info ul{
  padding-left:20px;
}
#company-info li{
  list-style:none;
  text-align:left;
  font-size:20px;
}
#statement {
  float:right;
  font-weight:bold;
}
#statement .title{
  font-size:30px;
  padding-bottom:10px;
}
 #statement ul{
  padding-right:20px;
}
#statement li{
  list-style:none;
  text-align:right;
  font-size:18px;
}
#billinfo{
  clear:both;
}
#shipto.lable {
  background-color:lightgray;
  font-weight:bold;
}
detail{
  margin-top:20px;
  width:100%;
}
#detail td{
  text-align:center;
  height:32px;
}
#detail.label{
  font-weight:bold;
  background-color: lightgray;
  height:32px;
}
td#t-order, td#t-balance {
  font-weight:bold;
}
</style>
<script type="text/javascript" src="view/javascript/jquery/jquery.min.js"></script>
<div id='sheet'>
    <div id='top'>
    <div id='company-info'>
        <ul>
        <li class='title'>JC Trading Inc.</li>
        <li>2340 Brickvale Dr.</li>
        <li>Elk Grove Village, IL 60007</li>
        <li>TEL : 847 - 544 - 8861</li>
        </ul>
    </div>
    <div id="statement">
        <ul>
        <li class="title">Statement &amp; Receipt</li>
        <li>Billing Date : 07/18/2013</li>
        <li>Rep : Mr.Park <?php //echo $salesrep; ?></li>
        <li>Term : Net 30</li>
        </ul>
    </div>
    </div>
  
    <?php
        $shipto = nl2br($shipto);
        $address =<<<HTML
        <ul>
        <li>$store_name</li>
        <li>$address1</li>
        <li>$city , $state, $zipcode</li>
        <li>TEL : $phone1</li>
        </ul>
HTML;
    //if ('' == $billto) $billto = $address;
        if ('' == $shipto) $shipto = $address;
  ?>
    <div id='billinfo'>
    <!--table id='billto' border='1' cellspacing=0 cellpading=1>
        <tr>
        <td style='padding-left:20px;'>BILL TO</td>
        </tr>
        <tr>
        <td style='padding-left:20px;'>
            <?php echo $billto; ?>
        </td>
        </tr>
    </table-->
    <table id='shipto' border='1'>
        <tr>
        <td style='padding-left:20px;'>SHIP TO</td>
        </tr>
        <tr>
        <td style='padding-left:20px;'>
            <?php echo $shipto; ?>
        </td>
        </tr>
    </table>
    </div>

        <table id='detail' border=1px;>
    <tr>
        <td class='label'>Date
        <input type=hidden name='txids' value=<?php echo $txids; ?> >
        <input type=hidden name='store_id' value=<?php echo $store_id; ?> >
        </td>
        <td class='label'>DESCRIPTION</td>
        <td class='label'>Amount</td>
        <td class='label'>Balance</td>
    </tr>
    <?php
        $tOrder = $tBalance = 0;
        foreach( $aTX as $tx ) {
        $order_date = substr( $tx['order_date'], 0 , 10 );
        $txid = $tx['txid'];
        $amount = $balance = $tx['total'];
        $desc = "$txid Order : $amount";
        
        $tOrder += $amount;
        $tBalance += $balance;
        
    ?>
    <tr style='padding:0px'>
        <td><?php echo $order_date; ?></td>
        <td><?php echo $desc; ?></td>
        <td><?php echo $amount; ?></td>
        <td><?php echo $balance; ?></td>
    </tr>
    <?php
    }
    ?>
    <tr>
        <td>&nbsp;</td>
        <td><input name='credit-desc' style='text-align:center;border:none;width:400px;background-color:#e9e9e9;height:25px;' /></td>
        <td><input name='credit' style='text-align:center;border:none;width:90px;background-color:#e9e9e9;height:25px;' /></td>
        <td>&nbsp;</td>
    </tr>
    
    <tr><td colspan=4></td></tr>
    <tr>
        <td >&nbsp;</td>
        <td style="text-align:right; font-weight:bold; padding-right:10px;" >Total</td>
        <td id='t-order'><?php echo $tOrder; ?></td>
        <td id='t-balance'><?php echo $tBalance; ?></td>
    </tr>
    </table>
    <div style='text-align:right;margin-top: 20px;'>
        <button id='print'>Print</button>
    </div>
</div>
    <script>
        $( 'input[name="credit"]' ).bind('click', function(e) {
        $(this).select();
    }).bind( 'focus', function(e) {
        $(this).select();
        $target = $(e.target);
        $_credit = $target.val();
        if ( $_credit == '' ) $_credit = 0;
        $_credit = parseFloat($_credit);
    }).bind( 'keypress', function(e) {
        if ( e.which == 13 ) {   // enter key
            var $tOrder = $( 'td#t-order' ).text();
            var $tOrder = parseFloat( $tOrder );

            $tBalance = $( 'td#t-balance' ).text();
            $tBalance = parseFloat( $tBalance );

            $credit = $(e.target).val();
            if ( $credit == '' ) $credit = 0;
            $credit = parseFloat($credit);
            
            $tOrder = $tOrder + parseFloat( $credit );
            $tBalance = $tBalance + parseFloat( $credit );
            
            $( 'td#t-order' ).html($tOrder);
            $( 'td#t-balance' ).html($tBalance);
        }
    });
    
        $('#print').bind('click',function(e) {

        store_id = $( 'input[name="store_id"]' ).val();
        txids = $('table#detail').find('input[name="txids"]').val();
        credit = $( 'input[name="credit"]' ).val();
        comment = $( 'input[name="credit-desc"]' ).val();
        total = $( 'td#t-order' ).text();
        data = {
            'store_id' : store_id,
            'txids' : txids,
            'credit' : credit,
            'comment' : comment,
            'total' : total
        }
        $.ajax({
            type:'get',
            url:'/invoice/statement/saveStatement',
            dataType:'html',
            data: data,
            success:function(html){
                //$('#account_history').html(html);
                //document.getElementById('account_history').innerHTML = html;
              window.print();
            }
        });
    });
</script>
