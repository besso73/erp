        <tr class='footer'>
            <!--td></td-->
            <td></td>
            <td></td>
            <td colspan='3'>Original price : </td>
            <td colspan='2'>
                    <?php
                //echo $total_total;
                echo round($originalPrice,2);
                    ?>
            </td>
        </tr>
        <?php
        if ( $unitDiscounts > 0 ) {
        ?>
        <tr class='footer'>
            <!--td style='border:1px solid gray;'></td-->
            <td style='border:1px solid gray;'></td>
            <td style='border:1px solid gray;'></td>
            <td style='border:1px solid gray;' colspan='3'>Unit D/C</td>
            <td style='border:1px solid gray;' colspan='2'>- <?php echo round($unitDiscounts,2); ?></td>
        </tr>
        
        <?php
    }
        ?>
        
        <?php
        $_discount = $total_total - $subtotal_total;
        if ($_discount > 0) {
        ?>
        <tr class='footer'>
            <!--td style='border:1px solid gray;'></td-->
            <td style='border:1px solid gray;'></td>
            <td style='border:1px solid gray;'></td>
            <td style='border:1px solid gray;' colspan='3'>Account D/C</td>
            <td style='border:1px solid gray;'>-<?php echo $store_discount_percent; ?>%</td>
            <td style='border:1px solid gray;'>-<?php echo $_discount; ?></td>
        </tr>
        <?php
    }
        ?>
        <tr>
            <td colspan='5'>
               
            </td>
            <td colspan='2'>
                Total : <?php echo $subtotal_total; ?>
            </td>
        </tr>
