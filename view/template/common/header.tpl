<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" xml:lang="<?php echo $lang; ?>">
<head>

    <title>DURE</title>

    <meta name="keywords" content="<?php echo META_TAG ?>">
    <meta name="subject" content="">
    <meta name="title" content="">
    <meta NAME="robots" content="ALL">
    <meta NAME="author" content="">
    <meta NAME="description" content=""> 
    
    <base href="<?php echo $base; ?>" />

    <?php foreach ($links as $link){ ?>
    <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
    <?php } ?>

    <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="view/stylesheet/stylesheet.css" />
    <link rel="stylesheet" type="text/css" href="view/javascript/jquery/ui/themes/ui-lightness/ui.all.css" />

    <?php foreach ($styles as $style){ ?>
        <link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
    <?php } ?>

    <script type="text/javascript" src="view/javascript/jquery/jquery-1.7.1.min.js"></script>
 
    <script type="text/javascript" src="view/javascript/jquery/jquery.ui.core.js"></script>
    <script type="text/javascript" src="view/javascript/jquery/jquery.ui.widget.js"></script>
    <script type="text/javascript" src="view/javascript/jquery/jquery.ui.mouse.js"></script>
    <script type="text/javascript" src="view/javascript/jquery/jquery.ui.draggable.js"></script>
    <script type="text/javascript" src="view/javascript/jquery/jquery.ui.droppable.js"></script>
    <script type="text/javascript" src="view/javascript/jquery/superfish/js/superfish.js"></script>

    <script type="text/javascript" src="view/javascript/jquery/tab.js"></script>
    <script type="text/javascript" src="view/javascript/jquery/autoresize.jquery.js"></script>
    <script type="text/javascript" src="view/javascript/datePicker/date.js"></script>

    <script src="view/javascript/datePicker/jquery.datePicker.js"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="view/javascript/datePicker/datePicker.css">

    <script src="view/javascript/jquery/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="view/stylesheet/jquery.fancybox-1.3.4.css" />

    <!-- jquery UI for atc ... -->
    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/base/jquery-ui.css" id="theme">
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>


    <style>
    .datepicker{ position:absolute;  z-index:99; }
    </style>
    <!---->
    <?php foreach ($scripts as $script){ ?>
    <script type="text/javascript" src="<?php echo $script; ?>"></script>
    <?php } ?>

    <script type="text/javascript">
    //-----------------------------------------
    // Confirm Actions (delete, uninstall)
    //-----------------------------------------
    $(document).ready(function(){
        $('#form').submit(function(){
            if ($(this).attr('action').indexOf('delete',1) != -1){
                if (!confirm ('<?php echo $text_confirm; ?>')){  return false; }
            }
        });
        // Confirm Uninstall
        $('a').click(function(){
            if ($(this).attr('href') != null && $(this).attr('href').indexOf('uninstall',1) != -1){
                if (!confirm ('<?php echo $text_confirm; ?>')){  return false; }
            }
        });
    });
    </script>
</head>

<body>

<?php
$companyid = $this->user->getCompanyId();
$companyName = $this->user->getCompanyName();
$username  = $this->user->getUserName();
?>

<div id="container">
<!--div id="header">
    <div class="logo">
        <a href='/' style='text-decoration:none;color:white;'>
        Administration
        </a>
    </div>
    <?php if ($logged){ ?>
        <div class="div2"><img src="view/image/lock.png" alt="" style="position: relative; top: 3px;" />&nbsp;<?php echo $logged; ?></div>
    <?php } ?>
</div-->

<?php if ($logged){ ?>
<div id="menu">
    <ul class="nav-left">

        <?php /* ?>
        <li id="reports"><a class="top">DASHBOARD</a>
            <ul>
                    <li><a href="/common/home">Basic</a></li>
                    <li><a href="/report/product">Product</a></li>
                    <li><a href="/report/account">Account</a></li>
                    <li><a href="/report/sales">Sales</a></li>
            </ul>
        </li>
        <?php */ ?>

        <?php /* ?>
        <?php if ($this->user->hasPermission('access','material/lookup')){ ?>
        <li id="material"><a class="top">Material</a>
            <ul>
                    <li><a href="<?php echo $lnk_material_lookup; ?>"><?php echo $text_material_lookup; ?></a></li>
                    <li><a href="<?php echo $lnk_material_productpackage; ?>">Product Map</a></li>
                    <li><a href="<?php echo $lnk_material_history; ?>">History</a></li>
            </ul>
        </li>
        <?php } ?>
        <?php */ ?>
        
        
        <?php if ($this->user->hasPermission('access','product/inventory')){ ?>
        <li id="product"><a class="top">Basic Information</a>
            <ul>
                    <li><a href="product/category">Category</a></li>
                    <li><a href="product/price">Product</a></li>
                    <li><a href="product/unit">Unit</a></li>
            </ul>
        </li>
        <?php } ?>

        <?php if ($this->user->hasPermission('access','sales/order')){ ?>
        <li id="sales">
            <a class="top">SALES</a>
            <ul>
                    <li><a href="<?php echo $lnk_sales_order; ?>"><?php echo $text_sales_order; ?></a></li>
                    <li><a href="<?php echo $lnk_sales_lookup; ?>"><?php echo $text_sales_lookup; ?></a></li>
            </ul>
        </li>
        <?php } ?>



        <?php /* ?>
        <?php
        if ($this->user->hasPermission('access','invoice/list')){
            $lnk_invoice_list   = HTTPS_SERVER . 'invoice/list&token='  . $this->session->data['token'];    
            $lnk_invoice_search = HTTPS_SERVER . 'invoice/search&token='. $this->session->data['token'];    
        ?>
        <li id="invoice"><a class="top">INVOICE</a>
            <ul>
            <li><a href="<?php echo $lnk_invoice_list; ?>">Invoice List</a></li>
            <li><a href="/invoice/statement/getlist">Show Statement</a></li>
            <li><a href="<?php echo $lnk_invoice_search; ?>">Invoice Search</a></li>
            </ul>
        </li>
        <?php } ?>
        <?php */ ?>
        
        
        <?php
        /*** statement
        if ($this->user->hasPermission('access','statement/list')) {

            $lnk_statement_list   = HTTPS_SERVER . 'statement/list';
            $lnk_statement_account_list = HTTPS_SERVER . 'statement/statement';
            $lnk_statement_pay = HTTPS_SERVER . 'statement/detail';    
            $lnk_statement_report = HTTPS_SERVER . 'statement/report';    
            //$lnk_quickbook = HTTPS_SERVER . 'statement/quickbook';    
        
        ?>
        <li id="statement"><a class="top">STATEMENT</a>
            <ul>
            <!--li><a href="<?php echo $lnk_statement_account_list; ?>">Account List</a></li-->
            <li><a href="<?php echo $lnk_statement_list; ?>">Statement List</a></li>
            <li><a href="<?php echo $lnk_statement_pay; ?>">Process Statement</a></li>
            <!--li><a href="<?php echo $lnk_quickbook; ?>">Quickbook Update</a></li-->
            </ul>
        </li>
        <?php } 
        end of statement ***/
        ?>

        <?php
        if ($this->user->hasPermission('access','store/list')){
            $lnk_store_list = HTTPS_SERVER . 'store/list';
            $lnk_btrip_plan = HTTPS_SERVER . 'store/btrip';        
        ?>
        <li id="sales"><a class="top">ACCOUNT</a>
            <ul>
                    <li><a href="<?php echo $lnk_store_list; ?>">Account List</a></li>
            </ul>
        </li>
        <?php } ?>

        <!--li id="tools">
            <a class="top">TOOLS</a>
            <ul>
                <li><a href="/common/filemanager">File Manager</a></li>
            </ul>
        </li-->

        <?php /* ?>
        <?php $lnk_send_sms = '/user/sms'; ?>
        <li id="user"><a class="top">SMS</a>
            <ul><li><a href="<?php echo $lnk_send_sms; ?>">Send SMS</a></li></ul>
        </li>
        <?php */ ?>

        <?php if ($this->user->hasPermission('access', 'report/excel')){ ?>

        <!--li id="excel">
             <a class="top" href="/report/csv" style="">EXPORT</a>
        </li-->
        <?php } ?>

        <?php if ($this->user->hasPermission('access', 'tool/csv')){ ?>
        <li id="excel">
             <a class="top" href="/tool/import" style="">DATA CENTER</a>
        </li>

        <?php } ?>

    </ul>




    <ul class="nav right">
        <?php if ($this->user->hasPermission('access','setting/setting')){ ?>
        <li id="system"><a class="top">ADMIN</a>
            <ul>
                <!--li><a href="<?php echo $setting; ?>"><?php echo $text_setting; ?></a></li-->
                <?php
                if ($this->user->companyid == 'admin' ) {
                ?>
                    <li><a href="company/list">Company</a></li>
                    <?php
                    }
                ?>

                <li><a class="parent"><?php echo $text_users; ?></a>
                        <ul>
                        <li><a href="<?php echo $user; ?>">Employees</a></li>
                        <li><a href="<?php echo $user_group; ?>">Groups</a></li>
                        </ul>
                </li>
                
                <?php
                if ($this->user->companyid == 'admin' ) {
                ?>
                
                <!--li>
                        <a class="parent"><?php echo $text_localisation; ?></a>
                        <ul>
                        <li><a href="<?php echo $language; ?>"><?php echo $text_language; ?></a></li>
                        <li><a href="<?php echo $currency; ?>"><?php echo $text_currency; ?></a></li>
                        <li><a href="<?php echo $stock_status; ?>"><?php echo $text_stock_status; ?></a></li>
                        <li><a href="<?php echo $order_status; ?>"><?php echo $text_order_status; ?></a></li>
                        <li><a href="<?php echo $country; ?>"><?php echo $text_country; ?></a></li>
                        <li><a href="<?php echo $zone; ?>"><?php echo $text_zone; ?></a></li>
                        <li><a href="<?php echo $geo_zone; ?>"><?php echo $text_geo_zone; ?></a></li>
                        <li><a href="<?php echo $tax_class; ?>"><?php echo $text_tax_class; ?></a></li>
                        <li><a href="<?php echo $length_class; ?>"><?php echo $text_length_class; ?></a></li>
                        <li><a href="<?php echo $weight_class; ?>"><?php echo $text_weight_class; ?></a></li>
                        </ul>
                </li-->
                <li><a href="<?php echo $error_log; ?>"><?php echo $text_error_log; ?></a></li>
                <!--li><a href="<?php echo $backup; ?>"><?php echo $text_backup; ?></a></li-->
                <?php @$this->load->language('tool/csv'); ?>
                
                <?php if (@$this->language->get('text_csvmenu') != NULL){ ?>
                <li><a href="<?php echo (((HTTPS_SERVER) ? HTTPS_SERVER : HTTP_SERVER) . '/tool/csv&token=' . $this->session->data['token']); ?>">
                    <?php echo $this->language->get('text_csvmenu'); ?></a></li>
                <?php } ?>
            
            
                <?php
                }   // end of admin .... do it later
                ?>
            
                <?php if ($this->user->hasPermission('access', 'report/excel')){ ?>
                <li id="excel">
                    <a href="/report/excel">Query</a>
                </li>
                <?php } ?>

            </ul>
        </li>
        <?php } ?>

        <li><a class="top" id='change_password' href='user/user/changepassword'>Password</a></li>
        
        <li>
            <a class="top" href="<?php echo $logout; ?>">
                    <?php echo ucwords($username) ?>
                    <?php echo $text_logout; ?>
            </a>
        </li>
    </ul>

<script type="text/javascript">
$(document).ready(function(){
    $('.nav').superfish({
        hoverClass:'sfHover',pathClass:'overideThisToUse',delay:0,
        animation:{height:'show'},speed:'normal',autoArrows:false,dropShadows:false,
        disableHI:false, /* set to true to disable hoverIntent detection */
        onInit:function(){},onBeforeShow:function(){},onShow:function(){},onHide:function(){}
    });
    $('.nav').css('display', 'block');
});
</script>

<script type="text/javascript"><!-- 
function getURLVar(urlVarName){
    var urlHalves = String(document.location).toLowerCase().split('?');
    var urlVarValue = '';
    if (urlHalves[1]){
        var urlVars = urlHalves[1].split('&');
        for (var i = 0; i <= (urlVars.length); i++){
            if (urlVars[i]){
                var urlVarPair = urlVars[i].split('=');
                if (urlVarPair[0] && urlVarPair[0] == urlVarName.toLowerCase()){
                    urlVarValue = urlVarPair[1];
                    }
            }
        }
    }
    return urlVarValue;
}

$(document).ready(function(){
    route = getURLVar('route');
    if (!route){
        $('#dashboard').addClass('selected');
    } else {
        part = route.split('/');
        url = part[0];
        if (part[1]){  url += '/' + part[1]; }
        $('a[href*=\'' + url + '\']').parents('li[id]').addClass('selected');
    }
});
//-->
</script>

</div>
<?php } // $logged ?>

<div id="content">