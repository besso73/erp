<?php

//$html = $_POST['html'];

$html =<<<HTML
<link rel="stylesheet" type="text/css" href="/css/bootstrap.css" />
<!--link rel="stylesheet" type="text/css" href="/css/invoice.css" /-->
<script type="text/javascript" src="/view/javascript/jquery/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="/js/invoice.js"></script>
<style>
/* invoice ---------------------------------------------------*/
#sheet{
    width: 800px;
    margin: auto;
}

#top, #billinfo, #metainfo, #detail{
    width: 95%;
    margin: auto;
    margin-top: 20px;
    /* margin-bottom: 20px; */
}

#general-info {
    float:right;
    margin-right: 20px;
}

#general-info h3 {
    text-align:right;
    padding-right:10px;
    margin: 0;
}
#general-info table {
    text-align:center;
    font-size:12px;
}

#general-info table td {
    padding: 3px 5px;
}


#metainfo td{
    text-align:center;
    padding: 3px 5px;
}

#item-wrapper {
    min-height: 500px;
}

#detail{
    margin-top: 3px;

    /* height:100%; */
    max-height:700px;
    min-height:500px;
}


#detail th {
    font-size:13px;
    padding: 3px 5px;
    border: 1px solid gray;
}

#detail td{
    text-align:center;
    font-size: 14px;
    padding: 5px;
    border: 1px solid gray;
    /* border-bottom: hidden; */
}


#detail tr:nth-last-child(2) td
{
      border-bottom: 1px solid gray;
}

#detail tr:last-child td {
      border-bottom: 1px solid gray;
}


#detail td.footer{
    border-top: 1px solid gray;
    border-bottom: 1px solid gray;
}


#detail .label{
  border:1px solid black;
  height:32px;
}
</style>

<div id='sheet' >
    <table id='top' border=0>
        <tr>
            <td id='company-info'>
                    <table>
                            <tr>
                            <td><img id='logo-image' src='/image/icon/logo.jpg' /></td>
                            </tr>
                            <tr>
                            <td>&nbsp;&nbsp;7789 North Caldwell Ave.</td>
                            </tr>
                            <tr>
                            <td>&nbsp;&nbsp;Niles, IL, 60714</td>
                            </tr>
                    </table>
            </td>
            <td id='general-info' align='right' valign='bottom'>
                    <h3>INVOICE</h3>
                    <table border=1 cellspacing=0 cellpading=1>
                            <tr>
                            <td>Phone #</td>
                            <td>Fax #</td>
                            <td>DATE</td>
                            <td>INVOICE #</td>
                            </tr>
                            <tr>
                            <td>847.663.0900</td>
                            <td>847.663.0905</td>
                            <td>04/23/2014</td>
                            <td>14040001</td>
                            </tr>
                    </table>
            </td>
        </tr>
    </table>
    
    
    <table id='billinfo'>
        <tr>
            <td width='400px'>
                    <table cellspacing=0 cellpading=1>
                            <tr>
                            <td style='padding-left:20px;font-size:13px;'>Bill To</td>
                            </tr>
                            <tr>
                            <td style='padding-left:20px;'>
                            #1 Beauty Supply(I)<br />
2700 S. Orange Blossom Trail<br />
Orlando,FL,32805<br />
TEL : 407-540-1200                        
                            </td>
                            </tr>
                    </table>
            </td>
            <td>
                    <table cellspacing=0 cellpading=1>
                            <tr>
                            <td style='padding-left:20px;font-size:13px;'>Ship To</td>
                            </tr>
                            <tr>
                            <td style='padding-left:20px;'>
                            #1 Beauty Supply(I)<br />
2700 S. Orange Blossom Trail<br />
Orlando,FL,32805<br />
TEL : 407-540-1200
                            </td>
                            </tr>
                    </table>
            </td>
        </tr>
    </table>

    <table id='metainfo' border='1' cellspacing=0 cellpading=1>
        <tr> 
            <td>P.O.#</td>
            <td>TERMS</td>

            <td>REP</td>
            <td>SHIP DATE</td>
            <td>SHIP VIA</td>
            <td>F.O.B.</td>
        </tr>
        <tr>
            <td>FL1200-20140422-1</td>
            <td>Cod</td>
            <td>joon</td>

            <td></td>
            <td>PCK</td>
            <td></td>
        </tr>
    </table>
            <table id='detail' border='1'>
        <thead>
        <tr>
            <th>Item Code</th>
            <th>Description</th>
            <th>Ordered</th>
            <th>Shipped</th>
            <th>Unit Price</th>
            <th>Amount</th>
        </tr>
        </thead>
        <tbody>
        <tr class='item '>
            <td style='border-bottom:hidden;'>3S2407</td>
            <td style='border-bottom:hidden;'>30S TubeGlue 1.5oz</td>
            <td style='border-bottom:hidden;'>1</td>
            <td style='border-bottom:hidden;'>1</td>
            <td style='border-bottom:hidden;'>33.36</td>
            <td style='border-bottom:hidden;'>33.36</td>
        </tr>
                    <tr class='item '>
            <td>3S2414</td>
            <td>30S Cream Remover 4oz1dz</td>
            <td>1</td>
            <td>1</td>
            <td>30.72</td>
            <td>30.72</td>
        </tr>
                    <tr class='item '>
            <td>3S2421</td>
            <td>30S Essen Display 54pc</td>
            <td>1</td>
            <td>1</td>
            <td>10.00</td>
            <td>10.00</td>
        </tr>
                    <tr class='item '>
            <td>3S2421</td>
            <td>30S Essen Display 54pc</td>
            <td>1</td>
            <td>1</td>
            <td>10.00</td>
            <td>5.00</td>
        <tr class='item '>
            <td>3S2421</td>
            <td>30S Essen Display 54pc</td>
            <td>1</td>
            <td>1</td>
            <td>10.00</td>
            <td>5.00</td>
        </tr>
        <tr class='item '>
            <td>3S2407</td>
            <td>30S TubeGlue 1.5oz</td>
            <td>1</td>
            <td>1</td>
            <td>33.36</td>
            <td>33.36</td>
        </tr>
                    <tr class='item '>
            <td>3S2414</td>
            <td>30S Cream Remover 4oz1dz</td>
            <td>1</td>
            <td>1</td>
            <td>30.72</td>
            <td>30.72</td>
        </tr>
                    <tr class='item '>
            <td>3S2421</td>
            <td>30S Essen Display 54pc</td>
            <td>1</td>
            <td>1</td>
            <td>10.00</td>
            <td>10.00</td>
        </tr>
                    <tr class='item '>
            <td>3S2421</td>
            <td>30S Essen Display 54pc</td>
            <td>1</td>
            <td>1</td>
            <td>10.00</td>
            <td>5.00</td>
        </tr>
        <tr class='item '>
            <td>3S2421</td>
            <td>30S Essen Display 54pc</td>
            <td>1</td>
            <td>1</td>
            <td>10.00</td>
            <td>5.00</td>
        </tr>
        <tr class='item '>
            <td>3S2407</td>
            <td>30S TubeGlue 1.5oz</td>
            <td>1</td>
            <td>1</td>
            <td>33.36</td>
            <td>33.36</td>
        </tr>
                    <tr class='item '>
            <td>3S2414</td>
            <td>30S Cream Remover 4oz1dz</td>
            <td>1</td>
            <td>1</td>
            <td>30.72</td>
            <td>30.72</td>
        </tr>
                    <tr class='item '>
            <td>3S2421</td>
            <td>30S Essen Display 54pc</td>
            <td>1</td>
            <td>1</td>
            <td>10.00</td>
            <td>10.00</td>
        </tr>
                    <tr class='item '>
            <td>3S2421</td>
            <td>30S Essen Display 54pc</td>
            <td>1</td>
            <td>1</td>
            <td>10.00</td>
            <td>5.00</td>
        </tr>
        <tr class='item '>
            <td>3S2421</td>
            <td>30S Essen Display 54pc</td>
            <td>1</td>
            <td>1</td>
            <td>10.00</td>
            <td>5.00</td>
        </tr>
        <tr class='item '>
            <td>3S2407</td>
            <td>30S TubeGlue 1.5oz</td>
            <td>1</td>
            <td>1</td>
            <td>33.36</td>
            <td>33.36</td>
        </tr>
                    <tr class='item '>
            <td>3S2414</td>
            <td>30S Cream Remover 4oz1dz</td>
            <td>1</td>
            <td>1</td>
            <td>30.72</td>
            <td>30.72</td>
        </tr>
                    <tr class='item '>
            <td>3S2421</td>
            <td>30S Essen Display 54pc</td>
            <td>1</td>
            <td>1</td>
            <td>10.00</td>
            <td>10.00</td>
        </tr>
                    <tr class='item '>
            <td>3S2421</td>
            <td>30S Essen Display 54pc</td>
            <td>1</td>
            <td>1</td>
            <td>10.00</td>
            <td>5.00</td>
        </tr>
        <tr class='item '>
            <td>3S2421</td>
            <td>30S Essen Display 54pc</td>
            <td>1</td>
            <td>1</td>
            <td>10.00</td>
            <td>5.00</td>
        </tr>
        <tr class='item '>
            <td>3S2407</td>
            <td>30S TubeGlue 1.5oz</td>
            <td>1</td>
            <td>1</td>
            <td>33.36</td>
            <td>33.36</td>
        </tr>
                    <tr class='item '>
            <td>3S2414</td>
            <td>30S Cream Remover 4oz1dz</td>
            <td>1</td>
            <td>1</td>
            <td>30.72</td>
            <td>30.72</td>
        </tr>
                    <tr class='item '>
            <td>3S2421</td>
            <td>30S Essen Display 54pc</td>
            <td>1</td>
            <td>1</td>
            <td>10.00</td>
            <td>10.00</td>
        </tr>
        <tr class='item '>
            <td>3S2421</td>
            <td>30S Essen Display 54pc</td>
            <td>1</td>
            <td>1</td>
            <td>10.00</td>
            <td>5.00</td>
        </tr>
        <tr class='item '>
            <td>3S2407</td>
            <td>30S TubeGlue 1.5oz</td>
            <td>1</td>
            <td>1</td>
            <td>33.36</td>
            <td>33.36</td>
        </tr>
                    <tr class='item '>
            <td>3S2414</td>
            <td>30S Cream Remover 4oz1dz</td>
            <td>1</td>
            <td>1</td>
            <td>30.72</td>
            <td>30.72</td>
        </tr>
                    <tr class='item '>
            <td>3S2421</td>
            <td>30S Essen Display 54pc</td>
            <td>1</td>
            <td>1</td>
            <td>10.00</td>
            <td>10.00</td>
        </tr>
                    <tr class='item '>
            <td>3S2421</td>
            <td>30S Essen Display 54pc</td>
            <td>1</td>
            <td>1</td>
            <td>10.00</td>
            <td>5.00</td>
        </tr>
                    <tr class='item'>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan='2'>
                Origin price : 79.08            </td>
        </tr>
                    <tr class='item'>
            <td>Discount : 10%</td>
            <td></td>
            <td></td>
            <td></td>
            <td>-10%</td>
            <td>7.91</td>
        </tr>
        <tr>
            <td class='footer' colspan='4'>
            
            </td>
            <td class='footer' colspan='2'>
                Total : 71.17            </td>
        </tr>
        </tbody>
    </table>

</div>
HTML;

//echo $html; exit;

//==============================================================
//==============================================================
//==============================================================
include("/home/kimj0603/public_html/backyard/www/system/library/mpdf/mpdf.php");

$mpdf=new mPDF('en-GB-x','A4','','',10,10,10,10,6,3); 

$mpdf->SetDisplayMode('fullpage');

$mpdf->list_indent_first_level = 0;    // 1 or 0 - whether to indent the first level of a list

// LOAD a stylesheet
$stylesheet = file_get_contents('/Users/besso/Sites/backyard/www/system/library/mpdf/examples/mpdfstyletables.css');
$mpdf->WriteHTML($stylesheet,1);    // The parameter 1 tells that this is css/style only and no body/html/text

$mpdf->WriteHTML($html);

$mpdf->Output();
//$mpdf->Output('/Users/besso/Sites/backyard/www/data/invoice/besso.pdf', 'F');
exit;
//==============================================================
//==============================================================
//==============================================================

?>