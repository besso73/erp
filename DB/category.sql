- Set category as more document style

CREATE TABLE `admin_category` (
    id int(11) NOT NULL AUTO_INCREMENT,
    code_a char(2) not null,
    name_a varchar(255) not null default '',
    code_b char(2) not null,
    name_b varchar(255) not null default '',
    code_color char(2) not null,
    name_color varchar(255) not null default '',
    code_size char(2) not null,
    name_size varchar(255) not null default '',
    code_c varchar(3) not null,
    name_c varchar(255) not null default '',
    isProduct int(1) not null default 0,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=69 DEFAULT CHARSET=utf8 COLLATE=utf8_bin


CREATE TABLE `category` (
    `category_id` int(11) NOT NULL AUTO_INCREMENT,
    `image` varchar(255) COLLATE utf8_bin DEFAULT NULL,
    `parent_id` int(11) NOT NULL DEFAULT '0',
    `sort_order` int(3) NOT NULL DEFAULT '0',
    `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
    `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
    `status` int(1) NOT NULL DEFAULT '1',
    `top` tinyint(1) DEFAULT NULL,
    `column` int(3) DEFAULT NULL,
    PRIMARY KEY (`category_id`)
) ENGINE=MyISAM AUTO_INCREMENT=69 DEFAULT CHARSET=utf8 COLLATE=utf8_bin