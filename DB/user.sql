CREATE TABLE `user` (
    `user_id` int(11) NOT NULL AUTO_INCREMENT,
    `user_group_id` int(11) NOT NULL,
    `username` varchar(20) COLLATE utf8_bin NOT NULL DEFAULT '',
    `password` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
    `firstname` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
    `lastname` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
    `email` varchar(96) COLLATE utf8_bin NOT NULL DEFAULT '',
    `status` int(1) NOT NULL,
    `ip` varchar(15) COLLATE utf8_bin NOT NULL DEFAULT '',
    `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
    `approver` varchar(30) COLLATE utf8_bin DEFAULT NULL,
    `telephone` varchar(20) COLLATE utf8_bin DEFAULT NULL,
    PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=44 DEFAULT CHARSET=utf8 COLLATE=utf8_bin

alter table user add companyid int(11) not null;
update user set companyid = 1 ;
