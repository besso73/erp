CREATE TABLE `company` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `companyid` varchar(30) COLLATE utf8_bin NOT NULL DEFAULT '',
        `name` varchar(50) COLLATE utf8_bin NOT NULL DEFAULT '',
        `email` varchar(96) COLLATE utf8_bin NOT NULL DEFAULT '',
        `phone` varchar(20) COLLATE utf8_bin DEFAULT NULL,
        `status` int(1) NOT NULL,
        `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
        PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=44 DEFAULT CHARSET=utf8 COLLATE=utf8_bin
;

insert company set id = 1, companyid = 'aroo', name = 'Aroo technology', email = 'besso@live.com', phone='', status = 1, date_added = now();






CREATE TABLE  admin_stores (
    id int(11) NOT NULL AUTO_INCREMENT,
    accountno varchar(10) NOT NULL,
    name varchar(100) DEFAULT NULL,
    storetype char(1) DEFAULT NULL,
    address1 varchar(100) DEFAULT NULL,
    address2 varchar(100) DEFAULT NULL,
    city varchar(30) DEFAULT NULL,
    state varchar(10) DEFAULT NULL,
    zipcode varchar(10) DEFAULT NULL,
    phone1 varchar(50) DEFAULT NULL,
    phone2 char(12) DEFAULT NULL,
    salesrep varchar(10) DEFAULT NULL,
    status char(1) DEFAULT NULL,
    fax char(12) DEFAULT NULL,
    lat varchar(12) DEFAULT NULL,
    lng varchar(12) DEFAULT NULL,
    chrt char(1) DEFAULT NULL,
    parent varchar(10) DEFAULT NULL,
    comment text,
    email varchar(100) DEFAULT NULL,
    billto text,
    shipto text,
    discount varchar(200) DEFAULT '',
    owner varchar(50) DEFAULT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY accountno (accountno)
);


CREATE TABLE admin_products (
    product_id int(11) NOT NULL AUTO_INCREMENT,
    model varchar(64) NOT NULL,
    sku varchar(64) NOT NULL,
    category varchar(64) NOT NULL,
    name varchar(255) DEFAULT NULL,
    location varchar(128) NOT NULL,
    quantity int(4) NOT NULL DEFAULT '0',
    cost decimal(15,4) NOT NULL DEFAULT '0.0000',
    ws_price decimal(14,2) DEFAULT '0.00',
    rt_price decimal(14,2) DEFAULT '0.00',
    status int(1) NOT NULL DEFAULT '0',
    image varchar(255) DEFAULT NULL,
    price decimal(14,2) DEFAULT '0.00',
    date_added datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
    date_modified datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
    ups_weight decimal(5,2) DEFAULT NULL,
    thres int(11) DEFAULT NULL,
    pc varchar(6) DEFAULT NULL,
    dc int(11) DEFAULT '0',
    dc2 int(11) DEFAULT NULL,
    dc3 int(11) DEFAULT NULL,
    barcode varchar(255) DEFAULT NULL,
    `desc` text DEFAULT NULL,
    PRIMARY KEY (product_id),
    UNIQUE KEY idx_model (model),
    KEY model (model)
);

CREATE TABLE admin_transaction (
    txid char(20) NOT NULL,
    store_id int(11) NOT NULL,
    description text,
    order_user varchar(20) NOT NULL,
    approved_user varchar(12) DEFAULT NULL,
    approved_date date DEFAULT NULL,
    sold_ym char(6) DEFAULT NULL COMMENT 'for report',
    new_store char(1) DEFAULT NULL,
    term int(2) DEFAULT NULL,
    other_cost decimal(12,4) DEFAULT NULL,
    store_grade char(1) DEFAULT NULL,
    total decimal(12,2) DEFAULT NULL,
    payed_price decimal(12,2) DEFAULT NULL,
    payed_sum decimal(12,2) DEFAULT NULL,
    balance decimal(12,2) DEFAULT NULL,
    shipped_yn char(1) DEFAULT NULL,
    weight_sum decimal(12,2) DEFAULT NULL,
    order_date datetime DEFAULT NULL,
    approve_status varchar(10) DEFAULT NULL,
    invoice_no int(11) DEFAULT NULL,
    bankaccount varchar(30) DEFAULT NULL,
    executor varchar(20) DEFAULT NULL,
    readonly char(1) DEFAULT NULL,
    lift decimal(5,2) DEFAULT NULL,
    cod decimal(5,2) DEFAULT NULL,
    ship_method varchar(20) DEFAULT NULL,
    ship_appointment char(14) DEFAULT NULL,
    payment char(2) DEFAULT NULL,
    backorder int(11) DEFAULT NULL,
    shipped_by varchar(20) DEFAULT NULL,
    shipped_date char(14) DEFAULT NULL,
    billto text,
    shipto text,
    status char(1) DEFAULT NULL,
    invoice_description text,
    discount varchar(255) DEFAULT NULL,
    print_ship tinyint(4) DEFAULT '0',
    pc_date datetime DEFAULT NULL,
    post_check decimal(12,2) DEFAULT '0.00',
    cur_check decimal(12,2) DEFAULT '0.00',
    cur_cash decimal(12,2) DEFAULT '0.00',
    sign_yn char(1) DEFAULT 'N',
    subtotal decimal(12,2) DEFAULT NULL,
    PRIMARY KEY (txid),
    KEY idx_store_id (store_id)
);


CREATE TABLE admin_sales (
    id int(11) NOT NULL AUTO_INCREMENT,
    txid char(20) NOT NULL,
    product_id int(11) NOT NULL,
    model varchar(64) NOT NULL,
    order_quantity decimal(5,2) DEFAULT NULL,
    price1 decimal(12,2) DEFAULT NULL,
    price2 decimal(12,2) DEFAULT NULL,
    free int(11) DEFAULT NULL,
    damage int(11) DEFAULT NULL,
    discount decimal(5,2) DEFAULT NULL,
    total_price decimal(12,2) DEFAULT NULL,
    master_case_cnt int(3) DEFAULT NULL,
    weight_row decimal(5,2) DEFAULT NULL,
    order_date char(14) NOT NULL,
    discount2 decimal(5,2) DEFAULT NULL,
    cancel char(1) DEFAULT NULL,
    comment varchar(255) DEFAULT NULL,
    backorder int(11) DEFAULT '0',
    backfree int(11) DEFAULT '0',
    backdamage int(11) DEFAULT '0',
    shipped decimal(5,2) DEFAULT NULL,
    shipped_date char(14) DEFAULT NULL,
    promotion int(11) DEFAULT '0',
    backpromotion int(11) DEFAULT '0',
    description varchar(255) DEFAULT NULL,
    inventory int(11) DEFAULT NULL,
    PRIMARY KEY (id),
    KEY idx_txid (txid),
    KEY idx_model (model),
    KEY txid (txid)
);

CREATE TABLE admin_user (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_group_id` int(11) NOT NULL,
  `username` varchar(20) COLLATE utf8_bin NOT NULL DEFAULT '',
  `password` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `firstname` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `lastname` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `email` varchar(96) COLLATE utf8_bin NOT NULL DEFAULT '',
  `status` int(1) NOT NULL,
  `ip` varchar(15) COLLATE utf8_bin NOT NULL DEFAULT '',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `approver` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `telephone` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `companyid` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`user_id`)
);

CREATE TABLE admin_user_group  (
  `user_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_bin NOT NULL,
  `permission` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`user_group_id`)
);



CREATE TABLE `admin_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` int(1) NOT NULL DEFAULT '1',
  `top` tinyint(1) DEFAULT NULL,
  `column` int(3) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
);

CREATE TABLE `admin_category_description` (
  `category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `meta_keywords` varchar(255) COLLATE utf8_bin NOT NULL,
  `meta_description` varchar(255) COLLATE utf8_bin NOT NULL,
  `description` text COLLATE utf8_bin NOT NULL,
  `meta_keyword` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`category_id`,`language_id`),
  KEY `name` (`name`)
);

CREATE TABLE `admin_category_filter` (
  `category_id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL
);









