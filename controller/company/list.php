<?php
/*****
CREATE TABLE `company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyid` varchar(30) COLLATE utf8_bin NOT NULL DEFAULT '',
  `name` varchar(50) COLLATE utf8_bin NOT NULL DEFAULT '',
  `email` varchar(96) COLLATE utf8_bin NOT NULL DEFAULT '',
  `phone` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `status` int(1) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
)
*****/
class ControllerCompanyList extends Controller {
    private $error = array();

    // TODO. var to control access level with using library User
    private $isAdmin = false;

    // default method to show list    
    public function index() {
        $status = $this->user->isAdmin();
        if (!$status) {
            // error handle
            return false;
        }
        $this->getList();
    }

    // ajax proxy call
    public function getList() {

        # translation
        //$this->load->language('company/company');
        //$this->data['heading_title'] = $this->language->get('heading_title');

        # parcing request param
        $url = '';
        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        //$this->log->aPrint( $this->request->get );
        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'accountno';
        }
        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . $this->request->get['filter_name'];
            $filter_name = $this->request->get['filter_name'];
            $filter_name = base64_decode($filter_name);
        } else {
            $filter_name = NULL;
        }
        if (isset($this->request->get['filter_accountno'])) {
            $url .= '&filter_accountno=' . $this->request->get['filter_accountno'];
            $filter_accountno = $this->request->get['filter_accountno'];
        } else {
            $filter_accountno = NULL;
        }
        if (isset($this->request->get['filter_address1'])) {
            $url .= '&filter_address1=' . $this->request->get['filter_address1'];
            $filter_address1 = $this->request->get['filter_address1'];
        } else {
            $filter_address1 = NULL;
        }
        if (isset($this->request->get['filter_companytype'])) {
            $url .= '&filter_companytype=' . $this->request->get['filter_companytype'];
            $filter_companytype = $this->request->get['filter_companytype'];
        } else {
            $filter_companytype = NULL;
        }
        if (isset($this->request->get['filter_city'])) {
            $url .= '&filter_city=' . $this->request->get['filter_city'];
            $filter_city = $this->request->get['filter_city'];
        } else {
            $filter_city = NULL;
        }
        if (isset($this->request->get['filter_state'])) {
            $url .= '&filter_state=' . $this->request->get['filter_state'];
            $filter_state = $this->request->get['filter_state'];
        } else {
            $filter_state = NULL;
        }
        if (isset($this->request->get['filter_zipcode'])) {
            $url .= '&filter_zipcode=' . $this->request->get['filter_zipcode'];
            $filter_zipcode = $this->request->get['filter_zipcode'];
        } else {
            $filter_zipcode = NULL;
        }
        if (isset($this->request->get['filter_phone1'])) {
            $url .= '&filter_phone1=' . $this->request->get['filter_phone1'];
            $filter_phone1 = $this->request->get['filter_phone1'];
        } else {
            $filter_phone1 = NULL;  
        }
        if ( isset($this->request->get['filter_status']) && '' != $this->request->get['filter_status'] ) {
            $url .= '&filter_status=' . $this->request->get['filter_status'];
            $filter_status = $this->request->get['filter_status'];
        } else {
            $filter_status = '';
        }

        //todo. set order as DESC
        //$order = (isset($this->request->get['order'])) ? $this->request->get['order'] : '';
        $order = 'ASC';
        //todo. need to check error and success, besso-201103 
        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }
        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }
        if ($order == 'ASC') {  $url .= '&order=DESC';  } else {  $url .= '&order=ASC'; }
            $this->data['lnk_insert'] = HTTPS_SERVER . 'company/list/callUpdatePannel' . $url;
        $this->data['lnk_delete'] = HTTPS_SERVER . 'company/list/delete' . $url;

        # filter & sort
        $this->data['sort_name'] = HTTPS_SERVER . 'company/list' . '&sort=name' . $url;
        //$this->data['sort_accountno'] = HTTPS_SERVER . 'company/list' . '&sort=accountno' . $url;
        $this->data['sort_status'] = HTTPS_SERVER . 'company/list' . '&sort=status' . $url;
        $this->data['sort_companytype'] = HTTPS_SERVER . 'company/list' . '&sort=companytype' . $url;
        $this->data['sort_salesrep'] = HTTPS_SERVER . 'company/list' . '&sort=salesrep' . $url;
            $this->load->model('company/base');
        $this->data['company'] = array();
        $request = array(
            'filter_name'       => $filter_name,
            'filter_accountno'=> $filter_accountno,
            'filter_companytype'=> $filter_companytype,
            'filter_address1' => $filter_address1,
            'filter_city'     => $filter_city,
            'filter_state'    => $filter_state,
            'filter_zipcode'  => $filter_zipcode,
            'filter_phone1'   => $filter_phone1,
            'filter_status'   => $filter_status,
            'sort'            => $sort,
            'order'           => $order,
            //'start'           => ($page - 1) * $this->config->get('config_admin_limit'),
            'start'           => ($page - 1) * 100,
            //'limit'           => $this->config->get('config_admin_limit')
            'limit'           => '100'
        );
        //$this->log->aPrint( $request );
        $total = $this->model_company_base->getTotal($request);
        $list = $this->model_company_base->getList($request);

        foreach($list as $row) {
            $action = array();
            $action[] = array(
                'text' => $this->language->get('text_edit'),
                'href' => HTTPS_SERVER . 'company/list/update&id=' . $row['id'] . $url
            );
            $row['action'] = $action;
            $row['selected'] = '';
            $this->data['list'][] = $row;
        }
        $pagination = new Pagination();
        $pagination->total = $total;
        $pagination->page = $page;
        //$pagination->limit = $this->config->get('config_admin_limit');
        $pagination->limit = 100;
        $pagination->text = $this->language->get('text_pagination');
        $pagination->url = HTTPS_SERVER . 'company/list' . $url . '&page={page}';
        //$this->p(debug_backtrace()); exit;
        $this->data['pagination'] = $pagination->render();
        $this->data['total'] = $total;
        $this->data['filter_name'] = $filter_name;
        $this->data['filter_accountno'] = $filter_accountno;
        $this->data['filter_companytype'] = $filter_companytype;
        $this->data['filter_city'] = $filter_city;
        $this->data['filter_state'] = $filter_state;
        $this->data['filter_phone1'] = $filter_phone1;
        $this->data['filter_status'] = $filter_status;
        $this->data['sort'] = $sort;
        $this->data['order'] = $order;
        
        $this->template = 'company/list.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );
        $this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
    }

    // ajax call for update pannel
    public function callUpdatePannel() {
        $mode = 'insert';
        if (isset($this->request->get['id'])) {
            $id = $this->request->get['id'];
            $this->load->model('company/base');
            $this->data['item'] = $this->model_company_base->getItem($id);
            $mode = 'update';
        }
        $this->data['mode'] = $mode;
        $this->template = 'company/update.tpl';
        $this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
    }

    public function checkCompanyid() {
        $companyid = $this->request->post['companyid'];
        if ($companyid == '')   return false;
        $sql = "select id from company where companyid = '$companyid'";
        $count = $this->db->count($sql);
        if ( $count > 0 ) {
            echo json_encode(array('code'=>'500','msg'=>'Company Id already exist'));
            exit;
        }
        return true;
    }

    public function update() {
        
        $status = false;
        $this->load->model('company/base');
        $id = $this->request->post['id'];
        $mode = ($id != '') ? 'update' : 'insert';
	$ajax = true;
	if ( isset($this->request->post['ajax']) ) {
		$ajax = false;
	}
        if ('update' == $mode) {
            $status = $this->model_company_base->update($this->request->post);
        } else {
            // check for same login id (key)
            if ( ! $this->checkCompanyId() ) {
		$res = array('code'=>500,'msg'=>'Company Id already exist');
                echo json_encode($res);
            }
            $status = $this->model_company_base->insert($this->request->post);
        }
	
	$res = array('code'=>200,'msg'=>'success');
        echo json_encode($res);
    }

        // not used below
    public function generateAccount2Call() {
        $aSales = $this->user->getAllSales();
        //$this->log->aPrint( $aSales );
        $aBase = array();
        foreach($aSales as $sales) {
            $export_qry = '';
            $request = array('filter_salesrep'=>$sales['username'],'filter_balance'=>0,'start'=>0,'limit'=>500);
            $this->load->model('company/base');
            $company_total = $this->model_company_base->getTotalCompany($request);
            $aBase[$sales['username']] = $this->model_company_base->getCompany($request,$export_qry);
            //$this->log->aPrint( $company );
        }

        //$this->log->aPrint( $aBase ); exit;
        $aCompany = array();
        foreach($aBase as $rep => $company) {
            if ( $rep == $this->user->getUserName() ) {
                  foreach ($company as $row) {
                    $aDiff = array(); 
                      foreach( $row['tx'] as $tx ) {
                        $order_date = substr($tx['order_date'],0,10);
                        $order_ts = mktime(0, 0, 0, date(substr($order_date,5,2)), date(substr($order_date,8,2)), date(substr($order_date,0,4)));
                        $total = $tx['total'];
                        $tdate_ts = time(date('Y-m-d'));
                        //$this->log->aPrint( $order_ts );
                        //$this->log->aPrint( $tdate_ts );
                        $diff = round( ( $tdate_ts - $order_ts ) / ( 60 * 60 * 24 ) );
                        $diff = 182 - $diff;
                        $aDiff[$diff] = array($order_date,$total);
                    }
                    $b2Call = $this->checkRelavancy($aDiff);
                    if ($b2Call == true) {
                        $aCompany[$rep][$row['accountno']] = array(
                                'id' => $row['id'],
                                'name' => $row['name'],
                                'phone1' => $row['phone1'],
                                'lnk' => '/company/list&filter_accountno=' . $row['accountno'],
                                'last' => end($row['tx'])
                                );
                        //$aCompany[] = $row['accountno'];
                    }
                    //$this->log->aPrint( $aDiff );
                    }
            }
        }
        //$this->log->aPrint( count($aCompany) );
        //$this->log->aPrint( $aCompany );  exit;
        $this->data['company'] = $aCompany;
        $this->template = 'company/account2call.tpl';
        $this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
        //return $aCompany;
    }

    public function delete() {
        $this->load->model('company/base');
        //echo '<pre>'; print_r($this->request->post); echo '</pre>'; exit;
        if (isset($this->request->post['selected'])) {
            foreach ($this->request->post['selected'] as $id) {
                $this->model_company_base->delete($id);
            }
        }
        $this->getList();
    }

    public function getUniqAccountno() {
        //$this->log->aPrint( $this->request );
        $this->load->model('company/base');
        if ($newAccountno = $this->model_company_base->getUniqAccountno($this->request->post)) {
            echo json_encode(array('code'=>200,'message'=>$newAccountno));
        } else {
            echo json_encode(array('code'=>999,'message'=>'fail'));
        }
    }

    public function checkRelavancy($aDiff) {
        //$this->log->aPrint( $aDiff );
        $singleLimit = 90;
        $minLimit = 10;
        $cnt = count($aDiff);
        $aKeys = array_keys($aDiff);
        if ( $cnt == 1 ) {  return ($aKeys[0] < 90) ? true : false; } else {
            // need to check interval
            //$this->log->aPrint( $aKeys );
            $aInterval = array();
            $i = 0;
            foreach($aKeys as $interval) {
                if ( $i > 0 ) {
                    if ( ($aKeys[$i] - $aKeys[$i-1]) > $minLimit ) {
                        $aInterval[] = $aKeys[$i] - $aKeys[$i-1];
                    }
                    }
                if ( $i == count($aDiff) - 1 ) {  $aInterval[] = 182 - $aKeys[$i];  }
                $i++;
            }
            $avg = array_sum($aInterval) / count($aInterval);
            return ( $avg < end($aInterval) ) ? true : false;
            //$this->log->aPrint( $avg );
            //$this->log->aPrint( $aInterval );
            //$this->log->aPrint( '---------------' );
        }
    }

    public function printLabel() {
        $this->load->model('company/base');
        $idlist = $this->request->get['idlist'];
        $aList = explode(',',$idlist);
        $aCompany = array();
        foreach($aList as $list) { $aCompany[] = $this->model_company_base->getOneCompany($list); }
        $this->data['companys'] = $aCompany;
        $this->template = 'company/printLabel.tpl';
        $this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
    }



    /* Not using, refer insert() */
    /***
    public function update() {
        $this->load->model('company/base');
        if ($this->model_company_base->updateCompany($this->request->post)) {
            $this->getList();
        } else {
            echo "<script>alert('update fail');</script>";
        }
    }
      ***/

    // ajax call for update pannel
    public function callInsertPannel() {
        $ajax = isset($this->request->get['ajax']) ? true : false ;
        $from = isset($this->request->get['from']) ? $this->request->get['from'] : '' ;
        $company_id = isset($this->request->get['company_id']) ? $this->request->get['company_id'] : '';
        $this->load->model('company/base');
        $this->data['company'] = $this->model_company_base->getOneCompany($company_id);
        if ($ajax) {
            echo json_encode(array('code'=>200,'message'=>'success'));
        } else {
            //$this->data['action'] = HTTPS_SERVER . 'company/list/update';
            $this->data['action'] = HTTPS_SERVER . 'company/list/insert';
            /*
                if ('account' == $from) {
               $this->template = 'company/insertPannel2.tpl';
              } else {
               $this->template = 'company/insertPannel.tpl';
              }
               */
            $this->template = 'company/insertPannel.tpl';
            $this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
        }
    }

    public function export() {
        $this->load->language('company/list');
        $title = $this->language->get('heading_title');
        $export_qry = $this->request->get['export_qry'];
        $export_qry = urldecode($export_qry);
        //$this->log->aPrint( $export_qry ); exit;
            $ReflectionResponse = new ReflectionClass($this->response);
        if ($ReflectionResponse->getMethod('addheader')->getNumberOfParameters() == 2) {
            $this->response->addheader('Pragma', 'public');
            $this->response->addheader('Expires', '0');
            $this->response->addheader('Content-Description', 'File Transfer');
            $this->response->addheader("Content-type', 'text/octect-stream");
            $this->response->addheader("Content-Disposition', 'attachment;filename=" . $title . ".csv");
            $this->response->addheader('Content-Transfer-Encoding', 'binary');
            $this->response->addheader('Cache-Control', 'must-revalidate, post-check=0,pre-check=0');
        } else {
            $this->response->addheader('Pragma: public');
            $this->response->addheader('Expires: 0');
            $this->response->addheader('Content-Description: File Transfer');
            $this->response->addheader("Content-type:text/octect-stream");
            $this->response->addheader("Content-Disposition:attachment;filename=" . $title . ".csv");
            $this->response->addheader('Content-Transfer-Encoding: binary');
            $this->response->addheader('Cache-Control: must-revalidate, post-check=0,pre-check=0');
        }
        $this->load->model('tool/csv');
        $this->response->setOutput($this->model_tool_csv->csvExport($title,$export_qry));
    }

    public function getLatLng() {
        //$this->log->aPrint( $this->request );
        $this->load->model('company/base');
        $account = $this->request->get['account'];
        $rtn =  $this->model_company_base->getLatLng($account);
        echo json_encode($rtn);
    }

    // it communicate with iphone app 
    public function companyAccount() {
        $this->load->model('company/base');
        if ( $this->model_company_base->companyAccount($this->request->post) ) {
            echo 'TRUE';
        } else {
            echo 'FALSE';
        }
    }

    public function insertBtrip() {
            $this->load->model('company/base');
        if ( $this->model_company_base->insertBtrip($this->request->get) ) {
            echo 'TRUE';
        } else {
            echo 'FALSE';
        }
    }

    // it's for Crond to provide XML to IPhone app
    public function generateXML() {
        $filter_salesrep   = $this->util->parseRequest('filter_salesrep','get','JP');
        $this->data['filter_salesrep'] = $filter_salesrep;
        $req = array(
                //'filter_salesrep' => $filter_salesrep,
                'filter_status'   => '1'    // live account
                );
        $this->load->model('company/base');
        $aTmp = array();
        $res = $this->model_company_base->getCompany($req,$aTmp);
        //$this->log->aPrint( $res ); exit;
        // Creates the Document.
        $dom = new DOMDocument('1.0', 'UTF-8');
        $node = $dom->createElement('response');
        $responseNode = $dom->appendChild($node);
        // Iterates through the MySQL results, creating one Placemark for each row.
        $i = 0;
        foreach($res as $row) {
            // Creates a Placemark and append it to the Document.
            $accountNode = $dom->createElement('account');
            $placeNode = $responseNode->appendChild($accountNode);
            $address = $row['address1'] . $row['address2'] . ', ' . 
                $row['city'] . ', ' . $row['state'] . ' ' . $row['zipcode'];
            // Creates an id attribute and assign it the value of id column.
            $placeNode->setAttribute('id',$row['id']);
            $placeNode->setAttribute('accountno',$row['accountno']);
            $placeNode->setAttribute('salesrep',$row['salesrep']);
            //$name = '<![CDATA[' . ($row['name']) . ']]>';
            //$name = $placeNode->ownerDocument->createCDATASection($row['name']);
            //$this->log->aPrint( $name ); exit;
            $name = html_entity_decode($row['name']);
            $placeNode->setAttribute('name',$name);
            $placeNode->setAttribute('address',$address);
            $placeNode->setAttribute('tel',$row['phone1']);
            $placeNode->setAttribute('lat',$row['lat']);
            $placeNode->setAttribute('lng',$row['lng']);
            $i++;
        }
        $xmlOutput = $dom->saveXML();
        //echo $xmlOutput; exit;
        //header('Content-type: application/vnd.google-earth.kml+xml');
        $filename = date("Ymd") . '.xml';
        $fp = fopen( '/home/backyard/www/dev/view/template/company/XML/' . $filename , 'w' );
        fwrite($fp,$xmlOutput);
        fclose($fp);
    }
}
?>
