<?php
class ControllerReportCsv extends Controller { 
    public function index() {
        $this->data['lnk_export'] = HTTPS_SERVER . '/report/excel/export';
        $this->template = 'report/csv.tpl';
        $this->children = array(
            'common/header',    
            'common/footer'    
        );
        $this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
    }

    public function export() {
        $table = '';
        $query = $this->request->get['q'];

        switch ( $query ) {


            case ('categories') :
                $sql = "SELECT * from " . $this->companyid . "_category c ";
                $sql.= "  JOIN " . $this->companyid . "_category_description cd on c.category_id = cd.category_id ";
                $sql.= " order by c.category_id";
                break;

            case ('products') :
                $table = $query;
                $sql = "select p.category, p.model, p.name, p.ws_price, p.rt_price, p.quantity
                          from " . $this->companyid . "_products p
                         order by substr(p.model,3,4)";
                break;

            case ('price_base') :
                //$table = 'products';
                $sql = "select p.model, p.name, p.ws_price, p.rt_price, p.pc
                          from " . $this->companyid . "_products p
                         order by substr(p.model,3,4)";
                break;

            case ('product_base') :
                $sql = "select p.model, p.sku, p.name, p.quantity, p.cost, p.ws_price, p.rt_price, p.barcode, p.status
                      FROM " . $this->companyid . "_products p";
                break;

            case ('today_orders') :
                $sql = "SELECT *  from " . $this->companyid . "_transaction where order_date = substr(now(),1,10)" ;
                break;

            case ('yesterday_orders') :
                $sql = "SELECT *  from " . $this->companyid . "_transaction where order_date = adddate(substr(now(),1,10),-1)";
                break;

            case ('this_month') :
                $sql = "SELECT *  from " . $this->companyid . "_transaction where substr(order_date,6,2) = substr(now(),6,2)";
                break;

            case ('last_month') :
                $sql = "SELECT *  from " . $this->companyid . "_transaction where substr(order_date,6,2) = substr(adddate(now(),-30),6,2)";
                break;

            case ('accounts') :
                $sql = "SELECT * from " . $this->companyid . "_stores order by id";
                break;

            case ('stores') :

                $sql = "SELECT accountno, name, storetype, address1, address2, city, state, zipcode, phone1, phone2, fax, status
                          from " . $this->companyid . "_stores order by id";
                break;

        }
        $table = $query;
        //echo '<pre>'; print_r($sql); echo '</pre>'; exit;

        //$this->log->aPrint( $export_qry );exit;
        $ReflectionResponse = new ReflectionClass($this->response);
        if ($ReflectionResponse->getMethod('addheader')->getNumberOfParameters() == 2) {
            $this->response->addheader('Pragma', 'public');
            $this->response->addheader('Expires', '0');
            $this->response->addheader('Content-Description', 'File Transfer');
            $this->response->addheader("Content-type', 'text/octect-stream");
            $this->response->addheader("Content-Disposition', 'attachment;filename=". $table .".csv");
            $this->response->addheader('Content-Transfer-Encoding', 'binary');
            $this->response->addheader('Cache-Control', 'must-revalidate, post-check=0,pre-check=0');
        } else {
            $this->response->addheader('Pragma: public');
            $this->response->addheader('Expires: 0');
            $this->response->addheader('Content-Description: File Transfer');
            $this->response->addheader("Content-type:text/octect-stream");
            $this->response->addheader("Content-Disposition:attachment;filename=". $table .".csv");
            $this->response->addheader('Content-Transfer-Encoding: binary');
            $this->response->addheader('Cache-Control: must-revalidate, post-check=0,pre-check=0');
        }

        $this->load->model('tool/csv');
        $this->response->setOutput($this->model_tool_csv->csvExport($table,$sql));
    }
}
?>
