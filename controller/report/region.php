<?php
class ControllerReportRegion extends Controller {
    public function index() {
        isset($this->error['warning']) ? $this->data['error_warning'] = $this->error['warning'] : $this->data['error_warning'] = '';
        isset($this->session->data['success']) ? $this->data['success'] = $this->session->data['success'] : $this->data['success'] = '';
        unset($this->session->data['success']);
            $page   = $this->util->parseRequest('page','get','1');
        $sort   = $this->util->parseRequest('sort','get','');
        $order  = $this->util->parseRequest('order','get','DESC');
    
        $filter_from   = $this->util->parseRequest('filter_from','get',date("Y-m").'-01');
        $filter_to     = $this->util->parseRequest('filter_to','get',date("Y-m-t",strtotime("0 month")));
        $url = '';
        if ($page) $url.='&page='.$page;
        if ($sort) $url.='&sort='.$sort;
        if ($order) $url.='&order='.$order;
        
        $this->data['sort'] = $sort;
        $this->data['order'] = $order;
        $this->data['filter_from'] = $filter_from;
        $this->data['filter_to'] = $filter_to;
        
        $this->load->model('report/sale');
        $req = array(
          'sort'  => $sort,
          'order' => $order,
          'filter_from' => $filter_from,
          'filter_to' => $filter_to,
        );
            $kmlFilename = $filter_from . '-' . $filter_to . '.kml';
        $this->data['kmlFilename'] = $kmlFilename;
    //echo $kmlFilename;  exit;

        // cron generate state_capitals.kml automatically , it reduce DB cost
        //$res = $this->model_report_sale->stat_region($req);
        //$this->log->aPrint( $res );
        //$this->data['state'] = $res;

    // today
        //$order_total_day = 0;
        //foreach($res['today'] as $row) { $order_total_day += $row['qty']; }
        //$total_count_day = count($res['today']);
        //$this->data['today'] = $res['today'];
        //$this->data['order_total_day'] = $order_total_day;
        //$this->data['total_count_day'] = $total_count_day;
        //$this->log->aPrint( $res['today'] );

        //$order_total = 0;
        //foreach($res['this_month'] as $row) { $order_total += $row['total']; }
        //$total_count = count($res['this_month']);
        //$this->data['stat'] = $res['this_month'];
        //$this->data['order_total'] = $order_total;
        //$this->data['total_count'] = $total_count;
    //
        //$order_total_last = 0;
        //foreach($res['last_month'] as $row) { $order_total += $row['total']; }
        //$total_count_last = count($res['last_month']);
        //$this->data['lstat'] = $res['last_month'];
        //$this->data['order_total_last'] = $order_total_last;
        //$this->data['total_count_last'] = $total_count_last;

    // day
    //for($i=0;$i<7;$i++) {
    //  $week = date('w',strtotime("-1 day"));
    //  if ($week != 0 || $week != 6) {
    //    $pday_label = date('m-d(D)',strtotime("-1 day"));
    //    $pday_from = $pday_to = date('Y-m-d',strtotime("-1 day"));
    //    break;
    //  }
    //}
        //$this->data['pday_label'] = $pday_label;
        //$this->data['lnk_pday'] = HTTPS_SERVER . '/report/product&filter_from=' . $pday_from . '&filter_to=' . $pday_to;
        //$tday_label = date('m-d(D)',strtotime("0 month"));
        //$tday_from = $tday_to = date('Y-m-d',strtotime("0 month"));
        //$this->data['tday_label'] = $tday_label;
        //$this->data['lnk_tday'] = HTTPS_SERVER . '/report/product&filter_from=' . $tday_from . '&filter_to=' . $tday_to;
    
    // month
        $pmonth_label = date('Y-m',strtotime("-1 month"));
        $pmonth_from = date('Y-m-01',strtotime("-1 month"));
        $pmonth_to = date('Y-m-t',strtotime("-1 month"));
        $this->data['pmonth_label'] = $pmonth_label;
        $this->data['lnk_pmonth'] = HTTPS_SERVER . '/report/region&filter_from=' . $pmonth_from . '&filter_to=' . $pmonth_to;

    // todo. Need Label work
        $tmonth_label = date('Y-m');
        $tmonth_from = date('Y-m-01');
        $tmonth_to = date('Y-m-t');
        $this->data['tmonth_label'] = $tmonth_label;
        $this->data['lnk_tmonth'] = HTTPS_SERVER . '/report/region&filter_from=' . $tmonth_from . '&filter_to=' . $tmonth_to;
            $nmonth_label = date('Y-m',strtotime("+1 month"));
        $nmonth_from = date('Y-m-01',strtotime("+1 month"));
        $nmonth_to = date('Y-m-t',strtotime("+1 month"));
        $this->data['nmonth_label'] = $nmonth_label;
        $this->data['lnk_nmonth'] = HTTPS_SERVER . '/report/region&filter_from=' . $nmonth_from . '&filter_to=' . $nmonth_to;

    /***
    // quarter
        $pquarter_label = date('Y-m',strtotime("-6 month"));
        $pquarter_from = date('Y-m-01',strtotime("-6 month"));
        $pquarter_to = date('Y-m-t',strtotime("-6 month"));
        $this->data['pquarter_label'] = $pquarter_label;
        $this->data['lnk_pquarter'] = HTTPS_SERVER . '/report/product&filter_from=' . $pquarter_from . '&filter_to=' . $pquarter_to;
            $pquarter_label = date('Y-m',strtotime("-3 month"));
        $pquarter_from = date('Y-m-01',strtotime("-3 month"));
        $pquarter_to = date('Y-m-t',strtotime("-3 month"));
        $this->data['pquarter_label'] = $pquarter_label;
        $this->data['lnk_pquarter'] = HTTPS_SERVER . '/report/product&filter_from=' . $pquarter_from . '&filter_to=' . $pquarter_to;
      ***/
        $this->template = 'report/region.tpl';
        $this->children = array(
            'common/header',    
            'common/footer'    
        );
        
        $this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
    }

    public function generateKML() {
        $filter_from   = $this->util->parseRequest('filter_from','get',date("Y-m").'-01');
        $filter_to     = $this->util->parseRequest('filter_to','get',date("Y-m-t",strtotime("0 month")));
        $this->data['filter_from'] = $filter_from;
        $this->data['filter_to'] = $filter_to;

/*
        $filter_from = '2012-01-01';
        $filter_to = '2012-01-31';
            $filter_from = '2012-02-01';
        $filter_to = '2012-02-29';
            $filter_from = '2012-03-01';
        $filter_to = '2012-03-31';
*/
        
        $this->load->model('report/sale');
        $req = array(
          'filter_from' => $filter_from,
          'filter_to' => $filter_to,
        );
            $this->load->model('report/sale');
        $res = $this->model_report_sale->stat_region($req);
        //$this->log->aPrint( $res ); exit;

    // Creates the Document.
        $dom = new DOMDocument('1.0', 'UTF-8');
    
    // Creates the root KML element and appends it to the root document.
        $node = $dom->createElementNS('http://earth.google.com/kml/2.1', 'kml');
        $parNode = $dom->appendChild($node);
    
    // Creates a KML Document element and append it to the KML element.
        $dnode = $dom->createElement('Document');
        $docNode = $parNode->appendChild($dnode);
    
    // Creates the two Style elements, one for restaurant and one for bar, and append the elements to the Document element.
        $restStyleNode = $dom->createElement('Style');
        $restStyleNode->setAttribute('id', 'restaurantStyle');
        $restIconstyleNode = $dom->createElement('IconStyle');
        $restIconstyleNode->setAttribute('id', 'restaurantIcon');
        $restIconNode = $dom->createElement('Icon');
        //$restHref = $dom->createElement('href', 'http://maps.google.com/mapfiles/kml/pal2/icon63.png');
        $restHref = $dom->createElement('href','no showing up Marker : it spit js warning. investigate GGeoXML later');
        $restIconNode->appendChild($restHref);
        $restIconstyleNode->appendChild($restIconNode);
        $restStyleNode->appendChild($restIconstyleNode);
        $docNode->appendChild($restStyleNode);
    
        $barStyleNode = $dom->createElement('Style');
        $barStyleNode->setAttribute('id', 'barStyle');
        $barIconstyleNode = $dom->createElement('IconStyle');
        $barIconstyleNode->setAttribute('id', 'barIcon');
        $barIconNode = $dom->createElement('Icon');
        //$barHref = $dom->createElement('href', 'http://maps.google.com/mapfiles/kml/pal2/icon27.png');
        $barHref = $dom->createElement('href','no showing up Marker : it spit js warning. investigate GGeoXML later');
        $barIconNode->appendChild($barHref);
        $barIconstyleNode->appendChild($barIconNode);
        $barStyleNode->appendChild($barIconstyleNode);
        $docNode->appendChild($barStyleNode);
            $aStateLatLng = array(
        'AL'=>'-82.300568,32.377716',        'AZ'=>'-112.096962,33.448143',
        'AR'=>'-92.288986,34.746613',        'CA'=>'-121.493629,35.576668',
        'CO'=>'-104.984856,39.739227',        'CT'=>'-72.682198,41.464046',
        'DE'=>'-66.519722,39.157307',        'FL'=>'-84.281296,30.438118',
        'GA'=>'-84.388229,33.749027',        'ID'=>'-116.199722,43.617775',
        'IL'=>'-92.654961,40.198363',        'IN'=>'-86.162643,39.768623',
        'IA'=>'-93.603729,41.591087',        'KS'=>'-95.677956,39.048191',
        'KY'=>'-87.875374,38.186722',        'LA'=>'-91.187393,30.457069',
        'ME'=>'-64.781693,44.307167',        'MD'=>'-76.490936,38.978764',
        'MA'=>'-66.063698,42.058162',        'MI'=>'-84.555328,42.733635',
        'MN'=>'-93.102211,44.955097',        'MS'=>'-90.182106,32.303848',
        'MO'=>'-92.172935,36.579201',        'MT'=>'-112.018417,46.585709',
        'NE'=>'-100.699654,38.808075',        'NV'=>'-119.766121,39.163914',
        'NH'=>'-66.537994,43.206898',        'NJ'=>'-70.769913,40.220596',
        'NM'=>'-105.939728,35.682240',        'NC'=>'-78.639099,35.780430',
        'ND'=>'-100.783318,46.820850',        'NY'=>'-73.757874,42.652843',
        'OH'=>'-82.999069,40.961346',        'OK'=>'-97.503342,35.492207',
        'OR'=>'-123.030403,44.938461',        'PA'=>'-76.883598,40.264378',
        'RI'=>'-66.414963,40.830914',        'SC'=>'-75.033211,34.000343',
        'SD'=>'-100.346405,44.367031',        'TN'=>'-86.784241,36.165810',
        'TX'=>'-97.740349,30.274670',        'UT'=>'-111.888237,40.777477',
        'VT'=>'-72.580536,44.262436',        'VA'=>'-77.433640,37.538857',
        'WA'=>'-122.905014,47.035805',        'WV'=>'-81.612328,38.336246',
        'WI'=>'-91.384445,43.074684',        'WY'=>'-104.820236,41.140259'
    );

    // Iterates through the MySQL results, creating one Placemark for each row.
        $i = 0;
        foreach($res as $state => $sum) {
        // Creates a Placemark and append it to the Document.
        $node = $dom->createElement('Placemark');
        $placeNode = $docNode->appendChild($node);
    
        // Creates an id attribute and assign it the value of id column.
        $placeNode->setAttribute('id', 'placemark' . $i);
    
        // Create name, and description elements and assigns them the values of the name and address columns from the results.
        $name = $state . ':' . $sum;
        $nameNode = $dom->createElement('name',$name);
        $placeNode->appendChild($nameNode);
        $descNode = $dom->createElement('description', $sum);
        $placeNode->appendChild($descNode);
        $styleUrl = $dom->createElement('styleUrl', '#' . 'bar' . 'Style');
        $placeNode->appendChild($styleUrl);
    
        // Creates a Point element.
        $pointNode = $dom->createElement('Point');
        $placeNode->appendChild($pointNode);
    
        // Creates a coordinates element and gives it the value of the lng and lat columns from the results.
        $coorStr = $aStateLatLng[$state];
        $coorNode = $dom->createElement('coordinates', $coorStr);
        $pointNode->appendChild($coorNode);
        $i++;
    }
        $kmlOutput = $dom->saveXML();
    //echo $kmlOutput; exit;
    header('Content-type: application/vnd.google-earth.kml+xml');
    
    //echo $filter_from;
    //echo $filter_to;
        $filename = $filter_from . '-' . $filter_to . '.kml';
        $fp = fopen( '/home/backyard/www/view/template/report/KML/' . $filename , 'w' );
    fwrite($fp,$kmlOutput);
    fclose($fp);
    }
}?>
