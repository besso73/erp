<?php
class ControllerStatementList extends Controller {

    public $export_sql = '';

    public function index() {
        $this->getList();
    }
    
    public function importQB() {
        $this->load->model('statement/list');
        $this->model_ar_list->importQB();
    }

    public function getList() {
        isset($this->error['warning']) ? $this->data['error_warning'] = $this->error['warning'] : $this->data['error_warning'] = '';
        isset($this->request->get['page']) ?    $page = $this->request->get['page'] : $page = 1;
        isset($this->request->get['sort']) ? $sort = $this->request->get['sort'] :  $sort = 's.accountno';
        $url = '';
    
        if (isset($this->request->get['filter_store_name'])) {
            $url .= '&filter_store_name=' . $this->request->get['filter_store_name'];
            $filter_store_name = $this->request->get['filter_store_name'];
        } else {
            $filter_store_name = NULL;
        }
        
        if (isset($this->request->get['filter_bankaccount'])) {
            $url .= '&filter_bankaccount=' . $this->request->get['filter_bankaccount'];
            $filter_bankaccount = $this->request->get['filter_bankaccount'];
        } else {
            $filter_bankaccount = NULL;
        }
        if (isset($this->request->get['filter_txid'])) {
            $url .= '&filter_txid=' . $this->request->get['filter_txid'];
            $filter_txid = $this->request->get['filter_txid'];
        } else {
            $filter_txid = NULL;
        } 
    
        if (isset($this->request->get['filter_order_date_from'])) {
            $url .= '&filter_order_date_from=' . $this->request->get['filter_order_date_from'];
            $filter_order_date_from = $this->request->get['filter_order_date_from'];
        } else {
            
            $weekago = date('Y-m-d', strtotime('-7 days'));
            $filter_order_date_from = $weekago;
        }
        if (isset($this->request->get['filter_order_date_to'])) {
            $url .= '&filter_order_date_to=' . $this->request->get['filter_order_date_to'];
            $filter_order_date_to = $this->request->get['filter_order_date_to'];
        } else {
            $tdate = date('Y-m-d');
            $filter_order_date_to = $tdate;  
        }
        if (isset($this->request->get['filter_total'])) {
            $url .= '&filter_total=' . $this->request->get['filter_total'];
            $filter_total = $this->request->get['filter_total'];
        } else {
            $filter_total = NULL;  
        }
        if (isset($this->request->get['filter_memo'])) {
            $url .= '&filter_memo=' . $this->request->get['filter_memo'];
            $filter_memo = $this->request->get['filter_memo'];
        } else {
            $filter_memo = NULL;  
        }
        if ( isset($this->request->get['filter_paid_type']) && '' != $this->request->get['filter_paid_type'] ) {
            $url .= '&filter_paid_type=' . $this->request->get['filter_paid_type'];
            $filter_paid_type = $this->request->get['filter_paid_type'];
        } else {
            $filter_paid_type = NULL;  
        }

        # AR don't need User filtering
        if (isset($this->request->get['filter_order_user'])) {
            $url .= '&filter_order_user=' . $this->request->get['filter_order_user'];
            $filter_order_user = $this->request->get['filter_order_user'];
        } else {
            $filter_order_user = NULL;  
        }
        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';  
        }
        $order == 'ASC' ? $url .= '&order=DESC' : $url .= '&order=ASC';

        # link
        $this->data['lnk_insert'] = HTTP_SERVER . '/statement/order' . $url;
        $this->data['lnk_delete'] = HTTP_SERVER . '/statement/list/delete' . $url;
        $this->data['lnk_import'] = HTTP_SERVER . '/statement/list/importQB' . $url;

        # filter & sort
        $this->data['sort_store_name'] = HTTP_SERVER . '/statement/list' . '&sort=name' . $url;
        $this->data['sort_order_date'] = HTTP_SERVER . '/statement/list' . '&sort=x.order_date' . $url;
        // let's do for additional requirement , besso-201103 
        //$this->data['sort_accountno'] = HTTP_SERVER . 'store/lookup/' . $this->session->data['token'] . '&sort=accountno' . $url;
        //$this->data['sort_state'] = HTTP_SERVER . 'store/lookup/' . $this->session->data['token'] . '&sort=state' . $url;
        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'DESC';
        }

        # call data
        $this->load->model('statement/list');
        $this->data['list'] = array();
        $request = array(
            'filter_store_name'   => $filter_store_name,
            'filter_bankaccount'      => $filter_bankaccount,
            'filter_txid'     => $filter_txid,
            'filter_order_date_from'=> $filter_order_date_from,
            'filter_order_date_to'=> $filter_order_date_to,
            'filter_total' => $filter_total,
            'filter_memo'=> $filter_memo,
            'filter_paid_type'     => $filter_paid_type,
            'filter_order_user'  => $filter_order_user,
            'sort'            => $sort,
            'order'           => $order,
            'start'           => ($page - 1) * $this->config->get('config_admin_limit'),
            'limit'           => $this->config->get('config_admin_limit')
        );
        
        $total = $this->model_statement_list->getTotalList($request);
        $response = $this->model_statement_list->getList($request,$this->export_sql);
        $sum = $this->model_statement_list->getSumList($request);
        isset($sum) ? $this->data['sum'] = $sum[0] : $this->data['sum'] = array() ;

        # bad temporary
        /* besso blocked at 2014-01
        $sqlFile = "/home/backyard/www/data/sql";
        $handle = @fopen($sqlFile,'w');
        fwrite($handle,$this->export_sql);
        fclose($handle);
        */
        $this->data['export'] = HTTPS_SERVER . 'statement/list/export';
        $this->data['total'] = $total;

        $this->load->model('statement/base');

        foreach ( $response as $row ) {
            $action = array();
            $action[] = array(
                'text' => 'Edit',
                'href' => HTTP_SERVER . 'statement/detail' . '&store_id=' . $row[0]['store_id']
            );

            $_sid = $row[0]['statement_id'];
            $aSum = $this->model_statement_base->getStoreBalance($row[0]['store_id']);

            $row[0]['sum'] = $aSum[$_sid];
            $row[0]['action'] = $action;
            $row[0]['selected'] = isset($this->request->post['selected']) && in_array($result['id'], $this->request->post['selected']);
            $this->data['list'][] = $row;
        
        }

        //print 'total cnt : ' . $total;
        $pagination = new Pagination();
        $pagination->total = $total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_admin_limit');
        $pagination->text = $this->language->get('text_pagination');
        $pagination->url = HTTP_SERVER . '/statement/list' . $url . '&page={page}';
        $this->data['pagination'] = $pagination->render();
        
            
        $this->data['filter_store_name'] = $filter_store_name;
        $this->data['filter_bankaccount'] = $filter_bankaccount;
        $this->data['filter_txid'] = $filter_txid;
        $this->data['filter_total'] = $filter_total;
        $this->data['filter_order_date_from'] = $filter_order_date_from;
        $this->data['filter_order_date_to'] = $filter_order_date_to;
        $this->data['filter_memo'] = $filter_memo;
        $this->data['filter_paid_type'] = $filter_paid_type;
        $this->data['filter_order_user'] = $filter_order_user;
            
        $this->data['sort'] = $sort;
        $this->data['order'] = $order;
        

        $this->template = 'statement/list.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );
        $this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
    }

    public function add_date($givendate,$day=0,$mth=0,$yr=0) {
        $givendate = $givendate. ' 00:00:00';
        $cd = strtotime($givendate);
        $newdate = date('Y-m-d', mktime(date('h',$cd),
                    date('i',$cd), date('s',$cd), date('m',$cd)+$mth,
                    date('d',$cd)+$day, date('Y',$cd)+$yr));
        return $newdate;
    }

    public function sheet() {

        $statement_id = isset($this->request->get['id']) ? : false;
        if ( !$statement_id ) {
            echo '<pre>'; print_r('No Statement ID'); echo '</pre>';
            return false;
        }

        $request = array(
            'statement_id'   => $statement_id,
        );
        
        $this->load->model('statement/list');
        $res = $this->model_statement_list->getList($request);
        $aStatement = $res[$statement_id];

        $this->load->model('sales/order');
        foreach ($aStatement as $i => $s) {
            if ($aSales = $this->model_sales_order->getSales($s['txid'])) {
                $aStatement[$i]['items'] = $aSales;
            }
        }
        $this->data['list'] = $aStatement;
        //echo '<pre>'; print_r($aStatement); echo '</pre>'; exit;

        $store_id = $aStatement[0]['store_id'];

        $req = array(
            'companyid' => $this->companyid
        );
        $this->load->model('company/base');
        $res = $this->model_company_base->getList($req);
        $aCompany = $res[0];
        $this->data['company'] = $aCompany;

        $this->load->model('sales/order');
        if ($aStore = $this->model_sales_order->selectStore($store_id)) {
            if ( empty($aStore['shipto']) ) {
               $shipto = '<ul class="shipto"><li>' . $aStore['name'] . '</li><li>' . $aStore['address1'] . '</li><li>' . $aStore['city'] . ', ' . $aStore['state'] . ', ' . $aStore['zipcode'] . '</li><li>TEL : ' . $aStore['phone1'] . '</li></ul>';
               $aStore['shipto'] = $shipto;
            }
            if ( empty($aStore['billto']) ) {
               $aStore['billto'] = $shipto;
            }
            $this->data['store'] = $aStore;
        }
        
        /***
            if ($res = $this->model_sales_order->selectTransaction($txid)) {
                $this->data['txid'] = $res['txid'];
                $this->data['store_id']   = $res['store_id'];
                $this->data['description'] = $res['invoice_description'];
                $this->data['salesrep'] = $res['order_user'];
                $this->data['total_total'] = $res['total'];  
                $this->data['order_date']    = $res['order_date'];
                $this->data['other_cost']    = $res['other_cost'];
                $this->data['weight_sum']    = $res['weight_sum'];
                $this->data['balance']    = $res['balance'];
                $this->data['payed_sum']    = $res['payed_sum'];
                $this->data['term']    = $res['term'];
                $this->data['approved_user']    = $res['approved_user'];
                $this->data['approved_date']    = $res['approved_date'];
                $this->data['approve_status']    = $res['approve_status'];
                $this->data['shipped_yn']    = $res['shipped_yn'];
                $this->data['invoice_no']    = $res['invoice_no'];
                $this->data['billto']    = $res['billto'];
                $this->data['shipto']    = $res['shipto'];
                
                $this->data['payment']   = $res['payment'];
                $this->data['shipped_date']  = substr($res['shipped_date'],0,10);
                $this->data['shipped_by']    = $res['shipped_by'];
                $this->data['ship_method']   = $res['ship_method'];
                $this->data['store_dc']    = $res['discount'];
                
                $this->data['store_discount_percent']    = $res['discount'];
                $this->data['total_total'] = $res['total'];
                $this->data['subtotal_total'] = $res['subtotal'];
                
                $this->data['cod']    = $res['cod'];
                $this->data['lift']    = $res['lift'];
        
                //# catalog for salesa
                //$this->catalog = $this->config->getCatalogMobile();
                //$this->data['catalog'] = $this->catalog;
        
                // firstname retrieve
                $this->data['firstname'] = $this->user->getFirstName($res['order_user']);
                // decide whether you are approver or not.
                if ('sales' == $this->user->getGroupName($this->data['salesrep'])) {
                    $approver = $this->user->getApprover($this->data['salesrep']);
                    if (''!=$approver) {
                        $approver = explode(',',$approver);
                        if (in_array($this->user->getUserName(),$approver)) {
                             $this->bApprove = true;
                        }
                    }
                    }
            } else {
            // todo. use modal for any fail and mail to it team , besso-201103
            echo 'selectTransaction fail';
            exit;
            }
            //$this->log->aPrint( $this->data );
        
            if ($res = $this->model_sales_order->selectStore($this->data['store_id'])) {
               $this->data['store_name']   = $res['name'];
               $this->data['storetype']    = $res['storetype'];
               $this->data['accountno']    = $res['accountno'];
               $this->data['address1']   = $res['address1'];
               $this->data['city']   = $res['city'];
               $this->data['state']  = $res['state'];
               $this->data['zipcode']    = $res['zipcode'];
               $this->data['phone1']    = $res['phone1'];
               $this->data['fax']    = $res['fax'];
            } else {
               echo 'selectStore fail';
               exit;
            }
        
            if ($res = $this->model_sales_order->selectStoreARTotal($this->data['store_id'])) {
            $this->data['store_ar_total'] = $res;
            } else {
               //echo 'selectStoreARTotal fail';
               //exit;
            }
        
            if ($res = $this->model_sales_order->selectStoreHistory($this->data['store_id'])) {
            $this->data['store_history'] = $res;
            } else {
               //echo 'selectStoreHistory fail';
               //exit;
            }
        
            # for mode::show, show prepare new tied data set with sales table
            if ($res = $this->model_sales_order->getSales($txid)) {
                    $this->data['sales'] = $res;
            }
            
            if ($res = $this->model_sales_order->selectShip($this->data['txid'])) {
            $this->data['ship'] = $res;
            } else {
               // todo. no result
               // echo 'selectShip fail';
               $this->data['ship'] = array();
            }
        
            if ($res = $this->model_sales_order->selectPay($this->data['txid'])) {
            $this->data['pay'] = $res;
            } else {
               // todo. no result
               //echo 'selectPay fail';
               //exit;
            }
        
            $this->data['ddl'] = 'update';
        */
        
        $this->template = 'statement/sheet.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));

    }

    public function delete() {
        $this->load->model('statement/list');
        $deleteList =  $this->request->post['selected'];
        foreach($deleteList as $txid) {
            $this->model_ar_list->deleteTransaxtion($txid);
        }
            $this->session->data['success'] = "Delete done : " . count($deleteList);
        $this->redirect(HTTP_SERVER . '/statement/list');
        $this->getList();
    }

    public function export() {
        $title = 'statement';

        // bad temporary
        $sqlFile = "/home/backyard/www/data/sql";
        $handle = @fopen($sqlFile,'r');
        $export_qry = fread($handle,8192);
        fclose($handle);
        //$this->log->aPrint( $export_qry ); exit;
            $ReflectionResponse = new ReflectionClass($this->response);
        if ($ReflectionResponse->getMethod('addheader')->getNumberOfParameters() == 2) {
            $this->response->addheader('Pragma', 'public');
            $this->response->addheader('Expires', '0');
            $this->response->addheader('Content-Description', 'File Transfer');
            $this->response->addheader("Content-type', 'text/octect-stream");
            $this->response->addheader("Content-Disposition', 'attachment;filename=" . $title . ".csv");
            $this->response->addheader('Content-Transfer-Encoding', 'binary');
            $this->response->addheader('Cache-Control', 'must-revalidate, post-check=0,pre-check=0');
        } else {
            $this->response->addheader('Pragma: public');
            $this->response->addheader('Expires: 0');
            $this->response->addheader('Content-Description: File Transfer');
            $this->response->addheader("Content-type:text/octect-stream");
            $this->response->addheader("Content-Disposition:attachment;filename=" . $title . ".csv");
            $this->response->addheader('Content-Transfer-Encoding: binary');
            $this->response->addheader('Cache-Control: must-revalidate, post-check=0,pre-check=0');
        }
        $this->load->model('tool/csv');
        $this->response->setOutput($this->model_tool_csv->csvExport($title,$export_qry));
    }
}
?>
