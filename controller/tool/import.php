<?php
class ControllerToolImport extends Controller {
    
    private $error = array();
    
    public function index() {

        //echo '<pre>'; print_r($this->request); echo '</pre>'; exit;

        if (!isset($this->session->data['token'])) {
            $this->session->data['token'] = 0;
        }
        
        $this->load->language('tool/csv');
        $this->document->title = $this->language->get('heading_title');
        $this->load->model('tool/csv');

        if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validate()) {
            //header('Content-Type: text/html; charset=euc-kr'); 
            //setlocale(LC_ALL, 'en_US.UTF-8');
          
            //$this->log->aPrint(  iconv_get_encoding() );
        

            $filename = $this->request->files['csv_import']['tmp_name'];

            //$table = $this->request->files['csv_import']['name'];
            $table = $this->request->post['table'];

            $enc = mb_detect_encoding($filename,'auto');
            //echo $enc; exit;
            // if a charanter is valid ASCII, it's valid UTF8 , subjet of Unicode 
            if ('iso-8859-1' == $enc) {
                $filename = iconv('iso-8859-1','utf-8//TRANSLIT//IGNORE',$filename);
            }
            
            if (is_uploaded_file($filename)) {
                $content = file_get_contents($filename);
                //$this->log->aPrint( $content );
            } else {
                $content = false;
            }
            
            if ($content) {

                $this->model_tool_csv->csvImport($filename, $table);
                $this->session->data['success'] = $this->language->get('text_success');
                $this->redirect(HTTPS_SERVER . '/tool/import');
            } else {
                $this->error['warning'] = $this->language->get('error_empty');
            }
        }

        $this->data['heading_title'] = $this->language->get('heading_title');
        $this->data['text_select_all'] = $this->language->get('text_select_all');
        $this->data['text_unselect_all'] = $this->language->get('text_unselect_all');
        $this->data['entry_export'] = $this->language->get('entry_export');
        $this->data['entry_import'] = $this->language->get('entry_import');
        $this->data['button_backup'] = $this->language->get('button_backup');
        $this->data['button_restore'] = $this->language->get('button_restore');
        $this->data['tab_general'] = $this->language->get('tab_general');

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }
        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }
        
        $this->document->breadcrumbs = array();
        $this->document->breadcrumbs[] = array(
           'href'      => HTTPS_SERVER . '/common/home',
           'text'      => $this->language->get('text_home'),
          'separator' => FALSE
        );
        $this->document->breadcrumbs[] = array(
           'href'      => HTTPS_SERVER . '/tool/import',
           'text'      => $this->language->get('heading_title'),
          'separator' => ' :: '
        );

        $this->data['restore'] = HTTPS_SERVER . '/tool/import';
        $this->data['csv'] = HTTPS_SERVER . '/tool/import/import';
        $this->data['csv_import'] = HTTPS_SERVER . 'tool/import';
        $this->data['csv_export'] = HTTPS_SERVER . '/tool/import/csvImport';

        $this->data['tables'] = $this->model_tool_csv->getTables();
        $this->template = 'tool/import.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );
        $this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
    }

    private function validate() {
        if (!$this->user->hasPermission('modify', 'tool/import')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }
        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function product () {
        echo '<pre>'; print_r("Developing"); echo '</pre>';
        //echo '<pre>'; print_r($this->request); echo '</pre>';
    }
}
?>
