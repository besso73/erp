<?php
//controller/store/atc.php
class ControllerStoreATC extends Controller {

    private $error = array();

    public function accounts() {
         
        $query = isset($this->request->get['query']) ?  $this->request->get['query'] : '';
        
        $this->load->model('store/store');
        $cache_key = $this->model_store_store->table->stores;
        $accounts = $this->cache->get($cache_key);
        $idx = 0;
        $result = array();
        if ( empty($accounts) ) {
            // TODO cache later with crond job
            $msg = 'Fail to get the cached products for auto-complete with key : ' . $cache_key ;
            $this->log->write($msg);
            $accounts = $this->model_store_store->getStore();
        }

        foreach($accounts as $store) {

            if ($idx == ATC_MAX_RETURN) break;
            $query = strtolower($query);
                
            if (strstr($query,' ')) {
                $aWord = explode(' ',$query);
                $ptn = '(.*)';
                  foreach($aWord as $word) {
                    $ptn .= '(' . $word . ')(.*)';
                    }
            } else {
                $ptn = $query;
            }
            
            if ( preg_match("/$ptn/", strtolower($store['name'])) ) {
                $result[] = $store;
                $idx++;
                continue;
            }
/*
            if ( preg_match("/$ptn/", strtolower($store['model'])) ) {
                $result[] = $store;
                $idx++;
                continue;
            }
*/
        }
        echo json_encode($result);
    }

}
?>
