<?php
class ControllerProductPhistory extends Controller {
     public function index() {
        $this->load->model('product/phistory');
        $this->getList();
    }

     private function getList() {
        isset($this->error['warning']) ? $this->data['error_warning'] = $this->error['warning'] : $this->data['error_warning'] = '';
        isset($this->session->data['success']) ? $this->data['success'] = $this->session->data['success'] : $this->data['success'] = '';
        unset($this->session->data['success']);
            $page   = $this->util->parseRequest('page','get','1');
        $sort   = $this->util->parseRequest('sort','get','ph.up_date');
        $order  = $this->util->parseRequest('order','get','ASC');
        $filter_model    = $this->util->parseRequest('filter_model','get');
        $filter_from     = $this->util->parseRequest('filter_from','get',date('Y-m-d'));
        $filter_to       = $this->util->parseRequest('filter_to','get',date('Y-m-d'));
        $url = '';
        if ($page) $url.='&page='.$page;
        if ($sort) $url.='&sort='.$sort;
        if ($order) $url.='&order='.$order;
        if ($filter_model) $url.='&filter_model='.$filter_model;
        if ($filter_from)  $url.='&filter_from='.$filter_from;
        if ($filter_to)    $url.='&filter_to='.$filter_to;
    //echo 'url : ' . $url; 
        $this->data['heading_title'] = 'Product - product history';
        $this->data['text_enabled'] = $this->language->get('text_enabled');
        $this->data['text_disabled'] = $this->language->get('text_disabled');
        $this->data['text_no_results'] = $this->language->get('text_no_results');
        $this->data['text_image_manager'] = $this->language->get('text_image_manager');
        $this->document->breadcrumbs = array();
        $this->document->breadcrumbs[] = array(
           'href'      => HTTPS_SERVER . '/common/home',
      'text'      => $this->language->get('text_home'),
      'separator' => FALSE
       );
        $this->document->breadcrumbs[] = array(
      'href'      => HTTPS_SERVER . '/product/phistory' . $url,
      'text'      => $this->data['heading_title'],
      'separator' => ' :: '
       );
        $this->data['phistorys'] = array();
        $data = array(
            'filter_model'        => $filter_model,
            'filter_from'        => $filter_from,
            'filter_to'          => $filter_to,
            'sort'            => $sort,
            'order'           => $order,
            'start'           => ($page - 1) * $this->config->get('config_admin_limit'),
            'limit'           => $this->config->get('config_admin_limit')
        );
        $phistory_total = $this->model_product_phistory->getTotalPhistory($data);
        //$this->log->aPrint( $phistory_total ); exit;
        $results = $this->model_product_phistory->getPhistory($data);
        //$this->log->aPrint( $results ); exit;
        foreach ($results as $result) {
      /* no update / insert
        $action = array();
        $action[] = array(
                'text' => $this->language->get('text_edit'),
                'href' => HTTPS_SERVER . '/product/phistory/update' . '&id=' . $result['id'] . $url
            );
              */
        $this->data['phistory'][] = array(
                //'id'       => $result['id'],
                //'code'     => $result['code'],
              'name'     => $result['name'],
              'final'    => $result['final'],
              'model'    => $result['model'],
                //'quantity' => $result['quantity'],
                //'cat'      => $result['cat'],
              'up_date'  => $this->util->date_format_kr($result['up_date'],true),
              'diff'     => $result['pdiff'],
              'rep'      => $result['rep'],
                //'comment'  => $result['comment'],
                //'selected' => isset($this->request->post['selected']) && in_array($result['id'], $this->request->post['selected']),
                //'action'   => $action
          );
    }
        //$this->data['button_copy'] = $this->language->get('button_copy');
        //$this->data['button_insert'] = $this->language->get('button_insert');
        //$this->data['button_delete'] = $this->language->get('button_delete');
        //$this->data['button_filter'] = $this->language->get('button_filter');
        
        
        $this->data['sort_model'] = HTTPS_SERVER . '/product/phistory' . '&sort=p.name' . $url;
        $pagination = new Pagination();
        $pagination->total = $phistory_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_admin_limit');
        $pagination->text = $this->language->get('text_pagination');
        $pagination->url = HTTPS_SERVER . '/product/phistory' . $url . '&page={page}';
        $this->data['pagination'] = $pagination->render();
        
        $this->data['filter_model'] = $filter_model;
        $this->data['filter_from'] = $filter_from;
        $this->data['filter_to'] = $filter_to;
        $this->data['sort'] = $sort;
        $this->data['order'] = $order;
        $this->template = 'product/phistory.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );
        $this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
    }
}
?>
