<?php
class ControllerProductUnit extends Controller {

    private $error = array();
    public $export_qry = '';
    public $title = '';
    
    public function index() {
        $this->load->language('product/unit');
        $this->document->title = $this->language->get('heading_title');
        $this->title =  $this->language->get('heading_title');
        $this->load->model('product/unit');
        $this->getList();
    }

    private function getList() {
      // get main categories for Unit mapping
      $categories = $this->getSubCategories2(0);
      $this->data['categories'] = $categories;
      
      $this->template = 'product/unit.tpl';
      $this->children = array(
        'common/header',
        'common/footer'
      );
      $this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
    }

    // todo, change with category_id
    public function getSubCategories2($code) {
        // refer catalog/category for all category related stuff
        $this->load->model('catalog/category');
        $aCategories = $this->model_catalog_category->getSubCategories($code);
        return $aCategories;
    }
    
    /* for ajax */
    public function getSubCategories() {
        $cid = $this->request->get['cid'];
        $this->load->model('product/base');
        $aSubCategories = $this->model_product_base->getSubCategoriesById($cid);
        
        if ( !empty($aSubCategories) ) {
            echo json_encode($aSubCategories);
        }
        echo '';
    }
    
    /* ajax retrieve each Unit set as HTML */
    public function displayUnits() {
      $category_id = $this->request->get['category_id'];
      $this->load->model('product/unit');
      $categories = $this->model_product_unit->getProductsByCategoryId($category_id);
      
      $html = '<ul id="list">';
      foreach( $categories as $cat ) {
        $html .= '<li id="'. $cat['product_id'] .'" class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>'. $cat['name'] .'</li>';
      }
      $html .= '</ul><ul id="unit"></ul>';
      
      //echo '<pre>'; print_r($categories); echo '</pre>';  
      echo $html;
    }
/******************************************/


    public function export() {
        $this->load->language('product/unit');
        $title = $this->language->get('heading_title');
        $export_sql = $this->request->get['export_sql'];    
        $ReflectionResponse = new ReflectionClass($this->response);
        if ($ReflectionResponse->getMethod('addheader')->getNumberOfParameters() == 2) {
            $this->response->addheader('Pragma', 'public');
            $this->response->addheader('Expires', '0');
            $this->response->addheader('Content-Description', 'File Transfer');
            $this->response->addheader("Content-type', 'text/octect-stream");
            $this->response->addheader("Content-Disposition', 'attachment;filename=" . $title . ".csv");
            $this->response->addheader('Content-Transfer-Encoding', 'binary');
            $this->response->addheader('Cache-Control', 'must-revalidate, post-check=0,pre-check=0');
        } else {
            $this->response->addheader('Pragma: public');
            $this->response->addheader('Expires: 0');
            $this->response->addheader('Content-Description: File Transfer');
            $this->response->addheader("Content-type:text/octect-stream");
            $this->response->addheader("Content-Disposition:attachment;filename=" . $title . ".csv");
            $this->response->addheader('Content-Transfer-Encoding: binary');
            $this->response->addheader('Cache-Control: must-revalidate, post-check=0,pre-check=0');
        }
        $this->load->model('tool/csv');
        $this->response->setOutput($this->model_tool_csv->csvExport($title,$export_sql));
    }

    public function insertCategory() {
        $this->load->model('product/base');
        if ( $this->model_product_base->insertCategory($this->request->post) ) {
            echo json_encode(array('code'=>200));
        }
        echo json_encode(array('code'=>500));   
    }

    public function insert() {
        $this->load->language('product/unit');
        $this->document->title = $this->language->get('heading_title');
            $this->load->model('product/unit');
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_product_price->addProduct($this->request->post);
            $this->session->data['success'] = $this->language->get('text_success');
            $url = '';
            if (isset($this->request->get['filter_name'])) {
                $url .= '&filter_name=' . $this->request->get['filter_name'];
            }
            if (isset($this->request->get['filter_model'])) {
                $url .= '&filter_model_from=' . $this->request->get['filter_model_from'];
            }
        /***
            if (isset($this->request->get['filter_model_to'])) {
                $url .= '&filter_model_to=' . $this->request->get['filter_model_to'];
            }
          ***/
            if (isset($this->request->get['filter_price'])) {
                $url .= '&filter_price=' . $this->request->get['filter_price'];
            }
    
            if (isset($this->request->get['filter_quantity'])) {
                $url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
            }
    
            if (isset($this->request->get['filter_status'])) {
                $url .= '&filter_status=' . $this->request->get['filter_status'];
            }
    
            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
    
            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }
    
            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }
    
            $this->redirect(HTTPS_SERVER . '/product/unit' . $url);
        }
        $this->getForm();
        }
    
    public function update() {
            $this->load->model('product/unit');
        $req = array();
        if (isset($this->request->get['product_id'])) {
            $product_id = $this->request->get['product_id'];
        }
        if (isset($this->request->get['ws_price'])) {
            $key = 'ws_price';
            $val = $this->request->get['ws_price'];
        }
        if (isset($this->request->get['rt_price'])) {
            $key = 'rt_price';
            $val = $this->request->get['rt_price'];
        }
        if (isset($this->request->get['quantity'])) {
            $key = 'quantity';
            $val = $this->request->get['quantity'];
        }
        $this->load->model('product/unit');
        $this->model_product_price->update($product_id,$key,$val);
        }
    
    public function updatePackage() {
        $this->load->model('product/unit');
        $this->model_product_price->updatePackage($this->request->get);
    }
    
    public function delete() {
        $this->load->model('product/base');
        //echo '<pre>'; print_r($this->request->post); echo '</pre>'; exit;
        if (isset($this->request->post['selected'])) {
            foreach ($this->request->post['selected'] as $id) {
                $this->model_product_base->delete($id);
            }
        }
        $this->getList();
    }
    
    public function copy() {
        $this->load->language('product/unit');
        $this->document->title = $this->language->get('heading_title');
        $this->load->model('product/unit');
    
        if (isset($this->request->post['selected']) && $this->validateCopy()) {
            foreach ($this->request->post['selected'] as $product_id) {
                $this->model_product_price->copyProduct($product_id);
            }
    
            $this->session->data['success'] = $this->language->get('text_success');
    
            $url = '';
    
            if (isset($this->request->get['filter_name'])) {
                $url .= '&filter_name=' . $this->request->get['filter_name'];
            }
    
            if (isset($this->request->get['filter_model'])) {
                $url .= '&filter_model=' . $this->request->get['filter_model'];
            }
    
            if (isset($this->request->get['filter_price'])) {
                $url .= '&filter_price=' . $this->request->get['filter_price'];
            }
    
            if (isset($this->request->get['filter_quantity'])) {
                $url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
            }
    
            if (isset($this->request->get['filter_status'])) {
                $url .= '&filter_status=' . $this->request->get['filter_status'];
            }
    
            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
    
            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }
            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }
            $this->redirect(HTTPS_SERVER . '/product/unit' . $url);
        }
        $this->getList();
    }
    


   
    // not fo ajax

    
    
    public function callUpdatePannel() {

        
        // TODO. please set cache for it later, besso
        $aCategories = $this->getSubCategories2(0);
            $this->data['categories'] = $aCategories;
            $mode = isset($this->request->get['mode']) ? $this->request->get['mode'] : 'insert';
        if ('insert' == $mode) {
            $this->data['action'] = HTTPS_SERVER . 'product/unit/insertProduct';
        } else {
            $id = $this->request->get['product_id'];
            $this->load->model('product/unit');
            $this->data['data'] = $this->model_product_price->getOneData($id);
            $this->data['action'] = HTTPS_SERVER . '/product/unit/insertProduct';
            
            /*
            $code = $this->data['data']['category'];
            $this->load->model('catalog/category');
            $aCategory = $this->model_catalog_category->getCategory($code);
            $this->data['data']['parent_id'] = $aCategory['parent_id'];
              */
        }
            $this->data['mode'] = $mode;
        $this->template = 'product/updatePannel.tpl';
        $this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
    }
    
    /* not using, see insertProduct() */
    /*
    public function updateProduct() {
        //$this->log->aPrint( $this->request );     exit;
        $this->load->model('product/unit');
        if ($this->model_product_price->updateProduct($this->request->post)) {
            $this->getList();
        } else {
            echo "<script>alert('update fail');</script>";
        }
    }
      */
    
    public function insertProduct() {
        
        $status = false;
        $this->load->model('product/unit');
        $mode = isset($this->request->post['mode']) ? $this->request->post['mode'] : 'insert';
        if ('update' == $mode) {
            $status = $this->model_product_price->updateProduct($this->request->post);
        } else {
            $status = $this->model_product_price->insertProduct($this->request->post);
        }
        if ($status) {
            echo json_encode(array('code'=>200));
        } else {
            echo json_encode(array('code'=>500));
        }
    }
    
    public function updateBarcode() {
        $this->load->model('product/unit');
        if ($this->model_product_price->updateBarcode($this->request->post)) {
            echo json_encode(array('code'=>200));
        } else {
            echo json_encode(array('code'=>500));
        }
    }
    
    public function updatePrice() {
        $this->load->model('product/unit');
        if ($this->model_product_price->updatePrice($this->request->get)) {} else {}
    }
    
    public function updateThres() {
        $this->load->model('product/unit');
        if ($this->model_product_price->updateThres($this->request->get)) {} else {}
    }
    public function updateQuantity2() {
        $this->load->model('product/unit');
        if ($this->model_product_price->updateQuantity2($this->request->get)) {} else {}
    }
    
    
    public function lookupProductHistory() {
        $code = $this->request->get['code'];
        $cmd = 'grep -ri ' . $code . ' /home/backyard/www/system/logs/inventory.log | tail -20';
        echo '<pre>';
        system($cmd);
        echo '</pre>';
    }
    
    public function getProductByModel() {
        $this->load->model('product/unit');
        
        $model = $this->request->post['model'];
        $aProduct = $this->model_product_price->getProductByModel($model);
        if (count($aProduct) > 0) {
            echo json_encode(array('code'=>'200','message'=>'exist'));
        } else {
            echo json_encode(array('code'=>'500','message'=>'no exist'));
        }
    }
}
?>
