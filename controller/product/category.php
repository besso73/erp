<?php 
class ControllerProductCategory extends Controller {

    private $error = array();

    public function index() {
        $this->language->load('catalog/category');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('catalog/category');
        //$this->getListTree();
        $this->getList();
    }

    public function insert() {
        $this->language->load('catalog/category');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('catalog/category');
        
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

            $mode = isset($this->request->post['mode']) ? $this->request->post['mode'] : 'insert';

            if ( $mode == 'insert' ) {
                $this->model_catalog_category->addCategory($this->request->post);
            } else {
                $this->model_catalog_category->editCategory($this->request->post);
            }
            $this->session->data['success'] = $this->language->get('text_success');
            $url = '';
            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            $this->redirect($this->url->link('product/category', $url, 'SSL')); 
        }
        $this->getForm();

    }

    // decide 
    public function decideCategoryCode() {

        $pcode = isset($this->request->post['pcode']) ? $this->request->post['pcode'] : '';
        $name  = isset($this->request->post['name'])  ? $this->request->post['name']  : '';

        $this->load->model('catalog/category');
        if ( !empty($pcode) ) {
            $subCodes = $this->model_catalog_category->getSub2Codes($pcode);
        }
        $decided = '';

        if ( $pcode == '0' ) $pcode = '';

        if ( !empty($subCodes) ) {
            
            $aName = explode(' ', $name);

            if ( count($aName) > 1 ) {
                
                // prefix first two
                $prefix2 = strtoupper(substr($aName[0],0,1)) . strtoupper(substr($aName[1],0,1));
                if ( !in_array($prefix2, $subCodes) ) {
                    echo $pcode . $prefix2; exit;
                }

            }

            
            if ( count($aName) == 1 ) {            
                // first 2 
                $first2 = strtoupper(substr($aName[0],0,2));
                if ( !in_array($first2, $subCodes) ) {
                    echo $pcode . $first2; exit;
                }

                // first and last
                $firstLast = strtoupper(substr($aName[0],0,1)) . strtoupper(substr($aName[0],-1));
                if ( !in_array($firstLast, $subCodes) ) {
                    echo $pcode . $firstLast; exit;
                }

                // first 1 + number
                for ( $i=0 ; $i<10; $i++ ) {
                    $firstNum = strtoupper(substr($aName[0],0,1)) . $i;
                    if ( !in_array($firstNum, $subCodes) ) {
                        echo $pcode . $firstNum; exit;
                    }
                }

                // last2
                $last2 = strtoupper(substr($aName[0],-2));
                if ( !in_array($last2, $subCodes) ) {
                    echo $pcode . $last2; exit;
                }

            }

        } else { // no subCodes

            $aName = explode(' ', $name);

            if ( count($aName) > 1 ) {
                // prefix first two
                $prefix2 = strtoupper(substr($aName[0],0,1)) . strtoupper(substr($aName[1],0,1));
                echo $pcode . $prefix2; exit;
            }

            
            if ( count($aName) == 1 ) {            
                // first 2 
                $first2 = strtoupper(substr($aName[0],0,2));
                echo $pcode . $first2; exit;
            }

        }

        echo 'error';

    }


    public function update() {

        $this->language->load('catalog/category');
        $this->document->setTitle($this->language->get('heading_title'));
        
        $this->load->model('catalog/category');
        
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

            $this->model_catalog_category->editCategory($this->request->get['category_id'], $this->request->post);
            
            $this->session->data['success'] = $this->language->get('text_success');
            
            $url = '';

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
                        
            $this->redirect($this->url->link('catalog/category', $url, 'SSL'));
        }
        $this->getForm();

    }

    public function delete() {
        $this->language->load('catalog/category');
        $this->document->setTitle($this->language->get('heading_title'));
    
        $this->load->model('catalog/category');
        
        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $category_id) {
                $this->model_catalog_category->deleteCategory($category_id);
            }

            $this->session->data['success'] = $this->language->get('text_success');
            
            $url = '';

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            
            $this->redirect($this->url->link('product/category', $url, 'SSL'));
        }
        
        $this->getList();
    }
    
    public function repair() {
        $this->language->load('catalog/category');
            $this->document->setTitle($this->language->get('heading_title'));
        
        $this->load->model('catalog/category');
        
        if ($this->validateRepair()) {
            $this->model_catalog_category->repairCategories();

            $this->session->data['success'] = $this->language->get('text_success');
            
            $this->redirect($this->url->link('catalog/category', 'token=' . $this->session->data['token'], 'SSL'));
        }
        
        $this->getList();   
    }

    public function getListTree() {
            
        $this->load->model('catalog/category');
        $categories = $this->model_catalog_category->getCategoriesNameTree(0);

        //echo '<pre>'; print_r($categories); echo '</pre>';

        $aCsv = array();
        foreach ($categories as $first => $aSecond ) {
            foreach ($aSecond as $second => $aThird) {
                  foreach ($aThird as $third => $dummy) {
                    //$aCsv[] = $first . ','  . $second . ',' . $third;
                    $aCsv[] = array($first, $second, $third);
                    }
            }
        }

/*
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=test.csv");
        header("Pragma: no-cache");
        header("Expires: 0");
*/
        //echo '<pre>'; print_r($aCsv); echo '</pre>';
        $fp = fopen('/tmp/test.csv', 'w');
        foreach ($aCsv as $fields) {
            fputcsv($fp, $fields);
        }
        fclose($fp);


    }

    protected function getList() {
        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';
        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }
        
        // todo. prepare common method for breadcrumbs, besso 201401
        $this->data['breadcrumbs'] = array();
            $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('product/category', $url, 'SSL'),
            'separator' => ' :: '
        );

        $this->data['insert'] = $this->url->link('product/category/insert', $url, 'SSL');
        $this->data['delete'] = $this->url->link('product/category/delete', $url, 'SSL');
        $this->data['repair'] = $this->url->link('product/category/repair', $url, 'SSL');
        $this->data['categories'] = array();
        $data = array(
            'start' => ($page - 1) * $this->config->get('config_admin_limit'),
            'limit' => $this->config->get('config_admin_limit')
        );
        $category_total = $this->model_catalog_category->getTotalCategories();
        $this->data['total'] = $category_total;
         
        // TODO. set parent_id as 0 manually , besso 201401
        $parent_id = 0;
        $results = $this->model_catalog_category->getCategories($parent_id);

        foreach ($results as $result) {
            $action = array();

            $action[] = array(
                'text' => $this->language->get('text_edit'),
                'href' => $this->url->link('product/category/update', '&category_id=' . $result['category_id'] . $url, 'SSL')
            );

            $this->data['categories'][] = array(
                'category_id' => $result['category_id'],
                'name'        => $result['name'],
                'sort_order'  => $result['sort_order'],
                'selected'    => isset($this->request->post['selected']) && in_array($result['category_id'], $this->request->post['selected']),
                'action'      => $action
            );
        }

        $this->data['heading_title'] = $this->language->get('heading_title');
        $this->data['text_no_results'] = $this->language->get('text_no_results');
        $this->data['column_name'] = $this->language->get('column_name');
        $this->data['column_sort_order'] = $this->language->get('column_sort_order');
        $this->data['column_action'] = $this->language->get('column_action');
        $this->data['button_insert'] = $this->language->get('button_insert');
        $this->data['button_delete'] = $this->language->get('button_delete');
        $this->data['button_repair'] = $this->language->get('button_repair');

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }
        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }

        $pagination = new Pagination();
        $pagination->total = $category_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_admin_limit');
        $pagination->text = $this->language->get('text_pagination');
        $pagination->url = $this->url->link('product/category', $url . '&page={page}', 'SSL');
        $this->data['pagination'] = $pagination->render();
            /* for tree nativation quick
        $url = '';
        
        $this->data['button_insert'] = $this->language->get('button_insert');
        $this->data['button_delete'] = $this->language->get('button_delete');
            $this->data['insert'] = $this->url->link('product/category/insert', $url, 'SSL');
        $this->data['delete'] = $this->url->link('product/category/delete', $url, 'SSL');
        
        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }
        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }
        
        $category_total = $this->model_catalog_category->getTotalCategories();
        $this->data['total'] = $category_total;
            $this->categories_json();

          ***/
            $this->template = 'catalog/category_list.tpl';
            $this->children = array(
            'common/header',
            'common/footer'
        );
            $this->response->setOutput($this->render(TRUE));
    }

    public function mapview() {
        
        $this->language->load('catalog/category');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('catalog/category');

        $url = '';
        
        $this->data['button_insert'] = $this->language->get('button_insert');
        $this->data['button_delete'] = $this->language->get('button_delete');
        $this->data['insert'] = $this->url->link('product/category/insert', $url, 'SSL');
        $this->data['delete'] = $this->url->link('product/category/delete', $url, 'SSL');
        
        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }
        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }
        
        $category_total = $this->model_catalog_category->getTotalCategories();
        $this->data['total'] = $category_total;
            $this->categories_json();

            $this->template = 'catalog/category_mapview.tpl';
            $this->children = array(
            'common/header',
            'common/footer'
        );
        $this->response->setOutput($this->render(TRUE));
    }

    public function categories_json() {
        $this->load->model('catalog/category');
        $categories = $this->model_catalog_category->getCategories(0);
        $json_string = json_encode($categories);
        $file = DIR_DATA . '/categories.json';
        file_put_contents($file, $json_string);
    }

    protected function getForm() {

        $this->data['heading_title'] = $this->language->get('heading_title');
        $this->data['text_none'] = $this->language->get('text_none');
        $this->data['text_default'] = $this->language->get('text_default');
        $this->data['text_image_manager'] = $this->language->get('text_image_manager');
        $this->data['text_browse'] = $this->language->get('text_browse');
        $this->data['text_clear'] = $this->language->get('text_clear');     
        $this->data['text_enabled'] = $this->language->get('text_enabled');
        $this->data['text_disabled'] = $this->language->get('text_disabled');
        $this->data['text_percent'] = $this->language->get('text_percent');
        $this->data['text_amount'] = $this->language->get('text_amount');
        $this->data['entry_name'] = $this->language->get('entry_name');
        $this->data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
        $this->data['entry_meta_description'] = $this->language->get('entry_meta_description');
        $this->data['entry_description'] = $this->language->get('entry_description');
        $this->data['entry_parent'] = $this->language->get('entry_parent');
        $this->data['entry_filter'] = $this->language->get('entry_filter');
        $this->data['entry_store'] = $this->language->get('entry_store');
        $this->data['entry_keyword'] = $this->language->get('entry_keyword');
        $this->data['entry_image'] = $this->language->get('entry_image');
        $this->data['entry_top'] = $this->language->get('entry_top');
        $this->data['entry_column'] = $this->language->get('entry_column');     
        $this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
        $this->data['entry_status'] = $this->language->get('entry_status');
        $this->data['entry_layout'] = $this->language->get('entry_layout');
        $this->data['button_save'] = $this->language->get('button_save');
        $this->data['button_cancel'] = $this->language->get('button_cancel');
        $this->data['tab_general'] = $this->language->get('tab_general');
        $this->data['tab_data'] = $this->language->get('tab_data');
        $this->data['tab_design'] = $this->language->get('tab_design');
        $this->data['entry_meta_keywords'] = $this->language->get('entry_meta_keywords');
        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }
        if (isset($this->error['name'])) {
            $this->data['error_name'] = $this->error['name'];
        } else {
            $this->data['error_name'] = array();
        }
            $this->data['breadcrumbs'] = array();
            $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );
            $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('product/category', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );
        
        if (!isset($this->request->get['category_id'])) {
            $this->data['action'] = $this->url->link('product/category/insert', 'token=' . $this->session->data['token'], 'SSL');
        } else {
            $this->data['action'] = $this->url->link('product/category/update', '&category_id=' . $this->request->get['category_id'], 'SSL');
        }
        
        $this->data['cancel'] = $this->url->link('product/category', 'token=' . $this->session->data['token'], 'SSL');

        // get category info with code
        if (isset($this->request->get['category_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $category_info = $this->model_catalog_category->getCategoryById($this->request->get['category_id']);
        }
        
        
        $this->load->model('localisation/language');
        
        $this->data['languages'] = $this->model_localisation_language->getLanguages();

        if (isset($this->request->post['category_description'])) {
            $this->data['category_description'] = $this->request->post['category_description'];
        } elseif (isset($this->request->get['category_id'])) {
            $this->data['category_description'] = $this->model_catalog_category->getCategoryDescriptions($this->request->get['category_id']);
        } else {
            $this->data['category_description'] = array();
        }

        if (isset($this->request->post['path'])) {
            $this->data['path'] = $this->request->post['path'];
            if (!empty($category_info)) {
                $this->data['path'] = $category_info['path'];
            }
        } else {
            $this->data['path'] = '';
        }
        
        if (isset($this->request->post['parent_id'])) {
            $this->data['parent_id'] = $this->request->post['parent_id'];
        } elseif (!empty($category_info)) {
            $this->data['parent_id'] = $category_info['parent_id'];
        } else {
            $this->data['parent_id'] = 0;
        }

        if (isset($this->request->post['code'])) {
            $this->data['code'] = $this->request->post['code'];
        } elseif (!empty($category_info)) {
            $this->data['code'] = $category_info['code'];
            $this->data['name'] = $category_info['name'];
            $this->data['category_id'] = $category_info['category_id'];
            $this->data['mode'] = 'update';
        } else {
            $this->data['code'] = 0;
            $this->data['mode'] = 'insert';
        }

        $this->load->model('product/category');
        
        if (isset($this->request->post['category_filter'])) {
            $filters = $this->request->post['category_filter'];
        } elseif (isset($this->request->get['category_id'])) {      
            $filters = $this->model_product_category->getCategoryFilters($this->request->get['category_id']);
        } else {
            $filters = array();
        }
    
        $this->data['category_filters'] = array();
        
        foreach ($filters as $filter_id) {
            $filter_info = $this->model_catalog_filter->getFilter($filter_id);
            
            if ($filter_info) {
                $this->data['category_filters'][] = array(
                    'filter_id' => $filter_info['filter_id'],
                    'name'      => $filter_info['group'] . ' &gt; ' . $filter_info['name']
                );
            }
        }   

        //echo '<pre>'; print_r($category_info); echo '</pre>';

        $parent_id = 0;

        $results = $this->model_catalog_category->getCategories($parent_id);

        foreach ($results as $result) {
            if ( isset($category_info) ) {
                if ( $result['category_id'] == $category_info['category_id'] ) continue;
            }
            $this->data['categories'][] = array(
                'category_id' => $result['category_id'],
                'name'        => $result['name'],
                'code'        => $result['code'],
                'sort_order'  => $result['sort_order'],
            );
        }
                              
        $this->load->model('setting/store');
        
        $this->data['stores'] = $this->model_setting_store->getStores();
        
        if (isset($this->request->post['category_store'])) {
            $this->data['category_store'] = $this->request->post['category_store'];
        } elseif (isset($this->request->get['category_id'])) {
            $this->data['category_store'] = $this->model_catalog_category->getCategoryStores($this->request->get['category_id']);
        } else {
            $this->data['category_store'] = array(0);
        }           
        
        if (isset($this->request->post['keyword'])) {
            $this->data['keyword'] = $this->request->post['keyword'];
        } else {
            $this->data['keyword'] = '';
        }

        $this->load->model('tool/image');
        $no_image = $this->model_tool_image->resize('no_image.jpg', 100, 100);
        if (isset($this->request->post['image'])) {
            $this->data['image'] = $this->request->post['image'];
        } elseif (!empty($category_info)) {
            $this->data['image'] = $category_info['image'];
        } else {
            $this->data['image'] = $no_image;
        }
        if ( empty($this->data['image']) ) {
            $this->data['image'] = $no_image;
        }
        if (isset($this->request->post['image']) && file_exists(DIR_IMAGE . $this->request->post['image'])) {
            $this->data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
        } elseif (!empty($category_info) && $category_info['image'] && file_exists(DIR_IMAGE . $category_info['image'])) {
            $this->data['thumb'] = $this->model_tool_image->resize($category_info['image'], 100, 100);
        } else {
            $this->data['thumb'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
        }
        
        //if ( )        
        //$this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
        if (isset($this->request->post['top'])) {
            $this->data['top'] = $this->request->post['top'];
        } elseif (!empty($category_info)) {
            $this->data['top'] = $category_info['top'];
        } else {
            $this->data['top'] = 0;
        }
        
        if (isset($this->request->post['column'])) {
            $this->data['column'] = $this->request->post['column'];
        } elseif (!empty($category_info)) {
            $this->data['column'] = $category_info['column'];
        } else {
            $this->data['column'] = 1;
        }
                
        if (isset($this->request->post['sort_order'])) {
            $this->data['sort_order'] = $this->request->post['sort_order'];
        } elseif (!empty($category_info)) {
            $this->data['sort_order'] = $category_info['sort_order'];
        } else {
            $this->data['sort_order'] = 0;
        }
        
        if (isset($this->request->post['status'])) {
            $this->data['status'] = $this->request->post['status'];
        } elseif (!empty($category_info)) {
            $this->data['status'] = $category_info['status'];
        } else {
            $this->data['status'] = 1;
        }
        
        /* we don't need layout. simple and straight, besso */        
        /*
        if (isset($this->request->post['category_layout'])) {
            $this->data['category_layout'] = $this->request->post['category_layout'];
        } elseif (isset($this->request->get['category_id'])) {
            $this->data['category_layout'] = $this->model_catalog_category->getCategoryLayouts($this->request->get['category_id']);
        } else {
            $this->data['category_layout'] = array();
        }
            $this->load->model('design/layout');
        
        $this->data['layouts'] = $this->model_design_layout->getLayouts();
          */
                        
        $this->template = 'catalog/category_form.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );
                
        $this->response->setOutput($this->render());
    }

    protected function validateForm() {
        if (!$this->user->hasPermission('modify', 'product/category')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        /***
        foreach ($this->request->post['category_description'] as $language_id => $value) {
            if ((utf8_strlen($value['name']) < 2) || (utf8_strlen($value['name']) > 255)) {
                $this->error['name'][$language_id] = $this->language->get('error_name');
            }
        }
          ***/

        foreach ($this->request->post['category_description'] as $language_id => $value) {
            if (( strlen(utf8_decode($value['name'])) < 2) || (strlen(utf8_decode($value['name'])) > 255)) {
               $this->error['name'][$language_id] = $this->language->get('error_name');
            }
        }
        if ($this->error && !isset($this->error['warning'])) {
            $this->error['warning'] = $this->language->get('error_warning');
        }
                    
        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }
    
    protected function validateDelete() {
        if (!$this->user->hasPermission('modify', 'product/category')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }
            if (!$this->error) {
            return true; 
        } else {
            return false;
        }
    }
    
    protected function validateRepair() {
        if (!$this->user->hasPermission('modify', 'product/category')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }
            if (!$this->error) {
            return true; 
        } else {
            return false;
        }
    }
            
    public function autocomplete() {
        $json = array();
        
        if (isset($this->request->get['filter_name'])) {
            $this->load->model('catalog/category');
            
            $data = array(
                'filter_name' => $this->request->get['filter_name'],
                'start'       => 0,
                'limit'       => 20
            );
            
            $results = $this->model_catalog_category->getCategories($data);
                
            foreach ($results as $result) {
                $json[] = array(
                    'category_id' => $result['category_id'], 
                    'name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
                );
            }       
        }
            $sort_order = array();
      
        foreach ($json as $key => $value) {
            $sort_order[$key] = $value['name'];
        }

        array_multisort($sort_order, SORT_ASC, $json);
            $this->response->setOutput(json_encode($json));
    }       
}
?>
