<?php
class ControllerSalesOrder extends Controller {

    private $error = array();
    private $bApprove = false;
    private $bManager = false;
    private $catalog = array();
    private $isMobile = false;
    private $isDebug  = false;
        
    public function index() {
        
        $this->help = true;

        $this->isDebug = ( isset($this->request->get['debug']) ) ? true : false ;
        //$this->log->aPrint( $this->isDebug );
        //todo. let's make global var 'isMobile'
        $this->isMobile = ( isset($this->request->get['mobile']) ) ? true : false ;
        //echo 'start index';
        $this->load->language('sales/order');
        $this->document->title = $this->language->get('heading_title');
        $this->bApprove = false;
        $this->bManager = false;
        // todo. more simply way please
        if ('manager' == $this->user->getGroupName($this->user->getUserName()) ) {
            $this->bManager = true;
            $this->bApprove = true;
        }
        $this->data['bManager'] = $this->bManager;
        
        $this->load->model('sales/order');
        if (isset($this->request->get['txid'])) {
              isset($this->request->get['mode']) ? $mode = $this->request->get['mode'] : $mode = 'edit';
                //isset($this->request->post['mode']) ? $mode = $this->request->post['mode'] : $mode = 'edit';
                $this->callOrderForm($this->request->get['txid'],$mode);
        } else {
            $this->callOrderForm();
        }
    }

    public function callLockedPannel() {
        $model = $this->request->get['model'];
        $this->load->model('sales/order');
        $this->data['qty'] = $this->model_sales_order->getSalesQuantity($model);
        //$aLocked = $this->model_sales_order->getSalesQuantity($model);
        //$this->load->library('json');
        //$this->response->setOutput(Json::encode($data));
        $this->template = 'sales/lockedPannel.tpl';
        $this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
    }

    public function updateApprove() {
        //$this->log->aPrint( $this->request->get );
        isset($this->request->get['txid']) ?    $txid = $this->request->get['txid'] : $txid = '';
        isset($this->request->get['status']) ?  $status = $this->request->get['status'] : $status = '';
        $this->load->model('sales/order');
        # AR total
        if ($this->model_sales_order->updateApprove($txid,$status)) { echo 'success'; }
        }
        public function arHistory() {
        //$this->log->aPrint( $this->request->get );
        isset($this->request->get['store_id']) ?    $store_id = $this->request->get['store_id'] : $store_id = '';
        $this->data['store_id'] = $store_id;
        $this->load->model('sales/order');
        # AR total
        if ($res = $this->model_sales_order->selectStoreARTotal($this->data['store_id'])) {
            $this->data['store_ar_total'] = $res;
        }
        if ($res = $this->model_sales_order->selectStoreHistory($this->data['store_id'])) {
            $this->data['store_history'] = $res;
        }
        //$this->log->aPrint( $res );
        $this->template = 'sales/arHistory.tpl';
        $this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
        }
        public function qbHistory() {  // tmp
        //$this->log->aPrint( $this->request->get );
        isset($this->request->get['store_id']) ?    $store_id = $this->request->get['store_id'] : $store_id = '';
        $this->data['store_id'] = $store_id;
        $this->load->model('sales/order');
        # history
        if ($res = $this->model_sales_order->quickbookHistory($this->data['store_id'])) {
            $this->data['store_history'] = $res;
        }
        //$this->log->aPrint( $res );
        $this->template = 'sales/qbHistory.tpl';
        $this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
    }

    // display order default form
    private function callOrderForm( $txid = '', $mode='edit' ) {
        
        //$this->log->aPrint( $this->request ); exit;
        
        /*
        $this->load->model('catalog/category');
        $category_total = $this->model_catalog_category->getTotalCategories();
        $parent_id = 0;
        $this->catalog = $this->model_catalog_category->getCategories($parent_id);
          */
        
        /*
        $this->catalog = $this->model_catalog_category->getCatalog();
        $this->data['catalog'] = $this->catalog;
          */
        
        $this->data['mode'] = $mode;

        # translate
        $this->data['button_save']   = $this->language->get('button_save');
        $this->data['button_cancel'] = $this->language->get('button_cancel');
        $this->data['heading_title'] = $this->language->get('heading_title');
        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }
        
        # data from model
        $url = '';  // query param
        if ($txid) {
            if ($res = $this->model_sales_order->selectTransaction($txid)) {
                $this->data['mode'] = 'update';
                
                $this->data['txid'] = $res['txid'];
                $this->data['status'] = $res['status'];
                $this->data['executor'] = $res['executor'];
                $this->data['store_id']   = $res['store_id'];
                $this->data['description'] = $res['description'];
                $this->data['salesrep'] = $res['order_user'];
                $this->data['total_total'] = $res['total'];
                $this->data['subtotal_total'] = $res['subtotal'];
                $this->data['total_discount_price'] = $res['total'] - $res['subtotal'];
                
                // TODO. Need to check subtotal
                $this->data['subtotal_total'] = $res['subtotal'];
                
                $this->data['order_date']    = substr($res['order_date'],0,10);
                $this->data['other_cost']    = $res['other_cost'];
                $this->data['weight_sum']    = $res['weight_sum'];
                $this->data['balance']    = $res['balance'];
                $this->data['payed_sum']    = $res['payed_sum'];
                $this->data['term']    = $res['term'];
                $this->data['approved_user']    = $res['approved_user'];
                $this->data['approved_date']    = $res['approved_date'];
                $this->data['approve_status']    = $res['approve_status'];
                $this->data['shipped_yn']    = $res['shipped_yn'];
                $this->data['invoice_no']    = $res['invoice_no'];
                $this->data['payment']     = $res['payment'];
                $this->data['ship_method'] = $res['ship_method'];
                $this->data['ship_cod']    = $res['cod'];
                $this->data['ship_lift']   = $res['lift'];
                $this->data['ship_appointment']   = $res['ship_appointment'];
                $this->data['shipto']      = $res['shipto'];
                $this->data['store_discount_percent']    = $res['discount'];

                $this->data['pc_date'] = ( isset($res['pc_date']) ) ? substr($res['pc_date'],0,10) : '' ;
                $this->data['post_check'] = $res['post_check'];
                $this->data['cur_check']  = $res['cur_check'];
                $this->data['cur_cash']   = $res['cur_cash'];

                // firstname retrieve
                $this->data['firstname'] = $this->user->getFirstName($res['order_user']);
                // decide whether you are approver or not.
                if ('sales' == $this->user->getGroupName($this->data['salesrep'])) {
                    $approver = $this->user->getApprover($this->data['salesrep']);
                    if (''!=$approver) {
                        $approver = explode(',',$approver);
                        if (in_array($this->user->getUserName(),$approver)) {
                             $this->bApprove = true;
                        }
                    }
                    }
                
            } else {
                // todo. use modal for any fail and mail to it team , besso-201103
                echo 'selectTransaction fail';  exit;
            }
            
            //$this->log->aPrint( $this->data ); exit;
            if ($res = $this->model_sales_order->selectStore($this->data['store_id'])) {
                $this->data['store_name']   = $res['name'];
                //$this->data['store_name']   = htmlspecialchars($res['name']);
                //$this->log->aPrint( $this->data['store_name'] );
                $this->data['storetype']    = $res['storetype'];
                $this->data['accountno']    = $res['accountno'];
                $this->data['address1']     = $res['address1'];
                $this->data['city']         = $res['city'];
                $this->data['state']        = $res['state'];
                $this->data['zipcode']      = $res['zipcode'];
                $this->data['phone1']       = $res['phone1'];
                $this->data['fax']          = $res['fax'];
                //$this->data['store_dc']    = $res['discount'];
                # shipto , shipto managing
                $address = $res['name'] . '\n' .
                            $res['address1'] . '\n' .
                            $res['city'] . "," . $res['state'] . "," . $res['zipcode'] . '\n' .
                           "TEL : " . $res['phone1'];
                if ($this->data['shipto'] == "") {
                  $this->data['shipto'] = $res['shipto'];
                    }
                if ( $this->data['shipto'] == "" ) $this->data['shipto'] = $address;
            } else {
                echo 'selectStore fail';  exit;
            }
            
            /* Ar later, besso
            if ($res = $this->model_sales_order->selectStoreARTotal($this->data['store_id'])) {
                  $this->data['store_ar_total'] = $res;
                    } else {}
                if ($res = $this->model_sales_order->selectStoreHistory($this->data['store_id'])) {
                    $this->data['store_history'] = $res;
            }
              */
           
            /* Sales from product/lib but sales/order */
            /*
            $this->load->model('product/lib');
            if ($res = $this->model_product_lib->getProductOrdered($this->data['txid'])) {
                $this->data['sales'] = $res;
            }
              */

            if ($res = $this->model_sales_order->getSales($txid)) {
                $this->data['sales'] = $res;
            }

            /* TODO not yet freegood , besso
            if ($res = $this->model_sales_order->selectFreegoodSum($this->data['txid'])) {
                $this->data['freegood_amount'] = $res;
            }
              */

            /*
            if ($res = $this->model_sales_order->selectShip($this->data['txid'])) {
                $this->data['ship'] = $res;
            } else {
                $this->data['ship'] = array();
            }
            if ($res = $this->model_sales_order->selectPay($this->data['txid'])) {
                $this->data['pay'] = $res;
            } else {
                // todo. no result
                //echo 'selectPay fail';
                //exit;
            }
              */

            

        } else {  // No txid
            # store
            $this->data['txid'] = '';
            $this->data['executor'] = '';
            $this->data['store_id']   = '';
            $this->data['store_name']   = '';
            $this->data['storetype']    = '';
            $this->data['store_dc']     = '';
            $this->data['accountno']    = '';
            $this->data['address1']   = '';
            $this->data['city']   = '';
            $this->data['state']  = '';
            $this->data['zipcode']    = '';
            $this->data['phone1']    = '';
            $this->data['fax']    = '';
            $this->data['description']    = '';
            $this->data['approved_user']  = '';
            $this->data['approved_date']  = '';
            $this->data['approve_status'] = '';
            $this->data['shipped_yn'] = 'N';
            $this->data['payment'] = 'cc';
            $this->data['ship_method'] = 'pck';
            $this->data['ship_cod'] = 0;
            $this->data['ship_lift'] = '0';
            $this->data['ship_appointment'] = $this->util->date_format_kr(date('Ymdhis'));
            $this->data['pc_date'] = '';
            $this->data['post_check'] = '0';
            $this->data['cur_check'] = '0';
            $this->data['cur_cash'] = '0';
            $this->data['invoice_no'] = '';
            $this->data['shipto'] = '';
            // todo. differ from insert/update
            $this->data['salesrep'] = $this->user->getUserName();
            $this->data['firstname'] = $this->user->getFirstName();
            //$this->data['user_id'] = $this->user->getId();
            //set default value
            $this->data['term']    = '30';
            $this->data['weight_sum']    = '0';
            $this->data['balance']    = '0';
            $this->data['payed_sum']    = '0';
            $this->data['total']    = '0';
            $this->data['order_date']    = $this->util->date_format_kr(date('Ymdhis'));
            $this->data['ship'] = array();
            $this->data['pay'][0]['pay_price']    = '0';
            $this->data['pay'][0]['pay_method']    = 'cash';
            $this->data['pay'][0]['pay_date']    = '';
            $this->data['pay'][0]['pay_num']    = '';
            $this->data['pay'][0]['pay_user']    = '';
            $this->data['freegood_amount']    = '0';
            $this->data['freegood_percent']    = '0';
            $this->data['mode'] = 'insert';
            $this->data['status'] = '1';

            $this->data['total_total'] = '';
            $this->data['subtotal_total'] = '';
            $this->data['store_discount_percent'] = '';
            $this->data['total_discount_price'] = '';
            $this->data['store_discount_price'] = '';
        } // no txid
        
        /* custom order chart 1 - tees */
        $tees = $this->getTees();
        $this->data['tees'] = $tees;
        
        $stockings = $this->getStockings();
        $this->data['stockings'] = $stockings;
        
        $hats = $this->getHats();
        $this->data['hats'] = $hats;
            $adores = $this->getAdores();
        $this->data['adores'] = $adores;

/*
        $belts = $this->getBelts();
        $this->data['belts'] = $belts;
*/

        # common for insert/update
        $this->data['bApprove'] = $this->bApprove;
        // [todo] it's ugly way to pass session
        $this->data['token']    = $this->session->data['token'];
        # lnk
        $this->data['lnk_cancel'] = HTTP_SERVER . '/sales/order' . $url;
        $this->data['lnk_list'] = HTTP_SERVER . '/sales/list' . $url;
        //$this->data['order_action'] = HTTP_SERVER . 'sales/order/saveOrder';
        //$this->data['order_action'] = HTTP_SERVER . 'index.php?route=sales/order/saveOrder';
        //$this->data['order_action'] = '/sales/order/saveOrder';
        $this->data['order_action'] = '/sales/order/saveOrder';
        
        if (isset($this->request->get['invoice'])) {
            $this->data['order_action'] .= '&invoice=true';
        }
        
        if ($this->isMobile == true) {
            $this->template = 'sales/mobile/order.tpl';
        } else {
            $this->template = 'sales/order.tpl';
        }
        if ($this->isDebug == true)  $this->template = 'sales/order_debug.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );
        $this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
    }
    
    public function saveOrder() {
       
        //$this->log->aPrint( $this->request->post ); exit;
    
        $this->load->model('sales/order');
        
        /* 
           * Generate Transaction ID
           */
        $salesrep = $this->request->post['salesrep'];
        $store_id = $this->request->post['store_id'];
        $mode = $this->request->post['mode'];
        $status = isset($this->request->post['status']) ? $this->request->post['status'] : 0;
        
        //
        $ajax = $this->request->post['ajax'];
        //echo '<pre>'; print_r($ajax); echo '</pre>'; exit;
        if ( $ajax == '1') {
            $status = 9;
        }
        
        $async = $this->request->post['async'];
        $accountno = $this->request->post['accountno'];
        $store_name = $this->request->post['store_name'];
        $storetype = $this->request->post['storetype'];
        $phone1 = $this->request->post['phone1'];
        $fax = $this->request->post['fax'];
        $city = $this->request->post['city'];
        $state = $this->request->post['state'];
        $zipcode = $this->request->post['zipcode'];
        $address1 = $this->request->post['address1'];
        $order_date = $this->request->post['order_date'];
        $description = $this->request->post['description'];
        $ship_id = $this->request->post['ship_id'];
        $ship_user = $this->request->post['ship_user'];
        $shipped_yn = $this->request->post['shipped_yn'];
        $ship_method = $this->request->post['ship_method'];
        $payment = $this->request->post['payment'];
        $ship_cod = $this->request->post['ship_cod'];
        $ship_appointment = $this->request->post['ship_appointment'];
        $shipto = $this->request->post['shipto'];
        $total_total = $this->request->post['total_total'];
        $store_discount_percent = $this->request->post['store_discount_percent'];
        $total_discount_price = $this->request->post['total_discount_price'];
        $subtotal_total = $this->request->post['subtotal_total'];
        $invoice_no = (isset($this->request->post['invoice_no'])) ? $this->request->post['invoice_no'] : 0 ;
        
        if ($mode == 'insert') {
            $txid = $this->generateTxid($accountno);
        } else {
            $txid = $this->request->post['txid'];
        }

        if ( $txid == '' ) {
            echo 'No transaction id';
            exit;
        }
        
        if ( !isset($this->request->post['id']) ) {
            //echo "<script>alert('Not correct order submit \nAre you using Firefox?')</script>";
            echo 'Not correct order submit. Are you using Firefox?';
            exit;
        }

        $aProductId = $this->request->post['id'];
        $aModel = $this->request->post['m'];
        /* remove desc from post list to lessen packet */
        //$aDesc = $this->request->post['desc'];
        $aPrice = $this->request->post['p'];
        $aInventory = $this->request->post['i'];
        $aOrderQty = $this->request->post['o'];
        $aShipped = (isset($this->request->post['s'])) ? $this->request->post['s'] : $aOrderQty;
        $aDiscount = $this->request->post['d'];
        $aTotal = $this->request->post['t'];
            $aSales = array();
        
        $_total_total = $_subtotal_total = 0;
        foreach($aProductId as $idx => $product_id) {
            if ('' == $aModel[$idx]) continue;
            $aSales[] = array(
                $txid,
                $order_date,
                $aProductId[$idx],
                $aModel[$idx],
                //addslashes($aDesc[$idx]),
                $aPrice[$idx],
                $aInventory[$idx],
                $aOrderQty[$idx],
                $aShipped[$idx],
                $aDiscount[$idx],
                $aTotal[$idx]
            );
            
            $rawOrderPrice = ( $aPrice[$idx] * $aShipped[$idx] );
            //echo '<pre>'; print_r($aShipped); echo '</pre>';
            $discountedOrderPrice = $rawOrderPrice * ((100 - $aDiscount[$idx]) / 100) ;
            //echo '<pre>'; print_r($discountedOrderPrice); echo '</pre>'; 
            $discountedOrderPrice = round($discountedOrderPrice,2);
            
            //echo '<pre>'; print_r($discountedOrderPrice); echo '</pre>'; 
            $_total_total += $discountedOrderPrice;
        }
        
        if (count( $aSales ) == 0) {
            echo 'No each sales records';
            exit;
        }
        
        /* TODO
           * Order validation in system later, besso
           * TODO
           * set general error processing for any error
           */
        $_total_total = round($_total_total,2);
        //$_total_total = 288.42;
            $diff = (float)$_total_total - (float)$total_total;
        //echo '<pre>'; print_r($diff); echo '</pre>'; exit;
        $diff = abs($diff);
        if ( $diff >= 1 ) {
                echo '<pre>'; print_r($diff); echo '</pre>';
        //if ( $diff != 0 ) {
            echo 'The sum of order is not match with total';
            echo '<br/>';
            echo 'If you see this error, Please copy below and Email to Administroator';
            echo '<br/>';
            echo '<pre>'; print_r('calculated : ' . $_total_total); echo '</pre>';
            echo '<br/>';
            echo '<pre>'; print_r('original : ' . $total_total); echo '</pre>';
            echo '<br/>';
            echo '<pre>'; print_r($aSales); echo '</pre>';
            exit;
        }
        //echo '<pre>'; print_r($diff); echo '</pre>'; exit;
/*
        if ($store_discount_percent > 0) {
            $_subtotal_total = $_total_total * ((100 - $store_discount_percent) / 100) ;
            $_subtotal_total = round($_subtotal_total,2);

            if ( $_subtotal_total != $subtotal_total ) {
                echo 'The subtotal order price is not match';
                echo '<br/>';
                echo 'If you see this error, Please copy below and Email to Administroator';
                echo '<br/>';
                echo '<pre>'; print_r('x : ' . $_subtotal_total); echo '</pre>';
                echo '<br/>';
                echo '<pre>'; print_r($subtotal_total); echo '</pre>';
                echo '<br/>';
                echo '<pre>'; print_r($aSales); echo '</pre>';
                exit;
            }
        }
*/
        # insert / UPDATE ". $this->table->sales . "
        if (!$this->model_sales_order->insertSales($aSales)) {
            echo 'insert sales fail';
            exit;
        }
        if ( $mode == 'insert' ) {
            $balance = $subtotal_total;
            $payed_sum = 0;
        }
        
        /* TODO
           * AR, Ship later, besso
           */
        $aTx = array(
            'txid' => $txid,
            'store_id' => $store_id,
            'description' => addslashes($description),
            'order_user' => $salesrep,  
            'sold_ym' => substr($order_date,0,4).substr($order_date,5,2),
            'total' => $total_total,
            'subtotal' => $subtotal_total,
            'order_date' => $order_date,
            //'balance' => $balance,
            //'payed_sum' => $payed_sum,
            'shipped_yn' => ( $shipped_yn == 'Shipeed' ) ? 'Y' : 'N',
            'payment' => $payment,
            'ship_method' => $ship_method,
            'ship_appointment' => $ship_appointment,
            'ship_cod' => $ship_cod,
            'shipto' => $shipto,
            'status' => $status,
            'discount' => $store_discount_percent,
            'invoice_no' => $invoice_no
        );

        # insert transaction , sales, ship_history, payment_history
        if ($mode == 'insert') {
            if (!$this->model_sales_order->insertTransaction($aTx) ) {
                echo 'fail to insert transaction';
                exit;
            }
        } else {    // update
            if (!$this->model_sales_order->updateTransaction($aTx) ) {
                echo 'fail to UPDATE ". $this->table->transaction . "';
                exit;
            }
        }
        
        $aStore = array(
            'id' => $store_id,
            'name' => $store_name,
            'accountno' => $accountno,
            'salesrep' => $salesrep,
            'address1' => $address1,
            'city' => $city,
            'state' => $state,
            'zipcode' => $zipcode,
            'storetype' => $storetype,
            'phone1' => $phone1,
            'fax' => $fax,
            'discount' => $store_discount_percent
        );

        // for sales information, let's delete first and insert
        if (!$this->model_sales_order->updateStore($aStore)) {
            echo 'fail to update store'; exit;
        }
        
        
        /* TODO
           * validation check later, besso
           */
        /*
        $this->load->model('report/sale');
        $aValidate = $this->model_report_sale->validate(array(),$txid);
        if ( count( $aValidate) > 0 ) {
            $sale_price = $aValidate['s_price'];
            $this->error['warning'] = "&nbsp;&nbsp;&nbsp;Check Sum vs Detail line</br>";
            $this->error['warning'] .= "Order price should be <font color=red>$sale_price</font></br>";
            $this->error['warning'] .= "Slight difference could be ignored";
        }
        //$this->log->aPrint( $aValidate ); exit;
          */
        
        
        //if ( $status == '0' ) $ajax = 0;
        if ('1' == $ajax) {
            echo json_encode(array('code'=>'200','txid'=>$txid));
            exit;
        } else {
            //$lnk_list = HTTP_SERVER . 'sales/list?invoice_txid=' . $txid . '&mode=' . $mode;
            $lnk_list = HTTP_SERVER . 'sales/order?txid=' . $txid;
            //$lnk_list = HTTP_SERVER . 'sales/list';
            $this->redirect($lnk_list);
        }

        return true;
    }

    private function generateTxid($accountno) {
        $this->load->model('sales/order');
        $txidPrefix = $accountno . '-' . date('Ymd');
        $txid = $this->model_sales_order->getTxid($txidPrefix);
        return $txid;
    }

    public function verify_txid() {
        # call model to check txid and return
        $this->load->model('sales/order');
        $response = $this->model_sales_order->getTxid($this->request->get['txid']);
        $this->data['txid'] = array();
        if (count($response) != 0 ) { $this->data['txid'] = $response;  }
        $this->template = 'sales/verify_txid_proxy.tpl';
        $this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
    }
    
    public function updateExecutor() {
        $this->load->model('sales/order');
        if ($this->model_sales_order->updateExecutor($this->request->get)) {  echo 'OK';  } else {  echo 'FAIL';  }
    }
    
    /* TODO move to util or else */
    public function getPaymentName($code) {
        // payment type
        $aPaymethod = array (
            'n3' => 'Net30',
            'n6' => 'Net60',
            'cc' => 'Cod',
            'pp' => 'prepaid',
            'pd' => 'paid',
            'bb' => 'Bill2Bill'
        );
        return $aPaymethod[$code];
    }

    /* get Tee info in cache */
    private function getTees() {
        $tees = $this->cache->get(CACHE_KEY_TEES);
        //echo '<pre>'; print_r($tees); echo '</pre>'; 
        //$tees = array();
        $refresh = false;
        if (count($tees) == 0) {
            $this->load->model('product/base');
            
            $tees = $this->model_product_base->getTees($refresh);
        }
        return $tees;
    }

    /* get stocking */
    private function getStockings() {
        $stockings = $this->cache->get(CACHE_KEY_STOCKINGS);
        //$stockings = array();
        $refresh = false;
        if (count($stockings) == 0) {
            $this->load->model('product/base');
            
            $stockings = $this->model_product_base->getStockings($refresh);
        }
        return $stockings;
    }

    /* get hats */
    private function getHats() {
        $hats = $this->cache->get(CACHE_KEY_HATS);
        //$hats = array();
        $refresh = false;
        if (count($hats) == 0) {
            $this->load->model('product/base');
            
            $hats = $this->model_product_base->getHats($refresh);
        }
        return $hats;
    }

    /* get adores */
    private function getAdores() {
        $adores = $this->cache->get(CACHE_KEY_ADORE);
        //$adores = array();
        $refresh = false;
        if (count($adores) == 0) {
            $this->load->model('product/base');
            $adores = $this->model_product_base->getAdores($refresh);
        }
        return $adores;
    }

    /* get belts */
    private function getBelts() {
        $belts = $this->cache->get(CACHE_KEY_BELT);
        
        //$belts = array();
        $refresh = false;
        if (count($belts) == 0) {
            
            $this->load->model('product/base');
            $belts = $this->model_product_base->getBelts($refresh);
        }
        return $belts;
    }

}
?>
