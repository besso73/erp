<?php
class ControllerSalesATC extends Controller {

    private $error = array();

    public function products() {
         
        $query = isset($this->request->get['query']) ?  $this->request->get['query'] : '';
        
        $this->load->model('product/price');
        $cache_key = $this->model_product_price->table->product;

        $products = $this->cache->get($cache_key);
        $idx = 0;
        $result = array();

        if ( empty($products) ) {
            // TODO. let's do on the fly for now, 2015-04
            //$msg = 'Fail to get the cached products for auto-complete with key : ' . $cache_key ;
            //$this->log->write($msg);
            //return false;

            $this->cache->delete('products');

            $this->load->model('product/price');
            $products = $this->model_product_price->getAllProducts();
            $this->load->model('product/price');
            $cache_key = $this->model_product_price->table->product;
            $this->cache->set($cache_key, $products);


        }

        foreach($products as $product) {

            if ($idx == ATC_MAX_RETURN) break;
            $query = strtolower($query);
                
            if (strstr($query,' ')) {
                $aWord = explode(' ',$query);
                $ptn = '(.*)';
                  foreach($aWord as $word) {
                    $ptn .= '(' . $word . ')(.*)';
                    }
            } else {
                $ptn = $query;
            }
            
            if ( preg_match("/$ptn/", strtolower($product['name'])) ) {
                $result[] = $product;
                $idx++;
                continue;
            }

            if ( preg_match("/$ptn/", strtolower($product['model'])) ) {
                $result[] = $product;
                $idx++;
                continue;
            }
        }
        echo json_encode($result);
    }

}
?>
