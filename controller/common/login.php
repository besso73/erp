<?php
class ControllerCommonLogin extends Controller { 
    
    private $error = array();
    private $defaultRoute = 'common/home';

    public function index() { 
        
				//echo '<pre>'; print_r('common/login'); echo '</pre>';	exit;
        /*
        $companyid = '';
        if ( isset($this->request->get['route']) ) {
            $aRoute = explode('/', str_replace('../', '', $this->request->get['route']));
            if ( empty($aRoute[1]) ) {
                $companyid = $aRoute[0];
            }
            
            if ( $companyid == 'test' ) {
                $this->request->server['REQUEST_METHOD'] = 'POST';
                $this->request->post['companyid'] = 'test';
                $this->request->post['username']  = 'Tester'; 
                $this->request->post['password']  = 'test';
            }
        }
        */
       
        $this->load->language('common/login');
        $this->document->title = $this->language->get('heading_title');
        $groupId = $this->user->getGroupID();
        
        //$redirectPath = $this->config->redirectGroup($groupId);
        $redirectPath = 'common/home';
        //$this->log->aPrint( $redirectPath );
        if ($this->user->isLogged() && isset($this->session->data['token'])) {
            if ($this->user->hasPermission('modify', 'catalog/category')) {
                $this->redirect(HTTPS_SERVER . ''.$this->defaultRoute.'');
            } else {
                $this->redirect(HTTPS_SERVER . ''. $redirectPath .'');
            }
        }
        if ( ($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) { 
            $this->session->data['token'] = md5(mt_rand()); 
            if (isset($this->request->post['redirect'])) {
                $this->redirect($this->request->post['redirect'] . '');
            } else {
                if ($this->user->hasPermission('modify', 'catalog/category')) {
                    $url = HTTPS_SERVER . $this->defaultRoute . '';
                    $this->redirect($url);
                    } else {
                    $this->redirect(HTTPS_SERVER . ''. $redirectPath .'');
                    }
            }
        }
        $this->data['heading_title'] = $this->language->get('heading_title');
        $this->data['text_login'] = $this->language->get('text_login');
        $this->data['entry_username'] = $this->language->get('entry_username');
        $this->data['entry_password'] = $this->language->get('entry_password');
        $this->data['button_login'] = $this->language->get('button_login');
        if (!isset($this->session->data['token'])) {
            $this->error['warning'] = $this->language->get('error_token');
        }
        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }
        
        $this->data['action'] = HTTPS_SERVER . 'common/login';
        
        /***z
        if (isset($this->request->post['companyid'])) {
            $this->data['companyid'] = $this->request->post['companyid'];
        } else {
            $this->data['companyid'] = '';
        }

        // check static configuration
        if ( defined('COMPANY') ) {
            $this->data['companyid'] = COMPANY;
        }
        ***/
        
        if (isset($this->request->post['username'])) {
            $this->data['username'] = $this->request->post['username'];
        } else {
            $this->data['username'] = '';
        }
        if (isset($this->request->post['password'])) {
            $this->data['password'] = $this->request->post['password'];
        } else {
            $this->data['password'] = '';
        }
        if (isset($this->request->get['route'])) {
            $route = $this->request->get['route'];
            unset($this->request->get['route']);
            
            /***
            if (isset($this->request->get['token'])) {
                unset($this->request->get['token']);
            }
              ***/
            
            $url = '';
            
            if ($this->request->get) {
                $url = '&' . http_build_query($this->request->get);
            }
            
            $this->data['redirect'] = HTTPS_SERVER . '' . $route . $url;
        } else {
            $this->data['redirect'] = '';   
        }
        $this->template = 'common/login.tpl';
        $this->children = array(
            'common/header',    
            'common/footer' 
        );
        $this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
    }

    // call User login method 
    private function validate() {
        if ( isset($this->request->post['username']) && 
             isset($this->request->post['password']) && 
             !$this->user->login($this->request->post['username'], $this->request->post['password'])) {
            $this->error['warning'] = $this->language->get('error_login');
        }
        
        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}
?>
