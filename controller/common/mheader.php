<?php 
class ControllerCommonmHeader extends Controller {
    protected function index() {
        $this->load->language('common/header');
        $this->data['title'] = $this->document->title;
        $this->data['base'] = (HTTPS_SERVER) ? HTTPS_SERVER : HTTP_SERVER;
        $this->data['charset'] = $this->language->get('charset');
        $this->data['lang'] = $this->language->get('code');    
        $this->data['direction'] = $this->language->get('direction');
        $this->data['links'] = $this->document->links;    
        $this->data['styles'] = $this->document->styles;
        $this->data['scripts'] = $this->document->scripts;
        $this->data['breadcrumbs'] = $this->document->breadcrumbs;
        
        $this->data['heading_title'] = $this->language->get('heading_title');
        
        $this->data['text_backup'] = $this->language->get('text_backup');
        $this->data['text_catalog'] = $this->language->get('text_catalog');
        $this->data['text_category'] = $this->language->get('text_category');
        $this->data['text_confirm'] = $this->language->get('text_confirm');
        $this->data['text_country'] = $this->language->get('text_country');
        $this->data['text_coupon'] = $this->language->get('text_coupon');
        $this->data['text_currency'] = $this->language->get('text_currency');            
        $this->data['text_customer'] = $this->language->get('text_customer');
        $this->data['text_customer_group'] = $this->language->get('text_customer_group');
        $this->data['text_sale'] = $this->language->get('text_sale');
        $this->data['text_download'] = $this->language->get('text_download');
        $this->data['text_error_log'] = $this->language->get('text_error_log');
        $this->data['text_extension'] = $this->language->get('text_extension');
        $this->data['text_feed'] = $this->language->get('text_feed');
        $this->data['text_front'] = $this->language->get('text_front');
        $this->data['text_geo_zone'] = $this->language->get('text_geo_zone');
        $this->data['text_dashboard'] = $this->language->get('text_dashboard');
        $this->data['text_help'] = $this->language->get('text_help');
        $this->data['text_information'] = $this->language->get('text_information');
        $this->data['text_language'] = $this->language->get('text_language');
        $this->data['text_localisation'] = $this->language->get('text_localisation');
        $this->data['text_logout'] = $this->language->get('text_logout');
        $this->data['text_contact'] = $this->language->get('text_contact');
        $this->data['text_manufacturer'] = $this->language->get('text_manufacturer');
        $this->data['text_module'] = $this->language->get('text_module');
        $this->data['text_order'] = $this->language->get('text_order');
        $this->data['text_order_status'] = $this->language->get('text_order_status');
        $this->data['text_payment'] = $this->language->get('text_payment');
        $this->data['text_product'] = $this->language->get('text_product'); 
        $this->data['text_reports'] = $this->language->get('text_reports');
        $this->data['text_report_purchased'] = $this->language->get('text_report_purchased');             
        $this->data['text_report_sale'] = $this->language->get('text_report_sale');
            $this->data['text_report_viewed'] = $this->language->get('text_report_viewed');
        $this->data['text_review'] = $this->language->get('text_review');
        $this->data['text_select_all'] = $this->language->get('text_select_all');
        $this->data['text_support'] = $this->language->get('text_support'); 
        $this->data['text_shipping'] = $this->language->get('text_shipping');        
            $this->data['text_setting'] = $this->language->get('text_setting');
        $this->data['text_stock_status'] = $this->language->get('text_stock_status');
        $this->data['text_system'] = $this->language->get('text_system');
        $this->data['text_tax_class'] = $this->language->get('text_tax_class');
        $this->data['text_total'] = $this->language->get('text_total');
        $this->data['text_unselect_all'] = $this->language->get('text_unselect_all');
        $this->data['text_user'] = $this->language->get('text_user');
        $this->data['text_user_group'] = $this->language->get('text_user_group');
        $this->data['text_users'] = $this->language->get('text_users');
            $this->data['text_documentation'] = $this->language->get('text_documentation');
            $this->data['text_weight_class'] = $this->language->get('text_weight_class');
        $this->data['text_length_class'] = $this->language->get('text_length_class');
        $this->data['text_opencart'] = $this->language->get('text_opencart');
            $this->data['text_zone'] = $this->language->get('text_zone');
        
        $this->data['error_install'] = $this->language->get('error_install');
        
        # add new trans , besso-201103
        $this->data['text_material'] = $this->language->get('text_material');
        $this->data['text_material_lookup'] = $this->language->get('text_material_lookup');
        $this->data['text_material_order'] = $this->language->get('text_material_order');
        
        $this->data['text_price'] = $this->language->get('text_price');
        $this->data['text_sales'] = $this->language->get('text_sales');
        $this->data['text_sales_lookup'] = $this->language->get('text_sales_lookup');
        $this->data['text_sales_order'] = $this->language->get('text_sales_order');
        
        
        if (!$this->user->isLogged() || !isset($this->session->data['token']) ) {
        $this->data['logged'] = '';
        $this->data['home'] = HTTPS_SERVER . '/common/login';
        } else {
            
        $this->data['install'] = is_dir(dirname(DIR_APPLICATION) . '/install');
        
        $this->data['logged'] = sprintf($this->language->get('text_logged'), strtoupper($this->user->getFirstName()));
        $this->data['home'] = HTTPS_SERVER . '/common/home'; 
            
        $this->data['backup'] = HTTPS_SERVER . '/tool/backup';
        $this->data['category'] = HTTPS_SERVER . '/catalog/category';
        $this->data['country'] = HTTPS_SERVER . '/localisation/country';
        $this->data['currency'] = HTTPS_SERVER . '/localisation/currency';
        $this->data['coupon'] = HTTPS_SERVER . '/sale/coupon';
        $this->data['customer'] = HTTPS_SERVER . '/sale/customer';
        $this->data['customer_group'] = HTTPS_SERVER . '/sale/customer_group';
        $this->data['download'] = HTTPS_SERVER . '/catalog/download';
        $this->data['error_log'] = HTTPS_SERVER . '/tool/error_log';
        $this->data['feed'] = HTTPS_SERVER . '/extension/feed';            
            
        $this->data['stores'] = array();
            
        $this->load->model('setting/store');
            
        $results = $this->model_setting_store->getStores();
            
            foreach ($results as $result) {
        $this->data['stores'][] = array(
                    'name' => $result['name'],
                    'href' => $result['url']
                );
    }
            
        $this->data['geo_zone'] = HTTPS_SERVER . '/localisation/geo_zone';
        $this->data['information'] = HTTPS_SERVER . '/catalog/information';
        $this->data['language'] = HTTPS_SERVER . '/localisation/language';
        $this->data['logout'] = HTTPS_SERVER . '/common/logout';
        $this->data['contact'] = HTTPS_SERVER . '/sale/contact';
        $this->data['manufacturer'] = HTTPS_SERVER . '/catalog/manufacturer';
        $this->data['module'] = HTTPS_SERVER . '/extension/module';
        $this->data['order'] = HTTPS_SERVER . '/sale/order';
        $this->data['order_status'] = HTTPS_SERVER . '/localisation/order_status';
        $this->data['payment'] = HTTPS_SERVER . '/extension/payment';
        $this->data['lnk_product'] = HTTPS_SERVER . '/catalog/product';
        $this->data['report_purchased'] = HTTPS_SERVER . '/report/purchased';
        $this->data['report_sale'] = HTTPS_SERVER . '/report/sale';
        $this->data['report_viewed'] = HTTPS_SERVER . '/report/viewed';
        $this->data['review'] = HTTPS_SERVER . '/catalog/review';
        $this->data['shipping'] = HTTPS_SERVER . '/extension/shipping';
        $this->data['setting'] = HTTPS_SERVER . '/setting/setting';
        $this->data['store'] = HTTP_CATALOG;
        $this->data['stock_status'] = HTTPS_SERVER . '/localisation/stock_status';
        $this->data['tax_class'] = HTTPS_SERVER . '/localisation/tax_class';
        $this->data['total'] = HTTPS_SERVER . '/extension/total';
        $this->data['user'] = HTTPS_SERVER . '/user/user';
        $this->data['user_group'] = HTTPS_SERVER . '/user/user_permission';
        $this->data['weight_class'] = HTTPS_SERVER . '/localisation/weight_class';
        $this->data['length_class'] = HTTPS_SERVER . '/localisation/length_class';
        $this->data['zone'] = HTTPS_SERVER . '/localisation/zone';
            
            # add new top link, besso-201103
        $this->data['lnk_material_lookup'] = HTTPS_SERVER . '/material/lookup';
        $this->data['lnk_material_productpackage']  = HTTPS_SERVER . '/material/productpackage';
        $this->data['lnk_material_history']  = HTTPS_SERVER . '/material/history';
        $this->data['lnk_material_order']  = HTTPS_SERVER . '/material/order';
            
            /* TODO
               * Generate more valid route than price
               * besso, 2014-03
               */
        $this->data['lnk_product_price'] = HTTPS_SERVER . '/product/price';
        $this->data['lnk_product_inventory'] = HTTPS_SERVER . '/product/inventory';
        $this->data['lnk_product_oem'] = HTTPS_SERVER . '/product/oem';
        $this->data['lnk_sales_lookup'] = HTTPS_SERVER . 'sales/list';
        $this->data['lnk_sales_order']  = HTTPS_SERVER . 'sales/order';
        $this->data['lnk_sales_stat']  = HTTPS_SERVER . '/sales/stat';
        $this->data['lnk_sales_statview']  = HTTPS_SERVER . '/sales/stat/view';
        $this->data['lnk_sales_calendar']  = HTTPS_SERVER . '/sales/calendar';
        $this->data['lnk_update_promotion']  = HTTPS_SERVER . '/sales/promotion';
        }
        $this->id = 'header';
        $this->template = 'common/m/header.tpl';
        $this->render();
    }
}
?>
