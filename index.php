<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1); 
error_reporting(E_ALL);

//echo '<pre>'; print_r(getallheaders()); echo '</pre>'; exit;
// Version
define('VERSION', '1.4.9.3');

// TODO need to change later with user timezone
date_default_timezone_set('America/Chicago');

// Configuration
require_once('config.php');
require_once('constants.php');

// Startup
require_once(DIR_SYSTEM . 'startup.php');

// Application Classes
require_once(DIR_SYSTEM . 'library/currency.php');
require_once(DIR_SYSTEM . 'library/user.php');
require_once(DIR_SYSTEM . 'library/weight.php');
require_once(DIR_SYSTEM . 'library/length.php');
require_once(DIR_SYSTEM . 'library/util.php');

// Registry
$registry = new Registry();

// Loader
$loader = new Loader($registry);
$registry->set('load', $loader);

/*
 * mobile detect
 * http://mobiledetect.net/
 */
//$detect = new Mobile_Detect();
//$registry->set('detect', $detect);

/*
$pdf = new mPDF();
$registry->set('pdf', $pdf);
*/

// mobile detect
$config = new Config();
$registry->set('config', $config);

// Url
$url = new Url($config->get('config_url'), $config->get('config_secure') ? $config->get('config_ssl') : $config->get('config_url'));    
$registry->set('url', $url);

// Database
$db = new DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
$registry->set('db', $db);

// Settings
$query = $db->query("SELECT * FROM " . DB_PREFIX . "setting");
//echo '<pre>'; print_r($query); echo '</pre>';	exit;
foreach ($query->rows as $setting) {
        $config->set($setting['key'], $setting['value']);
}

// Log 
$log = new Log($config->get('config_error_filename'));
$registry->set('log', $log);


// Error Handler
function error_handler($errno, $errstr, $errfile, $errline) {
    global $config, $log;

    switch ($errno) {
        case E_NOTICE:
        case E_USER_NOTICE:
            $error = 'Notice';
            break;
        case E_WARNING:
        case E_USER_WARNING:
            $error = 'Warning';
            break;
        case E_ERROR:
        case E_USER_ERROR:
            $error = 'Fatal Error';
            break;
        default:
            $error = 'Unknown';
            break;
    }
        if ($config->get('config_error_display')) {
        echo '<b>' . $error . '</b>: ' . $errstr . ' in <b>' . $errfile . '</b> on line <b>' . $errline . '</b>';
    }
    
        if ($config->get('config_error_log')) {
        $log->write('PHP ' . $error . ':  ' . $errstr . ' in ' . $errfile . ' on line ' . $errline, 'error.txt');
    }

        return TRUE;
}

// Error Handler
set_error_handler('error_handler');

// Request
$request = new Request();
$registry->set('request', $request);

// Response
$response = new Response();
$response->addHeader('Content-Type: text/html; charset=utf-8');
$registry->set('response', $response); 

// Session
$registry->set('session', new Session());

// Cache
$registry->set('cache', new Cache());

// Document
$registry->set('document', new Document());

// Language
$languages = array();

$query = $db->query("SELECT * FROM " . DB_PREFIX . "language"); 

foreach ($query->rows as $result) {
        $languages[$result['code']] = array(
        'language_id' => $result['language_id'],
        'name'        => $result['name'],
        'code'        => $result['code'],
        'locale'      => $result['locale'],
        'directory'   => $result['directory'],
        'filename'    => $result['filename']
    );
}

$config->set('config_language_id', $languages[$config->get('config_admin_language')]['language_id']);

$language = new Language($languages[$config->get('config_admin_language')]['directory']);
$language->load($languages[$config->get('config_admin_language')]['filename']); 
$registry->set('language', $language);

// Currency
$registry->set('currency', new Currency($registry));

// Weight
$registry->set('weight', new Weight($registry));

// Length
$registry->set('length', new Length($registry));

// User
$registry->set('user', new User($registry));

// Util
$registry->set('util', new Util($registry));

// Front Controller
$controller = new Front($registry);

// Login
$controller->addPreAction(new Action('common/home/login'));

// Permission
$controller->addPreAction(new Action('common/home/permission'));

//todo. need to check mobile or not
//http://mobiforge.com/developing/story/lightweight-device-detection-php

// Router
if (isset($request->get['route'])) {
		// for test account of Demo, besso
    $aRoute = explode('/', str_replace('../', '', $request->get['route']));
    if ( empty($aRoute[1]) ) {
        $action = new Action('common/login');
    } else {
        $action = new Action($request->get['route']);
    }
} else {
		$action = new Action('common/home');
}

// Dispatch
$controller->dispatch($action, new Action('error/not_found'));

//echo '<pre>'; print_r($response); echo '</pre>';	exit;

// Output
$response->output();
?>
