<?php
/* company */
define('ADMIN_COMPANY_ID', 'admin');
define('ADMIN_GROUP_ID', 1);

/* cache key lists
 * besso, 2014-03
 */
define('CACHE_KEY_PRODUCTS','products');
define('CACHE_KEY_TEES','tees');
define('CACHE_KEY_STOCKINGS','stockings');
define('CACHE_KEY_HATS','hats');
define('CACHE_KEY_ADORE','adores');
define('CACHE_KEY_BELT','belts');
define('ATC_MAX_RETURN',200);

define('LOGO_IMAGE','/image/icon/logo.jpg');
define('COMPANY_ADDRESS1','7789 North Caldwell Ave.');
define('COMPANY_ADDRESS2','Niles, IL, 60714');
define('COMPANY_TEL','847.663.0900');
define('COMPANY_FAX','847.663.0905');


define('TEMPLATE_EXTENSION','.tpl');
    /* error code */
define('ERROR_NO_ADMIN_ACCESS', 501);
define('ERROR_NO_ADMIN_GROUP_ACCESS', 502);

define('ERROR_SESSION_NO_COMPANYID', 701);
define('ERROR_SESSION_NO_USERGROUPID', 702);
