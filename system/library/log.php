<?php
final class Log {
    private $filename;
    public function __construct($filename) {
        $this->filename = $filename;
    }

    public function write ( $message, $filename = '' ) {
        if ($filename) {
            $file = DIR_LOGS . $filename;
        } else {
            $file = DIR_LOGS . $this->filename;
        }
        $handle = fopen($file, 'a+'); 
        fwrite($handle, date('Y-m-d G:i:s') . ' - ' . $message . "\n");
        fclose($handle);
    }

    public function aPrint($aMsg) {
        echo '<pre>'; print_r($aMsg); echo '</pre>';
    }
}
?>
