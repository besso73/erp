<?php

$html =<<<HTML
<div id='sheet'>
    <div id='top'>
        <div id='company-info' style='float:left'>
            <ul>
                    <li>
                            <img id='logo-image' src='/image/icon/logo.jpg' />
                    </li>
                    <li>7789 North Caldwell Ave.</li>
                    <li>Niles, IL, 60714</li>
                    <!--li>TEL : 847.663.0900</li-->
            </ul>
        </div>
        <div id='general-info' style='float:right;'>
            <h3>INVOICE</h3>
            <table border=1 cellspacing=0 cellpading=1>
                    <tr>
                            <td>Phone #</td>
                            <td>Fax #</td>
                            <td>DATE</td>
                            <td>INVOICE #</td>
                    </tr>
                    <tr>
                            <td>847.663.0900</td>
                            <td>847.663.0905</td>
                            <td>04/22/2014</td>
                            <td>14040002</td>
                    </tr>
            </table>
        </div>
    </div>
    
    
    <table id='billinfo'>
        <tr>
        <td>
            <table cellspacing=0 cellpading=1>
                <tr>
                    <td style='padding-left:20px;font-size:13px;'>Bill To</td>
                </tr>
                <tr>
                    <td style='padding-left:20px;'>
                            <ul style="height:92px;margin-bottom:0px">
            <li>DON'S</li>
            <li>1619 E. 87th St.</li>
            <li>Chicago , IL, 60617</li>
            <li>TEL : 773-721-4403</li>
            </ul>            </td>
                </tr>
            </table>
        </td>
        <td>
            <table cellspacing=0 cellpading=1>
                <tr>
                    <td style='padding-left:20px;font-size:13px;'>Ship To</td>
                </tr>
                <tr>
                    <td style='padding-left:20px;'>
                  DON'S<br />
    1619 E. 87th St.<br />
    Chicago,IL,60617<br />
    TEL : 773-721-4403</td>
                </tr>
            </table>
        </td>
        </tr>
    </table>

        <table id='metainfo' border='1' cellspacing=0 cellpading=1>
        <tr>
        <td>P.O.#</td>
        <td>TERMS</td>
        
        <td>REP</td>
        <td>SHIP DATE</td>
        <td>SHIP VIA</td>
        <td>F.O.B.</td>
        </tr>
        <tr>
        <td>4403-20140415-1</td>
        <td>Cod</td>
        <td>richard</td>
        
        <td></td>
        <td>PCK</td>
        <td></td>
        </tr>
    </table>
            <table id='detail' border='1'>
        <thead>
        <tr>
            <th>Item Code</th>
            <th>Description</th>
            <th>Ordered</th>
            <th>Shipped</th>
            <th>Unit Price</th>
            <th>Amount</th>
        </tr>
        </thead>
        <tbody>
                    <tr class='item '>
            <td>adr069</td>
            <td>adore #069 wild cherry</td>
            <td>12</td>
            <td>12</td>
            <td>2.59</td>
            <td>31.08</td>
        </tr>
                    <tr class='item '>
            <td>adr094</td>
            <td>adore #094 bordeaux</td>
            <td>12</td>
            <td>12</td>
            <td>2.59</td>
            <td>31.08</td>
        </tr>
                    <tr class='item '>
            <td>adr109</td>
            <td>adore #109 dark chocolate</td>
            <td>12</td>
            <td>12</td>
            <td>2.59</td>
            <td>31.08</td>
        </tr>
                    <tr class='item '>
            <td>adr117</td>
            <td>adore #117 aquamarine</td>
            <td>12</td>
            <td>12</td>
            <td>2.59</td>
            <td>31.08</td>
        </tr>
                    <tr class='item '>
            <td>adr120</td>
            <td>adore #120 black velvet</td>
            <td>12</td>
            <td>12</td>
            <td>2.59</td>
            <td>31.08</td>
        </tr>
                    <tr class='item '>
            <td>ann7101</td>
            <td>Annie #7101 Jet black 3.7oz bottle</td>
            <td>3</td>
            <td>3</td>
            <td>2.75</td>
            <td>8.25</td>
        </tr>
                    <tr class='item '>
            <td>ann7102</td>
            <td>Annie #7102 Natural black 3.7oz bottle</td>
            <td>3</td>
            <td>3</td>
            <td>2.75</td>
            <td>8.25</td>
        </tr>
                    <tr class='item '>
            <td>ann7103</td>
            <td>Annie #7103 Soft black 3.7oz bottle</td>
            <td>3</td>
            <td>3</td>
            <td>2.75</td>
            <td>8.25</td>
        </tr>
                    <tr class='item '>
            <td>ann7104</td>
            <td>Annie #7104 Dark Brown 3.7oz bottle</td>
            <td>3</td>
            <td>3</td>
            <td>2.75</td>
            <td>8.25</td>
        </tr>
                    <tr class='item '>
            <td>ann7105</td>
            <td>Annie #7105 Light Brown 3.7oz bottle</td>
            <td>3</td>
            <td>3</td>
            <td>2.75</td>
            <td>8.25</td>
        </tr>
                    <tr class='item '>
            <td>ann7106</td>
            <td>Annie #7106 Burgundy brown3.7oz bottle</td>
            <td>3</td>
            <td>3</td>
            <td>2.75</td>
            <td>8.25</td>
        </tr>
                    <tr class='item '>
            <td>BX-PXL</td>
            <td>P/C Men</td>
            <td>12</td>
            <td>12</td>
            <td>0.00</td>
            <td>0.00</td>
        </tr>
                    <tr class='item '>
            <td>PH2151-MTCO</td>
            <td>Panty Hose #2151 Med/Tall Coffee</td>
            <td>6</td>
            <td>6</td>
            <td>0.00</td>
            <td>0.00</td>
        </tr>
                    <tr class='item '>
            <td>PH2151-MTJB</td>
            <td>Panty Hose #2151 Med/Tall Jet Black</td>
            <td>12</td>
            <td>12</td>
            <td>0.00</td>
            <td>0.00</td>
        </tr>
                    <tr class='item '>
            <td>PH2151-MTJBR</td>
            <td>Panty Hose #2151 Med/Tall Jet Brown</td>
            <td>6</td>
            <td>6</td>
            <td>0.00</td>
            <td>0.00</td>
        </tr>
                    <tr class='item '>
            <td>PH2151-MTKH</td>
            <td>Panty Hose #2151 Med/Tall Khaki</td>
            <td>6</td>
            <td>6</td>
            <td>0.00</td>
            <td>0.00</td>
        </tr>
                    <tr class='item '>
            <td>PH2151-MTOA</td>
            <td>Panty Hose #2151 Med/Tall Oatmeal</td>
            <td>6</td>
            <td>6</td>
            <td>0.00</td>
            <td>0.00</td>
        </tr>
                    <tr class='item '>
            <td>PH2151-TKH</td>
            <td>Panty Hose #2151 Tall Khaki</td>
            <td>6</td>
            <td>6</td>
            <td>0.00</td>
            <td>0.00</td>
        </tr>
                    <tr class='item '>
            <td>PH2151-TOB</td>
            <td>Panty Hose #2151 Tall Off Black</td>
            <td>12</td>
            <td>12</td>
            <td>0.00</td>
            <td>0.00</td>
        </tr>
                    <tr class='item '>
            <td>PH2151-TOW</td>
            <td>Panty Hose #2151 Tall Off White</td>
            <td>6</td>
            <td>6</td>
            <td>0.00</td>
            <td>0.00</td>
        </tr>
                    <tr class='item '>
            <td>PH2151-TTA</td>
            <td>Panty Hose #2151 Tall Taupe</td>
            <td>12</td>
            <td>12</td>
            <td>0.00</td>
            <td>0.00</td>
        </tr>
                    <tr class='item '>
            <td>PH4001-CO</td>
            <td>Panty Hose #4001 (Regular) Coffee</td>
            <td>12</td>
            <td>12</td>
            <td>0.00</td>
            <td>0.00</td>
        </tr>
                    <tr class='item '>
            <td>PH4001-GR</td>
            <td>Panty Hose #4001 (Regular) Grey</td>
            <td>6</td>
            <td>6</td>
            <td>0.00</td>
            <td>0.00</td>
        </tr>
                    <tr class='item '>
            <td>PH4001-JB</td>
            <td>Panty Hose #4001 (Regular) Jet Black</td>
            <td>12</td>
            <td>12</td>
            <td>0.00</td>
            <td>0.00</td>
        </tr>
                    <tr class='item paging'>
            <td>PH4001-JBR</td>
            <td>Panty Hose #4001 (Regular) Jet Brown</td>
            <td>12</td>
            <td>12</td>
            <td>0.00</td>
            <td>0.00</td>
        </tr>
                    <tr class='item '>
            <td>PH4001-NA</td>
            <td>Panty Hose #4001 (Regular) Navy</td>
            <td>12</td>
            <td>12</td>
            <td>0.00</td>
            <td>0.00</td>
        </tr>
                    <tr class='item '>
            <td>PH4001-OB</td>
            <td>Panty Hose #4001 (Regular) Off Black</td>
            <td>12</td>
            <td>12</td>
            <td>0.00</td>
            <td>0.00</td>
        </tr>
                    <tr class='item '>
            <td>PH4001-PE</td>
            <td>Panty Hose #4001 (Regular) Pecan</td>
            <td>12</td>
            <td>12</td>
            <td>0.00</td>
            <td>0.00</td>
        </tr>
                    <tr class='item '>
            <td>PH4001-PEA</td>
            <td>Panty Hose #4001 (Regular) Peach</td>
            <td>12</td>
            <td>12</td>
            <td>0.00</td>
            <td>0.00</td>
        </tr>
                    <tr class='item '>
            <td>PH4001-WH</td>
            <td>Panty Hose #4001 (Regular) White</td>
            <td>12</td>
            <td>12</td>
            <td>0.00</td>
            <td>0.00</td>
        </tr>
                    <tr class='item '>
            <td>PH589-BL</td>
            <td>Panty Hose #589 Glitter Black</td>
            <td>6</td>
            <td>6</td>
            <td>0.00</td>
            <td>0.00</td>
        </tr>
                    <tr class='item '>
            <td>PH6002-12OB</td>
            <td>Panty Hose #6002 Queen 1-2X Off Black</td>
            <td>6</td>
            <td>6</td>
            <td>0.00</td>
            <td>0.00</td>
        </tr>
                    <tr class='item '>
            <td>PH6002-12OW</td>
            <td>Panty Hose #6002 Queen 1-2X Off White</td>
            <td>6</td>
            <td>6</td>
            <td>0.00</td>
            <td>0.00</td>
        </tr>
                    <tr class='item '>
            <td>PH6005-OB</td>
            <td>Panty Hose #6005 Queen Panel Off Black</td>
            <td>6</td>
            <td>6</td>
            <td>0.00</td>
            <td>0.00</td>
        </tr>
                    <tr class='item '>
            <td>PH6005-OW</td>
            <td>Panty Hose #6005 Queen Panel Off White</td>
            <td>6</td>
            <td>6</td>
            <td>0.00</td>
            <td>0.00</td>
        </tr>
                    <tr class='item '>
            <td>PH7001-BL</td>
            <td>Panty Hose #7001 (=5002Queen) Black</td>
            <td>12</td>
            <td>12</td>
            <td>0.00</td>
            <td>0.00</td>
        </tr>
                    <tr class='item '>
            <td>PH7001-JB</td>
            <td>Panty Hose #7001 (=5002Queen) Jet Black</td>
            <td>12</td>
            <td>12</td>
            <td>0.00</td>
            <td>0.00</td>
        </tr>
                    <tr class='item '>
            <td>PH7001-NA</td>
            <td>Panty Hose #7001 (=5002Queen) Navy</td>
            <td>6</td>
            <td>6</td>
            <td>0.00</td>
            <td>0.00</td>
        </tr>
                    <tr class='item '>
            <td>PH7001-OA</td>
            <td>Panty Hose #7001 (=5002Queen) Oatmeal</td>
            <td>6</td>
            <td>6</td>
            <td>0.00</td>
            <td>0.00</td>
        </tr>
                    <tr class='item '>
            <td>PH7001-OB</td>
            <td>Panty Hose #7001 (=5002Queen) Off Black</td>
            <td>12</td>
            <td>12</td>
            <td>0.00</td>
            <td>0.00</td>
        </tr>
                    <tr class='item '>
            <td>PH7001-PE</td>
            <td>Panty Hose #7001 (=5002Queen) Pecan</td>
            <td>12</td>
            <td>12</td>
            <td>0.00</td>
            <td>0.00</td>
        </tr>
                    <tr class='item '>
            <td>PH7001-SU</td>
            <td>Panty Hose #7001 (=5002Queen) Suntan</td>
            <td>6</td>
            <td>6</td>
            <td>0.00</td>
            <td>0.00</td>
        </tr>
                    <tr class='item '>
            <td>PH7001-TA</td>
            <td>Panty Hose #7001 (=5002Queen) Taupe</td>
            <td>6</td>
            <td>6</td>
            <td>0.00</td>
            <td>0.00</td>
        </tr>
                    <tr class='item '>
            <td>PH7001-WH</td>
            <td>Panty Hose #7001 (=5002Queen) White</td>
            <td>12</td>
            <td>12</td>
            <td>0.00</td>
            <td>0.00</td>
        </tr>
                    <tr class='item '>
            <td>PHKH265-BL</td>
            <td>Panty Hose Knee-Hi #265 (Queen) Black</td>
            <td>12</td>
            <td>12</td>
            <td>0.00</td>
            <td>0.00</td>
        </tr>
                    <tr class='item '>
            <td>PHKH265-CO</td>
            <td>Panty Hose Knee-Hi #265 (Queen) Coffee</td>
            <td>12</td>
            <td>12</td>
            <td>0.00</td>
            <td>0.00</td>
        </tr>
                    <tr class='item '>
            <td>PHKH265-OB</td>
            <td>Panty Hose Knee-Hi #265 (Queen) Off Black</td>
            <td>12</td>
            <td>12</td>
            <td>0.00</td>
            <td>0.00</td>
        </tr>
                    <tr class='item '>
            <td>PHKH265-WH</td>
            <td>Panty Hose Knee-Hi #265 (Queen) White</td>
            <td>12</td>
            <td>12</td>
            <td>0.00</td>
            <td>0.00</td>
        </tr>
                    <tr class='item '>
            <td>PHP2001-WH</td>
            <td>Panty Hose #P2001 Opaque Tights White</td>
            <td>12</td>
            <td>12</td>
            <td>0.00</td>
            <td>0.00</td>
        </tr>
                    <tr class='item '>
            <td>SOK47523-1013W</td>
            <td>Knocker Men</td>
            <td>12</td>
            <td>12</td>
            <td>0.00</td>
            <td>0.00</td>
        </tr>
                    <tr class='item '>
            <td>SOTN3000-BL</td>
            <td>Trouser Socks #N3000 Crew Black</td>
            <td>12</td>
            <td>12</td>
            <td>0.00</td>
            <td>0.00</td>
        </tr>
                    <tr class='item '>
            <td>SOTN3000-RE</td>
            <td>Trouser Socks #N3000 Crew Red</td>
            <td>12</td>
            <td>12</td>
            <td>0.00</td>
            <td>0.00</td>
        </tr>
                    <tr class='item'>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan='2'>
                Origin price : 204.90           </td>
        </tr>
                    <tr>
            <td class='footer' colspan='4'>
            
            </td>
            <td class='footer' colspan='2'>
                Total : 204.90          </td>
        </tr>
        </tbody>
    </table>

</div>
HTML;

//==============================================================
//==============================================================
//==============================================================
include("../mpdf.php");

$mpdf=new mPDF('en-GB-x','A4','','',10,10,10,10,6,3); 

$mpdf->SetDisplayMode('fullpage');

$mpdf->list_indent_first_level = 0;    // 1 or 0 - whether to indent the first level of a list

// LOAD a stylesheet
$stylesheet = file_get_contents('mpdfstyletables.css');
$mpdf->WriteHTML($stylesheet,1);    // The parameter 1 tells that this is css/style only and no body/html/text

$mpdf->WriteHTML($html);

$mpdf->Output();
exit;
//==============================================================
//==============================================================
//==============================================================

?>