<?php
final class User {

    public $companyid;
    private $user_id;
    private $user_group_id;
    private $username;
    private $firstname;
    private $approver;
    private $permission = array();
    private $config;
    private $table;

    private $user_table;
    private $user_group_table;

    public function __construct($registry) {
    
        $this->db = $registry->get('db');
        $this->request = $registry->get('request');
        $this->session = $registry->get('session');
        $this->config = $registry->get('config');
        $this->companyid = isset($this->session->data['companyid']) ? $this->session->data['companyid'] : '';
        if (isset($this->session->data['user_id'])) {
            
            if ( $this->companyid != '' ) {
                $user_table = $this->companyid . '_user';
                $user_group_table = $this->companyid . '_user_group';
            } else {
                $user_table = 'user';
                $user_group_table = 'user_group';
            }

            $sql = "SELECT * FROM " . $user_table . " WHERE user_id = '" . (int)$this->session->data['user_id'] . "'";

            $user_query = $this->db->query($sql);
            if ($user_query->num_rows) {

                $this->user_id = $user_query->row['user_id'];
                $this->user_group_id = $user_query->row['user_group_id'];
                $this->username = $user_query->row['username'];
                $this->approver = $user_query->row['approver'];
                $this->firstname = $user_query->row['firstname'];
                
                $sql = "UPDATE " . $user_table . " SET ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "' WHERE user_id = '" . (int)$this->session->data['user_id'] . "'";
                $this->db->query($sql);

                $sql = "SELECT permission FROM " . $user_group_table . " WHERE user_group_id = '" . (int)$user_query->row['user_group_id'] . "'";
                $user_group_query = $this->db->query($sql);
                $permissions = unserialize($user_group_query->row['permission']);
                //print_r( $permissions );
                if (is_array($permissions)) {
                  foreach ($permissions as $key => $value) {
                    $this->permission[$key] = $value;
                    }
                    }
            } else {
                $this->logout();
            }
        }
        $this->companyid = isset($this->session->data['companyid']) ? $this->session->data['companyid'] : '' ;
    }

    public function login($username, $password) {
        
        $user_table = 'user';
        $user_group_table = 'user_group';
        $sql = "SELECT * FROM " . $user_table . " WHERE LOWER(username) = '" . $this->db->escape(strtolower($username)) . "'
                   AND password = '" . $this->db->escape(md5($password)) . "'";
        // when you lost password, print it
        //echo $sql; exit;
        
        $user_query = $this->db->query($sql);
        
        if ($user_query->num_rows) {
            $this->session->data['user_id'] = $user_query->row['user_id'];
            $this->session->data['user_group_id'] = $user_query->row['user_group_id'];

            $this->table = new stdClass();
            $this->table->category = 'category';
            $this->table->category_description = 'category_description';
            $this->table->category_filter = 'category_filter';
            $this->table->product = 'products';
            $this->table->stores = 'stores';
            $this->table->transaction = 'transaction';
            $this->table->sales = 'sales';
            $this->table->category = 'category';
            $this->table->user = 'user';
            $this->table->user_group = 'user_group';
            
            $this->config->set( 'table', $this->table );

            $this->user_id = $user_query->row['user_id'];
            $this->username = $user_query->row['username'];

            $sql = "SELECT permission FROM " . $user_group_table . " WHERE user_group_id = '" . (int)$user_query->row['user_group_id'] . "'";
            $user_group_query = $this->db->query($sql);
            $permissions = unserialize($user_group_query->row['permission']);

            if (is_array($permissions)) {
                  foreach ($permissions as $key => $value) {
                    $this->permission[$key] = $value;
                    }
            }
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function logout() {
        unset($this->session->data['user_id']);
        $this->user_id = '';
        $this->username = '';
    }

    public function hasPermission($key,$value) {
        if (isset($this->permission[$key])) {
            return in_array($value, $this->permission[$key]);
        } else {
            return FALSE;
        }
    }

    public function isLogged() {
        return $this->user_id;
    }

    public function getId() {
        return $this->user_id;
    }

    public function getCompanyId() {
        return $this->companyid;
    }

    public function getCompanyName() {
        $sql = "SELECT name FROM company WHERE companyid = '" . $this->companyid . "'";
        $query = $this->db->query($sql);
        $aCompany = $query->row;
	if ( !empty($aCompany) ) {
            return $aCompany['name'];
        } else {
            return '';
        }
    }

    public function getUserName() {
        return $this->username;
    }

    public function getGroupID() {
        return $this->user_group_id;
    }

    // todo. not use username , besso 201105
    public function getFirstName($username = '') {
        if ($username != '') {
            $sql = "SELECT * FROM " . $this->companyid . "_user WHERE username = '" . $username . "'";
            //echo $sql;
            $user_query = $this->db->query($sql);
            return $user_query->row['firstname'];
        } else {          
            return $this->firstname;
        }
    }

    public function getGroupName($username = '') {
        if ($username != '') {
            $sql = "SELECT ug.name FROM user u," . $this->companyid . "_user_group ug WHERE u.user_group_id = ug.user_group_id and LOWER(u.username) = '" . strtolower($username) . "'";
            $query = $this->db->query($sql);
            return isset($query->row['name']) ? $query->row['name'] : '';
        }
    }

    public function getApprover($username = '') {
        if ($username != '') {
        $sql = "SELECT approver FROM user WHERE username = '" . $username . "'";
        //echo $sql;
        $query = $this->db->query($sql);
      return $query->row['approver'];
    }
    } 

    // todo. need exception check , besso , is it used? check
    public function getUserId($username) {
        $user_query = $this->db->query("SELECT user_id FROM " . $this->table->user . " WHERE LOWER(username) like '%" . $this->db->escape(strtolower($username)) . "%'");
        $username = $user_query->row;
        print_r($username);
    }

    public function getEmail($username) {
        $user_query = $this->db->query("SELECT email FROM user WHERE LOWER(username) like '%" . $this->db->escape(strtolower($username)) . "%'");
        $email = $user_query->row;
        return $email;
    }

    public function getSales() {
        $sql = "SELECT username FROM user WHERE user_group_id = 11 and status = 1 order by username";
        $query = $this->db->query($sql);
        $aSales = array();
        foreach($query->rows as $row) {  $aSales[] = $row['username']; }
        return $aSales;
    }

    public function getAllSales() {
        $sql = "SELECT * FROM user WHERE user_group_id = 11 and status = 1 and username != 'AROOINC' order by username";
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function isSales() {
        $sql = "SELECT username FROM user WHERE user_group_id = 11";
        $query = $this->db->query($sql);
        $aSales = $query->rows;
        foreach($aSales as $sales) {
            if ( $this->username == $sales['username'] ) {
            return true;
            break;
            } 
        }
      return false;
    }
  
    // [TODO] exception
    public function isManager($user) {
        if ('manager' == $this->getGroupName($user) || $user == 'joon' ) {
            return true;
            echo 'manager';
        } else {
            return false;
            echo 'not manager';
        }
    }

    public function isAdmin() {
        //echo '<pre>'; print_r($this->session); echo '</pre>'; exit;
        // check company first              
        if (empty($this->session->data['companyid'])) {
            return ERROR_SESSION_NO_COMPANYID;
        }
        if ($this->session->data['companyid'] != ADMIN_COMPANY_ID) {
            return ERROR_NO_ADMIN_ACCESS;
        }
       
        // check user group id as companyid 
        if (empty($this->session->data['user_group_id'])) {
            return ERROR_SESSION_NO_USERGROUPID;
        }
        if ($this->session->data['user_group_id'] != ADMIN_GROUP_ID) {
            return ERROR_NO_ADMIN_GROUP_ACCESS;
        }

        return true;
    }

}
?>
