<?php
final class Session {
    public $data = array();
    public $token = '';
            
    public function __construct() {
        if (!session_id()) {
            ini_set('session.use_cookies', 'On');
            ini_set('session.use_trans_sid', 'Off');
        
            session_set_cookie_params(0, '/');
/*
$sid = session_id();
print_r($sid);
exit;
*/
            //session_start();
$ok = @session_start();
if(!$ok){
session_regenerate_id(true); // replace the Session ID
session_start(); 
}
	//	$this->my_session_start();
        }
        $this->data =& $_SESSION;
    }

function my_session_start()
{
      $sn = session_name();
      if (isset($_COOKIE[$sn])) {
          $sessid = $_COOKIE[$sn];
      } else if (isset($_GET[$sn])) {
          $sessid = $_GET[$sn];
      } else {
          return session_start();
      }

     if (!preg_match('/^[a-zA-Z0-9,\-]{22,40}$/', $sessid)) {
          return false;
      }
      return session_start();
}
}
?>
