<?php
abstract class Controller {
    protected $registry;    
    protected $id;
    protected $template;
    protected $children = array();
    protected $data = array();
    protected $output;
    protected $mobile = false;
    protected $companyid;
    //protected $table;

    protected $help = false;
    
    protected $route;

    public function __construct($registry) {
        $this->registry = $registry;
        if (isset($this->request->get['route'])) {
        $this->route = $this->request->get['route'];
        $this->template = $this->route . TEMPLATE_EXTENSION;
        }
        $this->companyid = $this->user->companyid;
        /* move to model class
        if ($this->companyid != '') {
        $this->table = new stdClass();
        $this->table->product = $this->companyid . '_products';
        $this->table->store = $this->companyid . '_stores';
        $this->table->transaction = $this->companyid . '_transaction';
        $this->table->sales = $this->companyid . '_sales';
        }
          */
    }
    
    public function __get($key) {
        return $this->registry->get($key);
    }
    
    public function __set($key, $value) {
        $this->registry->set($key, $value);
    }
            
    protected function forward($route, $args = array()) {
        return new Action($route, $args);
    }

    protected function redirect($url) {
        header('Location: ' . str_replace('&amp;', '&', $url));
        exit();
    }
    
    protected function render($return = FALSE) {

        foreach ($this->children as $child) {
            $action = new Action($child);
            $file   = $action->getFile();
            $class  = $action->getClass();
            $method = $action->getMethod();
            $args   = $action->getArgs();
            if (file_exists($file)) {
                require_once($file);
                $controller = new $class($this->registry);
                $controller->index();
                $this->data[$controller->id] = $controller->output;
            } else {
                exit('Error: Could not load controller ' . $child . '!');
            }
        }
        if ($return) {
            return $this->fetch($this->template);
        } else {
            $this->output = $this->fetch($this->template);
        }
    }
    
    protected function fetch($filename) {
        $file = DIR_TEMPLATE . $filename;
    
        if (file_exists($file)) {
            extract($this->data);
              ob_start();
      
                require($file);
      
                $content = ob_get_contents();
              ob_end_clean();

                return $content;
        } else {
                exit('Error: Could not load template ' . $file . '!');
        }
    }

    protected function error( $msg, $redirect = '' ) {
        $this->data['msg'] = $msg;
        $this->data['redirect'] = $redirect;
        $this->template = 'common/error.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );
        $this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
    }
}
?>
