<?php
final class Action {

    protected $file;
    protected $class;
    protected $method;
    protected $args = array();

    public function __construct($route, $args = array()) {

        $path = '';
        $parts = explode('/', str_replace('../', '', $route));
				//echo '<pre>'; print_r($parts); echo '</pre>';	
        foreach ($parts as $part) { 
						$path .= $part;
						$dir = DIR_APPLICATION . 'controller/' . $path;
						if (is_dir($dir)) {

                $path .= '/';
                array_shift($parts);
                continue;
            }

            $file = DIR_APPLICATION . 'controller/' . str_replace('../', '', $path) . '.php';

            if (is_file($file)) {
                
                $this->file = DIR_APPLICATION . 'controller/' . str_replace('../', '', $path) . '.php';
                $this->class = 'Controller' . preg_replace('/[^a-zA-Z0-9]/', '', $path);
                if ($args) {
                    $this->args = $args;
                    }
                
                array_shift($parts);
                
                break;
            
            }

        }

        $method = array_shift($parts);
        if ($method) {
            $this->method = $method;
        } else {
            $this->method = 'index';
        }
        
    }

    public function getFile() {
        return $this->file;
    }
    
    public function getClass() {
        return $this->class;
    }
    
    public function getMethod() {
        return $this->method;
    }
    
    public function getArgs() {
        return $this->args;
    }
}
?>
