<?php
abstract class Model {
    
    protected $registry;
    public $table;
    protected $companyid;
    
    public function __construct($registry) {
        $this->registry = $registry;
        $this->companyid = isset($this->session->data['companyid']) ? $this->session->data['companyid'] : '' ;

        //echo '<pre>'; print_r($this->companyid); echo '</pre>';
        if ($this->companyid != '') {
            $this->table = new stdClass();
            $this->table->category = $this->companyid . '_category';
            $this->table->category_description = $this->companyid . '_category_description';
            $this->table->category_filter = $this->companyid . '_category_filter';
            $this->table->product = $this->companyid . '_products';
            $this->table->stores = $this->companyid . '_stores';
            $this->table->transaction = $this->companyid . '_transaction';
            $this->table->sales = $this->companyid . '_sales';
            $this->table->category = $this->companyid . '_category';
            $this->table->user = $this->companyid . '_user';
            $this->table->user_group = $this->companyid . '_user_group';
            $this->table->statement = $this->companyid . '_statement';
            $this->table->statement_detail = $this->companyid . '_statement_detail';
        }
    }
    
    public function __get($key) {
        return $this->registry->get($key);
    }
    
    public function __set($key, $value) {
        $this->registry->set($key, $value);
    }

}
?>
