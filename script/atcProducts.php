#!/usr/bin/php
<?php
require_once '../config.php';
require_once '../system/library/cache.php';

$link = mysql_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD);
if (!$link) {
    die('Could not connect: ' . mysql_error());
}

$db_selected = mysql_select_db(DB_DATABASE, $link);
if (!$db_selected) {
    die ('Can\'t use DB : ' . mysql_error());
}

$oCache = new Cache();
$key = 'products';

$oCache->delete($key);

$sql = "SELECT p.product_id, p.name as name, p.model, p.ws_price, p.rt_price,
               p.quantity
          FROM " . $this->table->product . " p
          JOIN product_description pd ON p.product_id = pd.product_id
         WHERE p.status = 1
           AND p.model != ''
           AND p.name != ''
         ORDER BY p.model ASC";

$result = mysql_query($sql);
if (!$result) {
    die('Invalid query: ' . mysql_error());
}

$data = array();
while ($row = mysql_fetch_assoc($result)) {
        $data[] = $row;
}
$oCache->set($key, $data);
mysql_close($link);
?>