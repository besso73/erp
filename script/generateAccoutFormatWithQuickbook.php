<?php
/*
 * generate Aroo format with Quickbook raw csv
 */
$filename = "data/aroo.quickbook.account";
$handle = fopen($filename, "rb");
$id = 1;

$newContent = '"storelocator.id","storelocator.accountno","storelocator.name","storelocator.storetype","storelocator.address1","storelocator.address2","storelocator.city","storelocator.state","storelocator.zipcode","storelocator.phone1","storelocator.phone2","storelocator.salesrep","storelocator.status","storelocator.fax","storelocator.lat","storelocator.lng","storelocator.chrt","storelocator.parent","storelocator.comment","storelocator.email","storelocator.billto","storelocator.shipto","storelocator.discount","storelocator.owner"' . "\n";

$errorContent = '';

$aKey = array();
while (!feof($handle)) {
        $newline = '';
        $errorLine = '';
        $line = fgets($handle);
        //echo $line . "\n";
            $accounts = explode("    ",$line);
        //var_dump($accounts);
        $name = $accounts[0];
        $name = ereg_replace(',','',$name);
        $address1 = $accounts[1];
        $address1 = ereg_replace(',','',$address1);
        $aCity = explode(',', $accounts[2]);
        $city = $aCity[0];
        $stateZipCombine = $aCity[1];
            $state = '';
        if (strstr($stateZipCombine,'.')) {
                $aState = explode('.', $stateZipCombine);
                $state = trim($aState[0]);
                $zipcode = trim($aState[1]);
        }
            $phone = $accounts[3];
        $phone = ereg_replace('Tel)','',$phone);
        $phone = ereg_replace('tel)','',$phone);
        $phone = trim($phone);
        $accountno = substr($phone, strlen($phone)-4, strlen($phone) );
        $accountno = substr($phone,-4);
    
        if ('' == $accountno) {
        $errorContent .= $line;
        continue;
    }

    /*
        if ($accountno == '') {
        var_dump($accounts);
        exit;
    }
      */
    
    /*    
        if ($accounot != '') {
        if (in_array($accountno,$aKey)) {
            //echo 'same : ' . $accountno . "\n";
        $accountno .= 'A';
        }
    }
      */
            $aKey[$id] = $accountno;
            $comment = '';
        if (isset($accounts[5])) {
                $comment = trim($accounts[5]);
        }
            $newline = '"' . $id .'",';
        $newline.= '"' . $accountno .'",';
        $newline.= '"' . $name .'",';
        $newline.= '"R",';      // storetype
        $newline.= '"' . $address1 .'",';
        $newline.= '"",';       // address2
        $newline.= '"' . $city .'",';
        $newline.= '"' . $state .'",';
        $newline.= '"' . $zipcode .'",';
        $newline.= '"' . $phone .'",';
        $newline.= '"",';       // phone2
        $newline.= '"richard",';
        $newline.= '"1",';      //status
        $newline.= '"",';       // fax
        $newline.= '"",';       // lat
        $newline.= '"",';       // lng
        $newline.= '"",';       // chrt
        $newline.= '"",';       // parent
        if ($comment != '' ) {
            $newline.= '"' . $comment .'",';
    } else {
            $newline.= '"",';
    }
        $newline.= '"",';       // email
        $newline.= '"",';       // billto
        $newline.= '"",';       // shipto
        $newline.= '"",';       // discount
        $newline.= '"besso"';        // owner
        $newline.= "\n";
            //echo $newline;
        $newContent .= $newline;
        $id++;

        //exit;
}
fclose($handle);

$newFile = 'data/storelocator.csv';
file_put_contents($newFile, $newContent);

$errorFile = 'data/error.csv';
file_put_contents($errorFile, $errorContent);
?>
