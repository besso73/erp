$(function() {

    /////////////////////////////////////////////////////////////////////////////
    // common
    /////////////////////////////////////////////////////////////////////////////

    //localStorage.clear();

    /* Display 'Select All/Unselect All' */
    $(".scrollbox").each(function(i) {
        $(this).attr('id', 'scrollbox_' + i);
        sbox = '#' + $(this).attr('id') + ' input';
        $(this).after('<span><a onclick="$(\'' + sbox + '\').prop(\'checked\', true);"><u>Select All</u></a> / <a onclick="$(\'' + sbox + '\').prop(\'checked\', false);"><u>Unselect All</u></a></span>');
    });

    // date picker binding
    $('input.date_pick')
    .bind('focusin',function(e) {  $tgt = $(e.target); $_date = $tgt.val();  })
    .datepicker({
        clickInput:true,createButton:false,startDate:'2000-01-01',dateFormat:'yy-mm-dd'
    })
    .bind('change',function(e) {
        var $tgt = $(e.target);
        if ($tgt.is('input[name=ship_appointment]')) {
            if ( $tgt.val() != $_date ) {
                $el_desc = $('textarea[name=description]');
                $desc = $el_desc.val();
                if ( $desc.indexOf("Shipping Date") == -1 ) {
                    if ($desc != '') {
                        $desc = $desc + '\n' + 'Shipping Date : ' + $tgt.val();
                    } else {
                        $desc = 'Shipping Date : ' + $(this).val();
                    }
                    $el_desc.val($desc);
                }
            }
        }
    });

    // todo. move to lib. common lib to calculate date difference
        $.fn.calculateDiffDays = function(day1,day2) {
        d1 = $.fn.parseDate(day1);
        d2 = $.fn.parseDate(day2);
        dd = (d2-d1)/(1000*60*60*24); 
        return parseInt(dd);
    };
            $.fn.parseDate = function(date) {
        var Ymd = date.split('-');
        return new Date(Ymd[0],Ymd[1],Ymd[2]);
    };

    /*
    var $clickNode = $('#order table tr');
        $('#order').bind('click',function(event) {
        if ($tgt.is('input')) {
            $tgt.select();
        }
    });
      */

    /////////////////////////////////////////////////////////////////////////////
    // account
    /////////////////////////////////////////////////////////////////////////////
    /*****
    // after event keydown completed , we need to release the binding
    $('#storeinfo input[name="accountno"]').bind('keydown',function(e) {
        if (e.keyCode == 13) {
            e.preventDefault(); $.fn.storeSubmit(e);
        }
    });

    $('#storeinfo input[name="store_name"]').bind('keydown',function(e) {
        if (e.keyCode == 13) {
            e.preventDefault(); $.fn.storeSubmit(e);
        }
    });
    
    $.fn.storeSubmit = function(e) {
        param = '';
        accountno = $('input[name=accountno]').val();
        if (accountno)    param += 'filter_accountno=' + encodeURIComponent(accountno);

        //salesrep = $('input[name=salesrep]').val();
        //if (salesrep)  param += '&filter_salesrep=' + encodeURIComponent(salesrep);

        store_name = $('input[name=store_name]').val();
        if (store_name)  param += '&filter_name=' + encodeURIComponent(store_name);
        $.ajax({
            type:'get',
            url:'/store/lookup/callback',
            dataType:'html',
            data:param,
            success:function(html) {
                $.fancybox(html);
            } 
        });
    }
    ****/
    
    $('.account-insert').bind('click',function(e) {
        e.preventDefault();
        var that = $(e.target),
            store_id = $('#storeinfo').find('input[name=store_id]').val(),
            data = '',
            mode = '';
        if (that.is('input'))    return false;
        if (that.hasClass('account-new')) {
            mode = 'new';
        } else {
            mode = 'edit';
        }

        data = {
            'store_id' : store_id,
            'mode' : mode
        }
        $.ajax({
            type: 'get',
            dataType: 'html',
            url: 'store/list/callInsertPannel',
            data: data,
            success:function(html) {
                $.fancybox(html);
            }
        });
    });

    $('.update-account').bind('click',function(e) {
        e.preventDefault();
        var that = $(e.target),
            store_id = that.data('id'),
            data = '';
        data = {
            'store_id' : store_id,
            'mode' : 'update'
        }
        $.ajax({
            type: 'get',
            dataType: 'html',
            url: 'store/list/callInsertPannel',
            data: data,
            success:function(html) {
                $.fancybox(html);
            }
        });
    });

    $('.update-account2').bind('click',function(e) {
        e.preventDefault();
        var that = $(e.target),
            store_id = that.data('id'),
            data = '';
        data = {
            'store_id' : store_id,
            'mode' : 'update',
            'from' : 'account'
        }
        $.ajax({
            type: 'get',
            dataType: 'html',
            url: 'store/list/callInsertPannel',
            data: data,
            success:function(html) {
                $.fancybox(html);
            }
        });
    });
    
    $('#account-check').live('focusout', function(e) {
        var that = $(e.target),
            accountno = that.val();
        $.ajax({
            type:'post',
            url:'/store/list/getUniqAccountno',
            data:'accountno=' + accountno,
            dataType: 'json',
            success:function(res) {
                if (res.code == '200') {
                    that.val(res.message);
                    } else {
                    alert('fail to get new account number');
                    return false;
                    }
            }
        });
    })
    
    $('.submit-update-store').live('click',function(e) {

        e.preventDefault();
        var that = $('#updateForm'),
            accountno = that.find('input[name=accountno]').val();
            
        if (accountno == '' ) {
            alert ('Input Account Number');
            return false;
        }
        if (that.find('input[name=name]').val() == '' ) {
            alert ('Input Account Name');
            return false;
        }
        if (that.find('input[name=accountno]').val() == '' ) {
            alert ('Input Account Number');
            return false;
        }

        that.submit();
        
        /*
        var action = that.attr('action'),
            data = that.serialize();
            $.ajax({
            type: 'POST',
            url: action, 
            data: data,
            dataType: 'json',
            success: function(res) {
                if (res.code == '200') {
                    $.fancybox.close();
                    }
            }
        });
          */
    });
            $('#account-history').bind('click',function(e) {
        
        e.preventDefault();
        var accountno = $('#storeinfo').find('input[name=accountno]').val();
        //url = '/report/account&accountno=' + accountno;
        var url = '/sales/list&filter_txid=' + accountno;
        window.open(url);
    });


    /////////////////////////////////////////////////////////////////////////////
    // category 
    /////////////////////////////////////////////////////////////////////////////
        $('.category-wrap #save').live('click',function(e) {

        e.preventDefault();
        var that = $(e.target),
            papa = that.closest('div.category-wrap'),
            level = papa.data('level'),
            oCode = papa.find('input[name=code' + level + ']'),
            oName = papa.find('input[name=name' + level + ']'),
            isProduct = papa.find('input[name="isProduct"]').is(':checked') ? true : false ,
            code = oCode.val(),
            name = oName.val();
        if ( level == 'size' ) {
            if ( code.length > 3 ) {
                alert('length should be less than 3!');
                return false;
            }
        } else {
            if ( code.length > 2 ) {
                alert('length should be less than 2!');
                return false;
            }
        }
        if ( code.length == '' ) {
            alert('Code empty!');
            return false;
        }
        if ( name == '' ) {
            alert('Name empty!');
            return false;
        }

        code = code.toUpperCase();
        oCode.val(code);
        if (isProduct) {
            // it's insert for category and product both
        }

        var data = {
            'level': level,
            'code' : code,
            'name' : name,
            'isProduct' : isProduct
        };
            $.ajax({
            type: 'POST',
            url: '/product/price/insertCategory', 
            data: data,
            dataType: 'json',
            success: function(res) {
                if (res.code == '200') {
                    
                    }
            }
        });
    });

    $.fn.fillProductModel = function(cat) {
        var eCat1 = $('select#cat1');
        var eCat2 = $('select#cat2');
        var eCat3 = $('select#cat3');
        var eCategory = $('input[name="category"]');
        var cat;

        if ( eCat3.val() != '0' ) {
            eCategory.val(eCat3.val());
            return false;
        }

        if ( eCat2.val() != '0' ) {
            eCategory.val(eCat2.val());
            return false;
        }

        if ( eCat1.val() != '0' ) {
            eCategory.val(eCat1.val());
            return false;
        }

    }

    $('select#cat1').live('change', function() {
        var code = $(this).val();

        if ( code == '0' ) {
            $('select#cat2').find('option').remove().end().hide();
            $('select#cat3').find('option').remove().end().hide();
            $('input[name="category"]').val('');
            return false;
        }

        $.ajax({
            url : 'product/price/getSubCategories&code=' + code,
            method : 'GET',
            dateType : 'JSON',
            success: function(data) {
                if ( data != '' ) {
                    var ele = jQuery.parseJSON(data);
                    $('select#cat2').find('option').remove().end();
                    $(ele).each(function( idx, o ) {
                        $('select#cat2')
                        .append($("<option></option>")
                        .attr("value",o.code)
                        .text(o.name)).
                        show();
                    }); 
                    
                    $.fn.fillProductModel();
                }
            }
        });
        
    });

    $('select#cat2').live('change', function() {
        var code = $(this).val();

        if ( code == '0' ) {
            $('select#cat3').find('option').remove().end().hide();
            $('input[name="category"]').val('');
            return false;
        }

        $.ajax({
            url : 'product/price/getSubCategories&code=' + code,
            method : 'GET',
            dateType : 'JSON',
            success: function(data) {
                if ( data != '' ) {
                    var ele = jQuery.parseJSON(data);
                    $('select#cat3').find('option').remove().end();
                    $(ele).each(function( idx, o ) {
                        $('select#cat3')
                        .append($("<option></option>")
                        .attr("value",o.code)
                        .text(o.name)).
                        show();
                    }); 
                    
                }

                $.fn.fillProductModel();
            }
        });
    });

    $('select#cat3').live('change', function() {
        $.fn.fillProductModel();
    });



    /////////////////////////////////////////////////////////////////////////////
    // product
    /////////////////////////////////////////////////////////////////////////////

    $('#product-insert').bind('click',function(e) {
        e.preventDefault();
        $.ajax({
            type:'get',
            url:'/product/price/callUpdatePannel',
            dataType:'html',
            data:'mode=insert',
            success:function(html) {
                $.fancybox(html);
            }
        });
    });

    /*
        $('#product-update').bind('click',function(e) {
        e.preventDefault();
        $.ajax({
            type:'get',
            url:'/product/price/callUpdatePannel',
            dataType:'html',
            data:'mode=update',
            success:function(html) {
                $.fancybox(html);
            }
        });
    });
      */

    $('#flush-product-cache').bind('click',function(e) {
        e.preventDefault();
        /* TODO
           * set general loading method
           * besso, 2014-03
           */
        $.ajax({
            type:'get',
            url:'/common/cache/ajaxFlushCache',
            dataType:'json',
            data:'reset=true&key=products',
            success:function(res) {
                if (res.code == '200') {
                    localStorage.setItem('p',res.message);
                    //alert('flush product cache done');
                    }
            }
        });
    });
    
    $('#save-product').live('click',function(e) {
        e.preventDefault();
        var that = $('#updateForm'),
            model = that.find('input[name=model]').val();
        
        //if (model == '' ) {
        //    alert ('Input item code');
        //    return false;
        //}
        
        if (that.find('input[name=name]').val() == '' ) {
            alert ('Input Product Name');
            return false;
        }
        if (that.find('input[name=rt_price]').val() == '' ) {
            alert ('Input Retail price');
            return false;
        }
        
        var action = $('#updateForm').attr('action');
        $.ajax({
            type: 'POST',
            url: action, 
            data: $('#updateForm').serialize(),
            dataType: 'json',
            success: function(res) {
                if (res.code == '200') {
                    $('#flush-product-cache').click();
                    $.fancybox.close();
                    window.location.href = 'product/price';
                }
            }
        });
    });

    $('#product-model').live('focusout', function(e) {
        var that = $(this),
            model = that.val();
        $.ajax({
            type:'post',
            url:'product/price/getProductByModel',
            data:'model=' + model,
            dataType: 'json',
            success:function(res) {
                if (res.code == '200') {
                    alert(model + ' already exist. \nPlease choose other name');
                    that.val('').focus();
                    }
            }
        });
    })
        /////////////////////////////////////////////////////////////////////////////
    // company
    /////////////////////////////////////////////////////////////////////////////
    $('.insert-company-panel, .update-company-panel').bind('click',function(e) {
        e.preventDefault();
        var that = $(e.target),
            href = that.attr('href');
        $.ajax({
            type: 'get',
            dataType:'html',
            url: href,
            success:function(html) {
                
                $.fancybox(html);
            }
        });
    });
            $('.save-company, .update-company').live('click',function(e) {
        e.preventDefault();
        var that = $('#updateForm'),
            companyid = that.find('input[name=companyid]').val();
        if (companyid == '' ) {
            alert ('Input Company ID');
            return false;
        }
        if (that.find('input[name=name]').val() == '' ) {
            alert ('Input Company Name');
            return false;
        }
        if (that.find('input[name=email]').val() == '' ) {
            alert ('Input Email');
            return false;
        }
       
        /* TODO serialize error happen and couldn't fix yet
        var action = that.attr('action'),
            data = $('#updateForm').serialize();
        $.ajax({
            type: 'POST',
            url: action, 
            data: data,
            dataType: 'json',
            success: function(res) {
                if (res.code == '200') {
                    $.fancybox.close();
                    }
            }
        });
          */

        var action = that.attr('action'),
            id = that.find('input[name="id"]').val(),
            companyid = that.find('input[name="companyid"]').val(),
            name = that.find('input[name="name"]').val(),
            email = that.find('input[name="email"]').val(),
            phone = that.find('input[name="phone"]').val(),
            status = that.find('select[name="status"]').val();

        var data = {
            'id':id,
            'companyid':companyid,
            'name':name,
            'email':email,
            'phone':phone,
            'status':status
        };
            $.ajax({
            type: 'POST',
            url: action, 
            data: data,
            dataType: 'json',
            success: function(res) {
                if (res.code == '200') {
                   $.fancybox.close();
                     window.location.reload(true);
                    } else if ( res.code == '500' ) {
                    alert (res.msg);
                    $('input[name="companyid"]').select().focus();
                    }
            }
        });
    });
            $('#companyid').live('focusout', function(e) {
        var that = $(this),
            companyid = that.val();
        $.ajax({
            type:'post',
            url:'company/list/checkCompanyid',
            data:'companyid=' + companyid,
            dataType: 'json',
            success:function(res) {
                if (res.code == '200') {
                    alert(companyid + ' already exist. \nPlease choose other');
                    that.val('').focus();
                    }
            }
        });
    })
    
    /////////////////////////////////////////////////////////////////////////////
    // order
    /////////////////////////////////////////////////////////////////////////////
    $('#show-invoice').bind('click',function(e) {
        e.preventDefault();
        var txid = $('input[name="txid"]').val();
        var url = '/invoice/sheet&txid=' + txid;
        var option = 'width=800,scrollbars=1';
        window.open(url, 'invoice', option);
    });
    
    $.fn.loader = function( msg, timeout ) {
        var html = '<div id="loader">'+ msg +'</div>',
            p = $('#product-atc-container').position();
        $("body").append(html);
        $("#loader").css('padding-top', p.top + 200 );

        setTimeout ( function() {
                $("#loader").remove();
            }, timeout
        );
    }

    /////////////////////////////////////////////////////////////////////////////
    // help
    /////////////////////////////////////////////////////////////////////////////
    $('#help').bind('click', function(e){
        e.preventDefault();
        var route = $(this).data('route');
            route += '/help';
        window.open(route);
    });

    $.fn.tooltip = function(o,html) {
        $(o).hover(
            function() {
                var offset = $(this).offset();
                $("#tooltip").html(html);
                $("#tooltip").css( "top", offset.top + 30 ).css("left", offset.left).css("display", "block");
                $("#tooltip").animate({ opacity: 1.0 }, 300);
            },
            function() {
                $("#tooltip").animate({ opacity: 0.0 }, 300, function() {
                    $("#tooltip").css("display", "none");
                });
            }
        );        
    }
});
