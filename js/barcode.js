$(function() {
    var barcode_form = $('#barcode-form');

    $('#barcode-atc').focus();
    
    // Barcode control
    var pressed = false; 
    var chars = []; 
        $(window).keypress(function(e) {
        if (e.which >= 48 && e.which <= 57) {
            chars.push(String.fromCharCode(e.which));
        }
        //console.log(e.which + ":" + chars.join("|"));
        //console.log(chars);
        if (pressed == false) {
            setTimeout(function() {
                if (chars.length >= 10) {
                    var barcode = chars.join("");
                    //console.log("Barcode Scanned: " + barcode);
                    // assign value to some input (or do whatever you want)
                    $('input[name="barcode"]').val(barcode);
                    $('#barcode-submit').click();
                    }
                chars = [];
                pressed = false;
            },500);
        }
        pressed = true;
    });

    $('#barcode-submit').bind('click', function(e) {
        var oProductId = $('input[name="product_id"]');
        var oModel = $('input[name="model"]');
    //debugger;
        if (oProductId.val() == '') {
            alert('Choose Product first');
            return false;
    }
        if (oModel.val() == '') {
            alert('Choose Product first');
            return false;
    }
        
        var action = barcode_form.attr('action');
        $.ajax({
            type: 'POST',
            url: action, 
            data: barcode_form.serialize(),
            dataType: 'json',
            success: function(res) {
                if (res.code == '200') {
                    $('#flush-product-cache').click();
                    //$.fancybox.close();
                    location.reload();
                    }
            }
        });
    });

    /*
        $('input[name="barcode"]').keypress(function(e) {
        if ( e.which === 13 ) {
            console.log("Prevent form submit.");
            e.preventDefault();
        }
    });
      */

    /* TODO focusein could be better, b
       */
    $('input').live('click',function(e) {
        $(this).select();
    });

    /////////////////////////////////////////////////////////////////////////////
    // barcode
    /////////////////////////////////////////////////////////////////////////////
        $.fn.setBarcodeDetailPanel = function(item) {
        $('#barcode-detail-panel input[name="product_id"]').val(item.product_id);
        $('#barcode-detail-panel input[name="model"]').val(item.model);
        $('#barcode-detail-panel input[name="name"]').val(item.name);
        $('#barcode-detail-panel input[name="quantity"]').val(item.quantity);
        $('#barcode-detail-panel input[name="rt_price"]').val(item.rt_price);
        $('#barcode-detail-panel input[name="barcode"]').val(item.barcode);
        $('#barcode-detail-panel input[name="barcode"]').select().focus();
    }
            $( "#barcode-atc" )
    .autocomplete({
        source: function (request, response) {
            $.ajax({
                url:"/sales/atc/products",
                dataType:"json",
                data: { query: request.term },
                success: function(data) {
                    response(data);
                    }
            });
        },
        select: function(event,ui) {
            $( "#barcode-atc" ).val( ui.item.name + " (" + ui.item.model + ")");
            $.fn.setBarcodeDetailPanel(ui.item);
            return false;
        },
        minLength: 1
        }).data( "autocomplete" )._renderItem = function( ul, item ) {
            return $( "<li></li>" )
        .data( "item.autocomplete", item )
        .append( "<a>" + item.name + " (" + item.model + ")" )
        .appendTo( ul );
    };

    $.fn.resetProductDetail = function() {
        $('#barcode-detail-panel input[name="product_id"]').val('');
        $('#barcode-detail-panel input[name="model"]').val('');
        $('#barcode-detail-panel input[name="name"]').val('');
        $('#barcode-detail-panel input[name="quantity"]').val('0').css('background-color','#FFF');
        $('#barcode-detail-panel input[name="rt_price"]').val('');
        $('#barcode-detail-panel input[name="barcode"]').val('');
        $('#barcode-atc').val('').select().focus();
    }
    
    /* check value is null of input box */ 
        $.fn.validateNull = function(obj) {
        if (obj.attr('value') == '') {
          alert('fill value : ' + obj.attr('name'));
          obj.css('background-color','red');
            return false;
        }
    }
    
        $.fn.submitBarcode = function(e) {
        e.preventDefault();
        var that = $('#barcode-form'),
            model = that.find('input[name=model]').val();
        //if (model == '' ) {
        //    alert ('Input item code');
        //    return false;
        //}
        if (that.find('input[name=name]').val() == '' ) {
            alert ('Input Product Name');
            return false;
        }
        if (that.find('input[name=rt_price]').val() == '' ) {
            alert ('Input Retail price');
            return false;
        }
        
        var action = that.attr('action');
        $.ajax({
            type: 'POST',
            url: action,
            data: that.serialize(),
            dataType: 'json',
            success: function(res) {
                if (res.code == '200') {
                    $('#result').show().text('success');
                    $.fn.resetProductDetail(); 
                    setTimeout(function() {
                        $('#result').hide();
                    }, 2000)
                    }
            }
        });
    };

    /*
    // besso Test Code
        $('input[name="product_id"]').val('3049');
        $('input[name="model"]').val('AATAHL3XL');
        $('input[name="name"]').val('AAA Long Sleeve T-Shirts 3X-Large Athletic Heather /DZ');
        $('input[name="rt_price"]').val('76.00');
      */

});
