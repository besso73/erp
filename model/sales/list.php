<?php
class ModelSalesList extends Model {

    public function getList($request = array()) {
        
        // todo. weird one. exclude all default condition from data to cache for * query in mysql , , besso-201103 
        if ($request) {
            $sql = "select x.txid, u.username as order_user, s.id as store_id, s.name as store_name ,
                          x.order_date,x.total ,x.subtotal, x.balance,x.approve_status,x.approved_user,
                         if ( (x.subtotal - x.payed_sum) > 0 , 'yet' , 'done' ) as payed_yn,
                         x.shipped_yn, x.shipped_date, x.term,x.status, x.executor, x.sign_yn, x.invoice_no, x.payed_sum
                      fROM "  . $this->table->transaction . " x , "  . $this->table->stores . " s, " . $this->table->user . " u 
                   where x.store_id = s.id and x.order_user = u.username ";
            /***
            if (!$this->user->isSales()) {
            $sql.= " and x.status != '0'";
            } 
              ***/
            // todo. this should be done by trigging , besso-201103 
            // ( select sum(price) from pay_history where txid = x.txid )
            if (isset($request['filter_txid']) && !is_null($request['filter_txid'])) {
                  $sql .= " AND LCASE(x.txid) LIKE '%" . $this->db->escape(strtolower($request['filter_txid'])) . "%'";
            }

            if (isset($request['filter_store_name']) && !is_null($request['filter_store_name'])) {
                $sql .= " AND LCASE(s.name) LIKE '%" . $this->db->escape(strtolower($request['filter_store_name'])) . "%'";
            }
            
            if (isset($request['filter_order_date']) && !is_null($request['filter_order_date'])) {
                $sql .= " AND LCASE(x.order_date) = '" . $this->db->escape(strtolower($request['filter_order_date'])) . "'";
            }
            
            if (isset($request['filter_order_date_from']) && !is_null($request['filter_order_date_from'])) {
                $sql .= " AND substr(x.order_date,1,10) between '" . $this->db->escape($request['filter_order_date_from']) . "' and '" . $this->db->escape($request['filter_order_date_to']) . "'";
            }
            
            if (isset($request['filter_ship']) && !is_null($request['filter_ship'])) {
                $sql .= " AND LCASE(x.shipped_yn) = '" . $this->db->escape(strtolower($request['filter_ship'])) . "'";
            }

            if ( isset($request['accountno']) && !empty($request['accountno']) ) {
                $sql .= " AND s.accountno = '" . $request['accountno'] . "'";
            }

            if (isset($request['filter_order_user']) && !is_null($request['filter_order_user'])) {
                  //$sql .= " AND x.order_user = ( select user_id from user where user_group_id = (select user_group_id from user_group where name = 'sales') and LCASE(username) LIKE '%" . $this->db->escape(strtolower($request['filter_order_user'])) . "%')";
                $sql .= " AND LCASE(u.username) = '" . $this->db->escape(strtolower($request['filter_order_user'])) . "'";
            }

            if ( isset($request['invoice_no']) && $request['invoice_no'] == 'exist' ) {
                $sql .= " AND x.invoice_no != ''";
            }

            if ( isset($request['filter_approve_status']) ) {
                $approve_status = $request['filter_approve_status'];
                if ( 'new' == $approve_status ) {
                  $sql .= " AND LCASE(x.approve_status) is null";
                    }elseif ( 'all' == $approve_status ) {
                  $sql .= " ";
                    } else {
                  $sql .= " AND LCASE(x.approve_status) = '" . $this->db->escape(strtolower($request['filter_approve_status'])) . "'";
                    }
            }

            if (isset($request['filter_ship']) && !is_null($request['filter_ship'])) {
                $sql .= " AND LCASE(x.shipped_yn) = '" . $this->db->escape(strtolower($request['filter_ship'])) . "'";
            }

            if ( isset($request['filter_status']) ) {
                $status = $request['filter_status'];
                $sql .= " AND x.status = $status";
            }

            if ( isset($request['sort']) ) {
                $sql .= " ORDER BY " . $request['sort'];
            } else {
                $sql .= " ORDER BY x.order_date";
            }
            
            if (isset($request['order']) && ($request['order'] == 'ASC')) {
                $sql .= " ASC";
            } else {
                $sql .= " DESC";
            }
            
            //$this->log->aPrint( $request );
            if (isset($request['limit']) || isset($request['limit'])) {
                if ($request['start'] < 0) $request['start'] = 0;
                if ($request['limit'] < 1) $request['limit'] = 40;
                $sql .= " LIMIT " . (int)$request['start'] . "," . (int)$request['limit'];
            }
            
            $query = $this->db->query($sql);
            return $query->rows;
        
        } else {
            $response = $this->cache->get('storelocator.' . $this->config->get('config_language_id'));
            if (!$response) {
            // todo. need to change it later under choosen period
                $query = $this->db->query("SELECT * from " . $this->table->transaction);
                $response = $query->rows;
                $this->cache->set('storelocator.' . $this->config->get('config_language_id'), $response);
            }
            return $response;
        }
    }

    public function getTotalList($request=array()) {
        if ($request) {

            $sql = "select count(*) as total
                      fROM "  . $this->table->transaction . " x , "  . $this->table->stores . " s, " . $this->table->user . " u 
                    where x.store_id = s.id and x.order_user = u.username ";

            if (isset($request['filter_txid']) && !is_null($request['filter_txid'])) {
                $sql .= " AND LCASE(x.txid) LIKE '%" . $this->db->escape(strtolower($request['filter_txid'])) . "%'";
            }

            if (isset($request['filter_store_name']) && !is_null($request['filter_store_name'])) {
                $sql .= " AND LCASE(s.name) LIKE '%" . $this->db->escape(strtolower($request['filter_store_name'])) . "%'";
            }

            if (isset($request['filter_order_date']) && !is_null($request['filter_order_date'])) {
                $sql .= " AND LCASE(x.order_date) = '" . $this->db->escape(strtolower($request['filter_order_date'])) . "'";
            }

            if (isset($request['filter_order_date_from']) && !is_null($request['filter_order_date_from'])) {
                $sql .= " AND substr(x.order_date,1,10) between '" . $this->db->escape($request['filter_order_date_from']) . "' and '" . $this->db->escape($request['filter_order_date_to']) . "'";
            }

            if ( isset($request['accountno']) && !empty($request['accountno']) ) {
                $sql .= " AND s.accountno = '" . $request['accountno'] . "'";
            }

            if (isset($request['filter_ship']) && !is_null($request['filter_ship'])) {
                $sql .= " AND LCASE(x.shipped_yn) = '" . $this->db->escape(strtolower($request['filter_ship'])) . "'";
            }
            if (isset($request['filter_order_user']) && !is_null($request['filter_order_user'])) {
                    //$sql .= " AND x.order_user = ( select user_id from user where user_group_id = (select user_group_id from user_group where name = 'sales') and LCASE(username) LIKE '%" . $this->db->escape(strtolower($request['filter_order_user'])) . "%')";
                $sql .= " AND LCASE(u.username) = '" . $this->db->escape(strtolower($request['filter_order_user'])) . "'";
            }
            if ( isset($request['filter_approve_status']) ) {
                $approve_status = $request['filter_approve_status'];
                if ( 'new' == $approve_status ) {
                    $sql .= " AND LCASE(x.approve_status) is null";
                    } elseif ( 'all' == $approve_status ) {
                    $sql .= " ";
                    } else {
                    $sql .= " AND LCASE(x.approve_status) = '" . $this->db->escape(strtolower($request['filter_approve_status'])) . "'";
                    }
            }
            if ( isset($request['filter_status']) ) {
                $status = $request['filter_status'];
                $sql .= " AND x.status = $status";
            }
            //$this->log->aPrint( $sql );
            $query = $this->db->query($sql);
            return $query->row['total'];
        } else {
            $query = $this->db->query("SELECT count(*) as total from " . $this->table->transaction);
            return $query->row['total'];
        }
    }

    public function deleteTransaction($txid) {
        $sql = "delete from " . $this->table->transaction . " where txid = '$txid'";
        $query = $this->db->query($sql);
    
        $sql = "delete from " . $this->table->sales . " where txid = '$txid'";
        //$query = $this->db->query($sql);
        if ($this->db->query($sql)) {
        $user = $this->user->getUserName();
        $msg = "Txid $txid Delete by $user";
        $this->log->write($msg,'delete.log');
    }
    
        $sql = "delete from ship where txid = '$txid'";
        $query = $this->db->query($sql);
        $sql = "delete from pay where txid = '$txid'";
        $query = $this->db->query($sql);
    
    }

    // it's temporary for sales only system
    public function updateShippedYN($request) {
        $txid = $request['txid'];
        $shipped_yn = $request['shipped_yn'];
        $shipped_yn = strtoupper($shipped_yn);
        $sql = "UPDATE ". $this->table->transaction . " set shipped_yn = '$shipped_yn' where txid = '$txid'";
        if ($this->db->query($sql)) {
      return true;
        } else {
      return false;
    }
    }

    public function updateSignYN($request) {
        $txid = $request['txid'];
        $sign_yn = $request['sign_yn'];
        $sign_yn = strtoupper($sign_yn);
        $sql = "UPDATE ". $this->table->transaction . " set sign_yn = '$sign_yn' where txid = '$txid'";
        if ($this->db->query($sql)) {
      return true;
        } else {
      return false;
    }
    }

    public function updateNotice($request) {
        //$this->log->aPrint( $request );
        $notice = $request['notice'];
        $user = $this->user->getUserName();
        $sql = "insert notice set notice = '$notice', date = now(), uploader = '$user'";
        if ($this->db->query($sql)) {
      return true;
        } else {
      return false;
    }
    }

    public function selectNotice() {
        //$this->log->aPrint( $request );
        $sql = "select * from notice order by id desc limit 1";
        $query = $this->db->query($sql);
      return $query->rows;
    }
}
?>
