<?php

class ModelProductBase extends Model {
    
    public function getTees( $refresh = false ) {
            $key = $this->companyid . '_' . CACHE_KEY_TEES;
        
        if ( $refresh == true ) {
            $this->cache->delete($key);
        }
    
        $sql = "SELECT p.product_id, p.name, p.model, p.ws_price, p.rt_price,
                       p.quantity, p.barcode
                    fROM " . $this->table->product . " p
                 WHERE p.status = 1
                   AND p.model != ''
                   AND p.name != ''
                   AND substr(p.model,1,2) = 'AA'
                 ORDER BY p.model ASC";
        $query = $this->db->query($sql);
        $aTee = $query->rows;
        
        $tees = array();
        foreach ( $query->rows as $row ) {
            $tees[$row['model']] = $row;
        }
        
        $this->cache->set($key, $tees);
        
        return $tees;
    }
    
        
    public function getStockings( $refresh = false ) {
            $key = $this->companyid . '_' . CACHE_KEY_STOCKINGS;
        
        if ( $refresh == true ) {
            $this->cache->delete($key);
        }
            $sql = "SELECT p.product_id, p.name, p.model, p.ws_price, p.rt_price,
                       p.quantity, p.barcode
                    fROM " . $this->table->product . " p
                 WHERE p.status = 1
                   AND p.model != ''
                   AND p.name != ''
                   AND substr(p.model,1,2) = 'PH'
                 ORDER BY p.model ASC";
        $query = $this->db->query($sql);
        $aTee = $query->rows;
        
        foreach ( $query->rows as $row ) {
            $stockings[$row['model']] = $row;
        }
       
        $stockings = array(); 
        $this->cache->set($key, $stockings);
        
        return $stockings;
    }

    public function getHats( $refresh = false ) {
            $key = $this->companyid . '_' . CACHE_KEY_HATS;
                    
        if ( $refresh == true ) {
            $this->cache->delete($key);
        }
            $sql = "SELECT p.product_id, p.name, p.model, p.ws_price, p.rt_price,
                       p.quantity, p.barcode
                    fROM " . $this->table->product . " p
                 WHERE p.status = 1
                   AND p.model != ''
                   AND p.name != ''
                   AND substr(p.model,1,2) = 'HT'
                 ORDER BY p.model ASC";
            $query = $this->db->query($sql);
        $aTee = $query->rows;
            $hats = array();        
        foreach ( $query->rows as $row ) {
            $hats[$row['model']] = $row;
        }
        
        $hats = array();
        $this->cache->set($key, $hats);
        
        return $hats;
    }

    public function getAdores( $refresh = false ) {
            $key = $this->companyid . '_' . CACHE_KEY_ADORE;
        if ( $refresh == true ) {
            $this->cache->delete($key);
        }
            $sql = "SELECT p.product_id, p.name, p.model, p.ws_price, p.rt_price,
            p.quantity, p.barcode
                  fROM " . $this->table->product . " p
                WHERE p.status = 1
                AND p.model != ''
                AND p.name != ''
                AND substr(p.model,1,3) = 'adr'
                ORDER BY p.model ASC";
        $query = $this->db->query($sql);
        $aTee = $query->rows;

        foreach ( $query->rows as $row ) {
            $adores[$row['model']] = $row;
        }
            $adores = array();
        $this->cache->set($key, $adores);

        return $adores;
    } 

    public function getBelts( $refresh = false ) {
            $key = $this->companyid . '_' . CACHE_KEY_BELT;
        if ( $refresh == true ) {
            $this->cache->delete($key);
        }
            $sql = "SELECT p.product_id, p.name, p.model, p.ws_price, p.rt_price,
            p.quantity, p.barcode
                  fROM " . $this->table->product . " p
                WHERE p.status = 1
                AND p.model != ''
                AND p.name != ''
                AND substr(p.model,1,2) = 'BT'
                ORDER BY p.model ASC";
        $query = $this->db->query($sql);
        $aTee = $query->rows;

        foreach ( $query->rows as $row ) {
            $belts[$row['model']] = $row;
        }
            $this->cache->set($key, $belts);

        return $belts;
    } 
    public function delete($id) {
        $sql = "DELETE from " . $this->table->product . " where product_id = '" . (int)$id . "'";
        $this->db->query($sql);
        return true;
    }
    public function getSubCategories( $code = 0 ) {
            $sql = "select c.category_id, c.code, parent_id, sort_order, status, name, description from " . $this->table->category . " c ";
        $sql.= "  JOIN " . $this->table->category_description . " cd on c.category_id = cd.category_id";
        $sql.= " WHERE parent_id = '$code' ";
        $categories = $this->db->query($sql);
        if ( $categories->rows ) {
            return $categories->rows;
        }

        return '';
    }

    public function getSubCategoriesById( $cid = 0 ) {
        //$sql = "select c.category_id, c.code, parent_id, sort_order, status, name, description from category c ";
        $sql = "select c.category_id, name from category c ";
        $sql.= "  JOIN category_description cd on c.category_id = cd.category_id";
        $sql.= " WHERE parent_id = '$cid' ";
        $categories = $this->db->query($sql);
        
        
        if ( $categories->rows ) {
          $response[0] = array('category_id' => 0,'name' => 'Choose Sub Category');
          return array_merge($response, $categories->rows);
          

        }

        return '';
    }

    /*

    public function getCategories( $id  = 0 ) {
            $sql = "select c.category_id, c.code, parent_id, sort_order, status, name, description from " . $this->table->category . " c ";
        $sql.= "  JOIN " . $this->table->category_description . " cd on c.category_id = cd.category_id";
        $sql.= " WHERE parent_id = '$id' ";
            $categories = $this->db->query($sql);
            $aCategory[$id] =  $categories->row;
        $aCategory[$id]['children'] = $this->children($categories->row['category_id']);

        return $aCategory;
    }
        // my recursive function
    private function children($parent_id = 0 , $arr = array()) {
            $sql = "select c.category_id, parent_id, sort_order, status, name, description from " . $this->table->category . " c ";
        $sql.= "  JOIN " . $this->table->category_description . " cd on c.category_id = cd.category_id";
        $sql.= " WHERE parent_id = '$parent_id' ";
        //echo $sql; exit;
        $categories = $this->db->query($sql);
        if ($categories->rows) {
        
            foreach($categories->rows as $row) {
            
                $id = $row['category_id'];
                
                //$row['children'] = $this->children($id , $row );
                $arr[$id] = $row;
                $arr[$id]['children'] = $this->children($id);
                
            }
        }

        return $arr ;
    }
      */

    private function hasChildren($id) {
            $sql = "select * from " . $this->table->category;
        $sql.= " where parent_id = '$id'";
        $categories = $this->db->query($sql);
        if ( count($categories->rows) > 0 ) {
            return true;
        }
        return false;
    }

    public function insertCategory($req) {
        
        $level = $req['level'];
        $code = $req['code'];
        $name = $req['name'];
        $isProduct = $req['isProduct'];
            $sql = "in";
    }
}
?>
