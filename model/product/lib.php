<?php
class ModelProductLib extends Model {
    // retrieve model,name for atc or else 
    public function getModelNamePerCat($cat = '') {
        $cond = '';
        if ($cat) {
        $cond .= " where substr(p.model,1,2) = '$cat' ";
    }
        $sql = 'select p.model, p.name FROM " . $this->table->product . " p';
        $sql.= $cond;
        $sql.= ' order by p.model';
        $query = $this->db->query($sql);
        return $query->rows;
    }

    // retrieve category - product map for atc batch
    // new product be exception. plz be careful of these manual stuff, , besso-201103 
    public function getCategoryMap() {
        $sql = "select p.model, pc.category_id FROM " . $this->table->product . " p, product_to_category pc ";
        $sql.= " where p.product_id = pc.product_id and pc.category_id not in ";
        $sql.= " ( select category_id from category where parent_id = 0 and category_id != '38') order by pc.category_id,p.model";
    //echo $sql;
        $query = $this->db->query($sql);
        return $query->rows;
    }

    // model is on center of backend process
    public function getProduct($model) {
        $sql = "select distinct p.*, p.name as product_name FROM " . $this->table->product . " p";
        $sql.= " where p.model = '" . $model . "'";
        $sql.= " order by p.name, p.model, p.pc";
        //$this->log->aPrint( $sql );
        $query = $this->db->query($sql);
        return $query->row;
    }

    // todo. 2012.03 gel default dc 2,5
    public function getProducts($model,$txid) {
        $sql = "select p.product_id, p.model, p.name as product_name,
                   p.ups_weight, p.ws_price, p.rt_price, s.order_quantity as cnt,p.thres,
                   s.free, s.damage, s.total_price, s.weight_row,
                   p.quantity, p.image, p.pc, s.promotion,
                   if ( p.dc > 0, p.dc, s.discount ) as dc1,
                   if ( p.dc2> 0, p.dc2,s.discount2) as dc2,
                   s.backorder,s.backfree,s.backdamage,s.backpromotion,s.price1 as rate,
                   x.store_id, sl.accountno
                fROM " . $this->table->product . " p
              join transaction x on x.txid = '$txid'
              join "  . $this->table->stores . " sl on x.store_id = sl.id
              left join " . $this->table->sales . " s on
                   p.model = s.model
                   and s.txid = '$txid'
              where p.model = '$model'
             order by p.sort_order,p.model";
        $query = $this->db->query($sql);
        $aReturn = $query->row;
        //$this->log->aPrint( $aReturn ); exit;
        /* TODO. exception pages : different price for specific account like Gini */
        if ( isset($aReturn['accountno']) ) {
            if ( $aReturn['accountno'] == 'FL3200' || $aReturn['accountno'] == 'TX3025' ||
            $aReturn['accountno'] == 'GA9222' || $aReturn['accountno'] == 'TX5636' ||
                $aReturn['accountno'] == 'IL3030' || $aReturn['accountno'] == 'CA7007' ||
            $aReturn['accountno'] == 'CA3760' ) {
        
        // Weft sealer 0.5 oz
            if ( $aReturn['model'] == '3S2449' || $aReturn['model'] == '3S2450' ) {
            $aReturn['ws_price'] = '19.50';
            }

        // Weft sealer 3.4 oz
            if ( $aReturn['model'] == '3S2453' || $aReturn['model'] == '3S2454' ) {
            $aReturn['ws_price'] = '39.80';
            }

        // I-REMI Conditioner 2oz
            if ( $aReturn['model'] == 'VN8711') {
            $aReturn['ws_price'] = '28.80';
            }

        // I-REMI Conditioner 8oz
            if ( $aReturn['model'] == 'VN8712') {
            $aReturn['ws_price'] = '20.94';
            }

        // I-REMI Silky Serum 2oz
            if ( $aReturn['model'] == 'VN8714') {
            $aReturn['ws_price'] = '46.80';
            }

            if ( $aReturn['model'] == '3S8625' || $aReturn['model'] == '3S8629' ) {
            $aReturn['ws_price'] = '65.88';
            $aReturn['rt_price'] = '71.88';
            }
            if ( $aReturn['model'] == 'SP8360' || $aReturn['model'] == 'SP8363' ||
                $aReturn['model'] == 'SP8378' || $aReturn['model'] == 'SP8376' ||
                $aReturn['model'] == 'SP8371' ) {
            $aReturn['ws_price'] = '12.48';
            }
            if ( $aReturn['model'] == 'SP8361' || $aReturn['model'] == 'SP8364' ||
                $aReturn['model'] == 'SP8379' || $aReturn['model'] == 'SP8377' ||
                $aReturn['model'] == 'SP8372' ) {
            $aReturn['ws_price'] = '20.70';
            }
            if ( $aReturn['model'] == 'SP8362' || $aReturn['model'] == 'SP8365' ) {
            $aReturn['ws_price'] = '35.82';
            }
            }
        }
        //$this->log->aPrint( $aReturn );
        return $aReturn;
    }

    public function getProductOrdered($txid) {
        $product_name = 'p.name_for_sales as product_name,';
        $sql = "select p.product_id, p.model,$product_name
                   p.ups_weight, p.ws_price, p.rt_price, s.order_quantity as cnt,
                   s.free, s.damage, s.total_price, s.weight_row,
                   p.quantity, p.image, p.pc, p.thres,
                   s.discount as dc1,
                   s.discount2 as dc2,
                   s.backorder,s.backfree,s.backdamage,s.promotion,s.backpromotion
                   ,s.price1 as rate
                fROM " . $this->table->product . " p
              join " . $this->table->sales . " s on
                   p.model = s.model 
                   and s.txid = '$txid'
             order by p.sort_order,p.model";
        $query = $this->db->query($sql);
        if ( isset($this->request->get['debug']) ) {
        $catalog = $this->config->getCatalogMobile();
        } else {
        $catalog = $this->config->getCatalog();
    }
            $aRtn = array();
        $aOrdered = $query->rows;
        //$this->log->aPrint( $catalog );
        $promotion_sum = $damage_sum = 0;
        foreach($catalog as $key => $aModel) {
        foreach( $aOrdered as $ordered ) {
        //$this->log->aPrint( $aModel );
        if (in_array($ordered['model'],$aModel)) {
            //$this->log->aPrint( $ordered );
            if ( $ordered['promotion'] > 0 ) {
            $promotion_sum += $ordered['promotion'] * $ordered['rate'];
            }
            if ( $ordered['damage'] > 0 ) {
            $damage_sum    += $ordered['damage'] * $ordered['rate'];
            }
            $ordered['promotion_sum'] = $promotion_sum;
            $ordered['damage_sum'] = $damage_sum;
            $aRtn[$key][] = $ordered;
            //$this->log->aPrint( $promotion_sum ); $this->log->aPrint( $damage_sum );
        }
    }
    }
        //$this->log->aPrint( $aRtn ); exit;
        //$this->log->aPrint( count($aRtn) );
        return $aRtn;
    }
}
?>
