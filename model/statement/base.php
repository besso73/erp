<?php
class ModelStatementBase extends Model {

    public function insertStatement($data) {

        $sql = "insert into " . $this->table->statement ;
        $sql.= "   set store_id = '" . $data['store_id'] . "',";
        $sql.= "       paid_type = '" . $data['paid_type'] . "',";
        $sql.= "       paid_amount = '" . $data['paid_amount'] . "',";
        $sql.= "       paid_date = '" . $data['paid_date'] . "',";
        $sql.= "       memo = '" . $data['memo'] . "',";
        $sql.= "       up_date = now()";
        if ($this->db->query($sql)) {
            return $this->db->getLastId();
        } else {
            return $sql;
        }
    }

    public function insertStatementDetail($data) {
        $sql = "insert into " . $this->table->statement_detail ;
        $sql.= "   set statement_id = '" . $data['statement_id'] . "',";
        $sql.= "       txid = '" . $data['txid'] . "',";
        $sql.= "       paid_amount = '" . $data['paying'] . "',";
        $sql.= "       balance = '" . $data['balance'] . "',";
        $sql.= "       paid_date = '" . $data['paid_date'] . "',";
        $sql.= "       up_date = now()";
        if ($this->db->query($sql)) {
            return $this->db->getLastId();
        } else {
            return $sql;
        }
    }
    
    /*
    Select sum(total), sum(payed_sum), sum(balance) from autonics_transaction where store_id = 1;
    select sum(paid_amount) from autonics_statement where store_id = 1;
     */
    public function getStoreBalance($store_id) {
        /*
        $sql = "Select sum(payed_sum) as paid_sum, sum(balance) as balance_sum, ";
        $sql.= " (select sum(paid_amount) from " . $this->table->statement ." where store_id = '$store_id') as paid_amount ";
        $sql.= "  from " . $this->table->transaction . " where store_id = '$store_id'";
        */

        $sql = "select s.id, sum(x.balance) as balance_sum,
                       sum(s.paid_amount) as paid_amount    
                  from test_transaction x
                  join test_statement s on s.store_id = x.store_id
                 where x.store_id = '$store_id'
                 group by s.id";
        //echo '<pre>'; print_r($sql); echo '</pre>';
        $query = $this->db->query($sql);

        $res = array();
        if ( !empty($query->rows) ) {
            foreach ( $query->rows as $row ) {
                $res[$row['id']] = $row;
            }
        }
        return $res;
    }

}
?>
