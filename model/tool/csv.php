<?php
class utf8encode_filter extends php_user_filter{ 
  function filter($in, $out, &$consumed, $closing) { 

    while ($bucket = stream_bucket_make_writeable($in)) { 
      /*
        echo '<pre>';
      print_r($bucket);
        echo '</pre>';
        */
        $bucket->data = utf8_encode($bucket->data); 
        $consumed += $bucket->datalen; 
        stream_bucket_append($out, $bucket); 
    } 
        return PSFS_PASS_ON; 
    } 
} 

class ModelToolCSV extends Model {

    public function getTables() {
        $table_data = array();
        $query = $this->db->query("SHOW TABLES FROM `" . DB_DATABASE . "`");

        foreach ($query->rows as $result) {
        $table_data[] = $result['Tables_in_' . DB_DATABASE];
        }

        return $table_data;
    }

    public function csvExport( $table, $sql = '' ) {
        
        $output = '';
        if (!$sql) {
            $sql = "SELECT * FROM `" . $table . "`"; // prefix already part of the table name being passed in
        }
        $query = $this->db->query($sql);

        if ( empty($query->rows) )  return false;

        $columns     = array_keys($query->row);

        $csv_terminated = "\n";
        $csv_separator = ",";
        $csv_enclosed = '"';
        $csv_escaped = "\\"; //linux
        $csv_escaped = '"';

        // Header Row need not have table name prefix
        /*
        if ( empty($table) ) {
            $output .= '"'. stripslashes(implode('","',$columns)) . "\"\n";
        } else {
            $output .= '"' . $table . '.' . stripslashes(implode('","' . $table . '.',$columns)) . "\"\n";
        }
        */

        $output .= '"'. stripslashes(implode('","',$columns)) . "\"\n";

        foreach ( $query->rows as $row ) {
            $schema_insert = '';

            $fields_cnt = count($row);

            foreach ($row as $k => $v) {
            
                if ($row[$k] == '0' || $row[$k] != '') {
            
                    if ($csv_enclosed == '') {
                        $schema_insert .= $row[$k];
                    } else {
                        $row[$k] = str_replace(array("\r","\n","\t"), "", $row[$k]);
                        $row[$k] = html_entity_decode($row[$k], ENT_COMPAT, "UTF-8");
                        $schema_insert .= $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed, $row[$k]) . $csv_enclosed;
                    }
                } else {
                    $schema_insert .= '';
                }
            
                if ($k < $fields_cnt - 1) {
                    $schema_insert .= $csv_separator;
                }
            }
            
            $output .= $schema_insert;
            $output .= $csv_terminated;
        }

        return $output;
    }


    public function csvImport($file, $table = '') {

        if (!$table) {
            exit('Could not retrieve table.');
        }

        $table = $this->getTableName($table);
        
        # let's change $fils as UTF-8
        ini_set('max_execution_time', 999999);

        // for mac linefeed, besso
        ini_set("auto_detect_line_endings", "1");

        setlocale( LC_ALL, 'en_US.UTF-8' );  

        //todo. change file as utf8 encoding before thru , besso 201105
        $handle = fopen($file,'r');
        /*
        stream_filter_register("utf8encode", "utf8encode_filter") or die("Failed to register filter"); 
        stream_filter_prepend($handle, "utf8encode"); 
          */
        if (!$handle) die('Cannot open uploaded file.');

        // Get Table name and Columns from header row
        $aColumn = array();
        
        
        $data = fgetcsv($handle, 1024, ",");

        // If the first line is blank, try second line
        /*
        if (!$data[0]) {
            $data = fgetcsv($handle, 1024, ",");
        }
          */
        foreach ($data as $d) {
            if (strpos($d, '.') !== false) {
                $tmp = explode('.', $d);
                //$table = $tmp[0];
                $columns[] = $tmp[1];
            } else {
                $columns[] = $d;
            }
        }


        /****
        //header('Content-Type: text/html; charset=euc-kr'); 
        mb_internal_encoding("EUC-KR");
        $s = '?????¿?
        echo $s;
        // return true if euc-kr
        //$enc = mb_detect_encoding($s,'euc-kr');
        $enc = mb_detect_encoding($s,'euc-kr');
        $a = mb_convert_encoding($s,$enc,'UTF-8');
        $this->log->aPrint( 'MB : ' . $a );
    
        $a = iconv($enc,'UTF-8',$s);
        $this->log->aPrint( 'ICONV : ' . $a );
        exit;
          ****/

        // if column name is column , it violates the native variable name rule.
        $aColumn = array();
        foreach ( $columns as $col ) {
            $aColumn[] = '`' . $col . '`';
        }
        $columnCount = count($aColumn);
        //var_dump( ( fread( $handle, 10000 ) ) );  
        $row_count = 0;

        $sql = "INSERT INTO " . DB_PREFIX . $table . "(". implode(',',$aColumn) .") VALUES ";
        while (($data = fgets($handle, 4096)) !== false) {
            $data = explode(',',$data);

            $sql .= '(';
            $row = '';

            foreach($data as $key=>$value) {
                
                if ( $key == $columnCount) break;

                $value = str_replace(',','',$value);
                $value = str_replace('"','',$value);
                
                $value = '"' . trim($value) . '",';
                $row.= $value;

            }
            $row = substr($row, 0, -1);
            $sql .= $row . '),';
        }
        $sql = substr($sql, 0, -1);
        fclose($handle);

        $truncateSql = "TRUNCATE TABLE "  . $table;
        $this->db->query($truncateSql);
        
        $this->db->query("set names utf8"); 
        $this->db->query($sql);
        //$this->cache->delete('product');

        return $row_count;
    }

    function validDate($date) {
        //replace / with - in the date
        $date = strtr($date,'/','-');
        //explode the date into date,month and year
        $datearr = explode('-', $date);
        //count that there are 3 elements in the array
        if (count($datearr) == 3) {
            list($d, $m, $y) = $datearr;
            /*checkdate - check whether the date is valid. strtotime - Parse about any English textual datetime description into a Unix timestamp. Thus, it restricts any input before 1901 and after 2038, i.e., it invalidate outrange dates like 01-01-2500. preg_match - match the pattern*/
        if (checkdate($m, $d, $y) && strtotime("$y-$m-$d") && preg_match('#\b\d{2}[/-]\d{2}[/-]\d{4}\b#', "$d-$m-$y")) { /*echo "valid date";*/
                return TRUE;
    } else {/*echo "invalid date";*/
                return FALSE;
    }
        } else {/*echo "invalid date";*/
            return FALSE;
        }
        /*echo "invalid date";*/
        return FALSE;
    }
    

    private function getTableName($name) {

        $tableName = '';
        if ( substr($name,0,8) == 'products' ) {
            $tableName = $this->table->product;
        }

        if ( substr($name,0,6) == 'stores' ) {
            $tableName = $this->table->stores;
        }

        return $tableName;
    }
    
}
?>
