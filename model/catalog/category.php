<?php
class ModelCatalogCategory extends Model {

    public function addCategory($data) {
        $code = isset($data['code']) ? $data['code'] : '';
        if ( empty($code) ) {
            return false;
        }

        $sql = "INSERT INTO category SET parent_id = '" . $data['parent_id'] . "'";
        $sql.= "    ,sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "'";
        $sql.= "    , code = '$code', date_modified = NOW(), date_added = NOW()";
        $this->db->query($sql);
        $category_id = $this->db->getLastId();
        
        if (isset($data['image'])) {
            $this->db->query("UPDATE category SET image = '" . $this->db->escape($data['image']) . "' WHERE category_id = '" . (int)$category_id . "'");
        }
        
        foreach ($data['category_description'] as $language_id => $value) {
            $sql = "INSERT INTO category_description SET category_id = '" . (int)$category_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', meta_keywords = '" . $this->db->escape($value['meta_keywords']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', description = '" . $this->db->escape($value['description']) . "'";
            $this->db->query($sql);
        }

        /*
        if (isset($data['category_store'])) {
            foreach ($data['category_store'] as $store_id) {
                $this->db->query("INSERT INTO category_description SET category_id = '" . (int)$category_id . "', store_id = '" . (int)$store_id . "'");
            }
        }
        if ($data['keyword']) {
            $this->db->query("INSERT INTO url_alias SET query = 'category_id=" . (int)$category_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
        }
        */
        
        $this->cache->delete('category');
    }

    public function getSub2Codes($pcode) {
        $sql = "select right(code,2) as sub from category where parent_id = '$pcode'";
        $query = $this->db->query($sql);
        $res = array();
        if ( !empty($query->rows) ) {
            foreach ( $query->rows as $row ) {
                $res[] = $row['sub'];
            }
        }
        return $res;
    }
    
    public function editCategory( $category_id, $data ) {
        
        $sql = "UPDATE category";
        $sql.= "   SET parent_id = '" . $data['parent_id'] . "',";
        $sql.= "       code = '" . $data['code'] . "',";
        $sql.= "       sort_order = '" . (int)$data['sort_order'] . "',";
        $sql.= "       status = '" . (int)$data['status'] . "',";
        $sql.= " date_modified = NOW() WHERE category_id = '" . $category_id . "'";

        $this->db->query($sql);

        $sql = "UPDATE category_description";
        $sql.= "   SET Name = '" . $data['category_description'][1]['name'] . "' WHERE category_id = '" . (int)$category_id . "'";
        $this->db->query($sql);

        $this->cache->delete('category');
    }
    
    public function deleteCategory($category_id) {
        $this->db->query("DELETE FROM category WHERE category_id = '" . (int)$category_id . "'");
        $this->db->query("DELETE FROM category_description WHERE category_id = '" . (int)$category_id . "'");
        /*
        $this->db->query("DELETE FROM category_to_store WHERE category_id = '" . (int)$category_id . "'");
        $this->db->query("DELETE FROM url_alias WHERE query = 'category_id=" . (int)$category_id . "'");
          */
            $sql = "SELECT category_id FROM category WHERE parent_id = '" . (int)$category_id . "'";
        $query = $this->db->query($sql);

        foreach ($query->rows as $result) {
        $this->deleteCategory($result['category_id']);
        }
        
        $this->cache->delete('category');
    } 

    public function getCategory($category_id) {

        $sql = "SELECT * from category c ";
        $sql.= "  JOIN category_description cd on c.category_id = cd.category_id ";
        $sql.= " WHERE c.category_id = '$category_id'";

        //$sql = "SELECT DISTINCT *, (SELECT keyword FROM url_alias WHERE query = 'category_id=" . (int)$category_id . "') AS keyword FROM category WHERE category_id = '" . (int)$category_id . "'";
        $query = $this->db->query($sql);
        return $query->row;
    } 

    public function getCategoryByCode($code) {
        
        $sql = "SELECT * from category c ";
        $sql.= "  JOIN category_description cd on c.category_id = cd.category_id ";
        $sql.= " WHERE c.code = '$code'";

        //$sql = "SELECT DISTINCT *, (SELECT keyword FROM url_alias WHERE query = 'category_id=" . (int)$category_id . "') AS keyword FROM category WHERE category_id = '" . (int)$category_id . "'";
        $query = $this->db->query($sql);
        return $query->row;
    } 

    public function getCategoryById($id) {
        $sql = "SELECT * from category c ";
        $sql.= "  JOIN category_description cd on c.category_id = cd.category_id ";
        $sql.= " WHERE c.category_id = '$id'";

        //$sql = "SELECT DISTINCT *, (SELECT keyword FROM url_alias WHERE query = 'category_id=" . (int)$category_id . "') AS keyword FROM category WHERE category_id = '" . (int)$category_id . "'";
        $query = $this->db->query($sql);
        return $query->row;
    } 

    public function getCategoriesNameTree( $id = 0 ) {
        
        $sql = "select cd.name, c.code from category c ";
        $sql.= "  JOIN category_description cd on c.category_id = cd.category_id";
        $sql.= " WHERE parent_id = '$id' ";
        $categories = $this->db->query($sql);

        $aCategory = array();
        if ($categories->rows) {

            foreach($categories->rows as $row) {
                $code = $row['code'];
                $name = $row['name'];
                $row = $this->treeNameChildren($row['code']);
                $aCategory[$name] =  $row;
                
            }

        }

        //echo '<pre>'; print_r($aCategory); echo '</pre>';
        return $aCategory;
    }

    private function treeNameChildren($parent_code, $arr = array()) {
        $sql = "select cd.name, c.code from category c ";
        $sql.= "  JOIN category_description cd on c.category_id = cd.category_id";
        $sql.= " WHERE parent_id = '$parent_code' ";
        //echo '<pre>'; print_r($sql); echo '</pre>';
        $categories = $this->db->query($sql);
        if ($categories->rows) {
        
            foreach($categories->rows as $row) {
            
                $code = $row['code'];
                $name = $row['name'];
                
                $row = $this->treeNameChildren($code);
                $arr[$name] = $row;

                if ( empty($row) ) {
                    $arr[$name] = '';
                    } else {
                    $arr[$name] = $row;
                    }
            }
        }

        return $arr ;
    }

    public function getCategoriesTree( $id = 0 ) {
            $sql = "select * from category c ";
        $sql.= "  JOIN category_description cd on c.category_id = cd.category_id";
        $sql.= " WHERE parent_id = '$id' ";
            $categories = $this->db->query($sql);
        $aCategory = array();
        if ($categories->rows) {

            foreach($categories->rows as $row) {
                $code = $row['code'];
                $row['children'] = $this->treeChildren($row['code']);
                $aCategory[] =  $row;
                
            }

        }
        return $aCategory;
    }
        // my recursive function
    private function treeChildren($parent_code, $arr = array()) {
        $sql = "select * from category c ";
        $sql.= "  JOIN category_description cd on c.category_id = cd.category_id";
        $sql.= " WHERE parent_id = '$parent_code' ";
            $categories = $this->db->query($sql);
        if ($categories->rows) {
        
            foreach($categories->rows as $row) {
            
                $code = $row['code'];
                
                $row['children'] = $this->treeChildren($code);
                $arr[] = $row;
                
            }
        }

        return $arr ;
    }
    
    // only return categories with this parent
    public function getSubCategories($parent_id) {

        //echo '<pre>'; print_r($parent_id); echo '</pre>'; exit;

        $category_data = array();
        $sql = "SELECT * FROM category c ";
        $sql.= "  LEFT JOIN category_description cd ON (c.category_id = cd.category_id) ";
        $sql.= " WHERE c.parent_id = '" . $parent_id . "'";
        $sql.= "   AND cd.language_id = '" . (int)$this->config->get('config_language_id') ;
        $sql.= "' ORDER BY c.sort_order, cd.name ASC";
        //$sql.= "' ORDER BY c.category_id ASC";
        $query = $this->db->query($sql);
        
        foreach ($query->rows as $result) {
            $category_data[] = array(
                'category_id' => $result['category_id'],
                'code'        => $result['code'],
                'name'        => $this->getPath($result['category_id'], $this->config->get('config_language_id')),
                'status'      => $result['status'],
                'sort_order'  => $result['sort_order']
            );
            
        }
        
    
        return $category_data;
    }

    // parent_id is changed from category_id to code, besso
    /* change as to return json */
    public function getCategories($parent_id) {

        $key_parent_id = str_replace('/','_',$parent_id);
        $category_data = $this->cache->get('category.' . $this->config->get('config_language_id') . '.' . $key_parent_id);

        // TODO. remove later
        $category_data = NULL;
        if (!$category_data) {
            $category_data = array();
            $sql = "SELECT * FROM category c ";
                $sql.= "  LEFT JOIN category_description cd ON (c.category_id = cd.category_id) ";
                $sql.= " WHERE c.parent_id = '" . $parent_id . "'";
                $sql.= "   AND cd.language_id = '" . (int)$this->config->get('config_language_id') ;
                $sql.= "' ORDER BY c.sort_order, cd.name ASC";
                //$sql.= "' ORDER BY c.category_id ASC";
            $query = $this->db->query($sql);

            // Add 1st one
            if ( empty($query->rows) ) {
                return array();
            }

            foreach ($query->rows as $result) {
                //if ( empty($result['code']) )   continue;
                $category_data[] = array(
                    'category_id' => $result['category_id'],
                    'code'        => $result['code'],
                    'name'        => $this->getPath($result['category_id'], $this->config->get('config_language_id')),
                    'status'      => $result['status'],
                    'sort_order'  => $result['sort_order']
                );
                $category_data = array_merge($category_data, $this->getCategories($result['category_id']));
            }
            
            $this->cache->set('category.' . $this->config->get('config_language_id') . '.' . $key_parent_id, $category_data);
        }

        return $category_data;

    }

    public function getCatalog() {
        $aCatalog = array();
        $sql = "select c.category_id, cd.name
                    from category c 
                  join category_description cd on c.category_id = cd.category_id
                 where parent_id = 0";
        $query = $this->db->query($sql);
        foreach ($query->rows as $result) {
            $cid = $result['category_id'];
            $sql = "select c.category_id, cd.name
                        from category c 
                      join category_description cd on c.category_id = cd.category_id
                     where parent_id = '$cid'";
            $query1 = $this->db->query($sql);
            $i = 0;
            $aSubCatalog = array();
            foreach ($query1->rows as $result1) {
                 $aSubCatalog[$i] = $result1['name'];
                $i++;
            }
            $aCatalog[$result['name']] = $aSubCatalog;
        }
        return $aCatalog;
    }

    /*
     *
     */
    public function getPath( $cid ) {

        if ( empty($cid) )    return '';

        $sql = "SELECT name, code, parent_id FROM category c ";
        $sql.= "  LEFT JOIN category_description cd ON (c.category_id = cd.category_id) ";
        $sql.= "WHERE c.category_id = '" . $cid . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY c.sort_order, cd.name ASC";

        $query = $this->db->query($sql);

        $category_info = $query->row;
        if ( $category_info['parent_id'] ) {
            return $this->getPath($category_info['parent_id'], $this->config->get('config_language_id')) . $this->language->get('text_separator') . $category_info['name'];
        } else {
            return $category_info['name'];
        }

    }

    public function getCategoryDescriptions($category_id) {

        $category_description_data = array();
        $sql = "SELECT * FROM category_description WHERE category_id = '" . (int)$category_id . "'";
        $query = $this->db->query($sql);
        foreach ($query->rows as $result) {
        $category_description_data[$result['language_id']] = array(
                'name'             => $result['name'],
                'meta_keywords'    => $result['meta_keywords'],
                'meta_description' => $result['meta_description'],
                'description'      => $result['description']
            );
        }
        return $category_description_data;
    }

    public function getCategoryStores($category_id) {
        $category_store_data = array();
        $query = $this->db->query("SELECT * FROM category_to_store WHERE category_id = '" . (int)$category_id . "'");
        foreach ($query->rows as $result) {
        $category_store_data[] = $result['store_id'];
        }
        return $category_store_data;
    }
    
    public function getTotalCategories() {
        $sql = "SELECT COUNT(*) AS total FROM category";
            $query = $this->db->query($sql);
        return $query->row['total'];
    }

    public function getTotalCategoriesByImageId($image_id) {
      $query = $this->db->query("SELECT COUNT(*) AS total FROM category WHERE image_id = '" . (int)$image_id . "'");
      return $query->row['total'];
    }

    /*
    It is designed to prepare best set for frontend 'select' box
    FE 'select' box has 3 depths of categories and each category should have size two length charactor or number.
      */
    public function prepareSelectBoxSet($code) {
            $aCategory = array();
        if ( !empty($code) ) {
            $cat1 = substr($code,0,2);
            $cat2 = substr($code,0,4);
            $cat3 = substr($code,0,6);
        }
        if ( !empty($cat1) ) {
            $aCategory[$cat1] = $this->getSiblings($cat1);
        }
        if ( !empty($cat2) ) {
            $aCategory[$cat2] = $this->getSiblings($cat2);
        }
        if ( !empty($cat3) ) {
            $aCategory[$cat3] = $this->getSiblings($cat3);
        }

        return $aCategory;
    }

    // only return categories with this parent
    public function getSiblings($code) {
            $category_data = array();
        $sql = "SELECT * FROM category c ";
        $sql.= "  LEFT JOIN category_description cd ON (c.category_id = cd.category_id) ";
        $sql.= " WHERE c.parent_id = ( select parent_id from category where code = '" . $code . "') ";
        $sql.= "   AND cd.language_id = '" . (int)$this->config->get('config_language_id') ;
        $sql.= "' ORDER BY c.sort_order, cd.name ASC";
        //$sql.= "' ORDER BY c.category_id ASC";
        $query = $this->db->query($sql);
        
        foreach ($query->rows as $result) {
            $category_data[] = array(
                'category_id' => $result['category_id'],
                'code'        => $result['code'],
                'name'        => $result['name'],
                'status'      => $result['status'],
                'sort_order'  => $result['sort_order']
            );
            
        }
        
        return $category_data;
    }
}
?>
