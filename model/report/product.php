<?php
class ModelReportProduct extends Model {
  ##############################################################################
  ###### Product
  ##############################################################################
    public function stat_product($request) {
    // today
        if (!is_null($request['filter_from']) && !is_null($request['filter_to'])) {
        $thismonth = mktime(0, 0, 0, date(substr($request['filter_from'],5,2)), date(substr($request['filter_from'],8,2)), date(substr($request['filter_from'],0,4)));
        $from = date("Y-m-01",$thismonth);
        $to   = date("Y-m-t",$thismonth);
        $month = date("Ym",$thismonth);
        }
        $working = $this->util->getWorkingDays($from,$to);
        //$this->log->aPrint( $working );
        $group = $rep = '';
        if ($group == 'rep') $rep = "substr(substring_index(s.txid,'-',-2),1,2),";
        $sql = "
        select $rep s.model as model, p.name, sum(s.order_quantity) as qty, sum(s.order_quantity * s.price1 ) as total
          from sales as s
          join ( product as p , product_description as pd ) on s.model = p.model and p.product_id = pd.product_id
         where s.order_quantity > 0
        ";
        if (!is_null($request['filter_from']) && !is_null($request['filter_to'])) {
        if ( $request['filter_from'] == $request['filter_to'] ) {
        $from = $to = $request['filter_from'];
        } else {
        $from = $to = date("Y-m-d");
    }
        }
        $sql .= " AND substr(s.order_date,1,10) between '" . $from . "' and '" . $to . "'";    
        $sql .= " group by $rep s.model order by $rep substr(s.model,1,2),sum(s.order_quantity * s.price1 ) desc";
        //$this->log->aPrint( $sql );
        $query = $this->db->query($sql);
        $aProduct = array();
        foreach($query->rows as $row) {
        $aProduct[ $row['model'] ] = array($row['name'],$row['qty'],$row['total']);
    }
        //$this->log->aPrint( $aProduct);
        $aCat = $this->config->getCatalog();
        //$this->log->aPrint( $aCat );
    // sort later
        $aCatSum = array();
        foreach($aCat as $k => $group) {
        foreach($group as $m) {
        if ( $m != '' ) {
            if ( isset($aProduct[$m]) && is_array($aProduct[$m]) ) {
            $aCatSum[$k][$m] = $aProduct[$m]; 
            }
        }
    }
    }
        $rtn['today'] = $aCatSum;

    // this month
        if (!is_null($request['filter_from']) && !is_null($request['filter_to'])) {
        $from = $request['filter_from'];
        $to   = $request['filter_to'];
        $month = substr($request['filter_from'],5,2);
        }
        //$this->log->aPrint( $working );
        $group = $rep = '';
        if ($group == 'rep') $rep = "substr(substring_index(txid,'-',-2),1,2),";
        $sql = "
        select $rep s.model as model, p.name, sum(s.order_quantity) as qty, sum(s.order_quantity * s.price1 ) as total
          from sales as s
          join ( product as p , product_description as pd ) on s.model = p.model and p.product_id = pd.product_id
         where s.order_quantity > 0
    ";
        $sql .= " AND substr(s.order_date,1,10) between '" . $from . "' and '" . $to . "'";    
        $sql .= " group by $rep s.model order by $rep substr(s.model,1,2),sum(s.order_quantity * s.price1 ) desc";
        //$this->log->aPrint( $sql );
        $query = $this->db->query($sql);
        $aProduct = array();
        foreach($query->rows as $row) {
        $aProduct[ $row['model'] ] = array($row['name'],$row['qty'],$row['total']);
    }
        //$this->log->aPrint( $aProduct);
        $aCat = $this->config->getCatalog();
        //$this->log->aPrint( $aCat );
        $aCatSum = array();
        foreach($aCat as $k => $group) {
        foreach($group as $m) {
        if ( $m != '' ) {
            if ( isset($aProduct[$m]) && is_array($aProduct[$m]) ) {
            $aCatSum[$k][$m] = $aProduct[$m]; 
            }
        }
    }
    }
    //exit;
        //$this->log->aPrint( $aCatSum ); exit;
        $rtn['this_month'] = $aCatSum;
        return $rtn;
    }
  

    public function ordersales($request) {
        $from  = $request['filter_from'];
        $to    = $request['filter_to'];
        $group = html_entity_decode($request['group']);
        $aCat = $this->config->getCatalog();
        $comma = implode("','",$aCat[$group]);
        $comma = "'" . $comma . "'";
        $sql = "
        select x.order_user as rep, sum( s.order_quantity ) as qty
        FROM "  . $this->table->transaction . " x, " . $this->table->sales . " s
       where x.txid = s.txid
         and substr(s.order_date,1,10) between '$from' and '$to'
         and s.model in ( $comma )
       group by x.order_user
       order by sum( s.order_quantity ) desc
    ";
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getSearch($req) {
        $id = $req['id'];
        $from = $req['from'];
        $to = $req['to'];
/*
               and date_format(x.order_date,'%m-%d-%Y') between '$from' and '$to'
               and x.status in ('1','2','3')
               and x.approve_status = 'approve'
               and date_format(x.order_date,'%m-%d-%Y') between '05-01-2012' and '05-18-2012'
                 */
    // mysql date_format between bug
        $from = substr($from,6,4) . '-'. substr($from,0,5);
        $to = substr($to,6,4) . '-'. substr($to,0,5);
        $sql = "select x.order_user, st.accountno, st.name, 
                   s.model,
                   (x.total - x.cod - x.lift) as tx_price,
                   s.order_quantity,
                   s.price1,
                   s.discount, s.discount2,
                   s.total_price 
                fROM "  . $this->table->transaction . " x,
               " . $this->table->sales . " s, "  . $this->table->stores . " st
             where x.txid = s.txid
               and st.id = x.store_id
               and date_format(x.order_date,'%Y-%m-%d') between '$from' and '$to'
               and x.status in ('1','2','3')
               and x.approve_status = 'approve'
             order by st.salesrep";
        //$this->log->aPrint( $sql );
        $query = $this->db->query($sql);
    //return $query->rows;
        //$this->log->aPrint( $query->rows ); exit;
            $aProduct = array();
        $aCatBase = $this->config->getCatalogBase();
        $aCat = $this->config->getCatalog();
        $i = 0;
        foreach($query->rows as $row) {
        //$aProduct[ $row['accountno'] ][$i] = $row;
        foreach($aCatBase as $base => $baseGroup) { // catalog base
        foreach($baseGroup as $group) {
          foreach($aCat[$group] as $m) {
            if ( $m == $row['model']) {
                if ( !isset($aProduct[ $row['order_user'] ]) ) {
                $aProduct[ $row['order_user'] ] = array();
            }
                if ( !isset($aProduct[ $row['order_user'] ][ $base ]) ) {
                $aProduct[ $row['order_user'] ][ $base ] = 0;
            }
                $aProduct[ $row['order_user'] ][ $base ] += $row['total_price']; 
            }
            }
        }
    }
        $i++;
    }
        //$this->log->aPrint( $aProduct );  exit;
        return($aProduct);
    }

    /*****
  It's specific case for Mr.Park.
    /data/catalog_base present more customized catalog and standard catalog should be integrated with this cat
  and it's based one all each transaction of specific day. so it's quite easy.
  [ query ]
  select x.order_user, st.accountno, st.name, 
         s.model,
         (x.total - x.cod - x.lift) as tx_price,
         s.order_quantity,
         s.price1,
         s.discount, s.discount2,
         s.total_price 
    FROM "  . $this->table->transaction . " x, " . $this->table->sales . " s, "  . $this->table->stores . " st
   where x.txid = s.txid
     and st.id = x.store_id
     and substr(x.order_date,1,10) between '2012-04-17' and '2012-04-17'
     and x.status in ('1','2','3')
     and x.approve_status = 'approve'
     and x.order_user = 'CS'
   order by x.store_id
  *****/
    public function getToday($req) {
        $id = $req['id'];
        $from = $req['from'];
        $to = $req['to'];

/*
               and date_format(x.order_date,'%m-%d-%Y') between '$from' and '$to'
               and x.status in ('1','2','3')
               and x.approve_status = 'approve'
               and date_format(x.order_date,'%m-%d-%Y') between '05-01-2012' and '05-18-2012'
*/
        $sql = "select x.order_user, st.accountno, st.name,
                   s.model, x.txid,
                   (x.total - x.cod - x.lift) as tx_price,
                   s.order_quantity,
                   s.price1,
                   s.discount, s.discount2,
                   s.total_price 
                fROM "  . $this->table->transaction . " x, " . $this->table->sales . " s, "  . $this->table->stores . " st
             where x.txid = s.txid
               and st.id = x.store_id
              and date_format(x.order_date,'%m-%d-%Y') between '$from' and '$to'
               and x.status in ('1','2','3')
               and x.approve_status = 'approve'
             order by substr(st.accountno,1,2)";
        //$this->log->aPrint( $sql );
        $query = $this->db->query($sql);
    //return $query->rows;
        //$this->log->aPrint( $query->rows );
            $aProduct = array();
        $aCatBase = $this->config->getCatalogBase();
        $aCat = $this->config->getCatalog();
        $i = 0;
        foreach($query->rows as $row) {
        //$aProduct[ $row['accountno'] ][$i] = $row;
        foreach($aCatBase as $base => $baseGroup) { // catalog base
        foreach($baseGroup as $group) {
          foreach($aCat[$group] as $m) {
            if ( $m == $row['model']) {
                if ( !isset($aProduct[ $row['txid'] ]) ) {
                $aProduct[ $row['txid'] ] = array();
            }
                if ( !isset($aProduct[ $row['txid'] ][ $base ]) ) {
                $aProduct[ $row['txid'] ][ $base ] = 0;
            }
                $aProduct[ $row['txid'] ][ $base ] += $row['total_price']; 
            }
            }
        }
    }
        $i++;
    }
        //$this->log->aPrint( $aProduct );  exit;
        return($aProduct);
    }
}
?>
