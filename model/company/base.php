<?php
class ModelCompanyBase extends Model {
    
    private $baseTables = array(
        'stores',
        'products',
        'transaction',
        'sales',
        'user',
        'user_group',
        'category',
        'category_description',
        'category_filter'
    );

    public function getList($req = array()) {
        $res = array();
        $sql = "select * from company where 1 = 1";
        
        $company_id = isset($req['companyid']) ? $req['companyid'] : '';
        if ( !empty($company_id) ) {
            $sql .= " and companyid = '$company_id'";
        }

        $sql.= " order by companyid";

        $query = $this->db->query($sql);
        $res = $query->rows;
        return $res;
        /*
        if ($request) {
            $sql = "select * from company";
            $query = $this->db->query($sql);
            $res = $query->rows;
            if (isset($request['filter_name']) && !is_null($request['filter_name'])) {
                $sql .= " AND LCASE(s.name) LIKE '%" . $this->db->escape(strtolower($request['filter_name'])) . "%'";
            }
            if (isset($request['filter_accountno']) && !is_null($request['filter_accountno'])) {
                $sql .= " AND LCASE(s.accountno) like '%" . $this->db->escape(strtolower($request['filter_accountno'])) . "%'";
            }
            if (isset($request['filter_companytype']) && !is_null($request['filter_companytype'])) {
                $sql .= " AND LCASE(s.companytype) = '" . $this->db->escape(strtolower($request['filter_companytype'])) . "'";
            }
            if (isset($request['filter_address1']) && !is_null($request['filter_address1'])) {
                $sql .= " AND LCASE(s.address1) LIKE '%" . $this->db->escape(strtolower($request['filter_address1'])) . "%'";
            }
            if (isset($request['filter_city']) && !is_null($request['filter_city'])) {
                $sql .= " AND LCASE(s.city) LIKE '%" . $this->db->escape(strtolower($request['filter_city'])) . "%'";
            }
            if (isset($request['filter_state']) && !is_null($request['filter_state'])) {
                $sql .= " AND LCASE(s.state) like '%" . $this->db->escape(strtolower($request['filter_state'])) . "%'";
            }
            if (isset($request['filter_zipcode']) && !is_null($request['filter_zipcode'])) {
                $sql .= " AND LCASE(s.zipcode) = '" . $this->db->escape(strtolower($request['filter_zipcode'])) . "'";
            }
            if (isset($request['filter_phone1']) && !is_null($request['filter_phone1'])) {
                $sql .= " AND LCASE(s.phone1) LIKE '%" . $this->db->escape(strtolower($request['filter_phone1'])) . "%'";
            }
            if (isset($request['filter_chrt']) && '' != $request['filter_chrt']) {
                $sql .= " AND LCASE(s.chrt) = '" . $this->db->escape(strtolower($request['filter_chrt'])) . "'";
            }
            if (isset($request['filter_status']) && '' != $request['filter_status']) {
                $sql .= " AND LCASE(s.status) = '" . $this->db->escape(strtolower($request['filter_status'])) . "'";
            }
            if (isset($request['filter_salesrep']) && !is_null($request['filter_salesrep'])) {
                $sql .= " AND s.salesrep = ( select username from user where user_group_id in (1,10,11) and LCASE(username) = '" . $this->db->escape(strtolower($request['filter_salesrep'])) . "')";
            }
            //$sql .= " and substr(s.zipcode,1,1) = '0'";
            if (isset($request['filter_balance']) && !is_null($request['filter_balance'])) {
                $filter_balance = $request['filter_balance'];
                $sql .= " and t1.balance_total > $filter_balance ";
            }
            $sort_data = array('name','status','companytype','salesrep');
            if (isset($request['sort']) && in_array($request['sort'], $sort_data)) {
                $sql .= " ORDER BY " . $request['sort'];
            } else {
                $sql .= " ORDER BY s.accountno";
            }
            if (isset($request['order']) && ($request['order'] == 'DESC')) {
                $sql .= " DESC";
            } else {
                $sql .= " ASC";
            }
            if ( isset($request['filter_page']) &&  !is_null($request['filter_page'])  ) {
                $request['start'] = 20 * ( $request['filter_page'] - 1 ); 
            }

            // todo. export_qry need not limit ext , besso-201103 
            $export_qry = $sql;

            if (isset($request['start']) || isset($request['limit'])) {
                if ($request['start'] < 0) {
                $request['start'] = 0;
            }
                if ($request['limit'] < 1) {
                $request['limit'] = 20;
            }
                $sql .= " LIMIT " . (int)$request['start'] . "," . (int)$request['limit'];
            }
            // add index for performances : alter table transaction add index idx_company_id (company_id);
            // $this->log->aPrint( $sql );
            $query = $this->db->query($sql);
            //$res = $query->rows;
            $aRtn = array();
            $i = 0;
            foreach($query->rows as $row) {
                $company_id = $row['id'];
                $sql = "select order_date,total from " . $this->table->transaction . " where company_id = $company_id";
                $query_tx = $this->db->query($sql);
                $row['tx'] = $query_tx->rows;
                $aRtn[$i] = $row;
                $i++;
                //$this->log->aPrint( $row );
            }
            //$this->log->aPrint( $aRtn );
            return $aRtn;
            } else {
            $response = $this->cache->get('companylocator.' . $this->config->get('config_language_id'));
            if (!$response) {
                $query = $this->db->query("SELECT * FROM companylocator");
                $response = $query->rows;
                $this->cache->set('companylocator.' . $this->config->get('config_language_id'), $response);
            }
            $res = $response;
        }
        return $res;
          */
    }
    
    public function getTotal($request=array()) {
        $res = array();
        $sql = "select count(id) from company";
        $count = $this->db->count($sql);
        return $count;
        /*
        if ($request) {
        $sql = "SELECT count(*) as total FROM companylocator s left join ";
        $sql.= " ( select company_id , sum(balance) as balance_total from " . $this->table->transaction . " where balance > 0 ";
        if (isset($request['filter_order_date_from']) && !is_null($request['filter_order_date_from'])) {
            $sql .= " AND LCASE(order_date) between '" . $this->db->escape(strtolower($request['filter_order_date_from'])) . "' and '" . $this->db->escape(strtolower($request['filter_order_date_to'])) . "'";
        }
        $sql.= " group by company_id ) t1 ";
        $sql.= " on s.id = t1.company_id where 1 = 1 ";
        if (isset($request['filter_name']) && !is_null($request['filter_name'])) {
            $sql .= " AND LCASE(s.name) LIKE '%" . $this->db->escape(strtolower($request['filter_name'])) . "%'";
        }
        if (isset($request['filter_accountno']) && !is_null($request['filter_accountno'])) {
            $sql .= " AND LCASE(s.accountno) like '%" . $this->db->escape(strtolower($request['filter_accountno'])) . "%'";
        }
        if (isset($request['filter_companytype']) && !is_null($request['filter_companytype'])) {
            $sql .= " AND LCASE(s.companytype) = '" . $this->db->escape(strtolower($request['filter_companytype'])) . "'";
        }
        if (isset($request['filter_address1']) && !is_null($request['filter_address1'])) {
            $sql .= " AND LCASE(s.address1) LIKE '%" . $this->db->escape(strtolower($request['filter_address1'])) . "%'";
        }
        if (isset($request['filter_city']) && !is_null($request['filter_city'])) {
            $sql .= " AND LCASE(s.city) LIKE '%" . $this->db->escape(strtolower($request['filter_city'])) . "%'";
        }
        if (isset($request['filter_state']) && !is_null($request['filter_state'])) {
            $sql .= " AND LCASE(s.state) like '%" . $this->db->escape(strtolower($request['filter_state'])) . "%'";
        }
        if (isset($request['filter_zipcode']) && !is_null($request['filter_zipcode'])) {
            $sql .= " AND LCASE(s.zipcode) = '" . $this->db->escape(strtolower($request['filter_zipcode'])) . "'";
        }
        if (isset($request['filter_phone1']) && !is_null($request['filter_phone1'])) {
            $sql .= " AND LCASE(s.phone1) LIKE '%" . $this->db->escape(strtolower($request['filter_phone1'])) . "%'";
        }
        if (isset($request['filter_chrt']) && '' != $request['filter_chrt']) {
            $sql .= " AND LCASE(s.chrt) = '" . $this->db->escape(strtolower($request['filter_chrt'])) . "'";
        }
        if (isset($request['filter_status']) && '' != $request['filter_status']) {
            $sql .= " AND LCASE(s.status) = '" . $this->db->escape(strtolower($request['filter_status'])) . "'";
        }
        if (isset($request['filter_salesrep']) && !is_null($request['filter_salesrep'])) {
            $sql .= " AND s.salesrep = ( select username from user where user_group_id in (1,10,11) and LCASE(username) = '" . $this->db->escape(strtolower($request['filter_salesrep'])) . "')";
        }
        if (isset($request['filter_balance']) && !is_null($request['filter_balance'])) {
            $filter_balance = $request['filter_balance'];
            $sql .= " and t1.balance_total > $filter_balance ";
        }
        //$this->log->aPrint( $sql );
        $query = $this->db->query($sql);
        } else {
        $query = $this->db->query("SELECT * as total FROM companylocator");
        }
        $res = $query->rows[0]['total'];
        return $res;
          */
    }
    
      /* update company , besso-201103 */
    public function update($data) {
        //echo '<pre>'; print_r($data); echo '</pre>'; exit;
        $sql = "UPDATE companylocator ";
        $sql.= " SET accountno = '" . $this->db->escape($data['accountno']) . "',";
        $sql.= "     name = '" . $this->db->escape($data['name']) . "',";
        $sql.= "     companytype = '" . $this->db->escape($data['companytype']) . "',";
        $sql.= "     address1 = '" . $this->db->escape($data['address1']) . "',";
        $sql.= "     city = '" . $this->db->escape($data['city']) . "',";
        $sql.= "     state = '" . $this->db->escape($data['state']) . "',";
        $sql.= "     zipcode = '" . $this->db->escape($data['zipcode']) . "',";
        $sql.= "     phone1 = '" . $this->db->escape($data['phone1']) . "',";
        $sql.= "     phone2 = '" . $this->db->escape($data['phone2']) . "',";
        $sql.= "     fax = '" . $this->db->escape($data['fax']) . "',";
        $sql.= "     salesrep = '" . $this->db->escape($data['salesrep']) . "',";
        $sql.= "     status = '" . $this->db->escape($data['status']) . "',";
        //$sql.= "     chrt = '" . $this->db->escape($data['chrt']) . "',";
        //$sql.= "     parent = '" . $this->db->escape($data['parent']) . "',";
        //$sql.= "     lat = '" . $this->db->escape($data['lat']) . "',";
        //$sql.= "     lng = '" . $this->db->escape($data['lng']) . "',";
        $sql.= "     email = '" . $this->db->escape($data['email']) . "',";
        //$sql.= "     billto = '" . $this->db->escape($data['billto']) . "',";
        //$sql.= "     shipto = '" . $this->db->escape($data['shipto']) . "',";
        $sql.= "     comment  = '" . $this->db->escape($data['comment']) . "',";
        $sql.= "     owner  = '" . $this->db->escape($data['owner']) . "',";
        $sql.= "     discount = '" . $this->db->escape($data['discount']) . "'";
        $sql.= " where id = '" . $this->db->escape($data['id']) . "'";
        //echo '<pre>'; print_r($sql); echo '</pre>'; exit;
        if ($this->db->query($sql)) {
            return true;
        }
        return false; 
    }
    
    public function insert($data) {
        $status = false;
        $sql = "insert into company (companyid, name, email, phone, status, date_added) values ";
        $sql.= " ( '" . $this->db->escape($data['companyid']) . "',";
        $sql.= "   '" . $this->db->escape($data['name']) . "',";
        $sql.= "   '" . $this->db->escape($data['email']) . "',";
        $sql.= "   '" . $this->db->escape($data['phone']) . "',";
        $sql.= "   '" . $this->db->escape($data['status']) . "',";
        $sql.= "   now() )";
        if ($this->db->query($sql)) {
            $this->createDBInstance($data);
        }
        return $status;
    }

    private function createDBInstance($data) {
            $status = false;
            $companyid = $data['companyid'];
        $email = $data['email'];
        $phone = $data['phone'];

        // check if DB exist
        
        // create tables
        $store_table = $companyid . '_stores';
        $sql = "
        CREATE TABLE  $store_table (
            id int(11) NOT NULL AUTO_INCREMENT,
            accountno varchar(10) NOT NULL,
            name varchar(100) DEFAULT NULL,
            storetype char(1) DEFAULT NULL,
            address1 varchar(100) DEFAULT NULL,
            address2 varchar(100) DEFAULT NULL,
            city varchar(30) DEFAULT NULL,
            state varchar(10) DEFAULT NULL,
            zipcode varchar(10) DEFAULT NULL,
            phone1 varchar(50) DEFAULT NULL,
            phone2 char(12) DEFAULT NULL,
            salesrep varchar(10) DEFAULT NULL,
            status char(1) DEFAULT 1,
            fax char(12) DEFAULT NULL,
            lat varchar(12) DEFAULT NULL,
            lng varchar(12) DEFAULT NULL,
            chrt char(1) DEFAULT NULL,
            parent varchar(10) DEFAULT NULL,
            comment text,
            email varchar(100) DEFAULT NULL,
            billto text,
            shipto text,
            discount varchar(200) DEFAULT '',
            owner varchar(50) DEFAULT NULL,
            PRIMARY KEY (id),
            UNIQUE KEY accountno (accountno)
        )
        ";
        $this->db->query($sql);
            $product_table = $companyid . '_products';
        $sql = "
        CREATE TABLE $product_table (
            product_id int(11) NOT NULL AUTO_INCREMENT,
            model varchar(64) NOT NULL,
            sku varchar(64) NOT NULL,
            category varchar(64) NOT NULL,
            name varchar(255) DEFAULT NULL,
            location varchar(128) NOT NULL,
            quantity int(4) NOT NULL DEFAULT '0',
            cost decimal(15,4) NOT NULL DEFAULT '0.0000',
            ws_price decimal(14,2) DEFAULT '0.00',
            rt_price decimal(14,2) DEFAULT '0.00',
            status int(1) NOT NULL DEFAULT '1',
            image varchar(255) DEFAULT NULL,
            price decimal(14,2) DEFAULT '0.00',
            date_added datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
            date_modified datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
            ups_weight decimal(5,2) DEFAULT NULL,
            thres int(11) DEFAULT NULL,
            pc varchar(6) DEFAULT NULL,
            dc int(11) DEFAULT '0',
            dc2 int(11) DEFAULT NULL,
            dc3 int(11) DEFAULT NULL,
            barcode varchar(255) DEFAULT NULL,
            `desc` text DEFAULT NULL,
            PRIMARY KEY (product_id),
            UNIQUE KEY idx_model (model),
            KEY model (model)
        )";

        $this->db->query($sql);
            $transaction_table = $companyid . '_transaction';
        $sql = "
        CREATE TABLE $transaction_table (
            txid varchar(30) NOT NULL,
            store_id int(11) NOT NULL,
            description text,
            order_user varchar(20) NOT NULL,
            approved_user varchar(12) DEFAULT NULL,
            approved_date date DEFAULT NULL,
            sold_ym char(6) DEFAULT NULL COMMENT 'for report',
            new_store char(1) DEFAULT NULL,
            term int(2) DEFAULT NULL,
            other_cost decimal(12,4) DEFAULT NULL,
            store_grade char(1) DEFAULT NULL,
            total decimal(12,2) DEFAULT 0,
            payed_price decimal(12,2) DEFAULT 0,
            payed_sum decimal(12,2) DEFAULT 0,
            balance decimal(12,2) DEFAULT 0,
            shipped_yn char(1) DEFAULT NULL,
            weight_sum decimal(12,2) DEFAULT 0,
            order_date datetime DEFAULT NULL,
            approve_status varchar(10) DEFAULT NULL,
            invoice_no int(11) DEFAULT NULL,
            bankaccount varchar(30) DEFAULT NULL,
            executor varchar(20) DEFAULT NULL,
            readonly char(1) DEFAULT NULL,
            lift decimal(5,2) DEFAULT NULL,
            cod decimal(5,2) DEFAULT NULL,
            ship_method varchar(20) DEFAULT NULL,
            ship_appointment char(14) DEFAULT NULL,
            payment char(2) DEFAULT NULL,
            backorder int(11) DEFAULT NULL,
            shipped_by varchar(20) DEFAULT '',
            shipped_date char(14) DEFAULT '',
            billto text,
            shipto text,
            status char(1) DEFAULT NULL,
            invoice_description text,
            discount varchar(255) DEFAULT NULL,
            print_ship tinyint(4) DEFAULT '0',
            pc_date datetime DEFAULT NULL,
            post_check decimal(12,2) DEFAULT '0.00',
            cur_check decimal(12,2) DEFAULT '0.00',
            cur_cash decimal(12,2) DEFAULT '0.00',
            sign_yn char(1) DEFAULT 'N',
            subtotal decimal(12,2) DEFAULT 0,
            PRIMARY KEY (txid),
            KEY idx_store_id (store_id)
        )
        ";
        $this->db->query($sql);
        
        $sales_table = $companyid . '_sales';
        $sql = " 
        CREATE TABLE $sales_table (
            id int(11) NOT NULL AUTO_INCREMENT,
            txid varchar(30) NOT NULL,
            product_id int(11) NOT NULL,
            model varchar(64) NOT NULL,
            order_quantity decimal(5,2) DEFAULT NULL,
            price1 decimal(12,2) DEFAULT NULL,
            price2 decimal(12,2) DEFAULT NULL,
            free int(11) DEFAULT NULL,
            damage int(11) DEFAULT NULL,
            discount decimal(5,2) DEFAULT NULL,
            total_price decimal(12,2) DEFAULT NULL,
            master_case_cnt int(3) DEFAULT NULL,
            weight_row decimal(5,2) DEFAULT NULL,
            order_date char(14) NOT NULL,
            discount2 decimal(5,2) DEFAULT NULL,
            cancel char(1) DEFAULT NULL,
            comment varchar(255) DEFAULT NULL,
            backorder int(11) DEFAULT '0',
            backfree int(11) DEFAULT '0',
            backdamage int(11) DEFAULT '0',
            shipped decimal(5,2) DEFAULT NULL,
            shipped_date char(14) DEFAULT NULL,
            promotion int(11) DEFAULT '0',
            backpromotion int(11) DEFAULT '0',
            description varchar(255) DEFAULT NULL,
            inventory int(11) DEFAULT NULL,
            PRIMARY KEY (id),
            KEY idx_txid (txid),
            KEY idx_model (model),
            KEY txid (txid)
        )
        ";
        $this->db->query($sql);

        // user
        $user_table = $companyid . '_user';
        $sql = " 
        CREATE TABLE $user_table (
          `user_id` int(11) NOT NULL AUTO_INCREMENT,
          `user_group_id` int(11) NOT NULL,
          `username` varchar(20) COLLATE utf8_bin NOT NULL DEFAULT '',
          `password` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
          `firstname` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
          `lastname` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
          `email` varchar(96) COLLATE utf8_bin NOT NULL DEFAULT '',
          `status` int(1) NOT NULL,
          `ip` varchar(15) COLLATE utf8_bin NOT NULL DEFAULT '',
          `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
          `approver` varchar(30) COLLATE utf8_bin DEFAULT NULL,
          `telephone` varchar(20) COLLATE utf8_bin DEFAULT NULL,
          `companyid` varchar(30) COLLATE utf8_bin DEFAULT NULL,
          PRIMARY KEY (`user_id`)
        )
        ";
        $this->db->query($sql);

        // category
        $category_table = $companyid . '_category';
        $sql = "
        CREATE TABLE $category_table (
          `category_id` int(11) NOT NULL AUTO_INCREMENT,
          `code` varchar(255),
          `image` varchar(255) COLLATE utf8_bin DEFAULT NULL,
          `parent_id` char(6) NOT NULL DEFAULT '',
          `sort_order` int(3) NOT NULL DEFAULT '0',
          `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
          `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
          `status` int(1) NOT NULL DEFAULT '1',
          `top` tinyint(1) DEFAULT NULL,
          `column` int(3) DEFAULT NULL,
          PRIMARY KEY (`category_id`)
        )";
        $this->db->query($sql);
    
        $category_description_table = $companyid . '_category_description';
        $sql = "
        CREATE TABLE $category_description_table (
          `category_id` int(11) NOT NULL,
          `language_id` int(11) NOT NULL,
          `name` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
          `meta_keywords` varchar(255) COLLATE utf8_bin NOT NULL,
          `meta_description` varchar(255) COLLATE utf8_bin NOT NULL,
          `description` text COLLATE utf8_bin NOT NULL,
          `meta_keyword` varchar(255) COLLATE utf8_bin DEFAULT NULL,
          PRIMARY KEY (`category_id`,`language_id`),
          KEY `name` (`name`)
        )";
        $this->db->query($sql);

        $category_filter_table = $companyid . '_category_filter';
        $sql = "
        CREATE TABLE $category_filter_table (
          `category_id` int(11) NOT NULL,
          `filter_id` int(11) NOT NULL
        )";
        $this->db->query($sql);

        // user_group
        $user_group_table = $companyid . '_user_group';
        $sql = " 
        CREATE TABLE $user_group_table (
          `user_group_id` int(11) NOT NULL AUTO_INCREMENT,
          `name` varchar(64) COLLATE utf8_bin NOT NULL,
          `permission` text COLLATE utf8_bin NOT NULL,
          PRIMARY KEY (`user_group_id`)
        )
        ";
        $this->db->query($sql);

        // add default user group
        $sql = "Insert into " . $user_group_table . " ( select * from user_group where user_group_id = '1' )";
        $this->db->query($sql);

        // add default user
        $sql = "Insert into " . $user_table . " SET user_id = '1', user_group_id = '1', username = 'admin', companyid = '$companyid', ";
        $sql.= "    password = md5('" . $companyid . "'), email = '$email', status='1', date_added = now(), telephone = '$phone'";
        $this->db->query($sql);

        $statement_table = $companyid . '_statement';
        $sql = "
        Create table $statement_table (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `store_id` int(11) NOT NULL,
            `paid_type` char(1) NOT NULL,
            `paid_amount` decimal(12,2) DEFAULT 0,
            `paid_code` varchar(255),
            `memo` text not null default '',
            `paid_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
            `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
            PRIMARY KEY (`id`)
        )";
        $this->db->query($sql);

        $statement_detail_table = $companyid . '_statement_detail';
        $sql = "
        Create table $statement_detail_table (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `statement_id` int(11) NOT NULL,
            `txid` varchar(30) NOT NULL,
            `invoice_no` int(11) DEFAULT NULL,
            `paid_amount` decimal(12,2) DEFAULT 0,
            `paid_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
            `balance` decimal(12,2) DEFAULT 0,
            `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
            PRIMARY KEY (`id`)
        )";
        $this->db->query($sql);

        return $status;
    }
    
    public function getUniqAccountno($req) {
            $accountno = $req['accountno'];
        $sql = "select accountno from companylocator where accountno like '$accountno%'";
        $query = $this->db->query($sql);
        $rows = $query->rows;
        //$rows = array('3535A','3535','3535B');
        if (count($rows) > 0 ) {
        $aRow = array();
        foreach($rows as $row) {
            $aRow[] = $row['accountno'];
        }
            $existAccountno = max($aRow);
        if (is_numeric($existAccountno)) {
            $accountno = $existAccountno . 'A';
        } else {
            $existAccountno = (string) $existAccountno;
            $existAccountno++;
            $accountno = $existAccountno;
        }
        }
        return $accountno;
    }
    
    // it drop table
    public function delete($id) {

        // table name is case sensitive
        $sql = "SELECT * FROM company WHERE id = '$id'";
        $query = $this->db->query($sql);
        $companyid = $query->row['companyid'];

        foreach ( $this->baseTables as $base ) {
            // [TODO] check if table exist for drop if exist
            $table = $companyid . '_' . $base;
            $sql = "DROP TABLE IF EXISTS $table";
            $this->db->query($sql);
        }
        $sql = "DELETE from company where id = '$id'";
        $query = $this->db->query($sql);
    }
    
    public function addFeatured($data) {
        $this->db->query("DELETE FROM company_featured");
        if (isset($data['company_featured'])) {
        foreach ($data['company_featured'] as $company_id) {
            $this->db->query("INSERT INTO company_featured SET company_id = '" . (int)$company_id . "'");
        }
    }
    }
    
    public function getFeaturedCompanys() {
        $company_featured_data = array();
        $query = $this->db->query("SELECT company_id FROM company_featured");
        foreach ($query->rows as $result) {
        $company_featured_data[] = $result['company_id'];
    }
      return $company_featured_data;
    }
    
    public function getCompanysByKeyword($keyword) {
        if ($keyword) {
        $query = $this->db->query("SELECT * FROM company p LEFT JOIN company_description pd ON (p.company_id = pd.company_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND (LCASE(p.name) LIKE '%" . $this->db->escape(strtolower($keyword)) . "%' OR LCASE(p.model) LIKE '%" . $this->db->escape(strtolower($keyword)) . "%')");
        return $query->rows;
        } else {
        return array();
    }
    }
    
    public function getCompanysByCategoryId($category_id) {
        $query = $this->db->query("SELECT * FROM company p LEFT JOIN company_description pd ON (p.company_id = pd.company_id) LEFT JOIN company_to_category p2c ON (p.company_id = p2c.company_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p2c.category_id = '" . (int)$category_id . "' ORDER BY p.name ASC");
      return $query->rows;
    }
    
    public function getCompanyDescriptions($company_id) {
        $company_description_data = array();
        $query = $this->db->query("SELECT * FROM company_description WHERE company_id = '" . (int)$company_id . "'");
        foreach ($query->rows as $result) {
        $company_description_data[$result['language_id']] = array(
          'name'             => $result['name'],
          'meta_keywords'    => $result['meta_keywords'],
          'meta_description' => $result['meta_description'],
          'description'      => $result['description']
        );
    }
    
      return $company_description_data;
    }
    
    public function getCompanyOptions($company_id) {
        $company_option_data = array();
        $company_option = $this->db->query("SELECT * FROM company_option WHERE company_id = '" . (int)$company_id . "' ORDER BY sort_order");
        foreach ($company_option->rows as $company_option) {
        $company_option_value_data = array();
        $company_option_value = $this->db->query("SELECT * FROM company_option_value WHERE company_option_id = '" . (int)$company_option['company_option_id'] . "' ORDER BY sort_order");
        foreach ($company_option_value->rows as $company_option_value) {
            $company_option_value_description_data = array();
            $company_option_value_description = $this->db->query("SELECT * FROM company_option_value_description WHERE company_option_value_id = '" . (int)$company_option_value['company_option_value_id'] . "'");
          foreach ($company_option_value_description->rows as $result) {
            $company_option_value_description_data[$result['language_id']] = array('name' => $result['name']);
            }
            $company_option_value_data[] = array(
            'company_option_value_id' => $company_option_value['company_option_value_id'],
            'language'                => $company_option_value_description_data,
            'quantity'                => $company_option_value['quantity'],
            'subtract'                => $company_option_value['subtract'],
            'price'                   => $company_option_value['price'],
            'prefix'                  => $company_option_value['prefix'],
            'sort_order'              => $company_option_value['sort_order']
          );
        }
    
        $company_option_description_data = array();
        $company_option_description = $this->db->query("SELECT * FROM company_option_description WHERE company_option_id = '" . (int)$company_option['company_option_id'] . "'");
        foreach ($company_option_description->rows as $result) {
            $company_option_description_data[$result['language_id']] = array('name' => $result['name']);
        }
    
        $company_option_data[] = array(
          'company_option_id'    => $company_option['company_option_id'],
          'language'             => $company_option_description_data,
          'company_option_value' => $company_option_value_data,
          'sort_order'           => $company_option['sort_order']
        );
    }
      return $company_option_data;
    }
    
    public function getCompanyImages($company_id) {
        $query = $this->db->query("SELECT * FROM company_image WHERE company_id = '" . (int)$company_id . "'");
      return $query->rows;
    }
    
    public function getCompanyDiscounts($company_id) {
        $query = $this->db->query("SELECT * FROM company_discount WHERE company_id = '" . (int)$company_id . "' ORDER BY quantity, priority, price");
      return $query->rows;
    }
    
    public function getCompanySpecials($company_id) {
        $query = $this->db->query("SELECT * FROM company_special WHERE company_id = '" . (int)$company_id . "' ORDER BY priority, price");
      return $query->rows;
    }
    
    public function getCompanyDownloads($company_id) {
        $company_download_data = array();
        $query = $this->db->query("SELECT * FROM company_to_download WHERE company_id = '" . (int)$company_id . "'");
        foreach ($query->rows as $result) {
        $company_download_data[] = $result['download_id'];
    }
      return $company_download_data;
    }
    
    // todo. get one company
    public function getCompanyCompanys($company_id) {
        $company_company_data = array();
        $query = $this->db->query("SELECT * FROM companylocator WHERE company_id = '" . (int)$company_id . "'");
        foreach ($query->rows as $result) {
        $company_company_data[] = $result['company_id'];
    }
      return $company_company_data;
    }
    
    public function getCompanyCategories($company_id) {
        $company_category_data = array();
        $query = $this->db->query("SELECT * FROM company_to_category WHERE company_id = '" . (int)$company_id . "'");
        foreach ($query->rows as $result) {
        $company_category_data[] = $result['category_id'];
    }
      return $company_category_data;
    }
    
    public function getCompanyRelated($company_id) {
        $company_related_data = array();
        $query = $this->db->query("SELECT * FROM company_related WHERE company_id = '" . (int)$company_id . "'");
        foreach ($query->rows as $result) {
        $company_related_data[] = $result['related_id'];
    }
      return $company_related_data;
    }
    
    public function getCompanyTags($company_id) {
        $company_tag_data = array();
        $query = $this->db->query("SELECT * FROM company_tags WHERE company_id = '" . (int)$company_id . "'");
        $tag_data = array();
        foreach ($query->rows as $result) {
        $tag_data[$result['language_id']][] = $result['tag'];
    }
        foreach ($tag_data as $language => $tags) {
        $company_tag_data[$language] = implode(',', $tags);
    }
      return $company_tag_data;
    }
    
    public function getTotalCompanysByStockStatusId($stock_status_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM company WHERE stock_status_id = '" . (int)$stock_status_id . "'");
      return $query->row['total'];
    }
    
    public function getTotalCompanysByImageId($image_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM company WHERE image_id = '" . (int)$image_id . "'");
      return $query->row['total'];
    }
    
    public function getTotalCompanysByTaxClassId($tax_class_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM company WHERE tax_class_id = '" . (int)$tax_class_id . "'");
      return $query->row['total'];
    }
    
    public function getTotalCompanysByWeightClassId($weight_class_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM company WHERE weight_class_id = '" . (int)$weight_class_id . "'");
      return $query->row['total'];
    }
    
    public function getTotalCompanysByLengthClassId($length_class_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM company WHERE length_class_id = '" . (int)$length_class_id . "'");
      return $query->row['total'];
    }
    
    public function getTotalCompanysByOptionId($option_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM company_to_option WHERE option_id = '" . (int)$option_id . "'");
      return $query->row['total'];
    }
    
    public function getTotalCompanysByDownloadId($download_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM company_to_download WHERE download_id = '" . (int)$download_id . "'");
      return $query->row['total'];
    }
    
    public function getTotalCompanysByManufacturerId($manufacturer_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM company WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");
      return $query->row['total'];
    }
    
    // todo. get one company
    public function getOneCompany($company_id) {
        $sql = "SELECT * FROM companylocator WHERE id = '" . (int)$company_id . "'";
        //$this->log->aPrint( $sql );
        $query = $this->db->query($sql);
      return $query->row;
    }
    
    public function getOneCompanyWithAccountno($accountno) {
        $sql = "SELECT * FROM companylocator WHERE accountno = '" . $accountno . "'";
        //$this->log->aPrint( $sql );
        $query = $this->db->query($sql);
      return $query->row;
    }
    
    
    public function getCompanyHistory($accountno) {
        $sql = "select *, (select comment from companylocator where accountno = '$accountno') as commnet
                  from company_history where accountno = '$accountno' order by assign_date";
        $query = $this->db->query($sql);
      return $query->rows;
    }
    
    public function updateComment($company_id,$comment) {
        $sql = "update companylocator set comment = '$comment' where id = $company_id";
        if ($query = $this->db->query($sql)) {
        return true;
        } else {
        return false;
    }
    }
    
    public function getLatLng($account) {
        $account = strtoupper($account);
        $sql = "select * from companylocator where accountno = '$account'";
        //$this->log->aPrint( $sql );
        $query = $this->db->query($sql);
      return $query->row;
    }
    
    public function companyAccount($req) {
        $id = $req['id'];
        $accountno = $req['accountno'];
        $rep = $req['rep'];
        $name = $req['name'];
        $address = $req['address'];
        $city = $req['city'];
        $state = $req['state'];
        $zipcode = $req['zipcode'];
        $tel = $req['tel'];
        $sql = "update companylocator set ";
        $sql .= "       accountno = '" . $accountno . "',";
        $sql .= "       salesrep = '" . $rep . "',";
        $sql .= "       name = '" . $name . "',";
        $sql .= "       address1 = '" . $address . "',";
        $sql .= "       city = '" . $city . "',";
        $sql .= "       state = '" . $state . "',";
        $sql .= "       zipcode = '" . $zipcode . "',";
        $sql .= "       phone1 = '" . $tel . "'";
        $sql .= " where id = " . $id ;
        if ( $query = $this->db->query($sql) ) {
        return true;
        } else {
        return false;
    }
    }
    
    public function insertBtrip($req) {
        //$this->log->aPrint( $req );    exit;
        $id = $req['id'];
        $title = $req['title'];
        $hotel = $req['hotel'];
        $account = $req['account'];
        $aAccount = explode(",",$account);
        $jsonAccount = json_encode($aAccount);
        $user = $this->user->getUserName();
        if ( $id == 'undefined' ) {  // insert
        $qry = "insert into btrip (rep,title,node,hotel,up_date) values";
        $qry.= " ('$user','$title','$jsonAccount','$hotel',now())";
        } else {
        $qry = "update btrip set title = '$title', rep = '$user', node = '$jsonAccount', up_date = now(), hotel = '$hotel'";
        $qry.= " where id = $id";
    }
        //$this->log->aPrint(  $qry );
        if ( $query = $this->db->query($qry) ) {
        return true;
        } else {
        return false;
    }
    }

}
?>
